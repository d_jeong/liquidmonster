/* Start Header ---------------------------------------------------------------
Copyright (C) 2019 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : GameList.h
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Sejeong Park
Secondary :
- End Header ----------------------------------------------------------------*/
#pragma once

enum class eStateType
{
    NONE = 0,

    LOGO, //1
    LOADING, //2
    
    MENU, // 3 
    HOW_TO_PLAY, //4 
    OPTION, // 5
    CREDIT, //6
    PAUSE, //7
    HOW_TO_PLAY_PAUSE,
    
    LEVEL_SELECT, //8

    CUTSCENE_BEGIN,//9
    CUTSCENE_END,//10

    LEVEL_1, //11
    LEVEL_2, //12
    LEVEL_3, //13
    LEVEL_4, //14
    LEVEL_5, //15

    CHAPTER2_LEVEL_1, //16
    CHAPTER2_LEVEL_2, //17
    CHAPTER2_LEVEL_3, //18
    CHAPTER2_LEVEL_4, //19

    CHAPTER2_LEVEL_5, // THIS IS ACTUAL CHAPTER 3-1, 20
    CHAPTER2_LEVEL_6, // 21, 3-2

    LEVEL_COUNTS, //22
};
