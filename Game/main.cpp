/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : main.cpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Haewon Shon
Secondary :
- End Header ----------------------------------------------------------------*/

#ifdef _DEBUG
#pragma comment(linker, "/SUBSYSTEM:console")

#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>
#include "windows.h"
#else

#pragma comment(linker, "/SUBSYSTEM:windows /ENTRY:mainCRTStartup")

#endif

#include "Engine/Engine.hpp"


int main(int argv, char *argc[])
{
    Engine *ENGINE = new Engine();

    ENGINE->Initialize();
    ENGINE->Update();
    ENGINE->DestroyAllSystems();

    delete ENGINE;

#ifdef _DEBUG
        _CrtDumpMemoryLeaks();
#endif
        return 0;
}