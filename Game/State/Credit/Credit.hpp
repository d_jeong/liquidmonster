/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : Credit.hpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Wonju Cho
Secondary :
- End Header ----------------------------------------------------------------*/

#pragma once
#include "../Game/State/State.hpp"
#include <vector>

#include "Data/Data.hpp"

class Object;

class Credit : public State
{
public:
    void Initialize() override;
    void Update(float dt) override;
    void ClearAll() override;

private:
    Object *             creditPicture;
    std::vector<eTexture> mObjects;
    int paintIndex;
};
