/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : Credit.cpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Wonju Cho
Secondary :
- End Header ----------------------------------------------------------------*/

#include "Credit.hpp"
#include "../Engine/Object/Object.hpp"
#include "../Engine/Object/ObjectType.hpp"
#include "../Engine/Data/Data.hpp"
#include "../Engine/FMOD/FmodManager.hpp"
#include "../Engine/Component/Sprite/Sprite.hpp"
#include "../Engine/Component/Transform/Transform.hpp"
#include "../Engine/Object/ObjectManager/ObjectManager.hpp"
#include "../Engine/Input/Input.hpp"
#include "../StateManager/StateManager.hpp"
#include "../Engine/Graphics/Graphics.hpp"
#include "../Option/Option.hpp"

void Credit::Initialize()
{
    this->SetCurrentStateType(eStateType::CREDIT);

    mLevelWidth = 1280;
    mLevelHeight = 720;
    paintIndex = 0;

    GRAPHICS->SetLevelWidthAndHeight(Vector2<int>(mLevelWidth, mLevelHeight));

    STATE_MANAGER->SetCurrentState(this->GetCurrentStateType());
    std::cout << "Credit Level initialized." << std::endl;

    creditPicture = new Object("Credit", eObjectType::NONE);
    creditPicture->AddComponent(new Sprite);
    creditPicture->AddComponent(new Transform);
    creditPicture->GetSprite()->SetColor(Vector4<float>(1.f, 1.f, 1.f, 1.f));

    if (OPTION == nullptr || OPTION->GetShouldUseEnglish() == true)
    {
        creditPicture->GetSprite()->LoadTextureFromData(eTexture::CREDIT);
    }
    else
    {
        creditPicture->GetSprite()->LoadTextureFromData(eTexture::K_CREDIT_1);
    }

    creditPicture->SetScale(Vector2<float>(1280, 720));
    creditPicture->SetPosition(Vector2<float>(0, 0));


    if (OPTION == nullptr || OPTION->GetShouldUseEnglish() == true)
    {
        mObjects.push_back(eTexture::CREDIT);
        mObjects.push_back(eTexture::CREDIT_TWO);
        mObjects.push_back(eTexture::CREDIT_THREE);
        mObjects.push_back(eTexture::CREDIT_FOUR);
        mObjects.push_back(eTexture::CREDIT_FIVE);
    }
    else
    {
        //mObjects.push_back(eTexture::K_CREDIT_1);
        mObjects.push_back(eTexture::K_CREDIT_2);
        mObjects.push_back(eTexture::K_CREDIT_3);
        mObjects.push_back(eTexture::K_CREDIT_4);
        mObjects.push_back(eTexture::K_CREDIT_5);
    }

    OBJECT_MANAGER->AddObject(creditPicture);

    OBJECT_MANAGER->Initialize();
}

void Credit::Update(float dt)
{
    if (Input::IsKeyTriggered(SDL_SCANCODE_SPACE) || Input::IsKeyTriggered(SDL_SCANCODE_RETURN))
    {
        FMOD_MANAGER->PlaySoundEffectSound("sound/sfx_button_click.mp3");
        paintIndex++;

        if (paintIndex == mObjects.size())
        {
            STATE_MANAGER->SetNextState(eStateType::MENU);
            paintIndex = mObjects.size() - 1;
        }

        creditPicture->GetSprite()->LoadTextureFromData(mObjects.at(paintIndex));
    }
}

void Credit::ClearAll()
{
    OBJECT_MANAGER->ClearObjects();
    mObjects.clear();
}
