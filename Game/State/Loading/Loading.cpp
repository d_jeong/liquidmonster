/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : Loading.cpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Haewon Shon
Secondary :
- End Header ----------------------------------------------------------------*/

#include "Loading.hpp"
#include "../Engine/Object/Object.hpp"
#include "../Engine/Object/ObjectType.hpp"
#include "../Engine/Data/Data.hpp"
#include "../Engine/Component/Sprite/Sprite.hpp"
#include "../Engine/Component/Transform/Transform.hpp"
#include "../Engine/Object/ObjectManager/ObjectManager.hpp"
#include "../Engine/Input/Input.hpp"
#include "../StateManager/StateManager.hpp"
#include "../Engine/Graphics/Graphics.hpp"
#include "../Engine/FMOD/FmodManager.hpp"

void Loading::Initialize()
{
    this->SetCurrentStateType(eStateType::LOADING);
    STATE_MANAGER->SetCurrentState(this->GetCurrentStateType());
    std::cout << "Current Level - Loading initialized." << std::endl;

    mLoadingScreen = new Object("Loading", eObjectType::NONE);
    mLoadingScreen->AddComponent(new Sprite());
    mLoadingScreen->AddComponent(new Transform());
    mLoadingScreen->GetSprite()->SetColor(Vector4<float>(1.f, 1.f, 1.f, 1.f));
    mLoadingScreen->GetSprite()->LoadTextureFromData(eTexture::LOADING_SCENE);
    mLoadingScreen->GetSprite()->SetAnimation(true);
    mLoadingScreen->GetSprite()->SetAnimationFrame(4);
    mLoadingScreen->GetSprite()->SetAnimationSpeed(1.5f);
    mLoadingScreen->SetScale(Vector2<float>(1280, 720));
    mLoadingScreen->SetPosition(Vector2<float>(0, 50));

    loadingPercent = new Object("LoadingPercent", eObjectType::NONE);
    loadingPercent->AddComponent(new Sprite());
    loadingPercent->AddComponent(new Transform());
    loadingPercent->GetSprite()->SetColor(Vector4<float>(1.f, 1.f, 1.f, 1.f));
    loadingPercent->GetSprite()->LoadTexture("texture/Level/loading_0per.png");
    loadingPercent->SetScale(Vector2<float>(500,125));
    loadingPercent->SetPosition(Vector2<float>(0, -200));

    OBJECT_MANAGER->AddObject(mLoadingScreen);
    OBJECT_MANAGER->AddObject(loadingPercent);

    OBJECT_MANAGER->Initialize();
}

void Loading::Update(float dt)
{
    if (GRAPHICS->IsDataLoadDone())
    {
        FMOD_MANAGER->PlayUISound("sound/background_1.mp3");
        FMOD_MANAGER->SetVolumeUISound(0.5f);
        STATE_MANAGER->SetNextState(eStateType::MENU);
    }
    else
    {
        GRAPHICS->DataLoadingUpdate();

        if (GRAPHICS->GetLoadingProcess() >=20 && GRAPHICS->GetLoadingProcess() < 40)
        {
            loadingPercent->GetSprite()->LoadTexture("texture/Level/loading_20per.png");
        }
        else if (GRAPHICS->GetLoadingProcess() >= 40 && GRAPHICS->GetLoadingProcess() < 60)
        {
            loadingPercent->GetSprite()->LoadTexture("texture/Level/loading_40per.png");
        }
        else if (GRAPHICS->GetLoadingProcess() >= 60 && GRAPHICS->GetLoadingProcess() < 80)
        {
            loadingPercent->GetSprite()->LoadTexture("texture/Level/loading_60per.png");
        }
        else if (GRAPHICS->GetLoadingProcess() >= 80 && GRAPHICS->GetLoadingProcess() < 95)
        {
            loadingPercent->GetSprite()->LoadTexture("texture/Level/loading_80per.png");
        }
        else if (GRAPHICS->GetLoadingProcess() >=95)
        {
            loadingPercent->GetSprite()->LoadTexture("texture/Level/loading_100per.png");
        }
    }
}