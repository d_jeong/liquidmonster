/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : Loading.hpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Haewon Shon
Secondary :
- End Header ----------------------------------------------------------------*/

#pragma once
#include "../State.hpp"

class Object;

class Loading : public State
{
public:
    void Initialize() override;
    void Update(float dt) override;

private:
    Object* mLoadingScreen;
    Object* loadingPercent;
};
