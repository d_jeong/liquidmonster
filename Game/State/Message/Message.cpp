/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : Message.cpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Haewon Shon
Secondary :
- End Header ----------------------------------------------------------------*/
#include "Message.hpp"
#include "../Engine/Application/Application.hpp"
#include "../Engine/Component/Sprite/Sprite.hpp"
#include "../Engine/Component/Transform/Transform.hpp"
#include "../Engine/Data/Data.hpp"
#include "../Engine/Engine/Engine.hpp"
#include "../Engine/FMOD/FmodManager.hpp"
#include "../Engine/Graphics/Graphics.hpp"
#include "../Engine/Input/Input.hpp"
#include "../Engine/JSON/LevelJson.hpp"
#include "../Engine/Object/Object.hpp"
#include "../Engine/Object/ObjectDepth.hpp"
#include "../Engine/Object/ObjectManager/ObjectManager.hpp"
#include "../Option/Option.hpp"
#include "../StateManager/StateManager.hpp"

const Vector2<float> buttonScale         = {300, 100};
const Vector2<float> selectedButtonScale = {300 * 1.25, 100 * 1.25};

void Message::CreateMessage(eMessageType messageType)
{
    if (mIsDestructiveMessageOn)
    {
        return;
    }

    mCurrentMessageType = messageType;

    mMessage = new Object("message", eObjectType::NONE);
    mMessage->AddComponent(new Sprite());
    mMessage->AddComponent(new Transform());
    mMessage->GetSprite()->SetColor(Vector4<float>(1.f, 1.f, 1.f, 1.f));
    mMessage->GetTransform()->SetScale({static_cast<float>(APPLICATION->GetWidth() * 0.8f), static_cast<float>(APPLICATION->GetHeight() * 0.8f)});

    // add yes, no
    mYesButton = new Object("yesButton", eObjectType::NONE);
    mYesButton->AddComponent(new Sprite());
    mYesButton->AddComponent(new Transform());
    mYesButton->GetSprite()->SetColor(Vector4<float>(1.f, 1.f, 1.f, 1.f));
    mYesButton->GetTransform()->SetPosition({-200.f, -150.f});

    mNoButton = new Object("noButton", eObjectType::NONE);
    mNoButton->AddComponent(new Sprite());
    mNoButton->AddComponent(new Transform());
    mNoButton->GetSprite()->SetColor(Vector4<float>(1.f, 1.f, 1.f, 1.0f));
    mNoButton->GetTransform()->SetPosition({200.f, -150.f});

    mNoButton->GetTransform()->SetScale(selectedButtonScale);
    mYesButton->GetTransform()->SetScale(buttonScale);

    SetTextures();

    if (STATE_MANAGER->IsPaused())
    {
        mMessage->SetDepth(0.9f);
        mYesButton->SetDepth(0.9f);
        mNoButton->SetDepth(0.9f);
        OBJECT_MANAGER->AddPauseObject(mMessage);
        OBJECT_MANAGER->AddPauseObject(mYesButton);
        OBJECT_MANAGER->AddPauseObject(mNoButton);
    }
    else
    {
        mMessage->SetDepth(UI);
        mYesButton->SetDepth(UI);
        mNoButton->SetDepth(UI);
        OBJECT_MANAGER->AddObject(mMessage);
        OBJECT_MANAGER->AddObject(mYesButton);
        OBJECT_MANAGER->AddObject(mNoButton);
    }
    mMessage->Initialize();
    mYesButton->Initialize();
    mNoButton->Initialize();

    // select no first
    mIsYesChosen            = false;
    mIsDestructiveMessageOn = true;
}

void Message::ClearMessage()
{
    if (STATE_MANAGER->IsPaused())
    {
        OBJECT_MANAGER->DeletePauseObject(mMessage);
        OBJECT_MANAGER->DeletePauseObject(mYesButton);
        OBJECT_MANAGER->DeletePauseObject(mNoButton);
    }
    else
    {
        OBJECT_MANAGER->DeleteObject(mMessage);
        OBJECT_MANAGER->DeleteObject(mYesButton);
        OBJECT_MANAGER->DeleteObject(mNoButton);
    }

    mIsDestructiveMessageOn = false;
    mCurrentMessageType     = eMessageType::NONE;
}

void Message::UpdateMessage()
{ 
    if (mCurrentMessageType == eMessageType::NONE)
    {
        return;
    }

    if (mIsYesChosen)
    {
        if (Input::IsKeyTriggered(SDL_SCANCODE_RETURN) || Input::IsKeyTriggered(SDL_SCANCODE_SPACE))
        {
            // confirm, exit
            FMOD_MANAGER->PlaySoundEffectSound("sound/sfx_button_click.mp3");
            ExecutiveMessage();
            ClearMessage();
        }
        else if (Input::IsKeyTriggered(SDL_SCANCODE_RIGHT))
        {
            FMOD_MANAGER->PlaySoundEffectSound("sound/sfx_button_change.mp3");
            mIsYesChosen = false;
            mNoButton->GetTransform()->SetScale(selectedButtonScale);
            mYesButton->GetTransform()->SetScale(buttonScale);
        }
    }
    else
    {
        if (Input::IsKeyTriggered(SDL_SCANCODE_RETURN) || Input::IsKeyTriggered(SDL_SCANCODE_SPACE))
        {
            FMOD_MANAGER->PlaySoundEffectSound("sound/sfx_button_click.mp3");
            ClearMessage();
        }
        else if (Input::IsKeyTriggered(SDL_SCANCODE_LEFT))
        {
            FMOD_MANAGER->PlaySoundEffectSound("sound/sfx_button_change.mp3");
            mIsYesChosen = true;
            mYesButton->GetTransform()->SetScale(selectedButtonScale);
            mNoButton->GetTransform()->SetScale(buttonScale);
        }
    }
}

void Message::ExecutiveMessage()
{
    switch (mCurrentMessageType)
    {
        case eMessageType::QUIT:
        case eMessageType::LOSE_PROGRESS:
            ENGINE->Quit();
            break;
        case eMessageType::PAUSE_TO_MAIN:
            STATE_MANAGER->UnPauseGame();
            STATE_MANAGER->SetNextState(eStateType::MENU);
            GRAPHICS->SetSkip(true);
            break;
        case eMessageType::DATA_RESET:
            LEVEL_JSON->ClearLevelLockFile();
            break;
    }
}

bool Message::IsMessageOn()
{
    return mIsDestructiveMessageOn;
}

void Message::SetTextures()
{
    bool isKorean = false;
    if (OPTION != nullptr)
    {
        isKorean = !OPTION->GetShouldUseEnglish();
    }
    if (isKorean)
    {
        switch (mCurrentMessageType)
        {
            case eMessageType::QUIT:
                mMessage->GetSprite()->LoadTextureFromData(eTexture::K_DEST_QUIT);
                mYesButton->GetSprite()->LoadTextureFromData(eTexture::K_MENU_QUIT);
                break;
            case eMessageType::LOSE_PROGRESS:
            case eMessageType::PAUSE_TO_MAIN:
                mMessage->GetSprite()->LoadTextureFromData(eTexture::K_DESTRUCTIVE_QUIT);
                mYesButton->GetSprite()->LoadTextureFromData(eTexture::K_MENU_QUIT);
                break;
            case eMessageType::DATA_RESET:
                mMessage->GetSprite()->LoadTextureFromData(eTexture::K_DESTRUCTIVE_DATA);
                mYesButton->GetSprite()->LoadTextureFromData(eTexture::K_DATA_RESET);
                break;
        }
        mNoButton->GetSprite()->LoadTextureFromData(eTexture::K_CANCLE);
    }
    else
    {
        switch (mCurrentMessageType)
        {
            case eMessageType::QUIT:
                mMessage->GetSprite()->LoadTextureFromData(eTexture::DESTRUCTIVE_QUIT);
                mYesButton->GetSprite()->LoadTextureFromData(eTexture::MENU_QUIT);
                break;
            case eMessageType::LOSE_PROGRESS:
            case eMessageType::PAUSE_TO_MAIN:
                mMessage->GetSprite()->LoadTextureFromData(eTexture::DESTRUCTIVE_QUIT);
                mYesButton->GetSprite()->LoadTextureFromData(eTexture::MENU_QUIT);
                break;
            case eMessageType::DATA_RESET:
                mMessage->GetSprite()->LoadTextureFromData(eTexture::DESTURCTIVE_DATA_RESET);
                mYesButton->GetSprite()->LoadTextureFromData(eTexture::DATA_RESET);
                break;
        }
        mNoButton->GetSprite()->LoadTextureFromData(eTexture::DESTRUCTIVE_CANCEL);
    }
}