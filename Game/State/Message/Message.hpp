/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : Message.hpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Haewon Shon
Secondary :
- End Header ----------------------------------------------------------------*/
#pragma once

class Object;

class Message
{
public:
    enum class eMessageType
    {
        QUIT,
        DATA_RESET,
        LOSE_PROGRESS,
        PAUSE_TO_MAIN,
        NONE
    };
    void CreateMessage(eMessageType messageType);
    void ClearMessage();
    void UpdateMessage();
    bool IsMessageOn();

private:
    void ExecutiveMessage();
    void SetTextures();

    eMessageType mCurrentMessageType;
    Object*      mMessage;
    Object*      mYesButton;
    Object*      mNoButton;
    bool         mIsYesChosen;
    bool         mIsDestructiveMessageOn;
};