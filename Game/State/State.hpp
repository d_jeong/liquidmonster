/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : State.hpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Daeun Jeong
Secondary :
- End Header ----------------------------------------------------------------*/

#pragma once
#include <string>
#include "../GameList.h"

class Object;

class State
{
public:
    virtual void Initialize()     = 0;
    virtual void Update(float dt) = 0;

    void SaveLevel();
    void SaveObject();
    void SaveLevelLock();

    void SaveGraphicsTile();
    void SavePhysicsTile();

    void LoadLevel();
    void LoadLevel(const std::string& levelID);

    virtual void ClearAll();

    std::string GetCurrentLevelName();
    void SetCurrentLevelName(std::string name);

    std::string GetNextLevelName();
    void SetNextLevelName(std::string name);

    void SetCurrentStateType(eStateType);
    eStateType GetCurrentStateType(void);

    void SetNextStateType(eStateType);
    eStateType GetNextStateType(void);

    void SetUpObjects(void);
    void ShowStateNumber(float dt);
protected:
    int mLevelWidth;
    int mLevelHeight;
private:
    eStateType mCurrentStateType = eStateType::NONE;
    eStateType mNextStateType = eStateType::NONE;
    
    std::string mCurrentLevelName = "";
    std::string mNextLevelName = "";
};