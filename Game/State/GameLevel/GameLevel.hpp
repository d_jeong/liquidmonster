/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : GameLevel.hpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Daeun Jeong
Secondary :
- End Header ----------------------------------------------------------------*/

#pragma once
#include "../Engine/Object/Object.hpp"
#include "../Game/State/State.hpp"
#include "../Game/State/StateManager/StateManager.hpp"
#include <iostream>

class GameLevel : public State
{
public:
    void Initialize() override;
    void Update(float dt) override;

private:
};
