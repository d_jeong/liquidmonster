/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : Menu.cpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Sejeong Park
Secondary :
- End Header ----------------------------------------------------------------*/

#include "Menu.hpp"
#include "../Engine/Component/Sprite/Sprite.hpp"
#include "../Engine/Component/Transform/Transform.hpp"
#include "../Engine/Data/Data.hpp"
#include "../Engine/FMOD/FmodManager.hpp"
#include "../Engine/Graphics/Graphics.hpp"
#include "../Engine/Input/Input.hpp"
#include "../Engine/Logger/Logger.hpp"
#include "../Engine/Object/Object.hpp"
#include "../Engine/Object/ObjectDepth.hpp"
#include "../Engine/Object/ObjectManager/ObjectManager.hpp"
#include "../Engine/Object/ObjectType.hpp"
#include "../StateManager/StateManager.hpp"
#include "../Engine/Engine/Engine.hpp"
#include "../Option/Option.hpp"

Object* objectLogo, * objectStart, * objectOption, * objectCredit, * objectQuit, * objectPress, * objectHowToPlay;

const Vector2<float> buttonScale = { 250, 80 };
const Vector2<float> selectedButtonScale = { 250 * 1.25, 80 * 1.25 };

void Menu::Initialize()
{
    mLevelWidth = 1280;
    mLevelHeight = 720;

    int yStart = 90, yOffset = 95;

    this->SetCurrentStateType(eStateType::MENU);

    GRAPHICS->SetLevelWidthAndHeight(Vector2<int>(mLevelWidth, mLevelHeight));
    GRAPHICS->SetBackground(BackgroundManager::eBackground::BACKGROUND_MENU);

    STATE_MANAGER->SetCurrentState(this->GetCurrentStateType());
    LOGGER.LogEvent("State Menu initialized.");

    objectLogo = new Object("GameLogo", eObjectType::GAME_LOGO);
    objectLogo->AddComponent(new Sprite());
    objectLogo->AddComponent(new Transform());
    objectLogo->GetSprite()->SetColor(Vector4<float>(1.f, 1.f, 1.f, 1.f));
    objectLogo->GetSprite()->LoadTextureFromData(eTexture::GAME_LOGO);
    objectLogo->SetScale(Vector2<float>(500, 300));
    objectLogo->SetPosition(Vector2<float>(0, 230));

    objectStart = new Object("Start", eObjectType::START);
    objectStart->AddComponent(new Sprite());
    objectStart->AddComponent(new Transform());
    objectStart->GetSprite()->SetColor(Vector4<float>(1.f, 1.f, 1.f, 1.f));

    if (OPTION == nullptr || OPTION->GetShouldUseEnglish() == true)
    {
        objectStart->GetSprite()->LoadTextureFromData(eTexture::MENU_PLAY);
    }
    else
    {
        objectStart->GetSprite()->LoadTextureFromData(eTexture::K_MENU_PLAY);
    }

    objectStart->SetScale(Vector2<float>(250, 80));
    objectStart->SetPosition(Vector2<float>(0, yStart));
    mObjects.push_back(objectStart);

    objectHowToPlay = new Object("objectHowToPlay", eObjectType::HOW_TO_PLAY);
    objectHowToPlay->AddComponent(new Sprite());
    objectHowToPlay->AddComponent(new Transform());
    objectHowToPlay->GetSprite()->SetColor(Vector4<float>(1.f, 1.f, 1.f, 1.f));

    if (OPTION == nullptr || OPTION->GetShouldUseEnglish() == true)
    {
        objectHowToPlay->GetSprite()->LoadTextureFromData(eTexture::MENU_HOW_TO_PLAY);
    }
    else
    {
        objectHowToPlay->GetSprite()->LoadTextureFromData(eTexture::K_MENU_HOWTO);
    }

    objectHowToPlay->SetScale(Vector2<float>(250, 80));
    objectHowToPlay->SetPosition(Vector2<float>(0, yStart - yOffset));
    mObjects.push_back(objectHowToPlay);

    objectOption = new Object("Option", eObjectType::OPTION);
    objectOption->AddComponent(new Sprite());
    objectOption->AddComponent(new Transform());
    objectOption->GetSprite()->SetColor(Vector4<float>(1.f, 1.f, 1.f, 1.f));

    if (OPTION == nullptr || OPTION->GetShouldUseEnglish() == true)
    {
        objectOption->GetSprite()->LoadTextureFromData(eTexture::MENU_OPTION);
    }
    else
    {
        objectOption->GetSprite()->LoadTextureFromData(eTexture::K_MENU_OPTION);
    }

    objectOption->SetScale(Vector2<float>(250, 80));
    objectOption->SetPosition(Vector2<float>(0, yStart - yOffset * 2));
    mObjects.push_back(objectOption);

    objectCredit = new Object("Credit", eObjectType::CREDIT);
    objectCredit->AddComponent(new Sprite());
    objectCredit->AddComponent(new Transform());
    objectCredit->GetSprite()->SetColor(Vector4<float>(1.f, 1.f, 1.f, 1.f));

    if (OPTION == nullptr || OPTION->GetShouldUseEnglish() == true)
    {
        objectCredit->GetSprite()->LoadTextureFromData(eTexture::MENU_CREDIT);
    }
    else
    {
        objectCredit->GetSprite()->LoadTextureFromData(eTexture::K_MENU_CREDIT);
    }

    objectCredit->SetScale(Vector2<float>(250, 80));
    objectCredit->SetPosition(Vector2<float>(0, yStart - yOffset * 3));
    mObjects.push_back(objectCredit);

    objectQuit = new Object("Quit", eObjectType::QUIT);
    objectQuit->AddComponent(new Sprite());
    objectQuit->AddComponent(new Transform());
    objectQuit->GetSprite()->SetColor(Vector4<float>(1.f, 1.f, 1.f, 1.f));

    if (OPTION == nullptr || OPTION->GetShouldUseEnglish() == true)
    {
        objectQuit->GetSprite()->LoadTextureFromData(eTexture::MENU_QUIT);
    }
    else
    {
        objectQuit->GetSprite()->LoadTextureFromData(eTexture::K_MENU_QUIT);
    }

    objectQuit->SetScale(Vector2<float>(250, 80));
    objectQuit->SetPosition(Vector2<float>(0, yStart - yOffset * 4));
    mObjects.push_back(objectQuit);

    OBJECT_MANAGER->AddObject(objectLogo);
    OBJECT_MANAGER->AddObject(objectStart);
    OBJECT_MANAGER->AddObject(objectHowToPlay);
    OBJECT_MANAGER->AddObject(objectOption);
    OBJECT_MANAGER->AddObject(objectCredit);
    OBJECT_MANAGER->AddObject(objectQuit);

    mCurrentObject = objectStart;

    OBJECT_MANAGER->Initialize();

    mIndex = 0;
}

void Menu::Update(float dt)
{
    mCurrentObject->SetScale(selectedButtonScale);

    if (Input::IsKeyTriggered(SDL_SCANCODE_UP))
    {
        if (mCurrentObject->GetObjectType() != mObjects.front()->GetObjectType())
        {
            FMOD_MANAGER->PlaySoundEffectSound("sound/sfx_button_change.mp3");
            --mIndex;
            mCurrentObject = mObjects.at(mIndex);
        }

        LOGGER.LogEvent("Key UP Pressed.");
        LOGGER.LogDebug(std::to_string(mIndex));
        LOGGER.LogDebug(mCurrentObject->GetName());
    }

    if (Input::IsKeyTriggered(SDL_SCANCODE_DOWN))
    {
        if (mCurrentObject->GetObjectType() != mObjects.back()->GetObjectType())
        {
            FMOD_MANAGER->PlaySoundEffectSound("sound/sfx_button_change.mp3");
            ++mIndex;
            mCurrentObject = mObjects.at(mIndex);
        }

        LOGGER.LogEvent("Key DOWN Pressed.");
        LOGGER.LogDebug(std::to_string(mIndex));
        LOGGER.LogDebug(mCurrentObject->GetName());
    }

    for (int i = 0; i < mObjects.size(); ++i)
    {
        if (mObjects.at(i) != mCurrentObject)
        {
            mObjects.at(i)->SetScale(buttonScale);
        }
    }

    if (Input::IsKeyTriggered(SDL_SCANCODE_SPACE) || Input::IsKeyTriggered(SDL_SCANCODE_RETURN))
    {
        FMOD_MANAGER->PlaySoundEffectSound("sound/sfx_button_click.mp3");

        if (mCurrentObject == objectStart)
        {
            STATE_MANAGER->SetNextState(eStateType::LEVEL_SELECT);
            return;
        }
        else if (mCurrentObject == objectOption)
        {
            STATE_MANAGER->SetNextState(eStateType::OPTION);
            return;
        }
        else if (mCurrentObject == objectCredit)
        {
            STATE_MANAGER->SetNextState(eStateType::CREDIT);
            return;
        }
        else if (mCurrentObject == objectQuit)
        {
            STATE_MANAGER->CreateDestructiveMessage();
        }
        else if (mCurrentObject == objectHowToPlay)
        {
            STATE_MANAGER->SetNextState(eStateType::HOW_TO_PLAY);
            return;
        }
    }
}

void Menu::ClearAll()
{
    OBJECT_MANAGER->ClearObjects();
    mObjects.clear();
}