/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : Menu.hpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Sejeong Park
Secondary :
- End Header ----------------------------------------------------------------*/

#pragma once
#include "../Game/State/State.hpp"
#include <iostream>
#include <vector>

class Object;

class Menu : public State
{
public:
    void Initialize() override;
    void Update(float dt) override;
    void ClearAll() override;

private:
    std::vector<Object*> mObjects;
    int                  mIndex = 0;
    Object* mCurrentObject = nullptr;
};
