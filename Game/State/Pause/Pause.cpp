/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : Pause.cpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Haewon Shon
Secondary :
- End Header ----------------------------------------------------------------*/

#include "Pause.hpp"
#include "../Engine/Application/Application.hpp"
#include "../Engine/Component/Sprite/Sprite.hpp"
#include "../Engine/Component/Transform/Transform.hpp"
#include "../Engine/Data/Data.hpp"
#include "../Engine/Engine/Engine.hpp"
#include "../Engine/FMOD/FmodManager.hpp"
#include "../Engine/Input/Input.hpp"
#include "../Engine/Logger/Logger.hpp"
#include "../Engine/Object/Object.hpp"
#include "../Engine/Object/ObjectDepth.hpp"
#include "../Engine/Object/ObjectManager/ObjectManager.hpp"
#include "../Engine/Object/ObjectType.hpp"
#include "../StateManager/StateManager.hpp"
#include "../Option/Option.hpp"

const Vector2<float> buttonScale = { 300*.9, 100*.9 };
const Vector2<float> selectedButtonScale = { 300 * 1.2, 100 * 1.2 };

void Pause::Initialize()
{
    int yStart = 260, yOffset = 110;

    this->SetCurrentStateType(eStateType::PAUSE);

    FMOD_MANAGER->SetSoundEffectForLoop(true);
    FMOD_MANAGER->SetBackgroundSoundPaused(true);
    FMOD_MANAGER->SetUISoundPaused(false);

    mObjectPause = new Object("Pause", eObjectType::NONE);
    mObjectPause->AddComponent(new Sprite());
    mObjectPause->AddComponent(new Transform());
    mObjectPause->GetSprite()->SetColor(Vector4<float>(1.f, 1.f, 1.f, 1.f));

    if (OPTION == nullptr || OPTION->GetShouldUseEnglish() == true)
    {
        mObjectPause->GetSprite()->LoadTextureFromData(eTexture::MENU_PAUSE);
    }
    else
    {
        mObjectPause->GetSprite()->LoadTextureFromData(eTexture::K_PAUSE_LOGO);
    }

    mObjectPause->SetScale(Vector2<float>(400, 150));
    mObjectPause->SetPosition(Vector2<float>(0, yStart + 15));

    mObjectResume = new Object("Resume", eObjectType::NONE);
    mObjectResume->AddComponent(new Sprite());
    mObjectResume->AddComponent(new Transform());
    mObjectResume->GetSprite()->SetColor(Vector4<float>(1.f, 1.f, 1.f, 1.f));

    if (OPTION == nullptr || OPTION->GetShouldUseEnglish() == true)
    {
        mObjectResume->GetSprite()->LoadTextureFromData(eTexture::MENU_RESUME);
    }
    else
    {
        mObjectResume->GetSprite()->LoadTextureFromData(eTexture::K_MENU_RESUME);
    }

    mObjectResume->SetScale(buttonScale);
    mObjectResume->SetPosition(Vector2<float>(0, yStart - yOffset));
    mObjectVector.push_back(mObjectResume);

    mObjectBackToMain = new Object("BackToMain", eObjectType::NONE);
    mObjectBackToMain->AddComponent(new Sprite());
    mObjectBackToMain->AddComponent(new Transform());
    mObjectBackToMain->GetSprite()->SetColor(Vector4<float>(1.f, 1.f, 1.f, 1.f));

    if (OPTION == nullptr || OPTION->GetShouldUseEnglish() == true)
    {
        mObjectBackToMain->GetSprite()->LoadTextureFromData(eTexture::MENU_MENU);
    }
    else
    {
        mObjectBackToMain->GetSprite()->LoadTextureFromData(eTexture::K_GO_MAIN);
    }

    mObjectBackToMain->SetScale(buttonScale);
    mObjectBackToMain->SetPosition(Vector2<float>(0, yStart - yOffset * 2));
    mObjectVector.push_back(mObjectBackToMain);

    mObjectHowToPlay = new Object("HowToPlay", eObjectType::NONE);
    mObjectHowToPlay->AddComponent(new Sprite());
    mObjectHowToPlay->AddComponent(new Transform());
    mObjectHowToPlay->GetSprite()->SetColor(Vector4<float>(1.f, 1.f, 1.f, 1.f));

    if (OPTION == nullptr || OPTION->GetShouldUseEnglish() == true)
    {
        mObjectHowToPlay->GetSprite()->LoadTextureFromData(eTexture::MENU_HOW_TO_PLAY);
    }
    else
    {
        mObjectHowToPlay->GetSprite()->LoadTextureFromData(eTexture::K_MENU_HOWTO);
    }

    mObjectHowToPlay->SetScale(buttonScale);
    mObjectHowToPlay->SetPosition(Vector2<float>(0, yStart - yOffset * 3));
    mObjectVector.push_back(mObjectHowToPlay);

    mObjectOption = new Object("Option", eObjectType::NONE);
    mObjectOption->AddComponent(new Sprite());
    mObjectOption->AddComponent(new Transform());
    mObjectOption->GetSprite()->SetColor(Vector4<float>(1.f, 1.f, 1.f, 1.f));

    if (OPTION == nullptr || OPTION->GetShouldUseEnglish() == true)
    {
        mObjectOption->GetSprite()->LoadTextureFromData(eTexture::MENU_OPTION);
    }
    else
    {
        mObjectOption->GetSprite()->LoadTextureFromData(eTexture::K_MENU_OPTION);
    }

    mObjectOption->SetScale(buttonScale);
    mObjectOption->SetPosition(Vector2<float>(0, yStart - yOffset * 4));
    mObjectVector.push_back(mObjectOption);

    mObjectQuit = new Object("Quit", eObjectType::NONE);
    mObjectQuit->AddComponent(new Sprite());
    mObjectQuit->AddComponent(new Transform());
    mObjectQuit->GetSprite()->SetColor(Vector4<float>(1.f, 1.f, 1.f, 1.f));

    if (OPTION == nullptr || OPTION->GetShouldUseEnglish() == true)
    {
        mObjectQuit->GetSprite()->LoadTextureFromData(eTexture::MENU_QUIT);
    }
    else
    {
        mObjectQuit->GetSprite()->LoadTextureFromData(eTexture::K_MENU_QUIT);
    }

    mObjectQuit->SetScale(buttonScale);
    mObjectQuit->SetPosition(Vector2<float>(0, yStart - yOffset * 5));
    mObjectVector.push_back(mObjectQuit);

    OBJECT_MANAGER->AddPauseObject(mObjectPause);
    OBJECT_MANAGER->AddPauseObject(mObjectResume);
    OBJECT_MANAGER->AddPauseObject(mObjectBackToMain);
    OBJECT_MANAGER->AddPauseObject(mObjectHowToPlay);
    OBJECT_MANAGER->AddPauseObject(mObjectOption);
    OBJECT_MANAGER->AddPauseObject(mObjectQuit);

    mCurrentObject = mObjectResume;

    mIndex = 0;
}

void Pause::Update(float dt)
{
    mCurrentObject->SetScale(selectedButtonScale);

    if (Input::IsKeyTriggered(SDL_SCANCODE_UP))
    {
        LOGGER.LogEvent("Key UP Pressed.");
        LOGGER.LogDebug(std::to_string(mIndex));
        LOGGER.LogDebug(mCurrentObject->GetName());

        --mIndex;

        if (mIndex < 0)
        {
            mIndex = 0;
        }
        else
        {
            FMOD_MANAGER->PlaySoundEffectSound("sound/sfx_button_change.mp3");
            mCurrentObject = mObjectVector.at(mIndex);
        }
    }

    if (Input::IsKeyTriggered(SDL_SCANCODE_DOWN))
    {
        LOGGER.LogEvent("Key DOWN Pressed.");
        LOGGER.LogDebug(std::to_string(mIndex));
        LOGGER.LogDebug(mCurrentObject->GetName());

        ++mIndex;

        if (mIndex >= mObjectVector.size())
        {
            mIndex = mObjectVector.size() - 1;
        }
        else
        {
            FMOD_MANAGER->PlaySoundEffectSound("sound/sfx_button_change.mp3");
            mCurrentObject = mObjectVector.at(mIndex);
        }
    }

    for (int i = 0; i < mObjectVector.size(); ++i)
    {
        if (mObjectVector.at(i) != mCurrentObject)
        {
            mObjectVector.at(i)->SetScale(buttonScale);
        }
    }

    if (Input::IsKeyTriggered(SDL_SCANCODE_SPACE) || Input::IsKeyTriggered(SDL_SCANCODE_RETURN))
    {
        FMOD_MANAGER->PlaySoundEffectSound("sound/sfx_button_click.mp3");

        if (mCurrentObject == mObjectResume)
        {
            STATE_MANAGER->UnPauseGame();
            FMOD_MANAGER->SetSoundEffectForLoop(false);
            FMOD_MANAGER->SetGroupOfSFXLoopPaused(true);

            FMOD_MANAGER->SetBackgroundSoundPaused(false);
            FMOD_MANAGER->SetUISoundPaused(true);

            return;
        }
        else if (mCurrentObject == mObjectBackToMain)
        {
            STATE_MANAGER->CreatePauseToMenuMessage();
        }
        else if (mCurrentObject == mObjectHowToPlay)
        {
            STATE_MANAGER->SetNextState(eStateType::HOW_TO_PLAY_PAUSE); 
        }
        else if (mCurrentObject == mObjectOption)
        {
            STATE_MANAGER->SetNextState(eStateType::OPTION);
        }
        else if (mCurrentObject == mObjectQuit)
        {
            STATE_MANAGER->CreateDestructiveMessage();
        }
    }

    if (Input::IsKeyTriggered(SDL_SCANCODE_ESCAPE))
    {
        FMOD_MANAGER->SetSoundEffectForLoop(false);
        FMOD_MANAGER->SetBackgroundSoundPaused(false);
        FMOD_MANAGER->SetUISoundPaused(true);
    }
}

void Pause::ClearAll()
{
    OBJECT_MANAGER->ClearPauseObjects();
    mObjectVector.clear();
}