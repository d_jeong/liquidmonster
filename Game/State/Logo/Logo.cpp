/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : Logo.cpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Yewon Lim
Secondary :
- End Header ----------------------------------------------------------------*/

#include "Logo.hpp"
#include "../Engine/Object/Object.hpp"
#include "../Engine/Object/ObjectType.hpp"
#include "../Engine/Object/ObjectDepth.hpp"
#include "../Engine/Data/Data.hpp"
#include "../Engine/FMOD/FmodManager.hpp"
#include "../Engine/Component/Sprite/Sprite.hpp"
#include "../Engine/Component/Transform/Transform.hpp"
#include "../Engine/Object/ObjectManager/ObjectManager.hpp"
#include "../Engine/Input/Input.hpp"
#include "../StateManager/StateManager.hpp"
#include "../Engine/Graphics/Graphics.hpp"

FMOD::Channel* channel = nullptr;

void Logo::Initialize()
{
    //STATE_MANAGER->SetCurrentState(this->GetStateType());
    std::cout << "Current Level - Logo initialized." << std::endl;

    this->SetCurrentStateType(eStateType::LOGO);

    mDigipenLogo = new Object("DigipenLogo", eObjectType::GAME_LOGO);
    mDigipenLogo->AddComponent(new Sprite());
    mDigipenLogo->AddComponent(new Transform());
    mDigipenLogo->GetSprite()->SetColor(Vector4<float>(1.f, 1.f, 1.f, 1.f));
    mDigipenLogo->GetSprite()->LoadTextureFromData(eTexture::DP_LOGO);
    mDigipenLogo->SetScale(Vector2<float>(1024, 247));
    mDigipenLogo->SetPosition(Vector2<float>(0, 0));

    mTeamLogo = new Object("TeamLogo", eObjectType::TEAM_LOGO);
    mTeamLogo->AddComponent(new Sprite());
    mTeamLogo->AddComponent(new Transform());
    mTeamLogo->GetSprite()->SetColor(Vector4<float>(1.f, 1.f, 1.f, 0.f));
    mTeamLogo->GetSprite()->LoadTextureFromData(eTexture::TEAM_LOGO);
    mTeamLogo->SetScale(Vector2<float>(484, 277));
    mTeamLogo->SetPosition(Vector2<float>(0, 0));

    mFmodLogo = new Object("FmodLogo", eObjectType::FMOD_LOGO);
    mFmodLogo->AddComponent(new Sprite());
    mFmodLogo->AddComponent(new Transform());
    mFmodLogo->GetSprite()->SetColor(Vector4<float>(1.f, 1.f, 1.f, 0.f));
    mFmodLogo->GetSprite()->LoadTextureFromData(eTexture::FMOD_LOGO);
    mFmodLogo->SetScale(Vector2<float>(728, 192));
    mFmodLogo->SetPosition(Vector2<float>(0, 0));

    mSkip = new Object("Skip", eObjectType::NONE);
    mSkip->AddComponent(new Sprite());
    mSkip->AddComponent(new Transform());
    mSkip->GetSprite()->SetColor(Vector4<float>(1.f, 1.f, 1.f, 1.f));
    mSkip->GetSprite()->LoadTexture("texture/Level/press_space_enter_to_skip.png");
    mSkip->SetScale(Vector2<float>(500*.9, 100*.9));
    mSkip->SetPosition(Vector2<float>(0, -300));

    OBJECT_MANAGER->AddObject(mSkip);
    OBJECT_MANAGER->AddObject(mDigipenLogo);
    OBJECT_MANAGER->AddObject(mTeamLogo);
    OBJECT_MANAGER->AddObject(mFmodLogo);

    OBJECT_MANAGER->Initialize();
}

void Logo::Update(float dt)
{
    if (Input::IsKeyTriggered(SDL_SCANCODE_SPACE) || Input::IsKeyTriggered(SDL_SCANCODE_RETURN))
    {
        std::cout << "Skip all logo." << std::endl;
        FMOD_MANAGER->StopSoundEffectSound();
        FMOD_MANAGER->SetVolumeSoundEffectSound(0.8f);
        SetNextState();

        return;
    }

    if (mDigipenLogo->GetSprite()->GetColor().w > 0)
    {
        mTime += dt;
        FMOD::Sound* sound = FMOD_MANAGER->GetSound(1, "sound/sfx_forest.mp3");

        if (FMOD_MANAGER->GetSoundEffectChannel()->getCurrentSound(&sound))
        {
            FMOD_MANAGER->SetVolumeSoundEffectSound(0.5f);
            FMOD_MANAGER->PlaySoundEffectSound("sound/sfx_forest.mp3");
        }

        mDigipenLogo->GetSprite()->SetColor(Vector4<float>(1.f, 1.f, 1.f, 1.f - mTime * 0.3f));
    }
    else if (mDigipenLogo->GetSprite()->GetColor().w < 0)
    {
        mTime -= dt;
        
        FMOD::Sound* sound = FMOD_MANAGER->GetSound(1, "sound/sfx_BAM_monkeySound.mp3");
        
        if (channel->getCurrentSound(&sound)&& mTime>0.5)
        {
            channel = FMOD_MANAGER->PlaySoundEffectSound("sound/sfx_BAM_monkeySound.mp3", channel);
            FMOD_MANAGER->SetVolumeSoundEffectSound(0.5f);
            FMOD_MANAGER->PlaySoundEffectSound("sound/sfx_BAM_monkeySound.mp3");
        }

        mTeamLogo->GetSprite()->SetColor(Vector4<float>(1.f, 1.f, 1.f, 0.f + (3.f - mTime) * 0.3f));

        if (mTime < 0.5)
        {
            channel->stop();
            mTeamLogo->GetSprite()->SetColor(Vector4<float>(1.f, 1.f, 1.f, 0.f));

            mCount += dt;

            mFmodLogo->GetSprite()->SetColor(Vector4<float>(1.f, 1.f, 1.f, mCount * 0.3f));

            if (mCount > 3)
            {
                SetNextState();
                FMOD_MANAGER->StopSoundEffectSound();
                FMOD_MANAGER->SetVolumeSoundEffectSound(0.8f);
            }

            return;
        }
    }
}

void Logo::SetNextState()
{
    if (GRAPHICS->IsDataLoadDone())
    {
        STATE_MANAGER->SetNextState(eStateType::MENU);
    }
    else
    {
        STATE_MANAGER->SetNextState(eStateType::LOADING);
    }
}
