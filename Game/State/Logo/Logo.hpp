/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : Logo.hpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Yewon Lim
Secondary :
- End Header ----------------------------------------------------------------*/

#pragma once
#include "../State.hpp"

class Object;

class Logo : public State
{
public:
    void Initialize() override;
    void Update(float dt) override;

private:
    void SetNextState();

    Object* mDigipenLogo;
    Object* mTeamLogo;
    Object* mFmodLogo;
    Object* mSkip;

    float   mTime = 0.0f;
    float   mCount = 0.0f;
};
