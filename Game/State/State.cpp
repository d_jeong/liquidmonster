/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : State.cpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Daeun Jeong
Secondary :
- End Header ----------------------------------------------------------------*/

#include "State.hpp"
#include "../Engine/Application/Application.hpp"
#include "../Engine/Data/Data.hpp"
#include "../Engine/Engine/Engine.hpp"
#include "../Engine/Imgui/ImGui_manager.hpp"
#include "../Engine/Json/JSON.hpp"
#include "../Engine/Json/LevelJson.hpp"
#include "../Engine/Object/Object.hpp"
#include "../Engine/Object/ObjectDepth.hpp"
#include "../Engine/Object/ObjectType.hpp"
#include "Graphics/Graphics.hpp"
#include "LevelSelect/LevelSelect.hpp"
#include "StateManager/StateManager.hpp"
#include "Option/Option.hpp"

void State::LoadLevel()
{
    if (mCurrentLevelName == "")
    {
        mCurrentLevelName = STATE_MANAGER->EnumToString(mCurrentStateType);
    }

    eStateType currentState = STATE_MANAGER->StringToEnum(mCurrentLevelName);

    if (static_cast<int>(currentState) == static_cast<int>(eStateType::LEVEL_COUNTS) - 1)
    {
        STATE_MANAGER->GetCurrentState()->SetNextStateType(eStateType::CUTSCENE_END);
    }
    else
    {
        int nextState = static_cast<int>(currentState) + 1;
        STATE_MANAGER->GetCurrentState()->SetNextStateType(static_cast<eStateType>(nextState));
    }

    JSON->LoadLevel(mCurrentLevelName + "/", mCurrentLevelName);
}

void State::LoadLevel(const std::string& levelID)
{
    eStateType currentState = STATE_MANAGER->StringToEnum(mCurrentLevelName);

    if (static_cast<int>(currentState) == static_cast<int>(eStateType::LEVEL_COUNTS) - 1)
    {
        STATE_MANAGER->GetCurrentState()->SetNextStateType(eStateType::CUTSCENE_END);
    }
    else
    {
        int nextState = static_cast<int>(currentState) + 1;
        STATE_MANAGER->GetCurrentState()->SetNextStateType(static_cast<eStateType>(nextState));
    }

    JSON->LoadLevel(levelID + "/", levelID);
}

void State::SaveLevel()
{
    for (auto& object : OBJECT_MANAGER->GetObjectVector())
    {
        JSON->SaveLevelObject(object, mCurrentLevelName + "/", mCurrentLevelName);
    }

    JSON->GetObjectDocument().SetObject();

    for (auto& pair : TILE_MAP->GetGraphicTiles())
    {
        JSON->TilesToDocument(pair.first, pair.second, eTileType::GRAPHICS, mCurrentLevelName + "/");
    }

    JSON->GetTileDocument().SetObject();

    for (auto& pair : TILE_MAP->GetPhysicsTiles())
    {
        JSON->TilesToDocument(pair.first, pair.second, eTileType::PHYSICS, mCurrentLevelName + "/");
    }

    JSON->GetTileDocument().SetObject();
}

void State::SaveObject()
{
    for (auto& object : OBJECT_MANAGER->GetObjectVector())
    {
        JSON->SaveLevelObject(object, mCurrentLevelName + "/", mCurrentLevelName);
    }

    JSON->GetObjectDocument().SetObject();
}

void State::SaveLevelLock()
{
    // When saving in the game level.
    if (eStateType::LEVEL_1 <= mCurrentStateType)
    {
        // LevelSelect 1-2 = 2. -> LEVEL_2 = 12. - LEVEL_1 = 11. + 1
        LEVEL_SELECT->SetIsAvailable(true, static_cast<int>(mCurrentStateType) - static_cast<int>(eStateType::LEVEL_1) + 1);
    }
    else if (mCurrentStateType == eStateType::CUTSCENE_END)
    {
        LEVEL_SELECT->SetIsAvailable(true, static_cast<int>(eStateType::LEVEL_COUNTS) - static_cast<int>(eStateType::LEVEL_1));
    }
    else // When saving in the out game. (Clear Level Lock Save File)
    {
        if (LEVEL_SELECT->GetIsAvailableVector().size() < static_cast<int>(eStateType::LEVEL_COUNTS) - static_cast<int>(eStateType::LEVEL_1))
        {
            LEVEL_SELECT->MakeVectorFull();
        }
        else
        {
            for (int i = 0; i <= static_cast<int>(eStateType::LEVEL_COUNTS) - static_cast<int>(eStateType::LEVEL_1); ++i)
            {
                if (i == 0 || i == 1)
                {
                    LEVEL_SELECT->SetIsAvailable(true, i);
                }
                else
                {
                    LEVEL_SELECT->SetIsAvailable(false, i);
                }
            }
        }
    }

    for (int i = 0; i <= static_cast<int>(eStateType::LEVEL_COUNTS) - static_cast<int>(eStateType::LEVEL_1); ++i)
    {
        LEVEL_JSON->LevelToDocument(i);
    }

    LEVEL_JSON->GetLevelLockDocument().SetObject();
}

void State::SaveGraphicsTile()
{
    for (auto& pair : TILE_MAP->GetGraphicTiles())
    {
        JSON->TilesToDocument(pair.first, pair.second, eTileType::GRAPHICS, mCurrentLevelName + "/");
    }

    JSON->GetTileDocument().SetObject();
}

void State::SavePhysicsTile()
{
    for (auto& pair : TILE_MAP->GetPhysicsTiles())
    {
        JSON->TilesToDocument(pair.first, pair.second, eTileType::PHYSICS, mCurrentLevelName + "/");
    }

    JSON->GetTileDocument().SetObject();
}

void State::ClearAll()
{
    OBJECT_MANAGER->ClearObjects();
    TILE_MAP->ClearAllTiles();
    TILE_MAP->ClearGraphicTiles();
    TILE_MAP->ClearPhysicsTiles();
    IMGUI->ResetClickedObject();
}

std::string State::GetCurrentLevelName()
{
    if (mCurrentLevelName == "")
    {
        mCurrentLevelName = STATE_MANAGER->EnumToString(mCurrentStateType);
    }

    return mCurrentLevelName;
}

void State::SetCurrentLevelName(std::string name)
{
    mCurrentLevelName = name;
}

std::string State::GetNextLevelName()
{
    if (mNextLevelName == "")
    {
        mNextLevelName = STATE_MANAGER->EnumToString(mNextStateType);
    }

    return mNextLevelName;
}

void State::SetNextLevelName(std::string name)
{
    mNextLevelName = name;
}

void State::SetCurrentStateType(eStateType stateType)
{
    mCurrentStateType = stateType;
}

eStateType State::GetCurrentStateType(void)
{
    if (mCurrentStateType == eStateType::NONE)
    {
        mCurrentStateType = STATE_MANAGER->StringToEnum(mCurrentLevelName);
    }

    return mCurrentStateType;
}

void State::SetNextStateType(eStateType nextState)
{
    mNextStateType = nextState;
}

eStateType State::GetNextStateType(void)
{
    if (mNextStateType == eStateType::NONE)
    {
        mNextStateType = STATE_MANAGER->StringToEnum(mNextLevelName);
    }

    return mNextStateType;
}

void State::ShowStateNumber(float dt)
{
    Object* object = OBJECT_MANAGER->FindObjectByName("StateNumber");

    if (object->GetSprite()->GetColor().w > 0)
    {
        object->GetSprite()->SetColor(Vector4<float>(1.f, 1.f, 1.f, object->GetSprite()->GetColor().w - dt * .2));
    }
}

void State::SetUpObjects(void)
{
    Object* mObjectPlayer1 = new Object("Player1", eObjectType::PLAYER_1);
    mObjectPlayer1->AddComponent(new Sprite());
    mObjectPlayer1->AddComponent(new Transform());
    mObjectPlayer1->AddComponent(new Player());
    mObjectPlayer1->AddComponent(new Collision());
    mObjectPlayer1->GetSprite()->SetColor(Vector4<float>(1.f, 1.f, 1.f, 0.7f));
    mObjectPlayer1->GetSprite()->LoadTextureFromData(eTexture::PLAYER_1_ANIMATION);
    mObjectPlayer1->GetSprite()->SetAnimation(true);
    mObjectPlayer1->GetSprite()->SetAnimationFrame(6);
    mObjectPlayer1->GetSprite()->SetAnimationSpeed(2.0f);
    mObjectPlayer1->SetPosition(Vector2<float>(-600, -290));
    mObjectPlayer1->SetScale(Vector2<float>(100, 100));
    mObjectPlayer1->SetVelocity(Vector2<float>(0, 0));
    mObjectPlayer1->SetSpawnPosition(Vector2<float>(0, 0));
    mObjectPlayer1->SetDepth(GAME_PLAYER);

    Object* mObjectPlayer2 = new Object("Player2", eObjectType::PLAYER_2);
    mObjectPlayer2->AddComponent(new Sprite());
    mObjectPlayer2->AddComponent(new Transform());
    mObjectPlayer2->AddComponent(new Player());
    mObjectPlayer2->AddComponent(new Collision());
    mObjectPlayer2->GetSprite()->SetColor(Vector4<float>(1.f, 1.f, 1.f, 0.7f));
    mObjectPlayer2->GetSprite()->LoadTextureFromData(eTexture::PLAYER_2_ANIMATION);
    mObjectPlayer2->GetSprite()->SetAnimation(true);
    mObjectPlayer2->GetSprite()->SetAnimationFrame(6);
    mObjectPlayer2->GetSprite()->SetAnimationSpeed(2.0f);
    mObjectPlayer2->SetPosition(Vector2<float>(-500, -290));
    mObjectPlayer2->SetScale(Vector2<float>(100, 100));
    mObjectPlayer2->SetVelocity(Vector2<float>(0, 0));
    mObjectPlayer2->SetSpawnPosition(Vector2<float>(0, 0));
    mObjectPlayer2->SetDepth(GAME_PLAYER);

    Object* mObjectCombined = new Object("PlayerCombined", eObjectType::PLAYER_COMBINED);
    mObjectCombined->AddComponent(new Sprite());
    mObjectCombined->AddComponent(new Transform());
    mObjectCombined->AddComponent(new Player());
    mObjectCombined->AddComponent(new Collision());
    mObjectCombined->GetSprite()->SetColor(Vector4<float>(1.f, 1.f, 1.f, 0.7f));
    mObjectCombined->GetSprite()->LoadTextureFromData(eTexture::PLAYER_COMBINED_ANIMATION);
    mObjectCombined->GetSprite()->SetAnimation(true);
    mObjectCombined->GetSprite()->SetAnimationFrame(6);
    mObjectCombined->GetSprite()->SetAnimationSpeed(2.0f);
    mObjectCombined->SetScale(Vector2<float>(150, 150));
    mObjectCombined->SetPosition(Vector2<float>(-1000, -1000));
    mObjectCombined->SetSpawnPosition(Vector2<float>(0, 0));
    mObjectCombined->SetIsActive(false);
    mObjectCombined->SetDepth(GAME_PLAYER);
    mObjectCombined->SetMass(3.f);

    Object* mObjectDoor = new Object("Door", eObjectType::DOOR);
    mObjectDoor->AddComponent(new Sprite());
    mObjectDoor->AddComponent(new Transform());
    mObjectDoor->AddComponent(new Door());
    mObjectDoor->GetSprite()->SetColor(Vector4<float>(1.f, 1.f, 1.f, 1.f));
    mObjectDoor->GetSprite()->LoadTextureFromData(eTexture::DOOR);
    mObjectDoor->SetScale(Vector2<float>(150, 225));
    mObjectDoor->SetPosition(Vector2<float>(550, -230));
    mObjectDoor->SetDepth(GAME_DOOR);

    Object* mObjectKey = new Object("Key", eObjectType::EATABLE);
    mObjectKey->AddComponent(new Sprite());
    mObjectKey->AddComponent(new Transform());
    mObjectKey->AddComponent(new Collision());
    mObjectKey->GetSprite()->SetColor(Vector4<float>(1.f, 1.f, 1.f, 1.f));

    if (mCurrentStateType == eStateType::CHAPTER2_LEVEL_6)
    {
        mObjectKey->GetSprite()->LoadTextureFromData(eTexture::WHITE_STAR);
    }
    else
    {
        mObjectKey->GetSprite()->LoadTextureFromData(eTexture::KEY);
    }

    mObjectKey->SetScale(Vector2<float>(80, 80));
    mObjectKey->SetPosition(Vector2<float>(0, -300));
    mObjectKey->SetVelocity(Vector2<float>(0, 0));

    Object* mObjectP1Arrow = new Object("Arrow1", eObjectType::ARROW_1);
    mObjectP1Arrow->AddComponent(new Sprite());
    mObjectP1Arrow->AddComponent(new Transform());
    mObjectP1Arrow->GetSprite()->SetColor(Vector4<float>(1.f, 1.f, 1.f, 1.f));
    mObjectP1Arrow->GetSprite()->LoadTextureFromData(eTexture::ARROW_1);
    mObjectP1Arrow->SetScale(Vector2<float>(50, 40));
    mObjectP1Arrow->SetPosition(Vector2<float>(0, 0));
    mObjectP1Arrow->SetDepth(GAME_UI);

    Object* mObjectP2Arrow = new Object("Arrow2", eObjectType::ARROW_2);
    mObjectP2Arrow->AddComponent(new Sprite());
    mObjectP2Arrow->AddComponent(new Transform());
    mObjectP2Arrow->GetSprite()->SetColor(Vector4<float>(1.f, 1.f, 1.f, 1.f));
    mObjectP2Arrow->GetSprite()->LoadTextureFromData(eTexture::ARROW_2);
    mObjectP2Arrow->SetScale(Vector2<float>(50, 40));
    mObjectP2Arrow->SetPosition(Vector2<float>(0, 0));
    mObjectP2Arrow->SetDepth(GAME_UI);

    Object* mObjectP1Spawn = new Object("P1Spawn", eObjectType::NONE);
    mObjectP1Spawn->AddComponent(new Sprite());
    mObjectP1Spawn->AddComponent(new Transform());
    mObjectP1Spawn->GetSprite()->SetColor(Vector4<float>(1.f, 1.f, 1.f, 1.f));
    mObjectP1Spawn->GetSprite()->LoadTextureFromData(eTexture::PLAYER_1_SPAWN_ANIMATION);
    mObjectP1Spawn->GetSprite()->SetAnimation(true);
    mObjectP1Spawn->GetSprite()->SetAnimationFrame(3);
    mObjectP1Spawn->GetSprite()->SetAnimationSpeed(2.0f);
    mObjectP1Spawn->SetScale(Vector2<float>(350, 350));
    mObjectP1Spawn->SetPosition(Vector2<float>(-450, 200));
    mObjectP1Spawn->SetVelocity(Vector2<float>(0, 0));

    Object* mObjectP2Spawn = new Object("P2Spawn", eObjectType::NONE);
    mObjectP2Spawn->AddComponent(new Sprite());
    mObjectP2Spawn->AddComponent(new Transform());
    mObjectP2Spawn->GetSprite()->SetColor(Vector4<float>(1.f, 1.f, 1.f, 1.f));
    mObjectP2Spawn->GetSprite()->LoadTextureFromData(eTexture::PLAYER_2_SPAWN_ANIMATION);
    mObjectP2Spawn->GetSprite()->SetAnimation(true);
    mObjectP2Spawn->GetSprite()->SetAnimationFrame(3);
    mObjectP2Spawn->GetSprite()->SetAnimationSpeed(2.0f);
    mObjectP2Spawn->SetScale(Vector2<float>(350, 350));
    mObjectP2Spawn->SetPosition(Vector2<float>(-100, 200));
    mObjectP2Spawn->SetVelocity(Vector2<float>(0, 0));

    Object* object = new Object("StateNumber", eObjectType::NONE);
    object->AddComponent(new Sprite());
    object->AddComponent(new Transform());
    object->GetSprite()->SetColor(Vector4<float>(1.f, 1.f, 1.f, 1.f));

    switch (mCurrentStateType)
    {
    case eStateType::LEVEL_1:
        object->GetSprite()->LoadTextureFromData(eTexture::LEVEL1_1);
        break;
    case eStateType::LEVEL_2:
        object->GetSprite()->LoadTextureFromData(eTexture::LEVEL1_2);
        break;
    case eStateType::LEVEL_3:
        object->GetSprite()->LoadTextureFromData(eTexture::LEVEL1_3);
        break;
    case eStateType::LEVEL_4:
        object->GetSprite()->LoadTextureFromData(eTexture::LEVEL1_4);
        break;
    case eStateType::LEVEL_5:
        object->GetSprite()->LoadTextureFromData(eTexture::LEVEL1_5);
        break;

    case eStateType::CHAPTER2_LEVEL_1:
        object->GetSprite()->LoadTextureFromData(eTexture::LEVEL2_1);
        break;
    case eStateType::CHAPTER2_LEVEL_2:
        object->GetSprite()->LoadTextureFromData(eTexture::LEVEL2_2);
        break;
    case eStateType::CHAPTER2_LEVEL_3:
        object->GetSprite()->LoadTextureFromData(eTexture::LEVEL2_3);
        break;
    case eStateType::CHAPTER2_LEVEL_4:
        object->GetSprite()->LoadTextureFromData(eTexture::LEVEL2_4);
        break;

    case eStateType::CHAPTER2_LEVEL_5:
        object->GetSprite()->LoadTextureFromData(eTexture::LEVEL3_1);
        break;
    case eStateType::CHAPTER2_LEVEL_6:
        object->GetSprite()->LoadTextureFromData(eTexture::LEVEL3_2);
        break;

    default:
        break;
    }

    object->SetScale(Vector2<float>(250 * .8, 125 * .8));
    object->SetPosition(Vector2<float>(0.f, APPLICATION->GetHeight() / 2.f - object->GetScale().y / 2));
    object->SetDepth(UI);

    OBJECT_MANAGER->AddObject(object);

    OBJECT_MANAGER->AddObject(mObjectPlayer1);
    OBJECT_MANAGER->AddObject(mObjectPlayer2);
    OBJECT_MANAGER->AddObject(mObjectCombined);

    OBJECT_MANAGER->AddObject(mObjectDoor);
    OBJECT_MANAGER->AddObject(mObjectKey);

    OBJECT_MANAGER->AddObject(mObjectP1Arrow);
    OBJECT_MANAGER->AddObject(mObjectP2Arrow);

    OBJECT_MANAGER->AddObject(mObjectP1Spawn);
    OBJECT_MANAGER->AddObject(mObjectP2Spawn);
}