/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : LevelSelect.cpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Daeun Jeong
Secondary :
- End Header ----------------------------------------------------------------*/

#include "LevelSelect.hpp"
#include "../Engine/Component/Sprite/Sprite.hpp"
#include "../Engine/Component/Transform/Transform.hpp"
#include "../Engine/Data/Data.hpp"
#include "../Engine/FMOD/FmodManager.hpp"
#include "../Engine/Input/Input.hpp"
#include "../Engine/Object/Object.hpp"
#include "../Engine/Object/ObjectManager/ObjectManager.hpp"
#include "../Engine/Object/ObjectType.hpp"
#include "../StateManager/StateManager.hpp"
#include "../Engine/Object/ObjectDepth.hpp"
#include "../Engine/Json/LevelJson.hpp"
#include "../Option/Option.hpp"

int index = 0;

LevelSelect* LEVEL_SELECT = new LevelSelect();

void LevelSelect::MakeDoor(std::string name, Vector2<float> position, eTexture textureEnum)
{
    Object* object = new Object(name, eObjectType::STATIC);
    object->AddComponent(new Sprite());
    object->AddComponent(new Transform());
    object->GetSprite()->SetColor(Vector4<float>(1.f, 1.f, 1.f, 1.f));
    object->GetSprite()->LoadTextureFromData(textureEnum);
    object->SetScale(Vector2<float>(100 * 1.1, 130 * 1.15));
    object->SetPosition(position);
    object->SetDepth(GAME_OBJECT);
    mButtons.push_back(object);
    OBJECT_MANAGER->AddObject(object);

    eTexture number = eTexture::LEVEL1_1;

    switch (index)
    {
    case 0: //Back
        number = eTexture::BACK;
        break;

    case 1:
        number = eTexture::LEVEL1_1;
        break;
    case 2:
        number = eTexture::LEVEL1_2;
        break;
    case 3:
        number = eTexture::LEVEL1_3;
        break;
    case 4:
        number = eTexture::LEVEL1_4;
        break;
    case 5:
        number = eTexture::LEVEL1_5;
        break;

    case 6:
        number = eTexture::LEVEL2_1;
        break;
    case 7:
        number = eTexture::LEVEL2_2;
        break;
    case 8:
        number = eTexture::LEVEL2_3;
        break;
    case 9:
        number = eTexture::LEVEL2_4;
        break;

    case 10:
        number = eTexture::LEVEL3_1;
        break;
    case 11:
        number = eTexture::LEVEL3_2;
        break;

    default:
        break;
    }

    MakeNumber("Number", Vector2<float>(position.x, position.y + 110), number);
    MakeStaffOnly("StaffOnly", position, index++);
}

void LevelSelect::MakeNumber(std::string name, Vector2<float> position, eTexture textureEnum)
{
    Object* object = new Object(name, eObjectType::STATIC);
    object->AddComponent(new Sprite());
    object->AddComponent(new Transform());
    object->GetSprite()->SetColor(Vector4<float>(1.f, 1.f, 1.f, 1.f));
    object->GetSprite()->LoadTextureFromData(textureEnum);
    object->SetScale(Vector2<float>(100, 50));
    object->SetPosition(position);
    object->SetDepth(GAME_UI);
    OBJECT_MANAGER->AddObject(object);
}

void LevelSelect::MakeStaffOnly(std::string name, Vector2<float> position, int index)
{
    name.append(std::to_string(index));
    Object* object = new Object(name, eObjectType::STATIC);
    object->AddComponent(new Sprite());
    object->AddComponent(new Transform());
    object->GetSprite()->SetColor(Vector4<float>(1.f, 1.f, 1.f, 1.f));

    if (OPTION == nullptr || OPTION->GetShouldUseEnglish() == true)
    {
        object->GetSprite()->LoadTextureFromData(eTexture::STAFF_ONLY);
    }
    else
    {
        object->GetSprite()->LoadTextureFromData(eTexture::K_STAFF_ONLY);
    }

    object->SetScale(Vector2<float>(100, 125));
    object->SetPosition(position);
    object->SetDepth(GAME_UI);
    object->GetSprite()->SetInVisible();
    OBJECT_MANAGER->AddObject(object);

    //if isAvailable is empty, push back.
    if (mIsAvailable.size() <= static_cast<int>(eStateType::LEVEL_COUNTS) - static_cast<int>(eStateType::LEVEL_1))
    {
        //if Release, set back, 1-1's isAvailable true.
        if (index == 0 || index == 1)
        {
            mIsAvailable.push_back(std::make_pair(true, object));
        }
        else
        {
            mIsAvailable.push_back(std::make_pair(false, object));
        }
    }
    else //if isAvailable is full, just update the objects.
    {
        mIsAvailable[index].second = object;
    }
}

void LevelSelect::Initialize()
{
    index = 0;

    LEVEL_JSON->Initialize();
    
    LEVEL_SELECT = this;

    std::cout << "Current Level - Level Select initialized." << std::endl;

    if (STATE_MANAGER->GetCurrentState()->GetCurrentStateType() == eStateType::OPTION)
    {
        return;
    }

    float xStart = -435, xOffset = 175, yStart = -276, yOffset = 230;

    mLevelWidth = 1280;
    mLevelHeight = 720;

    this->SetCurrentStateType(eStateType::LEVEL_SELECT);

    STATE_MANAGER->SetCurrentState(this->GetCurrentStateType());

    objectBackground = new Object("Background", eObjectType::NONE);
    objectBackground->AddComponent(new Sprite());
    objectBackground->AddComponent(new Transform());
    objectBackground->GetSprite()->SetColor(Vector4<float>(1.f, 1.f, 1.f, 1.f));
    objectBackground->GetSprite()->LoadTextureFromData(eTexture::LEVEL_SELECT_BG);
    objectBackground->SetScale(Vector2<float>(1600 * .8, 900 * .8)); //DONT CHANGE
    objectBackground->SetPosition(Vector2<float>(0, 0));
    objectBackground->SetDepth(GAME_BACKGROUND);

    objectEdge = new Object("Edge", eObjectType::NONE);
    objectEdge->AddComponent(new Sprite());
    objectEdge->AddComponent(new Transform());
    objectEdge->GetSprite()->SetColor(Vector4<float>(1.f, 1.f, 1.f, 1.f));
    objectEdge->GetSprite()->LoadTextureFromData(eTexture::LEVEL_SELECT_EDGE);
    objectEdge->SetScale(Vector2<float>(1250 * 1.02, 700 * 1.02)); //DONT CHANGE
    objectEdge->SetPosition(Vector2<float>(0, -5)); //DONT CHANGE
    objectEdge->SetDepth(GAME_PLAYER);

    MakeDoor("Back", Vector2<float>(xStart, yStart), eTexture::DOOR_EXIT);
    MakeDoor("Door1-1", Vector2<float>(xStart + xOffset, yStart), eTexture::DOOR);
    MakeDoor("Door1-2", Vector2<float>(xStart + xOffset * 2, yStart), eTexture::DOOR);
    MakeDoor("Door1-3", Vector2<float>(xStart + xOffset * 3, yStart), eTexture::DOOR);
    MakeDoor("Door1-4", Vector2<float>(xStart + xOffset * 4, yStart), eTexture::DOOR);
    MakeDoor("Door1-5", Vector2<float>(xStart + xOffset * 5, yStart), eTexture::DOOR);

    MakeDoor("Door2-1", Vector2<float>(-350, yStart + yOffset), eTexture::DOOR);
    MakeDoor("Door2-2", Vector2<float>(-350 + xOffset * 1.325, yStart + yOffset), eTexture::DOOR);
    MakeDoor("Door2-3", Vector2<float>(-350 + xOffset * 1.325 * 2, yStart + yOffset), eTexture::DOOR);
    MakeDoor("Door2-4", Vector2<float>(-350 + xOffset * 1.325 * 3, yStart + yOffset), eTexture::DOOR);

    MakeDoor("Door3-1", Vector2<float>(-325, 195), eTexture::DOOR);
    MakeDoor("Door3-2", Vector2<float>(-40, 195), eTexture::DOOR);

    OBJECT_MANAGER->AddObject(objectBackground);
    OBJECT_MANAGER->AddObject(objectEdge);

    mCurrentButton = OBJECT_MANAGER->FindObjectByName("Back");
    mCurrentButton->GetSprite()->LoadTextureFromData(eTexture::DOOR_EXIT_OPENED);

    OBJECT_MANAGER->Initialize();

    mIndex = 0;

    LEVEL_JSON->LoadLevelLockFromJson();
}

void LevelSelect::Update(float dt)
{
    for (int i = 0; i <= static_cast<int>(eStateType::LEVEL_COUNTS) - static_cast<int>(eStateType::LEVEL_1); ++i)
    {
        if (mIsAvailable[i].first == false)
        {
            if (mIsAvailable[i].second != nullptr)
            {
                mIsAvailable[i].second->GetSprite()->SetVisible();
            }
        }
    }

    if (Input::IsKeyTriggered(SDL_SCANCODE_LEFT))
    {
        FMOD_MANAGER->PlaySoundEffectSound("sound/sfx_button_change.mp3");

        if (mIndex == 0)
        {
            return;
        }

        if (mIsAvailable[mIndex - 1].first == false)
        {
            return;
        }

        --mIndex;

        if (mCurrentButton->GetName() == "Back")
        {
            mCurrentButton->GetSprite()->LoadTextureFromData(eTexture::DOOR_EXIT);
            mCurrentButton = mButtons.at(mIndex);
            mCurrentButton->GetSprite()->LoadTextureFromData(eTexture::OBJECT_DOOR_OPENED);
        }
        else if (mIndex == 0)
        {
            mCurrentButton->GetSprite()->LoadTextureFromData(eTexture::DOOR);
            mCurrentButton = mButtons.at(mIndex);
            mCurrentButton->GetSprite()->LoadTextureFromData(eTexture::DOOR_EXIT_OPENED);
        }
        else
        {
            mCurrentButton->GetSprite()->LoadTextureFromData(eTexture::DOOR);
            mCurrentButton = mButtons.at(mIndex);
            mCurrentButton->GetSprite()->LoadTextureFromData(eTexture::OBJECT_DOOR_OPENED);
        }
    }

    if (Input::IsKeyTriggered(SDL_SCANCODE_RIGHT))
    {
        FMOD_MANAGER->PlaySoundEffectSound("sound/sfx_button_change.mp3");

        if (mCurrentButton->GetName() == "Door3-2")
        {
            return;
        }

        if (mIsAvailable[mIndex + 1].first == false)
        {
            return;
        }

        ++mIndex;


        if (mCurrentButton->GetName() == "Back")
        {
            mCurrentButton->GetSprite()->LoadTextureFromData(eTexture::DOOR_EXIT);
            mCurrentButton = mButtons.at(mIndex);
            mCurrentButton->GetSprite()->LoadTextureFromData(eTexture::OBJECT_DOOR_OPENED);
        }
        else if (mIndex == 0)
        {
            mCurrentButton->GetSprite()->LoadTextureFromData(eTexture::DOOR);
            mCurrentButton = mButtons.at(mIndex);
            mCurrentButton->GetSprite()->LoadTextureFromData(eTexture::DOOR_EXIT_OPENED);
        }
        else
        {
            mCurrentButton->GetSprite()->LoadTextureFromData(eTexture::DOOR);
            mCurrentButton = mButtons.at(mIndex);
            mCurrentButton->GetSprite()->LoadTextureFromData(eTexture::OBJECT_DOOR_OPENED);
        }
    }

    if (Input::IsKeyTriggered(SDL_SCANCODE_UP))
    {
        FMOD_MANAGER->PlaySoundEffectSound("sound/sfx_button_change.mp3");

        if (0 <= mIndex && mIndex <= 3)
        {
            if (mIsAvailable[mIndex + 5].first == false)
            {
                return;
            }

            mIndex += 6;
        }
        else if (mIndex == 4 || mIndex == 5)
        {
            if (mIsAvailable[9].first == false)
            {
                return;
            }

            mIndex = 9;
        }
        else if (mIndex == 6)
        {
            if (mIsAvailable[10].first == false)
            {
                return;
            }

            mIndex = 10;
        }
        else if (7 <= mIndex && mIndex <= 9)
        {
            if (mIsAvailable[11].first == false)
            {
                return;
            }

            mIndex = 11;
        }
        else
        {
            return;
        }

        if (mCurrentButton->GetName() == "Back")
        {
            mCurrentButton->GetSprite()->LoadTextureFromData(eTexture::DOOR_EXIT);
            mCurrentButton = mButtons.at(mIndex);
            mCurrentButton->GetSprite()->LoadTextureFromData(eTexture::OBJECT_DOOR_OPENED);
        }
        else if (mIndex == 0)
        {
            mCurrentButton->GetSprite()->LoadTextureFromData(eTexture::DOOR);
            mCurrentButton = mButtons.at(mIndex);
            mCurrentButton->GetSprite()->LoadTextureFromData(eTexture::DOOR_EXIT_OPENED);
        }
        else
        {
            mCurrentButton->GetSprite()->LoadTextureFromData(eTexture::DOOR);
            mCurrentButton = mButtons.at(mIndex);
            mCurrentButton->GetSprite()->LoadTextureFromData(eTexture::OBJECT_DOOR_OPENED);
        }
    }

    if (Input::IsKeyTriggered(SDL_SCANCODE_DOWN))
    {
        FMOD_MANAGER->PlaySoundEffectSound("sound/sfx_button_change.mp3");

        if (10 <= mIndex && mIndex <= 11)
        {
            if (mIsAvailable[mIndex - 4].first == false)
            {
                return;
            }

            mIndex -= 4;
        }
        else if (6 <= mIndex && mIndex <= 9)
        {
            if (mIsAvailable[mIndex - 6].first == false)
            {
                return;
            }

            mIndex -= 6;
        }
        else
        {
            return;
        }

        if (mCurrentButton->GetName() == "Back")
        {
            mCurrentButton->GetSprite()->LoadTextureFromData(eTexture::DOOR_EXIT);
            mCurrentButton = mButtons.at(mIndex);
            mCurrentButton->GetSprite()->LoadTextureFromData(eTexture::OBJECT_DOOR_OPENED);
        }
        else if (mIndex == 0)
        {
            mCurrentButton->GetSprite()->LoadTextureFromData(eTexture::DOOR);
            mCurrentButton = mButtons.at(mIndex);
            mCurrentButton->GetSprite()->LoadTextureFromData(eTexture::DOOR_EXIT_OPENED);
        }
        else
        {
            mCurrentButton->GetSprite()->LoadTextureFromData(eTexture::DOOR);
            mCurrentButton = mButtons.at(mIndex);
            mCurrentButton->GetSprite()->LoadTextureFromData(eTexture::OBJECT_DOOR_OPENED);
        }
    }

    if (Input::IsKeyTriggered(SDL_SCANCODE_SPACE) || Input::IsKeyTriggered(SDL_SCANCODE_RETURN))
    {
        FMOD_MANAGER->PlaySoundEffectSound("sound/sfx_button_click.mp3");
        FMOD_MANAGER->SetUISoundPaused(true);
        FMOD_MANAGER->SetSoundEffectForLoop(false);

        FMOD::Sound* backgroundSound = nullptr;
        FMOD::Sound* backgroundSoundChapter3 = nullptr;
        FMOD::Sound* sound = nullptr;

        if (0 < mIndex && mIndex <= 5)
        {
            backgroundSound = FMOD_MANAGER->GetSound(0, "sound/background_5.mp3");
            backgroundSoundChapter3 = FMOD_MANAGER->GetSound(0, "sound/background_4.mp3");

            sound = nullptr;

            FMOD_MANAGER->GetBackgroundChannel()->getCurrentSound(&sound);

            if (sound == nullptr)
            {
                FMOD_MANAGER->PlayBackgroundSound("sound/background_chapter_1.mp3");
            }
            else if (sound == backgroundSound || sound == backgroundSoundChapter3)
            {
                FMOD_MANAGER->StopBackgroundSound();
                FMOD_MANAGER->PlayBackgroundSound("sound/background_chapter_1.mp3");
                FMOD_MANAGER->SetBackgroundSoundPaused(false);
            }
            else
            {
                FMOD_MANAGER->SetBackgroundSoundPaused(false);
            }
        }
        else if (5 < mIndex && mIndex <= 9)
        {
            FMOD::Sound* backgroundSound = FMOD_MANAGER->GetSound(0, "sound/background_chapter_1.mp3");
            FMOD::Sound* backgroundSoundChapter3 = FMOD_MANAGER->GetSound(0, "sound/background_4.mp3");
            FMOD::Sound* sound = nullptr;
            FMOD_MANAGER->GetBackgroundChannel()->getCurrentSound(&sound);

            if (sound == nullptr)
            {
                FMOD_MANAGER->PlayBackgroundSound("sound/background_5.mp3");
            }
            else if (sound == backgroundSound || sound == backgroundSoundChapter3)
            {
                FMOD_MANAGER->StopBackgroundSound();
                FMOD_MANAGER->PlayBackgroundSound("sound/background_5.mp3");
                FMOD_MANAGER->SetBackgroundSoundPaused(false);
            }
            else
            {
                FMOD_MANAGER->SetBackgroundSoundPaused(false);
            }
        }
        else if (10 <= mIndex && mIndex < 12)
        {
            FMOD::Sound* backgroundSound = FMOD_MANAGER->GetSound(0, "sound/background_chapter_1.mp3");
            FMOD::Sound* backgroundSoundChapter2 = FMOD_MANAGER->GetSound(0, "sound/background_5.mp3");

            FMOD::Sound* sound = nullptr;

            FMOD_MANAGER->GetBackgroundChannel()->getCurrentSound(&sound);

            if (sound == nullptr)
            {
                FMOD_MANAGER->PlayBackgroundSound("sound/background_4.mp3");
            }
            else if (sound == backgroundSound || sound == backgroundSoundChapter2)
            {
                FMOD_MANAGER->StopBackgroundSound();
                FMOD_MANAGER->PlayBackgroundSound("sound/background_4.mp3");
                FMOD_MANAGER->SetBackgroundSoundPaused(false);
            }
            else
            {
                FMOD_MANAGER->SetBackgroundSoundPaused(false);
            }
        }

        switch (mIndex)
        {
        case 0://BACK
            STATE_MANAGER->SetNextState(eStateType::MENU);
            FMOD_MANAGER->SetUISoundPaused(false);
            break;

        case 1:
            STATE_MANAGER->SetNextState(eStateType::CUTSCENE_BEGIN);
            return;
        case 2:
            STATE_MANAGER->SetNextState(eStateType::LEVEL_2);
            break;
        case 3:
            STATE_MANAGER->SetNextState(eStateType::LEVEL_3);
            break;
        case 4:
            STATE_MANAGER->SetNextState(eStateType::LEVEL_4);
            break;

        case 5:
            STATE_MANAGER->SetNextState(eStateType::LEVEL_5);
            break;
        case 6:
            STATE_MANAGER->SetNextState(eStateType::CHAPTER2_LEVEL_1);
            break;
        case 7:
            STATE_MANAGER->SetNextState(eStateType::CHAPTER2_LEVEL_2);
            break;
        case 8:
            STATE_MANAGER->SetNextState(eStateType::CHAPTER2_LEVEL_3);
            break;
        case 9:
            STATE_MANAGER->SetNextState(eStateType::CHAPTER2_LEVEL_4);
            break;
            break;

        case 10:
            STATE_MANAGER->SetNextState(eStateType::CHAPTER2_LEVEL_5);
            break;
        case 11:
            STATE_MANAGER->SetNextState(eStateType::CHAPTER2_LEVEL_6);
            break;
        default:
            break;
        }
    }
}

void LevelSelect::ClearAll()
{
    OBJECT_MANAGER->ClearObjects();
    mButtons.clear();
    //mIsAvailable.clear();
    index = 0;
}

std::pair<bool, Object*> LevelSelect::GetIsAvailable(int index)
{
    return mIsAvailable[index];
}

std::vector<std::pair<bool, Object*>> LevelSelect::GetIsAvailableVector(void)
{
    return mIsAvailable;
}

void LevelSelect::SetIsAvailable(bool isAvailable, int index)
{
    mIsAvailable[index].first = isAvailable;
}

void LevelSelect::ClearIsAvailable(void)
{
    mIsAvailable.clear();
}

void LevelSelect::MakeVectorFull(void)
{
    for (int i = 0; i <= static_cast<int>(eStateType::LEVEL_COUNTS) - static_cast<int>(eStateType::LEVEL_1); ++i)
    {
        //if Release, set back, 1-1's isAvailable true.
        if (i == 0 || i == 1)
        {
            mIsAvailable.push_back(std::make_pair(true, nullptr));
        }
        else
        {
            mIsAvailable.push_back(std::make_pair(false, nullptr));
        }
    }
}
