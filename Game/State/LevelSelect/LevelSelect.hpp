/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : LevelSelect.hpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Daeun Jeong
Secondary :
- End Header ----------------------------------------------------------------*/

#pragma once
#include "../Game/State/State.hpp"
#include <iostream>
#include <vector>
#include "../Engine/MathLibrary/Vector2.hpp"

class Object;
enum class eTexture;

class LevelSelect : public State
{
public:
    void MakeDoor(std::string name, Vector2<float> position, eTexture textureEnum);
    void MakeNumber(std::string name, Vector2<float> position, eTexture textureEnum);
    void MakeStaffOnly(std::string name, Vector2<float> position, int index);
    void Initialize() override;
    void Update(float dt) override;
    void ClearAll() override;

    std::pair<bool, Object*> GetIsAvailable(int index);
    std::vector<std::pair<bool, Object*>> GetIsAvailableVector(void);
    void SetIsAvailable(bool isAvailable, int index);
    void ClearIsAvailable(void);
    void MakeVectorFull(void);

private:
    Object* objectBackground, *objectEdge, *objectBack;

    std::vector<std::pair<bool, Object*>> mIsAvailable;

    std::vector<Object*> mButtons;
    int                  mIndex = 0;
    Object* mCurrentButton = nullptr;
};

extern LevelSelect* LEVEL_SELECT;