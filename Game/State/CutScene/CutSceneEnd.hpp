/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : CutSceneEnd.hpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Sejeong Park
Secondary :
- End Header ----------------------------------------------------------------*/

#pragma once
#include "../Engine/Object/Object.hpp"
#include "../Game/State/State.hpp"
#include "../Game/State/StateManager/StateManager.hpp"
#include <iostream>
#include <vector>

class CutSceneEnd : public State
{
public:
    void Initialize() override;
    void Update(float dt) override;
    void ClearAll() override;

private:
    Object* mObjScene = nullptr;
    Object* mSkip = nullptr;
    float mTime;
};
