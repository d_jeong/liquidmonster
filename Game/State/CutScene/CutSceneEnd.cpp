/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : CutSceneEnd.cpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Sejeong Park
Secondary :
- End Header ----------------------------------------------------------------*/

#pragma once

#include "CutSceneEnd.hpp"
#include "Graphics/Graphics.hpp"
#include "Object/ObjectManager/ObjectManager.hpp"
#include "../Engine/Component/Sprite/Sprite.hpp"
#include "../Engine/Component/Transform/Transform.hpp"
#include "../Engine/Input/Input.hpp"
#include "../Engine/Data/Data.hpp"
#include "../Engine/FMOD/FmodManager.hpp"
#include "../Option/Option.hpp"

void CutSceneEnd::Initialize()
{
    FMOD_MANAGER->StopBackgroundSound();
    FMOD_MANAGER->PlayBackgroundSound("sound/background_2.mp3");

    this->SetCurrentStateType(eStateType::CUTSCENE_END);
    mLevelHeight = 720;
    mLevelWidth = 1280;
    mTime = 0.f;

    GRAPHICS->SetLevelWidthAndHeight(Vector2<int>(mLevelWidth, mLevelHeight));

    STATE_MANAGER->SetCurrentState(this->GetCurrentStateType());
    std::cout << "CutScene is initialized." << std::endl;

    mObjScene = new Object("CutScene", eObjectType::TEAM_LOGO);
    mObjScene->AddComponent(new Sprite());
    mObjScene->AddComponent(new Transform());
    mObjScene->GetSprite()->SetColor(Vector4<float>(1.f, 1.f, 1.f, 1.f));
    mObjScene->GetSprite()->LoadTextureFromData(eTexture::CUTSCENE_END);
    mObjScene->GetSprite()->SetAnimation(true);
    mObjScene->GetSprite()->SetAnimationFrame(4);
    mObjScene->GetSprite()->SetAnimationSpeed(0.5f);
    mObjScene->SetScale(Vector2<float>(1280, 720));
    mObjScene->SetPosition(Vector2<float>(0, 0));

    mSkip = new Object("Skip", eObjectType::NONE);
    mSkip->AddComponent(new Sprite());
    mSkip->AddComponent(new Transform());
    mSkip->GetSprite()->SetColor(Vector4<float>(1.f, 1.f, 1.f, 1.f));

    if (OPTION == nullptr || OPTION->GetShouldUseEnglish() == true)
    {
        mSkip->GetSprite()->LoadTextureFromData(eTexture::PRESS_SPACE_TO_SKIP);
    }
    else
    {
        mSkip->GetSprite()->LoadTextureFromData(eTexture::K_PRESS_SKIP);
    }

    mSkip->SetScale(Vector2<float>(500 * .9, 100 * .9));
    mSkip->SetPosition(Vector2<float>(375, -300));

    OBJECT_MANAGER->AddObject(mObjScene);
    OBJECT_MANAGER->AddObject(mSkip);
    OBJECT_MANAGER->Initialize();
}

void CutSceneEnd::Update(float dt)
{
    mTime += dt;

    if (mTime > 7.0f)
    {
        STATE_MANAGER->SetNextState(eStateType::MENU);
        FMOD_MANAGER->StopBackgroundSound();
        FMOD_MANAGER->SetUISoundPaused(false);
    }

    if (Input::IsKeyTriggered(SDL_SCANCODE_SPACE) || Input::IsKeyTriggered(SDL_SCANCODE_RETURN))
    {
        std::cout << "Skip scene." << std::endl;
        STATE_MANAGER->SetNextState(eStateType::MENU);

        FMOD_MANAGER->StopBackgroundSound();
        FMOD_MANAGER->SetUISoundPaused(false);
        return;
    }
}

void CutSceneEnd::ClearAll()
{
    OBJECT_MANAGER->ClearObjects();
}
