/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : StateManager.cpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Daeun Jeong
Secondary :
- End Header ----------------------------------------------------------------*/

#include "StateManager.hpp"
#include "../../GameList.h"
#include "../Engine/Camera/Camera.hpp"
#include "../Engine/FMOD/FmodManager.hpp"
#include "../Engine/Graphics/Graphics.hpp"
#include "../Engine/Input/Input.hpp"
#include "../Engine/Object/ObjectManager/ObjectManager.hpp"
#include "../Game/State/Credit/Credit.hpp"
#include "../Game/State/CutScene/CutSceneBegin.hpp"
#include "../Game/State/CutScene/CutSceneEnd.hpp"
#include "../Game/State/GameLevel/GameLevel.hpp"
#include "../Game/State/HowToPlay/HowToPlay.hpp"
#include "../Game/State/LevelSelect/LevelSelect.hpp"
#include "../Game/State/Loading/Loading.hpp"
#include "../Game/State/Logo/Logo.hpp"
#include "../Game/State/Menu/Menu.hpp"
#include "../Game/State/Option/Option.hpp"
#include "../Game/State/Pause/Pause.hpp"
#include "../HowToPlay/HowToPlay_Pause.hpp"

#include <iostream>

StateManager* STATE_MANAGER = new StateManager();

StateManager::StateManager()
{
    STATE_MANAGER   = this;
    mChapterChange  = false;
    mChapterChange2 = false;
}

StateManager::~StateManager()
{
    delete STATE_MANAGER;
    STATE_MANAGER = nullptr;
}

void StateManager::Initialize()
{
    mCurrentState      = nullptr;
    mNextState         = nullptr;
    mPauseState        = nullptr;
    mStateChangeTimer  = 0.f;
    mShouldStateChange = false;

    mStates.insert(std::make_pair(eStateType::LOGO, new Logo));
    mStates.insert(std::make_pair(eStateType::LOADING, new Loading));
    mStates.insert(std::make_pair(eStateType::MENU, new Menu));
    mStates.insert(std::make_pair(eStateType::LEVEL_SELECT, new LevelSelect));
    mStates.insert(std::make_pair(eStateType::PAUSE, new Pause()));
    mStates.insert(std::make_pair(eStateType::CUTSCENE_BEGIN, new CutSceneBegin()));
    mStates.insert(std::make_pair(eStateType::CUTSCENE_END, new CutSceneEnd()));

    mStates.insert(std::make_pair(eStateType::LEVEL_1, new GameLevel()));
    mStates.insert(std::make_pair(eStateType::CREDIT, new Credit()));
    mStates.insert(std::make_pair(eStateType::OPTION, new Option()));
    mStates.insert(std::make_pair(eStateType::HOW_TO_PLAY, new HowToPlay()));
    mStates.insert(std::make_pair(eStateType::HOW_TO_PLAY_PAUSE, new HowToPlay_Pause()));

    SetCurrentState(eStateType::LOGO);
    SetPauseState();

    mCurrentState->Initialize();
}

void StateManager::Update(float dt)
{
    if (mChapterChange == true)
    {
        FMOD_MANAGER->StopBackgroundSound();
        FMOD_MANAGER->PlayBackgroundSound("sound/background_5.mp3");
        mChapterChange = false;
    }

    if (mChapterChange2 == true)
    {
        FMOD_MANAGER->StopBackgroundSound();
        FMOD_MANAGER->PlayBackgroundSound("sound/background_4.mp3");
        mChapterChange2 = false;
    }

    if (mIsKeyAvailable == true)
    {
        if (Input::IsKeyTriggered(SDL_SCANCODE_R))
        {
            FMOD_MANAGER->StopSoundEffectForLoop();
            Restart();
        }
    }

    if (mIsPaused == false)
    {
        if (mShouldStateChange == true)
        {
            mCurrentState->ClearAll();
            mCurrentState = mNextState;
            mCurrentState->SetCurrentStateType(mNextState->GetCurrentStateType());
            mNextState         = nullptr;
            mShouldStateChange = false;
            GRAPHICS->ClearParticleEmitters();

            if (mCurrentState->GetCurrentStateType() >= eStateType::LEVEL_1)
            {
                mCurrentState->SetUpObjects();
                mCurrentState->LoadLevel();
                OBJECT_MANAGER->Initialize();

                GRAPHICS->GetCamera()->Initialize();
            }
            else
            {
                mCurrentState->Initialize();
                GRAPHICS->GetCamera()->Initialize();
            }

            if (mCurrentState->GetCurrentStateType() == eStateType::CHAPTER2_LEVEL_1)
            {
                mChapterChange = true;
            }
            else if (mCurrentState->GetCurrentStateType() == eStateType::CHAPTER2_LEVEL_5)
            {
                mChapterChange2 = true;
            }
        }
        else
        {
            if (mStateChangeTimer > 0.f)
            {
                mStateChangeTimer -= dt;

                if (mStateChangeTimer < 0.4f && GRAPHICS->IsTransitioning() == false)
                {
                    GRAPHICS->TurnOnTransition();
                }
                if (mStateChangeTimer < 0.f)
                {
                    mShouldStateChange = true;
                }
            }

            if (mCurrentState->GetCurrentStateType() >= eStateType::LEVEL_1)
            {
                mCurrentState->ShowStateNumber(dt);
            }

            StateKeySetting();

            if (message.IsMessageOn())
            {
                message.UpdateMessage();
            }
            else
            {
                mCurrentState->Update(dt);
            }
        }
    }
    else
    {
        if (message.IsMessageOn())
        {
            message.UpdateMessage();
        }
        else
        {
            mPauseState->Update(dt);
        }
        if (mShouldStateChange && mIsPaused)
        {
            mPauseState->ClearAll();
            mPauseState = mNextState;
            mPauseState->SetCurrentStateType(mNextState->GetCurrentStateType());
            mNextState         = nullptr;
            mShouldStateChange = false;
            mPauseState->Initialize();
            GRAPHICS->GetCamera()->Initialize();
        }
    }

    if (Input::IsKeyTriggered(SDL_SCANCODE_ESCAPE) && !message.IsMessageOn())
    {
        if (mCurrentState->GetCurrentStateType() >= eStateType::LEVEL_1)
        {
            if (!mIsPaused == true)
            {
                mPauseState->Initialize();
            }
            else
            {
                mPauseState->ClearAll();
                mPauseState = mStates.find(eStateType::PAUSE)->second;
            }
            mIsPaused = !mIsPaused;
        }
    }
}

void StateManager::Delete()
{
    for (auto states : mStates)
    {
        delete states.second;
    }
    mStates.clear();
}

void StateManager::Restart()
{
    if (mCurrentState->GetCurrentStateType() >= eStateType::LEVEL_1)
    {
        mCurrentState->ClearAll();
        mCurrentState->SetUpObjects();
        mCurrentState->LoadLevel();
        OBJECT_MANAGER->Initialize();
    }
    else if (mCurrentState->GetCurrentStateType() == eStateType::HOW_TO_PLAY)
    {
        mCurrentState->ClearAll();
        mCurrentState->Initialize();
    }
}

State* StateManager::GetCurrentState(void)
{
    return mCurrentState;
}

void StateManager::SetCurrentState(eStateType stateType)
{
    // If stateType does not exist in state map
    if (mStates.find(stateType) == mStates.end())
    {
        // Add stateType
        mStates.insert(std::make_pair(stateType, new GameLevel()));
    }

    mCurrentState = mStates.find(stateType)->second;

    mCurrentState->SetCurrentStateType(stateType);
}

State* StateManager::GetNextState(void)
{
    return mNextState;
}

void StateManager::SetNextState(eStateType stateType)
{
    // If stateType does not exist in state map
    if (mStates.find(stateType) == mStates.end())
    {
        // Add stateType
        mStates.insert(std::make_pair(stateType, new GameLevel()));
    }

    mNextState = mStates.find(stateType)->second;
    mNextState->SetCurrentStateType(stateType);
    mShouldStateChange = true;
}

void StateManager::SetPauseState(void)
{
    mPauseState = mStates.find(eStateType::PAUSE)->second;

    mPauseState->SetCurrentStateType(eStateType::PAUSE);
}

bool StateManager::IsPaused(void)
{
    return mIsPaused;
}

bool StateManager::IsGameStopped(void)
{
    return mIsPaused || message.IsMessageOn();
}

void StateManager::PauseGame(void)
{
    if (mCurrentState->GetCurrentStateType() >= eStateType::LEVEL_1)
    {
        if (!mIsPaused)
        {
            mIsPaused = true;
            mPauseState->Initialize();
        }
    }
}

void StateManager::UnPauseGame(void)
{
    if (mIsPaused)
    {
        mIsPaused = false;
        mPauseState->ClearAll();
    }
}

eStateType StateManager::StringToEnum(std::string stateName)
{
    if (stateName == "")
    {
        return eStateType::NONE;
    }

    else if (stateName == "level1")
    {
        return eStateType::LEVEL_1;
    }
    else if (stateName == "level2")
    {
        return eStateType::LEVEL_2;
    }
    else if (stateName == "level3")
    {
        return eStateType::LEVEL_3;
    }
    else if (stateName == "level4")
    {
        return eStateType::LEVEL_4;
    }
    else if (stateName == "level5")
    {
        return eStateType::LEVEL_5;
    }

    else if (stateName == "chapter2_level1")
    {
        return eStateType::CHAPTER2_LEVEL_1;
    }
    else if (stateName == "chapter2_level2")
    {
        return eStateType::CHAPTER2_LEVEL_2;
    }
    else if (stateName == "chapter2_level3")
    {
        return eStateType::CHAPTER2_LEVEL_3;
    }
    else if (stateName == "chapter2_level4")
    {
        return eStateType::CHAPTER2_LEVEL_4;
    }
    else if (stateName == "chapter2_level5")
    {
        return eStateType::CHAPTER2_LEVEL_5;
    }
    else if (stateName == "chapter2_level6")
    {
        return eStateType::CHAPTER2_LEVEL_6;
    }

    return eStateType::NONE;
}

std::string StateManager::EnumToString(eStateType stateType)
{
    switch (stateType)
    {
        case eStateType::LEVEL_1:
            return "level1";
            break;
        case eStateType::LEVEL_2:
            return "level2";
            break;
        case eStateType::LEVEL_3:
            return "level3";
            break;
        case eStateType::LEVEL_4:
            return "level4";
            break;
        case eStateType::LEVEL_5:
            return "level5";
            break;

        case eStateType::CHAPTER2_LEVEL_1:
            return "chapter2_level1";
            break;
        case eStateType::CHAPTER2_LEVEL_2:
            return "chapter2_level2";
            break;
        case eStateType::CHAPTER2_LEVEL_3:
            return "chapter2_level3";
            break;
        case eStateType::CHAPTER2_LEVEL_4:
            return "chapter2_level4";
            break;
        case eStateType::CHAPTER2_LEVEL_5:
            return "chapter2_level5";
            break;
        case eStateType::CHAPTER2_LEVEL_6:
            return "chapter2_level6";
            break;

        default:
            return "";
            break;
    }
}

void StateManager::StateKeySetting(void)
{
    if (mIsKeyAvailable == true)
    {
        if (mCurrentState->GetCurrentStateType() > eStateType::LOADING)
        {
            if (Input::IsKeyTriggered(SDL_SCANCODE_F9))
            {
                if (static_cast<int>(eStateType::LEVEL_1) <= static_cast<int>(mCurrentState->GetCurrentStateType()) && static_cast<int>(mCurrentState->GetCurrentStateType()) < static_cast<int>(eStateType::LEVEL_COUNTS) - 1)
                {
                    STATE_MANAGER->SetNextState(mCurrentState->GetNextStateType());
                    return;
                }
            }
        }

        if (Input::IsKeyTriggered(SDL_SCANCODE_1))
        {
            FMOD_MANAGER->SetBackgroundSoundPaused(true);
        }
        if (Input::IsKeyTriggered(SDL_SCANCODE_2))
        {
            FMOD_MANAGER->SetBackgroundSoundPaused(false);
        }
        if (Input::IsKeyTriggered(SDL_SCANCODE_3))
        {
            FMOD_MANAGER->SetAllGameSoundPaused(true);
        }
        if (Input::IsKeyTriggered(SDL_SCANCODE_4))
        {
            FMOD_MANAGER->SetAllGameSoundPaused(false);
        }
    }
}

bool StateManager::GetIsKeyAvailable(void)
{
    return mIsKeyAvailable;
}

void StateManager::SetIsKeyAvailable(bool isActive)
{
    mIsKeyAvailable = isActive;
}

void StateManager::SetShouldStateChange(bool set)
{
    mShouldStateChange = set;
}

void StateManager::SetStateChangeTimer(float timer)
{
    mStateChangeTimer  = timer;
    mShouldStateChange = false;
}

void StateManager::CreateDestructiveMessage()
{
    // do not turn on message if it is already popped up
    if (message.IsMessageOn() || mCurrentState->GetCurrentStateType() == eStateType::LOADING || mCurrentState->GetCurrentStateType() == eStateType::LOGO)
    {
        return;
    }

    if (mCurrentState->GetCurrentStateType() >= eStateType::LEVEL_1)
    {
        message.CreateMessage(Message::eMessageType::LOSE_PROGRESS);
    }
    else
    {
        message.CreateMessage(Message::eMessageType::QUIT);
    }
}

void StateManager::CreatePauseToMenuMessage()
{
    if (message.IsMessageOn())
    {
        return;
    }

    message.CreateMessage(Message::eMessageType::PAUSE_TO_MAIN);
}

void StateManager::CreateDataResetMessage()
{
    if (message.IsMessageOn())
    {
        return;
    }

    message.CreateMessage(Message::eMessageType::DATA_RESET);
}