/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : StateManager.hpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Daeun Jeong
Secondary :
- End Header ----------------------------------------------------------------*/

#pragma once

#include "../Engine/System.hpp"
#include <unordered_map>
#include "../Message/Message.hpp"

class State;
enum class eStateType;

class StateManager : public System
{
public:
    StateManager();
    ~StateManager();

    void Initialize() override;
    void Update(float dt) override;
    void Delete();
    void Restart();

    State *GetCurrentState(void);
    void   SetCurrentState(eStateType);

    State *GetNextState(void);
    void   SetNextState(eStateType newState);
    
    void   SetPauseState(void);
    
    eStateType StringToEnum(std::string);
    std::string EnumToString(eStateType);

    bool IsPaused(void);
    bool IsGameStopped(void);
    void PauseGame(void);
    void UnPauseGame(void);

    void StateKeySetting(void);
    
    bool GetIsKeyAvailable(void);
    void SetIsKeyAvailable(bool);

    void SetShouldStateChange(bool set);
    void SetStateChangeTimer(float timer);

    void CreateDestructiveMessage();
    void CreateDataResetMessage();
    void CreatePauseToMenuMessage();

private:
    std::unordered_map<eStateType, State *> mStates;

    State *mCurrentState = nullptr;
    State *mNextState    = nullptr;

    State *mPauseState = nullptr;
    bool   mIsPaused   = false;

    bool mShouldRestart = false;

    bool mIsKeyAvailable = true;

    float mStateChangeTimer;
    bool  mShouldStateChange;

    bool mChapterChange;
    bool mChapterChange2;

    Message message;
};

extern StateManager *STATE_MANAGER;