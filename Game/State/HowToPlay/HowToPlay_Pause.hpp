/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : HowToPlay_Pause.hpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Haewon Shon
Secondary :
- End Header ----------------------------------------------------------------*/
#pragma once

#include "../State.hpp"

class Object;

class HowToPlay_Pause : public State
{
public:
    void Initialize() override;
    void Update(float dt) override;
    void ClearAll() override;

private:
    Object *mPressBack, *m1P, *m2P;
};