/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : HowToPlay_Pause.cpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Haewon Shon
Secondary :
- End Header ----------------------------------------------------------------*/

#include "HowToPlay_Pause.hpp"
#include "../Engine/Object/ObjectManager/ObjectManager.hpp"
#include "../Engine/Input/Input.hpp"
#include "../StateManager/StateManager.hpp"
#include "../Engine/Object/Object.hpp"
#include "../Engine/Object/ObjectType.hpp"
#include "../Engine/Component/Sprite/Sprite.hpp"
#include "../Engine/FMOD/FmodManager.hpp"
#include "../Engine/Component/Transform/Transform.hpp"
#include "../Engine/Data/Data.hpp"
#include "../Option/Option.hpp"

void HowToPlay_Pause::Initialize()
{
    Vector2<float> scale = {1045, 228};

    mPressBack = new Object("pressBack", eObjectType::NONE);
    mPressBack->AddComponent(new Sprite);
    mPressBack->AddComponent(new Transform);
    mPressBack->GetSprite()->SetColor(Vector4<float>(1.f, 1.f, 1.f, 1.f));

    if (OPTION == nullptr || OPTION->GetShouldUseEnglish() == true)
    {
        mPressBack->GetSprite()->LoadTextureFromData(eTexture::PRESS_SPACE_TO_BACK);
    }
    else
    {
        mPressBack->GetSprite()->LoadTextureFromData(eTexture::K_PRESS_GO_BACK);
    }

    mPressBack->SetScale(Vector2<float>(470, 101));
    mPressBack->SetPosition(Vector2<float>(13, -292));

    m1P = new Object("1P", eObjectType::NONE);
    m1P->AddComponent(new Sprite);
    m1P->AddComponent(new Transform);
    m1P->GetSprite()->SetColor(Vector4<float>(1.f, 1.f, 1.f, 1.f));

    if (OPTION == nullptr || OPTION->GetShouldUseEnglish() == true)
    {
        m1P->GetSprite()->LoadTextureFromData(eTexture::_1P_KEY_ENG);
    }
    else
    {
        m1P->GetSprite()->LoadTextureFromData(eTexture::_1P_KEY_KOR);
    }

    m1P->SetScale(Vector2<float>(scale));
    m1P->SetPosition(Vector2<float>(0, -110));

    m2P = new Object("2P", eObjectType::NONE);
    m2P->AddComponent(new Sprite);
    m2P->AddComponent(new Transform);
    m2P->GetSprite()->SetColor(Vector4<float>(1.f, 1.f, 1.f, 1.f));

    if (OPTION == nullptr || OPTION->GetShouldUseEnglish() == true)
    {
        m2P->GetSprite()->LoadTextureFromData(eTexture::_2P_KEY_ENG);
    }
    else
    {
        m2P->GetSprite()->LoadTextureFromData(eTexture::_2P_KEY_KOR);
    }

    m2P->SetScale(Vector2<float>(scale));
    m2P->SetPosition(Vector2<float>(0, 110));

    OBJECT_MANAGER->AddPauseObject(mPressBack);
    OBJECT_MANAGER->AddPauseObject(m1P);
    OBJECT_MANAGER->AddPauseObject(m2P);
}

void HowToPlay_Pause::Update(float dt)
{
    if (Input::IsKeyTriggered(SDL_SCANCODE_SPACE) || Input::IsKeyTriggered(SDL_SCANCODE_RETURN))
    {
        FMOD_MANAGER->PlaySoundEffectSound("sound/sfx_button_click.mp3");
        STATE_MANAGER->SetNextState(eStateType::PAUSE);
    }
}

void HowToPlay_Pause::ClearAll()
{
    OBJECT_MANAGER->ClearPauseObjects();
}