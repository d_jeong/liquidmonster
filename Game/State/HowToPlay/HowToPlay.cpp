/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : HowToPlay.cpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Daeun Jeong
Secondary :
- End Header ----------------------------------------------------------------*/

#include "HowToPlay.hpp"
#include "../Engine/Component/Collision/Collision.hpp"
#include "../Engine/Component/Player/Player.hpp"
#include "../Engine/Component/Sprite/Sprite.hpp"
#include "../Engine/Component/Transform/Transform.hpp"
#include "../Engine/Data/Data.hpp"
#include "../Engine/FMOD/FmodManager.hpp"
#include "../Engine/Graphics/Graphics.hpp"
#include "../Engine/Input/Input.hpp"
#include "../Engine/Object/Object.hpp"
#include "../Engine/Object/ObjectDepth.hpp"
#include "../Engine/Object/ObjectManager/ObjectManager.hpp"
#include "../Engine/Object/ObjectType.hpp"
#include "../Engine/TileMap/TileMap.hpp"
#include "../StateManager/StateManager.hpp"
#include "../Option/Option.hpp"

void HowToPlay::Initialize()
{
    this->SetCurrentStateType(eStateType::HOW_TO_PLAY);

    mLevelWidth = 1280;
    mLevelHeight = 720;

    for (int i = 5; i < 35; ++i)
    {
        TILE_MAP->CreatePhysicsTile(7100 + i, eTexture::TILE_EXAMPLE_1, false);
    }

    for (int i = 2; i < 18; ++i)
    {
        TILE_MAP->CreatePhysicsTile(4700 + 5 + i * 160, eTexture::TILE_EXAMPLE_1, false);
        TILE_MAP->CreatePhysicsTile(4700 + 29 + 5 + i * 160, eTexture::TILE_EXAMPLE_1, false);
    }

    TILE_MAP->CreatePhysicsTile(7100 + 19 - 160, eTexture::TILE_EXAMPLE_1, false);
    TILE_MAP->CreatePhysicsTile(7100 + 20 - 160, eTexture::TILE_EXAMPLE_1, false);
    TILE_MAP->CreatePhysicsTile(7100 + 19 - 320, eTexture::TILE_EXAMPLE_1, false);
    TILE_MAP->CreatePhysicsTile(7100 + 20 - 320, eTexture::TILE_EXAMPLE_1, false);

    GRAPHICS->SetLevelWidthAndHeight(Vector2<int>(mLevelWidth, mLevelHeight));

    STATE_MANAGER->SetCurrentState(this->GetCurrentStateType());
    std::cout << "HowToPlay Level initialized." << std::endl;

    howToPlay = new Object("HowToPlay", eObjectType::NONE);
    howToPlay->AddComponent(new Sprite);
    howToPlay->AddComponent(new Transform);
    howToPlay->GetSprite()->SetColor(Vector4<float>(1.f, 1.f, 1.f, 1.f));

    howToPlay->GetSprite()->LoadTextureFromData(eTexture::PLAYER_1_HOW_TO_PLAY);

    howToPlay->SetScale(Vector2<float>(320, 160));
    howToPlay->SetPosition(Vector2<float>(325, 120));

    howToPlay2 = new Object("HowToPlay2", eObjectType::NONE);
    howToPlay2->AddComponent(new Sprite);
    howToPlay2->AddComponent(new Transform);
    howToPlay2->GetSprite()->SetColor(Vector4<float>(1.f, 1.f, 1.f, 1.f));

    howToPlay2->GetSprite()->LoadTextureFromData(eTexture::PLAYER_2_HOW_TO_PLAY);

    howToPlay2->SetScale(Vector2<float>(320, 160));
    howToPlay2->SetPosition(Vector2<float>(-339, 120));

    rKeyToSpawn = new Object("RKeyToSpawn", eObjectType::NONE);
    rKeyToSpawn->AddComponent(new Sprite);
    rKeyToSpawn->AddComponent(new Transform);
    rKeyToSpawn->GetSprite()->SetColor(Vector4<float>(1.f, 1.f, 1.f, 1.f));

    if (OPTION == nullptr || OPTION->GetShouldUseEnglish() == true)
    {
        rKeyToSpawn->GetSprite()->LoadTextureFromData(eTexture::R_KEY_TO_RESPAWN);
    }
    else
    {
        rKeyToSpawn->GetSprite()->LoadTextureFromData(eTexture::K_R_RESTART);
    }

    rKeyToSpawn->GetSprite()->SetAnimation(true);
    rKeyToSpawn->GetSprite()->SetAnimationFrame(2);
    rKeyToSpawn->GetSprite()->SetAnimationSpeed(1.5f);
    rKeyToSpawn->SetScale(Vector2<float>(340, 60));
    rKeyToSpawn->SetPosition(Vector2<float>(0, 292));

    key1 = new Object("key1", eObjectType::EATABLE);
    key1->AddComponent(new Sprite());
    key1->AddComponent(new Transform());
    key1->AddComponent(new Collision());
    key1->GetSprite()->SetColor(Vector4<float>(1.f, 1.f, 1.f, 1.f));
    key1->GetSprite()->LoadTextureFromData(eTexture::KEY);
    key1->SetScale(Vector2<float>(80, 80));
    key1->SetPosition(Vector2<float>(-473, -187.5));
    key1->SetVelocity(Vector2<float>(0, 0));

    key2 = new Object("key2", eObjectType::EATABLE);
    key2->AddComponent(new Sprite());
    key2->AddComponent(new Transform());
    key2->AddComponent(new Collision());
    key2->GetSprite()->SetColor(Vector4<float>(1.f, 1.f, 1.f, 1.f));
    key2->GetSprite()->LoadTextureFromData(eTexture::KEY);
    key2->SetScale(Vector2<float>(80, 80));
    key2->SetPosition(Vector2<float>(452, -187.5));
    key2->SetVelocity(Vector2<float>(0, 0));

    pressBack = new Object("pressBack", eObjectType::NONE);
    pressBack->AddComponent(new Sprite);
    pressBack->AddComponent(new Transform);
    pressBack->GetSprite()->SetColor(Vector4<float>(1.f, 1.f, 1.f, 1.f));

    if (OPTION == nullptr || OPTION->GetShouldUseEnglish() == true)
    {
        pressBack->GetSprite()->LoadTextureFromData(eTexture::PRESS_SPACE_TO_BACK);
    }
    else
    {
        pressBack->GetSprite()->LoadTextureFromData(eTexture::K_PRESS_GO_BACK);
    }

    pressBack->SetScale(Vector2<float>(470, 101));
    pressBack->SetPosition(Vector2<float>(13, -292));

    player1 = new Object("Player1", eObjectType::PLAYER_1);
    player1->AddComponent(new Sprite);
    player1->AddComponent(new Transform);
    player1->AddComponent(new Player());
    player1->AddComponent(new Collision());
    player1->GetSprite()->SetColor(Vector4<float>(1.f, 1.f, 1.f, 0.7f));
    player1->GetSprite()->LoadTextureFromData(eTexture::PLAYER_1_ANIMATION);
    player1->GetSprite()->SetAnimation(true);
    player1->GetSprite()->SetAnimationFrame(6);
    player1->GetSprite()->SetAnimationSpeed(2.0f);
    player1->SetScale(Vector2<float>(100, 100));
    player1->SetPosition(Vector2<float>(0, -270));
    player1->SetVelocity(Vector2<float>(0, 0));
    player1->SetSpawnPosition(Vector2<float>(0, 0));
    player1->SetDepth(GAME_PLAYER);

    player2 = new Object("Player2", eObjectType::PLAYER_2);
    player2->AddComponent(new Sprite);
    player2->AddComponent(new Transform);
    player2->AddComponent(new Player());
    player2->AddComponent(new Collision());
    player2->GetSprite()->SetColor(Vector4<float>(1.f, 1.f, 1.f, 0.7f));
    player2->GetSprite()->SetAnimation(true);
    player2->GetSprite()->SetAnimationFrame(6);
    player2->GetSprite()->SetAnimationSpeed(2.0f);
    player2->GetSprite()->LoadTextureFromData(eTexture::PLAYER_2_ANIMATION);
    player2->SetScale(Vector2<float>(100, 100));
    player2->SetPosition(Vector2<float>(0, -270));
    player2->SetVelocity(Vector2<float>(0, 0));
    player2->SetSpawnPosition(Vector2<float>(0, 0));
    player2->SetDepth(GAME_PLAYER);

    mObjectCombined = new Object("PlayerCombined", eObjectType::PLAYER_COMBINED);
    mObjectCombined->AddComponent(new Sprite());
    mObjectCombined->AddComponent(new Transform());
    mObjectCombined->AddComponent(new Player());
    mObjectCombined->AddComponent(new Collision());
    mObjectCombined->GetSprite()->SetColor(Vector4<float>(1.f, 1.f, 1.f, 0.7f));
    mObjectCombined->GetSprite()->LoadTextureFromData(eTexture::PLAYER_COMBINED_ANIMATION);
    mObjectCombined->GetSprite()->SetAnimation(true);
    mObjectCombined->GetSprite()->SetAnimationFrame(6);
    mObjectCombined->GetSprite()->SetAnimationSpeed(2.0f);
    mObjectCombined->SetScale(Vector2<float>(150, 150));
    mObjectCombined->SetPosition(Vector2<float>(-1000, -1000));
    mObjectCombined->SetSpawnPosition(Vector2<float>(0, 0));
    mObjectCombined->SetIsActive(false);
    mObjectCombined->SetDepth(GAME_PLAYER);
    mObjectCombined->SetMass(3.f);

    mObjectP1Arrow = new Object("Arrow1", eObjectType::ARROW_1);
    mObjectP1Arrow->AddComponent(new Sprite());
    mObjectP1Arrow->AddComponent(new Transform());
    mObjectP1Arrow->GetSprite()->SetColor(Vector4<float>(1.f, 1.f, 1.f, 1.f));
    mObjectP1Arrow->GetSprite()->LoadTextureFromData(eTexture::ARROW_1);
    mObjectP1Arrow->SetScale(Vector2<float>(50, 40));
    mObjectP1Arrow->SetPosition(Vector2<float>(0, 0));
    mObjectP1Arrow->SetDepth(GAME_UI);

    mObjectP2Arrow = new Object("Arrow2", eObjectType::ARROW_2);
    mObjectP2Arrow->AddComponent(new Sprite());
    mObjectP2Arrow->AddComponent(new Transform());
    mObjectP2Arrow->GetSprite()->SetColor(Vector4<float>(1.f, 1.f, 1.f, 1.f));
    mObjectP2Arrow->GetSprite()->LoadTextureFromData(eTexture::ARROW_2);
    mObjectP2Arrow->SetScale(Vector2<float>(50, 40));
    mObjectP2Arrow->SetPosition(Vector2<float>(0, 0));
    mObjectP2Arrow->SetDepth(GAME_UI);

    mObjectP1Spawn = new Object("P1Spawn", eObjectType::NONE);
    mObjectP1Spawn->AddComponent(new Sprite());
    mObjectP1Spawn->AddComponent(new Transform());
    mObjectP1Spawn->GetSprite()->SetColor(Vector4<float>(1.f, 1.f, 1.f, 1.f));
    mObjectP1Spawn->GetSprite()->LoadTextureFromData(eTexture::PLAYER_1_SPAWN_ANIMATION);
    mObjectP1Spawn->GetSprite()->SetAnimation(true);
    mObjectP1Spawn->GetSprite()->SetAnimationFrame(3);
    mObjectP1Spawn->GetSprite()->SetAnimationSpeed(2.0f);
    mObjectP1Spawn->SetScale(Vector2<float>(350, 350));
    mObjectP1Spawn->SetPosition(Vector2<float>(325, 412));
    mObjectP1Spawn->SetVelocity(Vector2<float>(0, 0));

    mObjectP2Spawn = new Object("P2Spawn", eObjectType::NONE);
    mObjectP2Spawn->AddComponent(new Sprite());
    mObjectP2Spawn->AddComponent(new Transform());
    mObjectP2Spawn->GetSprite()->SetColor(Vector4<float>(1.f, 1.f, 1.f, 1.f));
    mObjectP2Spawn->GetSprite()->LoadTextureFromData(eTexture::PLAYER_2_SPAWN_ANIMATION);
    mObjectP2Spawn->GetSprite()->SetAnimation(true);
    mObjectP2Spawn->GetSprite()->SetAnimationFrame(3);
    mObjectP2Spawn->GetSprite()->SetAnimationSpeed(2.0f);
    mObjectP2Spawn->SetScale(Vector2<float>(350, 350));
    mObjectP2Spawn->SetPosition(Vector2<float>(-339, 412));
    mObjectP2Spawn->SetVelocity(Vector2<float>(0, 0));

    OBJECT_MANAGER->AddObject(howToPlay);
    OBJECT_MANAGER->AddObject(howToPlay2);
    OBJECT_MANAGER->AddObject(rKeyToSpawn);
    OBJECT_MANAGER->AddObject(key1);
    OBJECT_MANAGER->AddObject(key2);
    OBJECT_MANAGER->AddObject(pressBack);

    OBJECT_MANAGER->AddObject(player1);
    OBJECT_MANAGER->AddObject(player2);
    OBJECT_MANAGER->AddObject(mObjectCombined);
    OBJECT_MANAGER->AddObject(mObjectP1Spawn);
    OBJECT_MANAGER->AddObject(mObjectP2Spawn);
    OBJECT_MANAGER->AddObject(mObjectP1Arrow);
    OBJECT_MANAGER->AddObject(mObjectP2Arrow);

    OBJECT_MANAGER->Initialize();
}

void HowToPlay::Update(float dt)
{
    if (mIsKeyAvailable == true)
    {
        if (Input::IsKeyTriggered(SDL_SCANCODE_R))
        {
            FMOD_MANAGER->StopSoundEffectForLoop();
            STATE_MANAGER->Restart();
        }
    }

    if (Input::IsKeyTriggered(SDL_SCANCODE_SPACE) || Input::IsKeyTriggered(SDL_SCANCODE_RETURN))
    {
        FMOD_MANAGER->PlaySoundEffectSound("sound/sfx_button_click.mp3");
        STATE_MANAGER->SetNextState(eStateType::MENU);
    }

    if (Input::IsKeyTriggered(SDL_SCANCODE_W))
    {
        if (OPTION == nullptr || OPTION->GetShouldUseEnglish() == true)
        {
            howToPlay2->GetSprite()->LoadTextureFromData(eTexture::PLAYER_2_HOW_TO_JUMP);
        }
        else
        {
            howToPlay2->GetSprite()->LoadTextureFromData(eTexture::K_PLAYER_2_HOW_TO_JUMP);
        }
    }
    else if (Input::IsKeyTriggered(SDL_SCANCODE_A) || Input::IsKeyTriggered(SDL_SCANCODE_D))
    {
        if (OPTION == nullptr || OPTION->GetShouldUseEnglish() == true)
        {
            howToPlay2->GetSprite()->LoadTextureFromData(eTexture::PLAYER_2_HOW_TO_MOVE);
        }
        else
        {
            howToPlay2->GetSprite()->LoadTextureFromData(eTexture::K_PLAYER_2_HOW_TO_MOVE);
        }
    }
    else if (Input::IsKeyTriggered(SDL_SCANCODE_S))
    {
        if (OPTION == nullptr || OPTION->GetShouldUseEnglish() == true)
        {
            howToPlay2->GetSprite()->LoadTextureFromData(eTexture::PLAYER_2_HOW_TO_EAT);
        }
        else
        {
            howToPlay2->GetSprite()->LoadTextureFromData(eTexture::K_PLAYER_2_HOW_TO_EAT);
        }
    }
    else if (Input::IsKeyTriggered(SDL_SCANCODE_LSHIFT))
    {
        if (OPTION == nullptr || OPTION->GetShouldUseEnglish() == true)
        {
            howToPlay2->GetSprite()->LoadTextureFromData(eTexture::PLAYER_2_HOW_TO_SPIT);
        }
        else
        {
            howToPlay2->GetSprite()->LoadTextureFromData(eTexture::K_PLAYER_2_HOW_TO_SPIT);
        }
    }

    if (Input::IsKeyTriggered(SDL_SCANCODE_UP))
    {
        if (OPTION == nullptr || OPTION->GetShouldUseEnglish() == true)
        {
            howToPlay->GetSprite()->LoadTextureFromData(eTexture::PLAYER_1_HOW_TO_JUMP);
        }
        else
        {
            howToPlay->GetSprite()->LoadTextureFromData(eTexture::K_PLAYER_1_HOW_TO_JUMP);
        }
    }
    else if (Input::IsKeyTriggered(SDL_SCANCODE_LEFT) || Input::IsKeyTriggered(SDL_SCANCODE_RIGHT))
    {
        if (OPTION == nullptr || OPTION->GetShouldUseEnglish() == true)
        {
            howToPlay->GetSprite()->LoadTextureFromData(eTexture::PLAYER_1_HOW_TO_MOVE);
        }
        else
        {
            howToPlay->GetSprite()->LoadTextureFromData(eTexture::K_PLAYER_1_HOW_TO_MOVE);
        }
    }
    else if (Input::IsKeyTriggered(SDL_SCANCODE_DOWN))
    {
        if (OPTION == nullptr || OPTION->GetShouldUseEnglish() == true)
        {
            howToPlay->GetSprite()->LoadTextureFromData(eTexture::PLAYER_1_HOW_TO_EAT);
        }
        else
        {
            howToPlay->GetSprite()->LoadTextureFromData(eTexture::K_PLAYER_1_HOW_TO_EAT);
        }
    }
    else if (Input::IsKeyTriggered(SDL_SCANCODE_RSHIFT))
    {
        if (OPTION == nullptr || OPTION->GetShouldUseEnglish() == true)
        {
            howToPlay->GetSprite()->LoadTextureFromData(eTexture::PLAYER_1_HOW_TO_SPIT);
        }
        else
        {
            howToPlay->GetSprite()->LoadTextureFromData(eTexture::K_PLAYER_1_HOW_TO_SPIT);
        }
    }
}

void HowToPlay::ClearAll()
{
    OBJECT_MANAGER->ClearObjects();
    TILE_MAP->ClearAllTiles();
    GRAPHICS->ClearParticleEmitters();
}
