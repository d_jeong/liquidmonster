/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : HowToPlay.hpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Daeun Jeong
Secondary :
- End Header ----------------------------------------------------------------*/

#pragma once
#include "../Game/State/State.hpp"
#include <vector>

class Object;

class HowToPlay : public State
{
public:
    void Initialize() override;
    void Update(float dt) override;
    void ClearAll() override;

private:
    Object* howToPlay;
    Object* howToPlay2;
    Object* rKeyToSpawn;
    Object* key1;
    Object* key2;
    Object* pressBack;

    Object* player1;
    Object* player2;
    Object* mObjectCombined;
    Object* mObjectP1Spawn;
    Object* mObjectP2Spawn;
    Object* mObjectP1Arrow;
    Object* mObjectP2Arrow;

    bool mIsKeyAvailable = true;
};
