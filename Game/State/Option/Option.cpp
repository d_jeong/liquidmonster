/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : Credit.cpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Inyeong Han
Secondary :
- End Header ----------------------------------------------------------------*/

#include "Option.hpp"
#include "../Engine/Application/Application.hpp"
#include "../Engine/Component/Sprite/Sprite.hpp"
#include "../Engine/Component/Transform/Transform.hpp"
#include "../Engine/Data/Data.hpp"
#include "../Engine/FMOD/FmodManager.hpp"
#include "../Engine/Graphics/Graphics.hpp"
#include "../Engine/Input/Input.hpp"
#include "../Engine/Logger/Logger.hpp"
#include "../Engine/Object/Object.hpp"
#include "../Engine/Object/ObjectManager/ObjectManager.hpp"
#include "../Engine/Object/ObjectType.hpp"
#include "../StateManager/StateManager.hpp"
#include "../Engine/Json/LevelJson.hpp"
#include "../Engine/Object/ObjectDepth.hpp"
#include "../LevelSelect/LevelSelect.hpp"

Option* OPTION = nullptr;

Object* option, * full_screen, * full_on, *full_off,
*sound, *sound_100, *sound_0, 
*sound_25, *sound_50, *sound_75, 
*option_back, *data_reset, 
*option_language, *option_eng, *option_kora;

const Vector2<float> button1Scale = { 100, 100 };
const Vector2<float> selectedButton1Scale = { 100 * 1.25, 100 * 1.25 };

const Vector2<float> buttonScale = { 300, 100 };
const Vector2<float> selectedButtonScale = { 300 * 1.25, 100 * 1.25 };

void Option::Initialize()
{
    OPTION = this;

    mLevelWidth = 1280;
    mLevelHeight = 720;

    this->SetCurrentStateType(eStateType::OPTION);

    GRAPHICS->SetLevelWidthAndHeight(Vector2<int>(mLevelWidth, mLevelHeight));

    // STATE_MANAGER->SetCurrentState(this->GetCurrentStateType());
    std::cout << "Option initialized." << std::endl;

    option = new Object("Option", eObjectType::NONE);
    option->AddComponent(new Sprite());
    option->AddComponent(new Transform());
    option->GetSprite()->SetColor(Vector4<float>(1.f, 1.f, 1.f, 1.f));

    if (mShouldUseEnglish == true)
    {
        option->GetSprite()->LoadTextureFromData(eTexture::OPTION_LOGO);
    }
    else
    {
        option->GetSprite()->LoadTextureFromData(eTexture::K_OPTION_LOGO);
    }

    option->SetScale(Vector2<float>(500, 150));
    option->SetPosition(Vector2<float>(0, 275));

    sound = new Object("Sound_option", eObjectType::NONE);
    sound->AddComponent(new Sprite());
    sound->AddComponent(new Transform());
    sound->GetSprite()->SetColor(Vector4<float>(1.f, 1.f, 1.f, 1.f));

    if (mShouldUseEnglish == true)
    {
        sound->GetSprite()->LoadTextureFromData(eTexture::SOUND_OPTION);
    }
    else
    {
        sound->GetSprite()->LoadTextureFromData(eTexture::K_OPTION_SOUND);
    }

    sound->SetScale(Vector2<float>(250, 150));
    sound->SetPosition(Vector2<float>(-450, 130));

    sound_0 = new Object("Sound_off", eObjectType::NONE);
    sound_0->AddComponent(new Sprite());
    sound_0->AddComponent(new Transform());
    sound_0->GetSprite()->SetColor(Vector4<float>(1.f, 1.f, 1.f, 1.f));

    if (mShouldUseEnglish == true)
    {
        sound_0->GetSprite()->LoadTextureFromData(eTexture::SOUND_MUTE);
    }
    else
    {
        sound_0->GetSprite()->LoadTextureFromData(eTexture::K_MUTE);
    }

    sound_0->SetScale(Vector2<float>(100, 100));
    sound_0->SetPosition(Vector2<float>(-192, 130));
    mObjects.push_back(sound_0);

    sound_25 = new Object("Sound_75", eObjectType::NONE);
    sound_25->AddComponent(new Sprite());
    sound_25->AddComponent(new Transform());
    sound_25->GetSprite()->SetColor(Vector4<float>(1.f, 1.f, 1.f, 1.f));

    if (mShouldUseEnglish == true)
    {
        sound_25->GetSprite()->LoadTextureFromData(eTexture::SOUND_25);
    }
    else
    {
        sound_25->GetSprite()->LoadTextureFromData(eTexture::SOUND_25);
    }

    sound_25->SetScale(Vector2<float>(100, 100));
    sound_25->SetPosition(Vector2<float>(-48, 130));
    mObjects.push_back(sound_25);

    sound_50 = new Object("Sound_50", eObjectType::NONE);
    sound_50->AddComponent(new Sprite());
    sound_50->AddComponent(new Transform());
    sound_50->GetSprite()->SetColor(Vector4<float>(1.f, 1.f, 1.f, 1.f));

    if (mShouldUseEnglish == true)
    {
        sound_50->GetSprite()->LoadTextureFromData(eTexture::SOUND_50);
    }
    else
    {
        sound_50->GetSprite()->LoadTextureFromData(eTexture::SOUND_50);
    }

    sound_50->SetScale(Vector2<float>(100, 100));
    sound_50->SetPosition(Vector2<float>(100,130));
    mObjects.push_back(sound_50);

    sound_75 = new Object("Sound_75", eObjectType::NONE);
    sound_75->AddComponent(new Sprite());
    sound_75->AddComponent(new Transform());
    sound_75->GetSprite()->SetColor(Vector4<float>(1.f, 1.f, 1.f, 1.f));

    if (mShouldUseEnglish == true)
    {
        sound_75->GetSprite()->LoadTextureFromData(eTexture::SOUND_75);
    }
    else
    {
        sound_75->GetSprite()->LoadTextureFromData(eTexture::SOUND_75);
    }

    sound_75->SetScale(Vector2<float>(100, 100));
    sound_75->SetPosition(Vector2<float>(247, 130));
    mObjects.push_back(sound_75);

    sound_100 = new Object("Sound_on", eObjectType::NONE);
    sound_100->AddComponent(new Sprite());
    sound_100->AddComponent(new Transform());
    sound_100->GetSprite()->SetColor(Vector4<float>(1.f, 1.f, 1.f, 1.f));

    if (mShouldUseEnglish == true)
    {
        sound_100->GetSprite()->LoadTextureFromData(eTexture::SOUND_100);
    }
    else
    {
        sound_100->GetSprite()->LoadTextureFromData(eTexture::SOUND_100);
    }

    sound_100->SetScale(Vector2<float>(100, 100));
    sound_100->SetPosition(Vector2<float>(395, 130));
    mObjects.push_back(sound_100);

    full_screen = new Object("Full_screen", eObjectType::NONE);
    full_screen->AddComponent(new Sprite());
    full_screen->AddComponent(new Transform());
    full_screen->GetSprite()->SetColor(Vector4<float>(1.f, 1.f, 1.f, 1.f));

    if (mShouldUseEnglish == true)
    {
        full_screen->GetSprite()->LoadTextureFromData(eTexture::FULL_SCREEN);
    }
    else
    {
        full_screen->GetSprite()->LoadTextureFromData(eTexture::K_FULL_SCREEN);
    }

    full_screen->SetScale(Vector2<float>(250, 150));
    full_screen->SetPosition(Vector2<float>(-450, 0));

    full_on = new Object("Full_on", eObjectType::NONE);
    full_on->AddComponent(new Sprite());
    full_on->AddComponent(new Transform());
    full_on->GetSprite()->SetColor(Vector4<float>(1.f, 1.f, 1.f, 1.f));

    if (mShouldUseEnglish == true)
    {
        full_on->GetSprite()->LoadTextureFromData(eTexture::ON_BUTTON);
    }
    else
    {
        full_on->GetSprite()->LoadTextureFromData(eTexture::K_ON_BUTTON);
    }

    full_on->SetScale(Vector2<float>(100, 100));
    full_on->SetPosition(Vector2<float>(-100, 0));
    mObjects.push_back(full_on);

    full_off = new Object("Full_off", eObjectType::NONE);
    full_off->AddComponent(new Sprite());
    full_off->AddComponent(new Transform());
    full_off->GetSprite()->SetColor(Vector4<float>(1.f, 1.f, 1.f, 1.f));

    if (mShouldUseEnglish == true)
    {
        full_off->GetSprite()->LoadTextureFromData(eTexture::OFF_BUTTON);
    }
    else
    {
        full_off->GetSprite()->LoadTextureFromData(eTexture::K_OFF_BUTTON);
    }

    full_off->SetScale(Vector2<float>(100, 100));
    full_off->SetPosition(Vector2<float>(300, 0));
    mObjects.push_back(full_off);

    option_language = new Object("Language", eObjectType::NONE);
    option_language->AddComponent(new Sprite());
    option_language->AddComponent(new Transform());
    option_language->GetSprite()->SetColor(Vector4<float>(1.f, 1.f, 1.f, 1.f));

    if (mShouldUseEnglish == true)
    {
        option_language->GetSprite()->LoadTextureFromData(eTexture::OPTION_LANGUAGE);
    }
    else
    {
        option_language->GetSprite()->LoadTextureFromData(eTexture::K_OPTION_LANGUAGE);
    }

    option_language->SetScale(Vector2<float>(250, 150));
    option_language->SetPosition(Vector2<float>(-450, -130));

    option_eng = new Object("English", eObjectType::NONE);
    option_eng->AddComponent(new Sprite());
    option_eng->AddComponent(new Transform());
    option_eng->GetSprite()->SetColor(Vector4<float>(1.f, 1.f, 1.f, 1.f));

    if (mShouldUseEnglish == true)
    {
        option_eng->GetSprite()->LoadTextureFromData(eTexture::OPTION_ENGLISH);
    }
    else
    {
        option_eng->GetSprite()->LoadTextureFromData(eTexture::OPTION_ENGLISH);
    }

    option_eng->SetScale(Vector2<float>(100, 100));
    option_eng->SetPosition(Vector2<float>(-100, -130));
    mObjects.push_back(option_eng);

    option_kora = new Object("Korean", eObjectType::NONE);
    option_kora->AddComponent(new Sprite());
    option_kora->AddComponent(new Transform());
    option_kora->GetSprite()->SetColor(Vector4<float>(1.f, 1.f, 1.f, 1.f));

    if (mShouldUseEnglish == true)
    {
        option_kora->GetSprite()->LoadTextureFromData(eTexture::OPTION_KOREAN);
    }
    else
    {
        option_kora->GetSprite()->LoadTextureFromData(eTexture::OPTION_KOREAN);
    }

    option_kora->SetScale(Vector2<float>(100, 100));
    option_kora->SetPosition(Vector2<float>(300, -130));
    mObjects.push_back(option_kora);

    data_reset = new Object("data_reset", eObjectType::NONE);
    data_reset->AddComponent(new Sprite());
    data_reset->AddComponent(new Transform());
    data_reset->GetSprite()->SetColor(Vector4<float>(1.f, 1.f, 1.f, 1.f));

    if (mShouldUseEnglish == true)
    {
        data_reset->GetSprite()->LoadTextureFromData(eTexture::DATA_RESET);
    }
    else
    {
        data_reset->GetSprite()->LoadTextureFromData(eTexture::K_DATA_RESET);
    }

    data_reset->SetScale(Vector2<float>(300, 100));
    data_reset->SetPosition(Vector2<float>(-100, -260));
    mObjects.push_back(data_reset);

    option_back = new Object("Back", eObjectType::NONE);
    option_back->AddComponent(new Sprite());
    option_back->AddComponent(new Transform());
    option_back->GetSprite()->SetColor(Vector4<float>(1.f, 1.f, 1.f, 1.f));

    if (mShouldUseEnglish == true)
    {
        option_back->GetSprite()->LoadTextureFromData(eTexture::LEVEL_SELECT_BACK);
    }
    else
    {
        option_back->GetSprite()->LoadTextureFromData(eTexture::K_BACK);
    }

    option_back->SetScale(Vector2<float>(300, 100));
    option_back->SetPosition(Vector2<float>(300, -260));
    mObjects.push_back(option_back);

    if (STATE_MANAGER->IsPaused())
    {
        OBJECT_MANAGER->AddPauseObject(option);
        OBJECT_MANAGER->AddPauseObject(sound);
        OBJECT_MANAGER->AddPauseObject(sound_0);
        OBJECT_MANAGER->AddPauseObject(sound_25);
        OBJECT_MANAGER->AddPauseObject(sound_50);
        OBJECT_MANAGER->AddPauseObject(sound_75);
        OBJECT_MANAGER->AddPauseObject(sound_100);
        OBJECT_MANAGER->AddPauseObject(full_screen);
        OBJECT_MANAGER->AddPauseObject(full_on);
        OBJECT_MANAGER->AddPauseObject(full_off);
        OBJECT_MANAGER->AddPauseObject(option_language);
        OBJECT_MANAGER->AddPauseObject(option_eng);
        OBJECT_MANAGER->AddPauseObject(option_kora);
        OBJECT_MANAGER->AddPauseObject(data_reset);
        OBJECT_MANAGER->AddPauseObject(option_back);
    }
    else
    {
        OBJECT_MANAGER->AddObject(option);
        OBJECT_MANAGER->AddObject(sound);
        OBJECT_MANAGER->AddObject(sound_0);
        OBJECT_MANAGER->AddObject(sound_25);
        OBJECT_MANAGER->AddObject(sound_50);
        OBJECT_MANAGER->AddObject(sound_75);
        OBJECT_MANAGER->AddObject(sound_100);
        OBJECT_MANAGER->AddObject(full_screen);
        OBJECT_MANAGER->AddObject(full_on);
        OBJECT_MANAGER->AddObject(full_off);
        OBJECT_MANAGER->AddObject(option_language);
        OBJECT_MANAGER->AddObject(option_eng);
        OBJECT_MANAGER->AddObject(option_kora);
        OBJECT_MANAGER->AddObject(data_reset);
        OBJECT_MANAGER->AddObject(option_back);
    }

    mCurrentObject = sound_0;

    if (STATE_MANAGER->IsPaused())
    {
    }
    else
    {
        OBJECT_MANAGER->Initialize();
    }

    mIndex = 0;

    LEVEL_SELECT->Initialize();
}

void Option::Update(float dt)
{
    if (mShouldUseEnglish == true)
    {
        option->GetSprite()->LoadTextureFromData(eTexture::OPTION_LOGO);
        sound->GetSprite()->LoadTextureFromData(eTexture::SOUND_OPTION);
        sound_0->GetSprite()->LoadTextureFromData(eTexture::SOUND_MUTE);
        sound_25->GetSprite()->LoadTextureFromData(eTexture::SOUND_25);
        sound_50->GetSprite()->LoadTextureFromData(eTexture::SOUND_50);
        sound_75->GetSprite()->LoadTextureFromData(eTexture::SOUND_75);
        sound_100->GetSprite()->LoadTextureFromData(eTexture::SOUND_100);
        full_screen->GetSprite()->LoadTextureFromData(eTexture::FULL_SCREEN);
        full_on->GetSprite()->LoadTextureFromData(eTexture::ON_BUTTON);
        full_off->GetSprite()->LoadTextureFromData(eTexture::OFF_BUTTON);
        option_language->GetSprite()->LoadTextureFromData(eTexture::OPTION_LANGUAGE);
        option_eng->GetSprite()->LoadTextureFromData(eTexture::OPTION_ENGLISH);
        option_kora->GetSprite()->LoadTextureFromData(eTexture::OPTION_KOREAN);
        data_reset->GetSprite()->LoadTextureFromData(eTexture::DATA_RESET);
        option_back->GetSprite()->LoadTextureFromData(eTexture::LEVEL_SELECT_BACK);
    }
    else
    {
        option->GetSprite()->LoadTextureFromData(eTexture::K_OPTION_LOGO);
        sound_0->GetSprite()->LoadTextureFromData(eTexture::K_MUTE);
        sound_25->GetSprite()->LoadTextureFromData(eTexture::SOUND_25);
        sound_50->GetSprite()->LoadTextureFromData(eTexture::SOUND_50);
        sound_75->GetSprite()->LoadTextureFromData(eTexture::SOUND_75);
        sound_100->GetSprite()->LoadTextureFromData(eTexture::SOUND_100);
        full_screen->GetSprite()->LoadTextureFromData(eTexture::K_FULL_SCREEN);
        full_on->GetSprite()->LoadTextureFromData(eTexture::K_ON_BUTTON);
        full_off->GetSprite()->LoadTextureFromData(eTexture::K_OFF_BUTTON);
        sound->GetSprite()->LoadTextureFromData(eTexture::K_OPTION_SOUND);
        option_language->GetSprite()->LoadTextureFromData(eTexture::K_OPTION_LANGUAGE);
        option_eng->GetSprite()->LoadTextureFromData(eTexture::OPTION_ENGLISH);
        option_kora->GetSprite()->LoadTextureFromData(eTexture::OPTION_KOREAN);
        data_reset->GetSprite()->LoadTextureFromData(eTexture::K_DATA_RESET);
        option_back->GetSprite()->LoadTextureFromData(eTexture::K_BACK);
    }

    if ( mCurrentObject == sound_100 ||
        mCurrentObject == sound_75 ||mCurrentObject == sound_50 ||mCurrentObject == sound_25||
        mCurrentObject == sound_0)
    {
        mCurrentObject->SetScale(selectedButton1Scale);
    }
    else
    {
        mCurrentObject->SetScale(selectedButtonScale);
    }

    if (Input::IsKeyTriggered(SDL_SCANCODE_LEFT))
    {
        FMOD_MANAGER->PlaySoundEffectSound("sound/sfx_button_change.mp3");

        LOGGER.LogEvent("Key Left Pressed.");
        LOGGER.LogDebug(std::to_string(mIndex));
        LOGGER.LogDebug(mCurrentObject->GetName());

        if (mIndex == 0)
        {
            return;
        }

        --mIndex;

        if (mCurrentObject->GetName() == "Sound_off")
        {
            mCurrentObject = mObjects.at(mIndex);
            }
        else
        {
            mCurrentObject = mObjects.at(mIndex);
        }
    }

    if (Input::IsKeyTriggered(SDL_SCANCODE_RIGHT))
    {
        FMOD_MANAGER->PlaySoundEffectSound("sound/sfx_button_change.mp3");

        LOGGER.LogEvent("Key right Pressed.");
        LOGGER.LogDebug(std::to_string(mIndex));
        LOGGER.LogDebug(mCurrentObject->GetName());

        if (mIndex==10)
        {
            return;
        }

        ++mIndex;

        if (mCurrentObject->GetName() == "Sound_off")
        {
            mCurrentObject = mObjects.at(mIndex);
        }
        else
        {
            mCurrentObject = mObjects.at(mIndex);
        }
    }

    if (Input::IsKeyTriggered(SDL_SCANCODE_UP))
    {
        FMOD_MANAGER->PlaySoundEffectSound("sound/sfx_button_change.mp3");

        LOGGER.LogEvent("Key UP Pressed.");
        LOGGER.LogDebug(std::to_string(mIndex));
        LOGGER.LogDebug(mCurrentObject->GetName());

        if (mIndex == 5)
        {
            mIndex = 0;
        }
        else if (mIndex == 6)
        {
            mIndex = 4;
        }
        else if (mIndex == 7)
        {
            mIndex = 5;
        }
        else if (mIndex == 8)
        {
            mIndex = 6;
        }
        else if (mIndex == 9)
        {
            mIndex = 7;
        }
        else if (mIndex == 10)
        {
            mIndex = 8;
        }
        else
        {
            return;
        }
        if (mCurrentObject->GetName() == "Sound_off")
        {
            mCurrentObject = mObjects.at(mIndex);
        }
        else
        {
            mCurrentObject = mObjects.at(mIndex);
        }
    }

    if (Input::IsKeyTriggered(SDL_SCANCODE_DOWN))
    {
        FMOD_MANAGER->PlaySoundEffectSound("sound/sfx_button_change.mp3");

        LOGGER.LogEvent("Key UP Pressed.");
        LOGGER.LogDebug(std::to_string(mIndex));
        LOGGER.LogDebug(mCurrentObject->GetName());

        if (0 <= mIndex && mIndex <= 2)
        {
            mIndex = 5;
        }
        else if (3 <= mIndex && mIndex <= 4)
        {
            mIndex = 6;
        }
        else if (mIndex == 5)
        {
            mIndex = 7;
        }
        else if (mIndex == 6)
        {
            mIndex = 8;
        }
        else if (mIndex == 7)
        {
            mIndex = 9;
        }
        else if (mIndex == 8)
        {
            mIndex = 10;
        }
        else
        {
            return;
        }
        if (mCurrentObject->GetName() == "Sound_off")
        {
            mCurrentObject = mObjects.at(mIndex);
        }
        else
        {
            mCurrentObject = mObjects.at(mIndex);
        }
    }

    for (int i = 0; i < mObjects.size(); ++i)
    {
        if (mObjects.at(i) != mCurrentObject)
        {
            if ( mObjects.at(i) == sound_100 
                || mObjects.at(i) == sound_75 || mObjects.at(i) == sound_50 || mObjects.at(i) == sound_25 ||
                mObjects.at(i) == sound_0)
            {
                mObjects.at(i)->SetScale(button1Scale);
            }
            else
            {
                mObjects.at(i)->SetScale(buttonScale);
            }
        }
    }

    if (Input::IsKeyTriggered(SDL_SCANCODE_SPACE) || Input::IsKeyTriggered(SDL_SCANCODE_RETURN))
    {
        FMOD_MANAGER->PlaySoundEffectSound("sound/sfx_button_click.mp3");
        //*option, *full_screen, *full_on, *full_off, *sound, *sound_on, *sound_off, *option_back
        if (mCurrentObject == full_on)
        {
            APPLICATION->ToggleFullscreen(SDL_WINDOW_FULLSCREEN);
        }
        else if (mCurrentObject == full_off)
        {
            APPLICATION->ToggleFullscreen(0);
        }
        else if (mCurrentObject == sound_100)
        {
            FMOD_MANAGER->SetVolumeAllGameSound(1.00);
        }
        else if (mCurrentObject == sound_75)
        {
            FMOD_MANAGER->SetVolumeAllGameSound(0.75);
        }
        else if (mCurrentObject == sound_50)
        {
            FMOD_MANAGER->SetVolumeAllGameSound(0.50);
        }
        else if (mCurrentObject == sound_25)
        {
            FMOD_MANAGER->SetVolumeAllGameSound(0.25);
        }
        else if (mCurrentObject == sound_0)
        {
            FMOD_MANAGER->SetVolumeAllGameSound(0.00);
        }
        else if (mCurrentObject == option_back)
        {
            if (STATE_MANAGER->IsPaused())
            {
                STATE_MANAGER->SetNextState(eStateType::PAUSE);
            }
            else
            {
                STATE_MANAGER->SetNextState(eStateType::MENU);
            }
            return;
        }
        else if (mCurrentObject == data_reset)
        {
            STATE_MANAGER->CreateDataResetMessage();
        }
        else if (mCurrentObject == option_eng)
        {
            mShouldUseEnglish = true;
        }
        else if (mCurrentObject == option_kora)
        {
            mShouldUseEnglish = false;
        }
    }
}

void Option::ClearAll()
{
    if (STATE_MANAGER->IsPaused())
    {
        OBJECT_MANAGER->ClearPauseObjects();
    }
    else
    {
        OBJECT_MANAGER->ClearObjects();
    }
    mObjects.clear();
}

bool Option::GetShouldUseEnglish(void)
{
    return mShouldUseEnglish;
}
