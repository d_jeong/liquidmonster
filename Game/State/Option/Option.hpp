/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : Credit.hpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Inyeong Han
Secondary :
- End Header ----------------------------------------------------------------*/

#pragma once
#include "../Game/State/State.hpp"
#include <vector>

class Object;

class Option : public State
{
public:
    void Initialize() override;
    void Update(float dt) override;
    void ClearAll() override;

    bool GetShouldUseEnglish(void);
private:
    std::vector<Object*> mObjects;
    Object*              mCurrentObject;
    int mIndex = 0;
    bool mShouldUseEnglish = true;
};

extern Option* OPTION;