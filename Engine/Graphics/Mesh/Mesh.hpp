/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : Mesh.hpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Haewon Shon
Secondary :
- End Header ----------------------------------------------------------------*/

#pragma once
#include "../../MathLibrary/MathLibrary.hpp"
#include <vector>

enum class PointListPattern
{
    POINT_LIST_PATTERN_LINES,
    POINT_LIST_PATTERN_LINESTRIP,
    POINT_LIST_PATTERN_LINELOOP,
    POINT_LIST_PATTERN_TRIANGLES,
    POINT_LIST_TRIANGLESTRIP,
    POINT_LIST_TRIANGLEFAN
};

class Mesh
{
public:
    std::size_t    GetPointCount() const;
    Vector2<float> GetPoint(std::size_t index) const;
    Vector4<float> GetColor(std::size_t index = 0) const;
    Vector2<float> GetTextureCoordinate(std::size_t index) const;

    PointListPattern GetPointListPattern() const;
    void             SetPointListType(PointListPattern type);

    void AddPoint(Vector2<float> point) noexcept;
    void AddColor(Vector4<float> color) noexcept;
    void AddTextureCoordinate(Vector2<float> texture_coordinate) noexcept;

    void ClearPoints() noexcept;
    void ClearColors() noexcept;
    void ClearTextureCoordinates() noexcept;
    void Clear() noexcept;

private:
    std::vector<Vector2<float>> mPoints;
    std::vector<Vector4<float>> mColors;
    std::vector<Vector2<float>> mTextureCoordinates;
    PointListPattern            mPointListType = PointListPattern::POINT_LIST_PATTERN_LINES;
};

namespace MESH
{
    Mesh CreateEllipse(float width, float height, Vector4<float> color, size_t pointNum, bool isFilled);
    Mesh CreateQuad(Vector2<float> first, Vector2<float> second, Vector2<float> third, Vector2<float> fourth, Vector4<float> color, bool isFilled);
    Mesh CreateRectangle(float width, float height, Vector4<float> color, bool isFilled);
    Mesh CreateTriangle(Vector2<float> first, Vector2<float> second, Vector2<float> third, Vector4<float> color, bool isFilled);
    Mesh CreateLine(Vector2<float> first, Vector2<float> second, Vector4<float> color);
}