/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : Mesh.cpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Haewon Shon
Secondary :
- End Header ----------------------------------------------------------------*/

#include "Mesh.hpp"
#include <cmath> // std::sin, std::cos

std::size_t Mesh::GetPointCount() const
{
    return mPoints.size();
}

Vector2<float> Mesh::GetPoint(std::size_t index) const
{
    return mPoints[index];
}

Vector4<float> Mesh::GetColor(std::size_t index) const
{
    if (index >= mColors.size())
    {
        return mColors.back();
    }
    return mColors[index];
}

Vector2<float> Mesh::GetTextureCoordinate(std::size_t index) const
{
    if (mTextureCoordinates.empty())
    {
        return {0.f, 0.f};
    }
    if (index >= mTextureCoordinates.size())
    {
        return mTextureCoordinates.back();
    }
    return mTextureCoordinates[index];
}

PointListPattern Mesh::GetPointListPattern() const
{
    return mPointListType;
}

void Mesh::SetPointListType(PointListPattern type)
{
    mPointListType = type;
}

void Mesh::AddPoint(Vector2<float> point) noexcept
{
    mPoints.push_back(point);
}

void Mesh::AddColor(Vector4<float> color) noexcept
{
    mColors.push_back(color);
}

void Mesh::AddTextureCoordinate(Vector2<float> texture_coordinate) noexcept
{
    mTextureCoordinates.push_back(texture_coordinate);
}

void Mesh::ClearPoints() noexcept
{
    mPoints.clear();
}

void Mesh::ClearColors() noexcept
{
    mColors.clear();
}

void Mesh::ClearTextureCoordinates() noexcept
{
    mTextureCoordinates.clear();
}

void Mesh::Clear() noexcept
{
    ClearPoints();
    ClearColors();
    ClearTextureCoordinates();
}

namespace MESH
{
    Mesh CreateEllipse(float width, float height, Vector4<float> color, size_t pointNum, bool isFilled)
    {
        Mesh ellipseMesh;

        float angle     = 0.f;
        float angleTerm = TWO_PI / pointNum;

        float halfWidth  = width / 2.f;
        float halfHeight = height / 2.f;

        if (isFilled)
        {
            ellipseMesh.AddPoint({0.f, 0.f});
            ellipseMesh.SetPointListType(PointListPattern::POINT_LIST_TRIANGLEFAN);
        }
        else
        {
            ellipseMesh.SetPointListType(PointListPattern::POINT_LIST_PATTERN_LINELOOP);
        }

        while (angle <= TWO_PI)
        {
            ellipseMesh.AddPoint({halfWidth * std::cos(angle), halfHeight * std::sin(angle)});
        }

        ellipseMesh.AddColor(color);
        return ellipseMesh;
    }
    Mesh CreateQuad(Vector2<float> first, Vector2<float> second, Vector2<float> third, Vector2<float> fourth, Vector4<float> color, bool isFilled)
    {
        Mesh quadMesh;

        if (isFilled)
        {
            quadMesh.AddPoint({0.f, 0.f});
            quadMesh.SetPointListType(PointListPattern::POINT_LIST_TRIANGLEFAN);
        }
        else
        {
            quadMesh.SetPointListType(PointListPattern::POINT_LIST_PATTERN_LINELOOP);
        }

        quadMesh.AddPoint(first);
        quadMesh.AddPoint(second);
        quadMesh.AddPoint(third);
        quadMesh.AddPoint(fourth);

        quadMesh.AddColor(color);
        return quadMesh;
    }
    Mesh CreateRectangle(float width, float height, Vector4<float> color, bool isFilled)
    {
        Mesh rectangleMesh = CreateQuad({width / 2.f, height / 2.f}, {-width / 2.f, height / 2.f}, {-width / 2.f, -height / 2.f}, {width / 2.f, -height / 2.f}, color, isFilled);
        return rectangleMesh;
    }
    Mesh CreateTriangle(Vector2<float> first, Vector2<float> second, Vector2<float> third, Vector4<float> color, bool isFilled)
    {
        Mesh triangleMesh;

        if (isFilled)
        {
            triangleMesh.AddPoint({0.f, 0.f});
            triangleMesh.SetPointListType(PointListPattern::POINT_LIST_TRIANGLEFAN);
        }
        else
        {
            triangleMesh.SetPointListType(PointListPattern::POINT_LIST_PATTERN_LINELOOP);
        }

        triangleMesh.AddPoint(first);
        triangleMesh.AddPoint(second);
        triangleMesh.AddPoint(third);
        triangleMesh.AddColor(color);
        return triangleMesh;
    }
    Mesh CreateLine(Vector2<float> first, Vector2<float> second, Vector4<float> color)
    {
        Mesh lineMesh;

        lineMesh.AddPoint(first);
        lineMesh.AddPoint(second);
        lineMesh.AddColor(color);
        lineMesh.SetPointListType(PointListPattern::POINT_LIST_PATTERN_LINES);
        return lineMesh;
    }

}