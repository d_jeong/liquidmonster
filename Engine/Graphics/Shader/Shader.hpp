/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : Shader.hpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Haewon Shon
Secondary :
- End Header ----------------------------------------------------------------*/

#pragma once
#include "../../MathLibrary/MathLibrary.hpp"
#include <glew.h>

class Shader
{
public:
    enum eShaderType
    {
        SHADER_TYPE_COMMON,
        SHADER_TYPE_PARTICLE,
        SHADER_TYPE_BACKGROUND,
        SHADER_TYPE_TRANSITION,
        SHADER_COUNT
    };

    Shader(eShaderType shaderType);
    ~Shader();
    void EnableShader();
    void DisableShader();

    void SetUniformFloat(const GLchar *variableName, float value);
    void SetUniformInteger(const GLchar *variableName, int value);
    void SetUniformVec2(const GLchar *variableName, Vector2<float> &vector);
    void SetUniformVec3(const GLchar *variableName, Vector3<float> &vector);
    void SetUniformVec4(const GLchar *variableName, Vector4<float> &vector);
    void SetUniformMatrix3(const GLchar *variableName, Matrix3x3<float> &matrix);

private:
    GLuint mShaderID;
    GLuint Init(const char **vertex, const char **fragment);
    GLint  GetUniformLocation(const GLchar *variableName);
};
