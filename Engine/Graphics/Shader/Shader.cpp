/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : Shader.cpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Haewon Shon
Secondary :
- End Header ----------------------------------------------------------------*/

#include "Shader.hpp"
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

namespace
{
    const char *vertexInfo = "#version 330 core\n"
                             "layout(location = 0) in vec2 in_position;\n"
                             "layout(location = 1) in vec2 in_texCoord;\n"

                             "uniform mat3 CameraMatrix = mat3(1.0);\n"
                             "uniform mat3 TRSMatrix = mat3(1.0);\n"
                             "uniform mat3 NDCMatrix = mat3(1.0);\n"

                             "out vec2 texCoord_to_frag;\n"

                             "void main()\n"
                             "{\n"
                             "	  vec3 pos = NDCMatrix * CameraMatrix * TRSMatrix * vec3(in_position, 1.0);\n"
                             "    gl_Position = vec4(pos.xy, 0.0, 1.0); \n"
                             "    texCoord_to_frag = in_texCoord;\n"
                             "}\n";

    const char *fragmentInfo = "#version 330 core\n"
                               "layout(location = 0) out vec4 out_color;\n"

                               "in vec2 texCoord_to_frag;\n"

                               "uniform sampler2D textures;\n"
                               "uniform float alpha = 1.0;\n"

                               "uniform float totalFrameNum = 1.0;\n"
                               "uniform float currentFrameNum = 1.0;\n"
                               "uniform int isFlipped = 0;\n"

                               "void main()\n"
                               "{\n"
                               "    float texCoordOffset = currentFrameNum / totalFrameNum;\n"
                               "    vec2 new_texCoord = vec2((texCoord_to_frag.x / totalFrameNum) + texCoordOffset, texCoord_to_frag.y);\n"
                               "    if(isFlipped == 1)\n"
                               "    {\n"
                               "        new_texCoord.x = ((1.0 - texCoord_to_frag.x) / totalFrameNum) + texCoordOffset;\n"
                               "    }\n"
                               "    vec4 color = vec4(1.0, 1.0, 1.0, alpha);\n"
                               "    vec4 tex_color;\n"
                               "    tex_color = texture(textures, new_texCoord);\n"

                               "    if(tex_color.w * color.w < 0.1)\n"
                               "       discard;\n"

                               "    out_color = tex_color * color;\n"
                               "}\n";

    const char *particleVertexInfo = "#version 330 core\n"
                                     "layout(location = 0) in vec2 in_vertex;\n"
                                     "layout(location = 1) in vec2 in_texCoord;\n"

                                     "out vec2 texCoord_to_frag;\n"
                                     "out vec4 color_to_frag;\n"

                                     "uniform mat3 CameraMatrix = mat3(1.0);\n"
                                     "uniform mat3 NDCMatrix = mat3(1.0);\n"

                                     "uniform float scale;\n"
                                     "uniform vec2 offset;\n"
                                     "uniform vec4 color;\n"

                                     "void main()\n"
                                     "{\n"
                                     "	texCoord_to_frag = in_texCoord;\n"
                                     "	color_to_frag = color;\n"
                                     "	vec3 pos = NDCMatrix * CameraMatrix * vec3((in_vertex * scale) + offset, 1.0);\n"
                                     "	gl_Position = vec4(pos.xy, 0.0, 1.0);\n"
                                     "}\n";

    const char *particleFragmentInfo = "#version 330 core\n"
                                       "in vec2 texCoord_to_frag;\n"
                                       "in vec4 color_to_frag;\n"
                                       "out vec4 color;\n"

                                       "uniform sampler2D sprite;\n"

                                       "void main()\n"
                                       "{\n"
                                       "	vec4 tex_color = texture(sprite, texCoord_to_frag) * color_to_frag;\n"
                                       "	if(tex_color.w < 0.1) discard;\n"
                                       "	color = tex_color;\n"
                                       "}\n";

    const char *backgroundVertexInfo = "#version 330 core\n"
                                       "layout(location = 0) in vec2 in_position;\n"
                                       "layout(location = 1) in vec2 in_texCoord;\n"

                                       "uniform float offset;\n"
                                       "uniform float texCoordMultiplier;\n"

                                       "out vec2 texCoord_to_frag;\n"

                                       "void main()\n"
                                       "{\n"
                                       "    gl_Position = vec4(in_position * 2.0, 0.0, 1.0); \n"
                                       "    texCoord_to_frag = vec2(in_texCoord.x * texCoordMultiplier + offset, in_texCoord.y);\n"
                                       "}\n";

    const char *backgroundFragmentInfo = "#version 330 core\n"
                                         "layout(location = 0) out vec4 out_color;\n"

                                         "in vec2 texCoord_to_frag;\n"

                                         "uniform sampler2D textures;\n"

                                         "void main()\n"
                                         "{\n"
                                         "    vec4 tex_color;\n"
                                         "    tex_color = texture(textures, texCoord_to_frag);\n"
                                         "    out_color = tex_color;"
                                         "}\n";
    const char *transitionVertexInfo = "#version 330 core\n"
                                       "layout(location = 0) in vec2 in_position;\n"
                                       "layout(location = 1) in vec2 in_texCoord;\n"

                                       "out vec2 pos_to_frag;\n"

                                       "void main()\n"
                                       "{\n"
                                       "    gl_Position = vec4(in_position * 2.0, 0.0, 1.0); \n"
                                       "    pos_to_frag = in_position * 2.0;\n"
                                       "}\n";
    const char *transitionFragmentInfo = "#version 330 core\n"
                                         "layout(location = 0) out vec4 out_color;\n"

                                         "in vec2 pos_to_frag;\n"

                                         "uniform float time;\n"

                                         "void main()\n"
                                         "{\n"
                                         "    if(pos_to_frag.x + 1.0 < time * 4.0)\n"
                                         "          discard;\n"
                                         "    out_color = vec4(0.0, 0.0, 0.0, 1.0);\n"
                                         "}\n";
}

Shader::Shader(eShaderType shaderType)
{
    if (shaderType == SHADER_TYPE_COMMON)
    {
        mShaderID = Init(&vertexInfo, &fragmentInfo);
    }
    else if (shaderType == SHADER_TYPE_PARTICLE)
    {
        mShaderID = Init(&particleVertexInfo, &particleFragmentInfo);
    }
    else if (shaderType == SHADER_TYPE_BACKGROUND)
    {
        mShaderID = Init(&backgroundVertexInfo, &backgroundFragmentInfo);
    }
    else if (shaderType == SHADER_TYPE_TRANSITION)
    {
        mShaderID = Init(&transitionVertexInfo, &transitionFragmentInfo);
    }
    else
    {
        mShaderID = -1;
        std::cout << "[ERROR] failed to init shader" << std::endl;
    }
}

Shader::~Shader()
{
    glDeleteProgram(mShaderID);
}
void Shader::EnableShader()
{
    glUseProgram(mShaderID);
}

void Shader::DisableShader()
{
    glUseProgram(0);
}

void Shader::SetUniformFloat(const GLchar *variableName, float value)

{
    glUniform1f(GetUniformLocation(variableName), value);
}

void Shader::SetUniformInteger(const GLchar *variableName, int value)
{
    glUniform1i(GetUniformLocation(variableName), value);
}

void Shader::SetUniformVec2(const GLchar *variableName, Vector2<float> &vector)
{
    glUniform2f(GetUniformLocation(variableName), vector.x, vector.y);
}

void Shader::SetUniformVec3(const GLchar *variableName, Vector3<float> &vector)
{
    glUniform3f(GetUniformLocation(variableName), vector.x, vector.y, vector.z);
}

void Shader::SetUniformVec4(const GLchar *variableName, Vector4<float> &vector)
{
    glUniform4f(GetUniformLocation(variableName), vector.x, vector.y, vector.z, vector.w);
}

void Shader::SetUniformMatrix3(const GLchar *variableName, Matrix3x3<float> &matrix)
{
    glUniformMatrix3fv(GetUniformLocation(variableName), 1, GL_FALSE, &matrix.elements[0][0]);
}

GLuint Shader::Init(const char **vertex, const char **fragment)
{
    GLuint program = glCreateProgram();
    GLuint vShader = glCreateShader(GL_VERTEX_SHADER);
    GLuint fShader = glCreateShader(GL_FRAGMENT_SHADER);

    GLint compileResult;

    // compile vertex shader
    glShaderSource(vShader, 1, vertex, NULL);
    glCompileShader(vShader);
    glGetShaderiv(vShader, GL_COMPILE_STATUS, &compileResult);
    if (compileResult == GL_FALSE)
    {
        GLint LogLength;
        glGetShaderiv(vShader, GL_INFO_LOG_LENGTH, &LogLength);
        std::vector<char> LogError(LogLength);
        glGetShaderInfoLog(vShader, LogLength, &LogLength, &LogError[0]);
        std::cout << "[ERROR] VERTEX SHADER COMPILE FAILED" << std::endl;
        std::cout << &LogError[0] << std::endl;
        glDeleteShader(vShader);
        return 0;
    }

    // compile fragment shader
    glShaderSource(fShader, 1, fragment, NULL);
    glCompileShader(fShader);
    glGetShaderiv(fShader, GL_COMPILE_STATUS, &compileResult);
    if (compileResult == GL_FALSE)
    {
        GLint LogLength;
        glGetShaderiv(fShader, GL_INFO_LOG_LENGTH, &LogLength);
        std::vector<char> LogError(LogLength);
        glGetShaderInfoLog(fShader, LogLength, &LogLength, &LogError[0]);
        std::cout << "[ERROR] FRAGMENT SHADER COMPILE FAILED" << std::endl;
        std::cout << &LogError[0] << std::endl;
        glDeleteShader(fShader);
        return 0;
    }

    glAttachShader(program, vShader);
    glAttachShader(program, fShader);
    glLinkProgram(program);
    glValidateProgram(program);
    glDeleteShader(vShader);
    glDeleteShader(fShader);
    std::cout << "SUCCESSFULLY COMPILE SHADER" << std::endl;

    return program;
}

GLint Shader::GetUniformLocation(const GLchar *variableName)
{
    return glGetUniformLocation(mShaderID, variableName);
}
