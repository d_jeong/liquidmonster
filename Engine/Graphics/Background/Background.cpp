/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : Background.hpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Haewon Shon
Secondary :
- End Header ----------------------------------------------------------------*/

#include "Background.hpp"
#include "../../Component/Sprite/Sprite.hpp"

Background::Background()
{
    mSprite = new Sprite;
}

Background::~Background()
{
    if(mSprite)
        delete mSprite;
}

Sprite* Background::GetSprite()
{
    return mSprite;
}

float Background::GetDepth(void) const
{
    return mDepth;
}

void Background::SetDepth(float depth)
{
    mDepth = depth;
}

float Background::GetSpeed(void) const
{
    return mSpeed;
}

void Background::SetSpeed(float speed)
{
    mSpeed = speed;
}