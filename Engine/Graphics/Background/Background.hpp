/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : Background.hpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Haewon Shon
Secondary :
- End Header ----------------------------------------------------------------*/
#pragma once

class Sprite;

class Background
{
public:
    Background();
    ~Background();

    Sprite* GetSprite();

    float GetDepth(void) const;
    void SetDepth(float depth);

    // Get/Set speed for parallaxed background
    float GetSpeed(void) const;
    void SetSpeed(float speed);

private:
    Sprite* mSprite;
    float   mDepth;
    float   mSpeed;
};