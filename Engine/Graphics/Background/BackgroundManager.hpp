/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : BackgroundManager.hpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Haewon Shon
Secondary :
- End Header ----------------------------------------------------------------*/
#pragma once

#include <vector>
#include "Background.hpp"

class BackgroundManager
{
public:
    enum eBackground
    {
        BACKGROUND_NONE,
        BACKGROUND_MENU,
        BACKGROUND_FACTORY,
        BACKGROUND_CHAPTER2,
        BACKGROUND_CHAPTER3,
    };
    BackgroundManager();
    ~BackgroundManager();
    void SetCurrentBackground(enum eBackground background);
    std::vector<Background*> GetBackgrounds(void) const;
private:
    std::vector<Background*> mBackgrounds;
    enum eBackground         mCurrentBackground;
};