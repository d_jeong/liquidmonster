/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : BackgroundManager.cpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Haewon Shon
Secondary :
- End Header ----------------------------------------------------------------*/

#include "BackgroundManager.hpp"
#include "../../Component/Sprite/Sprite.hpp"
#include "../../Data/Data.hpp"
#include "../Engine/FMOD/FmodManager.hpp"

BackgroundManager::BackgroundManager()
{
    mCurrentBackground = eBackground::BACKGROUND_NONE;
}

BackgroundManager::~BackgroundManager()
{
    for (Background *background : mBackgrounds)
    {
        delete background;
    }
    mBackgrounds.clear();
}

void BackgroundManager::SetCurrentBackground(enum eBackground background)
{
    if (mCurrentBackground != background)
    {
        mCurrentBackground = background;

        switch (mCurrentBackground)
        {
            case BACKGROUND_NONE:
                mBackgrounds.clear();
                break;
            case BACKGROUND_MENU:
            {
                Background *background = new Background;
                background->GetSprite()->LoadTextureFromData(eTexture::BACKGROUND_DAY_BACKGROUND);
                background->SetDepth(2.f);
                background->SetSpeed(0.f);

                Background *background_back_machine = new Background;
                background_back_machine->GetSprite()->LoadTextureFromData(eTexture::BACKGROUND_DAY_BUILDING_BACK);
                background_back_machine->SetDepth(2.f);
                background_back_machine->SetSpeed(0.1f);

                Background *background_front_machine = new Background;
                background_front_machine->GetSprite()->LoadTextureFromData(eTexture::BACKGROUND_DAY_BUILDING_FRONT);
                background_front_machine->SetDepth(2.f);
                background_front_machine->SetSpeed(0.15f);

                Background *background_light = new Background;
                background_light->GetSprite()->LoadTextureFromData(eTexture::BACKGROUND_DAY_LIGHT);
                background_light->SetDepth(2.f);
                background_light->SetSpeed(0.f);

                mBackgrounds.push_back(background);
                mBackgrounds.push_back(background_back_machine);
                mBackgrounds.push_back(background_front_machine);
                mBackgrounds.push_back(background_light);

                break;
            }
            case BACKGROUND_FACTORY:
            {
                Background *background = new Background;
                background->GetSprite()->LoadTextureFromData(eTexture::BACKGROUND_DAY_BACKGROUND);
                background->SetDepth(2.f);
                background->SetSpeed(0.f);

                Background *background_back_machine = new Background;
                background_back_machine->GetSprite()->LoadTextureFromData(eTexture::BACKGROUND_DAY_BUILDING_BACK);
                background_back_machine->SetDepth(2.f);
                background_back_machine->SetSpeed(0.1f);

                Background *background_front_machine = new Background;
                background_front_machine->GetSprite()->LoadTextureFromData(eTexture::BACKGROUND_DAY_BUILDING_FRONT);
                background_front_machine->SetDepth(2.f);
                background_front_machine->SetSpeed(0.15f);

                Background *background_light = new Background;
                background_light->GetSprite()->LoadTextureFromData(eTexture::BACKGROUND_DAY_LIGHT);
                background_light->SetDepth(2.f);
                background_light->SetSpeed(0.f);

                Background *background_window = new Background;
                background_window->GetSprite()->LoadTextureFromData(eTexture::BACKGROUND_WINDOW);
                background_window->SetDepth(2.f);
                background_window->SetSpeed(0.2f);

                Background *background_convey = new Background;
                background_convey->GetSprite()->LoadTextureFromData(eTexture::BACKGROUND_CONVEY);
                background_convey->SetDepth(2.f);
                background_convey->SetSpeed(0.3f);

                Background *background_machine = new Background;
                background_machine->GetSprite()->LoadTextureFromData(eTexture::BACKGROUND_MACHINE);
                background_machine->SetDepth(2.f);
                background_machine->SetSpeed(0.3f);

                mBackgrounds.push_back(background);
                mBackgrounds.push_back(background_back_machine);
                mBackgrounds.push_back(background_front_machine);
                mBackgrounds.push_back(background_light);
                mBackgrounds.push_back(background_window);
                mBackgrounds.push_back(background_convey);
                mBackgrounds.push_back(background_machine);

                break;
            }
            case BACKGROUND_CHAPTER2:
            {
                Background *background = new Background;
                background->GetSprite()->LoadTextureFromData(eTexture::BACKGROUND_SUNSET_BACKGROUND);
                background->SetDepth(2.f);
                background->SetSpeed(0.f);

                Background *background_back_machine = new Background;
                background_back_machine->GetSprite()->LoadTextureFromData(eTexture::BACKGROUND_SUNSET_BUILDING_BACK);
                background_back_machine->SetDepth(2.f);
                background_back_machine->SetSpeed(0.1f);

                Background *background_front_machine = new Background;
                background_front_machine->GetSprite()->LoadTextureFromData(eTexture::BACKGROUND_SUNSET_BUILDING_FRONT);
                background_front_machine->SetDepth(2.f);
                background_front_machine->SetSpeed(0.15f);

                Background *background_light = new Background;
                background_light->GetSprite()->LoadTextureFromData(eTexture::BACKGROUND_SUNSET_LIGHT);
                background_light->SetDepth(2.f);
                background_light->SetSpeed(0.f);

                Background *background_window = new Background;
                background_window->GetSprite()->LoadTextureFromData(eTexture::BACKGROUND_WINDOW);
                background_window->SetDepth(2.f);
                background_window->SetSpeed(0.2f);

                Background *background_convey = new Background;
                background_convey->GetSprite()->LoadTextureFromData(eTexture::BACKGROUND_CONVEY);
                background_convey->SetDepth(2.f);
                background_convey->SetSpeed(0.3f);

                Background *background_machine = new Background;
                background_machine->GetSprite()->LoadTextureFromData(eTexture::BACKGROUND_MACHINE);
                background_machine->SetDepth(2.f);
                background_machine->SetSpeed(0.3f);

                mBackgrounds.push_back(background);
                mBackgrounds.push_back(background_back_machine);
                mBackgrounds.push_back(background_front_machine);
                mBackgrounds.push_back(background_light);
                mBackgrounds.push_back(background_window);
                mBackgrounds.push_back(background_convey);
                mBackgrounds.push_back(background_machine);
            }
            break;
            case BACKGROUND_CHAPTER3:
            {
                Background *background = new Background;
                background->GetSprite()->LoadTextureFromData(eTexture::BACKGROUND_NIGHT_BACKGROUND);
                background->SetDepth(2.f);
                background->SetSpeed(0.f);

                Background *background_light = new Background;
                background_light->GetSprite()->LoadTextureFromData(eTexture::BACKGROUND_NIGHT_LIGHT);
                background_light->SetDepth(2.f);
                background_light->SetSpeed(0.15f);

                Background *background_back_machine = new Background;
                background_back_machine->GetSprite()->LoadTextureFromData(eTexture::BACKGROUND_NIGHT_BUILDING_BACK);
                background_back_machine->SetDepth(2.f);
                background_back_machine->SetSpeed(0.1f);

                Background *background_front_machine = new Background;
                background_front_machine->GetSprite()->LoadTextureFromData(eTexture::BACKGROUND_NIGHT_BUILDING_FRONT);
                background_front_machine->SetDepth(2.f);
                background_front_machine->SetSpeed(0.15f);

                Background *background_window = new Background;
                background_window->GetSprite()->LoadTextureFromData(eTexture::BACKGROUND_WINDOW);
                background_window->SetDepth(2.f);
                background_window->SetSpeed(0.2f);

                Background *background_convey = new Background;
                background_convey->GetSprite()->LoadTextureFromData(eTexture::BACKGROUND_CONVEY);
                background_convey->SetDepth(2.f);
                background_convey->SetSpeed(0.3f);

                Background *background_machine = new Background;
                background_machine->GetSprite()->LoadTextureFromData(eTexture::BACKGROUND_MACHINE);
                background_machine->SetDepth(2.f);
                background_machine->SetSpeed(0.3f);

                mBackgrounds.push_back(background);
                mBackgrounds.push_back(background_back_machine);
                mBackgrounds.push_back(background_front_machine);
                mBackgrounds.push_back(background_light);
                mBackgrounds.push_back(background_window);
                mBackgrounds.push_back(background_convey);
                mBackgrounds.push_back(background_machine);
            }
            break;
        }
    }
}

std::vector<Background *> BackgroundManager::GetBackgrounds(void) const
{
    return mBackgrounds;
}
