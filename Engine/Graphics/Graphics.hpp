/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : Graphics.hpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Haewon Shon
Secondary :
- End Header ----------------------------------------------------------------*/
#pragma once

#include <map>
#include <string>
#include <glew.h>
#include "../System.hpp"
#include "../MathLibrary/MathLibrary.hpp"
#include "Background/BackgroundManager.hpp"

class Object;
class Sprite;
class ParticleEmitter;
class Shader;
class Camera;
class Data;

class Graphics : public System
{
public:
    Graphics();
    ~Graphics() override;
    void Initialize() override;
    void Update(float dt) override;

    void AddSprite(Object *object);
    void DeleteSprite(Object *object);
    void ReplaceSpriteWithNewDepth(Object *object, float newDepth);

    void AddUISprite(Object *object);
    void DeleteUISprite(Object *object);

    Data *GetTextureFromData();

    void SetBoxDrawing(bool isTurn);
    bool GetBoxDrawing();

    void SetTileBoxDrawing(bool isTurn);
    bool GetTileBoxDrawing();

    void SetLevelWidthAndHeight(Vector2<int>);
    Vector2<int> GetLevelWidthAndHeight(void);

    void SetBackground(BackgroundManager::eBackground background);
    BackgroundManager::eBackground GetBackground(void);

    void AddParticleEmitter(ParticleEmitter *emitter);
    void ClearParticleEmitters(void);

    Matrix3x3<float> GetCameraMatrix(void) const;
    Matrix3x3<float> GetInverseCameraMatrix(void) const;

    void DataLoadingUpdate();
    bool IsDataLoadDone();
    int  GetLoadingProcess();

    bool GetIsZoomAvailable(void);
    void SetIsZoomAvailable(bool isActive);

    Camera *GetCamera(void);

    void SetSkip(bool set);

    void TurnOnTransition(void);
    bool IsTransitioning();

private:
    void InitializeSpriteBuffer();

    void DrawObjects() const;
    void DrawPauseObjects() const;
    void DrawTileMap() const;
    void DrawBackground() const;
    void DrawParticles(float dt) const;
    void DrawUIs() const;
    void DrawTransition() const;

    Vector4<float> mClearColor;

    Shader *mShader;
    Shader *mParticleShader;
    Shader *mBackgroundShader;
    Shader *mTransitionShader;

    GLuint mSpriteVAO;
    GLuint mSpriteVBO[2]; // position, texture coord

    std::vector<Object *>          mObjectsToDraw;
    std::vector<Object *>          mUIObjectsToDraw;
    std::vector<ParticleEmitter *> mParticleEmitters;

    void               InitializeCamera(void);
    Camera *           mCamera;
    BackgroundManager *mBackgroundManager;

    Data *mTextureData;
    bool  mIsBoxDrawing;
    bool  mIsTileBoxDrawing;

    Vector2<int> mLevelSize;
    BackgroundManager::eBackground mBackgroundType;
    bool mIsZoomAvailable = false;
    bool                           mShouldSkip;

    float mTransitionTimer;
    bool  mIsTransitioning;
    const float mTransitionTime = 0.5f;
};

extern Graphics *GRAPHICS;