/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : ParticleEmitter.cpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Haewon Shon
Secondary :
- End Header ----------------------------------------------------------------*/

#include "ParticleEmitter.hpp"
#include "../Texture/Texture.hpp"

ParticleEmitter::ParticleEmitter(float lifeTime, int particleNum, int respawnNum, const Vector2<float>& basePos, const Vector4<float>& baseColor, Texture* texture)
    : mGeneratorLifeTime(lifeTime), mParticleNum(particleNum), mBasePosition(basePos), mTexture(texture), mRespawnParticleNum(respawnNum)
{
    for (int i = 0; i < particleNum; ++i)
        mParticleVector.push_back(Particle());
}

ParticleEmitter::~ParticleEmitter()
{}

std::vector<ParticleEmitter::Particle> ParticleEmitter::GetParticleVector(void)
{
    return mParticleVector;
}

Texture* ParticleEmitter::GetTexture() const
{
    return mTexture;
}

int ParticleEmitter::FindFirstUnusedParticle()
{
    // Search from last used particle
    for (int i = lastUsedParticle; i < mParticleNum; ++i)
    {
        if (mParticleVector[i].life <= 0.0f)
        {
            lastUsedParticle = i;
            return i;
        }
    }
    // if fails to find in first search, search from begin of the storage
    for (int i = 0; i < lastUsedParticle; ++i)
    {
        if (mParticleVector[i].life <= 0.0f)
        {
            lastUsedParticle = i;
            return i;
        }
    }
    // if fails, override first particle
    lastUsedParticle = 0;
    return 0;
}