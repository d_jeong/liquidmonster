/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : ParticleEmitter.hpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Haewon Shon
Secondary :
- End Header ----------------------------------------------------------------*/

#pragma once

#include "../../MathLibrary/Vector2.hpp"
#include "../../MathLibrary/Vector4.hpp"
#include <vector>

class Texture;

class ParticleEmitter
{
public:
    struct Particle
    {
        Vector2<float> position;
        Vector2<float> velocity;
        Vector4<float> color;
        float          life;
        float          scale;
        float          floorHeight;
    };

    ParticleEmitter(float lifeTime, int particleNum, int respawnNum, const Vector2<float>& basePos, const Vector4<float>& baseColor, Texture* texture);
    ~ParticleEmitter();

    virtual void          UpdateParticles(float dt) = 0;
    std::vector<Particle> GetParticleVector(void);
    Texture*              GetTexture(void) const;

protected:
    virtual void RespawnParticle(Particle& particle) = 0;
    int          FindFirstUnusedParticle(void);
    int          lastUsedParticle = 0;

    // particle generator informaton
    float          mGeneratorLifeTime; /* -1 to infinite generation */
    int            mParticleNum;
    int            mRespawnParticleNum;
    Vector2<float> mBasePosition;

    std::vector<Particle> mParticleVector;
    Texture*              mTexture;
};