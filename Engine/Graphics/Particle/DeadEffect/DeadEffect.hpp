/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : DeadEffect.hpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Haewon Shon
Secondary :
- End Header ----------------------------------------------------------------*/
#pragma once

#include "../../Engine/Object/Object.hpp"
#include "../ParticleEmitter.hpp"

class DeadEffect : public ParticleEmitter
{
public:
    // Player info, texture
    DeadEffect(Object* pPlayer, Texture* texture);
    void UpdateParticles(float dt);
    void GenerateEffect(void);

private:
    void RespawnParticle(Particle& particle);

    Object* mPlayer;
    Vector4<float> mBaseColor;
};