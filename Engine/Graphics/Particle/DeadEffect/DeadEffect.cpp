/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : DeadEffect.cpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Haewon Shon
Secondary :
- End Header ----------------------------------------------------------------*/

#include "DeadEffect.hpp"
#include <iostream>

DeadEffect::DeadEffect(Object* pPlayer, Texture* texture) : ParticleEmitter(2.f, 10, 10, pPlayer->GetPosition(), Vector4{0.5f, 0.5f, 1.f, 0.7f}, texture)
{
    mPlayer    = pPlayer;
    mBaseColor = Vector4{1.f, 1.f, 1.f, 1.f};
}

void DeadEffect::UpdateParticles(float dt)
{
    // Update all particles
    for (int i = 0; i < mParticleNum; ++i)
    {
        Particle& p = mParticleVector[i];
        if (p.life > 0.0f)
        {
            // update alive particle
            p.position += p.velocity * dt;
            p.life -= dt; // reduce life of particle
            p.velocity.y -= 100.f * dt;
        }
    }
}

void DeadEffect::GenerateEffect(void)
{
    std::cout << "Dead effect called" << std::endl;
    // mFloorHeight = mPlayer->GetPosition().y - (mPlayer->GetScale().y / 2.f);
    for (int i = 0; i < mRespawnParticleNum; ++i)
    {
        int unusedParticle = FindFirstUnusedParticle();
        RespawnParticle(mParticleVector[unusedParticle]);
    }
}

void DeadEffect::RespawnParticle(Particle& particle)
{
    // player's feet
    float randX = static_cast<float>(rand() % static_cast<int>(mPlayer->GetScale().x) * 2.f) - mPlayer->GetScale().x;
    randX       = (randX >= 0.f) ? randX + 30.f : randX - 30.f;
    float randY = rand() % 30 + 25.f;

    particle.position = mPlayer->GetPosition();
    particle.velocity = {randX, randY};
    particle.life     = 4.f;
    particle.scale    = (float)(rand() % 3000) / 100.f + 10.f;
    particle.color    = mBaseColor;
}