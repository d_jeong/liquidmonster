/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : MoveEffect.cpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Haewon Shon
Secondary :
- End Header ----------------------------------------------------------------*/

#include "MoveEffect.hpp"
#include <iostream>

MoveEffect::MoveEffect(Object* pPlayer, Texture* texture) 
    : ParticleEmitter(2.f, 20, 5, pPlayer->GetPosition(), Vector4{0.5f, 0.5f, 1.f, 0.7f}, texture)
{
    mPlayer = pPlayer;
    mBaseColor = Vector4{1.f, 1.f, 1.f, 1.f};
}

void MoveEffect::UpdateParticles(float dt)
{
    mDelay -= dt;
    // Update all particles
    for (int i = 0; i < mParticleNum; ++i)
    {
        Particle& p = mParticleVector[i];
        if (p.life > 0.0f)
        {
            // update alive particle
            p.position += p.velocity * dt;
            p.life -= dt; // reduce life of particle
            p.velocity.y -= 60.f * dt;

            if (p.position.y < p.floorHeight)
            {
                p.position.y = p.floorHeight;
            }
        }
    }
}

void MoveEffect::GenerateEffect(void)
{
    if (mDelay < 0.f)
    {
        mDelay       = mEffectDelay;
        for (int i = 0; i < mRespawnParticleNum; ++i)
        {
            int unusedParticle = FindFirstUnusedParticle();
            RespawnParticle(mParticleVector[unusedParticle]);
        }
    }
}

void MoveEffect::RespawnParticle(Particle& particle)
{
    // player's feet
    float randX = static_cast<float>(rand() % static_cast<int>(mPlayer->GetScale().x)) - (mPlayer->GetScale().x / 2.f);
    float randY = rand() % 10 + 20.f;

    particle.position = mPlayer->GetPosition() - Vector2<float>{randX, mPlayer->GetScale().y / 2.f - 10.f};
    particle.velocity = {-randX / 2.f, randY};
    particle.life     = 2.f;
    particle.scale    = (float)(rand() % 300) / 100.f + 10.f;
    particle.color    = mBaseColor;
    particle.floorHeight = mPlayer->GetPosition().y - (mPlayer->GetScale().y / 2.f);
}