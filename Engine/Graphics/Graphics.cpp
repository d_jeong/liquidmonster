/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : Graphics.cpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Haewon Shon
Secondary :
- End Header ----------------------------------------------------------------*/

#include "Graphics.hpp"
#include "../Application/Application.hpp"
#include "../Camera/Camera.hpp"
#include "../Data/Data.hpp"
#include "../ImGui/ImGui_manager.hpp"
#include "../MathLibrary/Matrix3x3.hpp"
#include "../Object/Object.hpp"
#include "Particle/ParticleEmitter.hpp"
#include "Shader/Shader.hpp"

Graphics *GRAPHICS = nullptr;

namespace
{
    // from finders engine
    // Prepared Attributes
    GLfloat buffer_position_data[] = {
        0.5f, 0.5f, 0.5f, -0.5f, -0.5f, 0.5f, -0.5f, -0.5f,
    };

    GLfloat buffer_coordinate_data[] = // texture's coordinate!
        {
            1.f, 0.f, 1.f, 1.f, 0.f, 0.f, 0.f, 1.f,
    };

    GLfloat color_data[] = {1.f, 1.f, 1.f, 1.f, 1.f, 1.f, 1.f, 1.f, 1.f, 1.f, 1.f, 1.f, 1.f, 1.f, 1.f, 1.f};
}

void Graphics::Initialize()
{
    IMGUI = new ImGui_manager;

    GLint glewInitResult = glewInit();

    if (GLEW_OK != glewInitResult)
    {
        std::cout << "[ERROR]" << glewGetErrorString(glewInitResult) << std::endl;
        return;
    }
    mShader           = new Shader(Shader::eShaderType::SHADER_TYPE_COMMON);
    mParticleShader   = new Shader(Shader::eShaderType::SHADER_TYPE_PARTICLE);
    mBackgroundShader = new Shader(Shader::eShaderType::SHADER_TYPE_BACKGROUND);
    mTransitionShader = new Shader(Shader::eShaderType::SHADER_TYPE_TRANSITION);

    mClearColor = {1.f, 1.f, 1.f, 1.f};

    InitializeSpriteBuffer();

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    InitializeCamera();

    mTextureData = new Data;
    mTextureData->Initialize_Logos();
    mTextureData->Initialize_Texture();
    mIsBoxDrawing     = false;
    mIsTileBoxDrawing = false;

    mBackgroundManager = new BackgroundManager;

    mShouldSkip = false;
}

void Graphics::InitializeSpriteBuffer()
{
    glGenVertexArrays(1, &mSpriteVAO);
    glBindVertexArray(mSpriteVAO);

    // VBO position
    glGenBuffers(1, &mSpriteVBO[0]);
    glBindBuffer(GL_ARRAY_BUFFER, mSpriteVBO[0]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(buffer_position_data), buffer_position_data, GL_STATIC_DRAW);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, (void *)0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    // Texture Coordinate
    glGenBuffers(1, &mSpriteVBO[1]);
    glBindBuffer(GL_ARRAY_BUFFER, mSpriteVBO[1]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(buffer_coordinate_data), buffer_coordinate_data, GL_STATIC_DRAW);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(GLfloat) * 2, (void *)0);
    glEnableVertexAttribArray(1);

    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

Graphics::Graphics()
{
    GRAPHICS = this;
}

Graphics::~Graphics()
{
    GRAPHICS = nullptr;
#ifdef _DEBUG
    IMGUI->Delete();
#endif

    glDeleteBuffers(1, &mSpriteVAO);
    glDeleteBuffers(1, &mSpriteVBO[0]);
    glDeleteBuffers(1, &mSpriteVBO[1]);

    if (mShader)
        delete mShader;

    if (mParticleShader)
        delete mParticleShader;

    if (mBackgroundShader)
        delete mBackgroundShader;

    if (mTransitionShader)
        delete mTransitionShader;

    if (mCamera)
        delete mCamera;

    if (mTextureData)
        delete mTextureData;
}

void Graphics::Update(float dt)
{
    if (mShouldSkip == true)
    {
        mShouldSkip = false;
        return;
    }

    if (mIsZoomAvailable == false)
    {
        // Update camera
        mCamera->Update(dt);
    }
    else
    {
#ifdef _DEBUG
        {
            if (Input::IsKeyTriggered(SDL_SCANCODE_KP_7))
            {
                mCamera->ZoomHeight(0.3f);
                mCamera->ZoomWidth(0.3f);
            }

            if (Input::IsKeyTriggered(SDL_SCANCODE_KP_8))
            {
                mCamera->ZoomHeight(-0.3f);
                mCamera->ZoomWidth(-0.3f);
            }
        }
#endif // _DEBUG
    }

    // object's position, color, texture, size
    // Get current state's objects, set uniforms or inputs with position;
    glClear(GL_COLOR_BUFFER_BIT);
    glClearColor(mClearColor.x, mClearColor.y, mClearColor.z, mClearColor.w);

    float width, height;
    width  = 1280; //(float)APPLICATION->GetWidth();
    height = 720;  //(float)APPLICATION->GetHeight();

    Matrix3x3<float> camera = mCamera->GetCameraMatrix();
    Matrix3x3<float> ndc(2.f / width, 0.f, 0.f, 0.f, 2.f / height, 0.f, 0.f, 0.f, 1.f);

    glBindVertexArray(mSpriteVAO);

    DrawBackground();
    if (!STATE_MANAGER->IsPaused())
    {
        DrawTileMap();
        DrawObjects();
        DrawParticles(dt);
        DrawUIs();
    }
    else
    {
        DrawPauseObjects();
    }

    if (mIsTransitioning)
    {
        mTransitionTimer -= dt;
        if (mTransitionTimer < 0.f)
        {
            mIsTransitioning = false;
        }
        else
        { 
            DrawTransition();
        }
    }

    glBindVertexArray(0);
#ifdef _DEBUG
    if (IMGUI->IsLoadDone())
    {
        IMGUI->Render();
    }
#endif

    SDL_GL_SwapWindow(SDL_GL_GetCurrentWindow());
    SDL_Delay(1);
}

bool operator<(std::pair<std::string, Sprite *> a, std::pair<std::string, Sprite *> b)
{
    return a.second->GetOwner()->GetDepth() < b.second->GetOwner()->GetDepth();
}
void Graphics::AddSprite(Object *object)
{
    mObjectsToDraw.push_back(object);
    std::sort(begin(mObjectsToDraw), end(mObjectsToDraw), [](Object *obj1, Object *obj2) { return obj1->GetDepth() < obj2->GetDepth(); });
}

void Graphics::DeleteSprite(Object *object)
{
    for (auto it = begin(mObjectsToDraw); it != end(mObjectsToDraw); ++it)
    {
        if (*it == object)
        {
            mObjectsToDraw.erase(it);
            break;
        }
    }
}

void Graphics::ReplaceSpriteWithNewDepth(Object *object, float newDepth)
{
    object->SetDepth(newDepth);
    std::sort(begin(mObjectsToDraw), end(mObjectsToDraw), [](Object *obj1, Object *obj2) { return obj1->GetDepth() < obj2->GetDepth(); });
}

void Graphics::AddUISprite(Object *object)
{
    mUIObjectsToDraw.push_back(object);
}

void Graphics::DeleteUISprite(Object *object)
{
    for (auto it = begin(mUIObjectsToDraw); it != end(mUIObjectsToDraw); ++it)
    {
        if (*it == object)
        {
            mUIObjectsToDraw.erase(it);
            break;
        }
    }
}

Data *Graphics::GetTextureFromData()
{
    return mTextureData;
}

void Graphics::SetBoxDrawing(bool turn)
{
    mIsBoxDrawing = turn;
}

bool Graphics::GetBoxDrawing()
{
    return mIsBoxDrawing;
}

void Graphics::SetTileBoxDrawing(bool isTurn)
{
    mIsTileBoxDrawing = isTurn;
}

bool Graphics::GetTileBoxDrawing()
{
    return mIsTileBoxDrawing;
}

void Graphics::InitializeCamera(void)
{
    if (mCamera != nullptr)
    {
        delete mCamera;
    }
    mCamera = new Camera((float)APPLICATION->GetWidth(), (float)APPLICATION->GetHeight());
    mCamera->Initialize();
}

void Graphics::SetLevelWidthAndHeight(Vector2<int> levelSize)
{
    mLevelSize = levelSize;

    mCamera->SetLevelWidth(static_cast<float>(mLevelSize.x));
    mCamera->SetLevelHeight(static_cast<float>(mLevelSize.y));
}

Vector2<int> Graphics::GetLevelWidthAndHeight(void)
{
    return mLevelSize;
}

void Graphics::SetBackground(BackgroundManager::eBackground background)
{
    mBackgroundType = background;
    mBackgroundManager->SetCurrentBackground(background);
}

BackgroundManager::eBackground Graphics::GetBackground(void)
{
    return mBackgroundType;
}

void Graphics::AddParticleEmitter(ParticleEmitter *emitter)
{
    mParticleEmitters.push_back(emitter);
}

void Graphics::ClearParticleEmitters(void)
{
    for (ParticleEmitter *emitter : mParticleEmitters)
    {
        delete emitter;
    }
    mParticleEmitters.clear();
}

Matrix3x3<float> Graphics::GetCameraMatrix(void) const
{
    return mCamera->GetCameraMatrix();
}

Matrix3x3<float> Graphics::GetInverseCameraMatrix(void) const
{
    return mCamera->GetInverseCameraMatrix();
}

void Graphics::DataLoadingUpdate()
{
    mTextureData->UpdateTextureLoad();
}

bool Graphics::IsDataLoadDone()
{
    return mTextureData->IsDataLoadDone();
}

int Graphics::GetLoadingProcess()
{
    return mTextureData->GetLoadingProcess();
}

bool Graphics::GetIsZoomAvailable(void)
{
    return mIsZoomAvailable;
}

void Graphics::SetIsZoomAvailable(bool isActive)
{
    mIsZoomAvailable = isActive;
}

Camera *Graphics::GetCamera(void)
{
    return mCamera;
}

void Graphics::SetSkip(bool set)
{
    mShouldSkip = set;
}

void Graphics::TurnOnTransition(void)
{
    mIsTransitioning = true;
    mTransitionTimer = mTransitionTime * 2.f;
}

bool Graphics::IsTransitioning()
{
    return mIsTransitioning;
}

void Graphics::DrawObjects() const
{
    float width, height;
    width  = 1280; //(float)APPLICATION->GetWidth();
    height = 720;  //(float)APPLICATION->GetHeight();

    Matrix3x3<float> camera = mCamera->GetCameraMatrix();
    Matrix3x3<float> ndc(2.f / width, 0.f, 0.f, 0.f, 2.f / height, 0.f, 0.f, 0.f, 1.f);
    // Sprite
    {
        mShader->EnableShader();
        mShader->SetUniformMatrix3("CameraMatrix", camera);
        mShader->SetUniformMatrix3("NDCMatrix", ndc);

        for (auto obj : mObjectsToDraw)
        {
            if (obj->GetIsActive() == false)
                continue;

            Sprite *sprite = obj->GetSprite();

            if (sprite->GetIsVisible() == false || sprite->GetTexture() == nullptr)
                continue;

            glActiveTexture(GL_TEXTURE0);
            glBindTexture(GL_TEXTURE_2D, sprite->GetTexture()->GetTextureID());
            // obj.second.

            Matrix3x3<float> TRSmatrix = obj->GetTransform()->GetModelToWorldTransform();
            mShader->SetUniformMatrix3("TRSMatrix", TRSmatrix);

            Vector4<float> color = sprite->GetColor();
            mShader->SetUniformFloat("alpha", color.w);

            if (sprite->IsAnimation())
            {
                mShader->SetUniformFloat("totalFrameNum", static_cast<float>(sprite->GetAnimationFrameNum()));
                mShader->SetUniformFloat("currentFrameNum", static_cast<float>(sprite->GetCurrentFrame()));
            }
            else
            {
                mShader->SetUniformFloat("totalFrameNum", 1.f);
                mShader->SetUniformFloat("currentFrameNum", 1.f);
            }

            // for players, check flipped
            eObjectType objType = static_cast<eObjectType>(sprite->GetOwner()->GetObjectType());

            if (objType == eObjectType::PLAYER_1 || objType == eObjectType::PLAYER_2 || objType == eObjectType::PLAYER_COMBINED)
            {
                if (sprite->GetOwner()->GetComponentByType<Player>(eComponentType::PLAYER)->GetContollerDirection() == eControllerDirection::CONTROLLER_DIRECTION_LEFT)
                {
                    mShader->SetUniformInteger("isFlipped", 0);
                }
                else
                {
                    mShader->SetUniformInteger("isFlipped", 1);
                }
            }
            else
            {
                mShader->SetUniformInteger("isFlipped", 0);
            }
            glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
        }
    }
    // Debug Drawing
    {
        if (mIsBoxDrawing)
        {
            glBindTexture(GL_TEXTURE_2D, mTextureData->GetAllTexture()[eTexture::BOUNDARY]->GetTextureID());
            for (auto obj : mObjectsToDraw)
            {
                if (obj->GetIsActive() == false)
                    continue;

                // if object has collision component
                if (obj->GetComponentByType<Collision>(eComponentType::COLLISION) != nullptr)
                {
                    mShader->SetUniformFloat("totalFrameNum", 1.f);
                    mShader->SetUniformFloat("currentFrameNum", 1.f);
                    Matrix3x3<float> TRSmatrix = obj->GetTransform()->GetModelToWorldCollisionTransform();
                    mShader->SetUniformMatrix3("TRSMatrix", TRSmatrix);
                    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
                }

                if (obj->GetObjectType() == eObjectType::PRESSURE)
                {
                    mShader->SetUniformFloat("totalFrameNum", 1.f);
                    mShader->SetUniformFloat("currentFrameNum", 1.f);
                    Matrix3x3<float> TRSmatrix = obj->GetComponentByType<Pressure>(eComponentType::PRESSURE)->GetCollider1()->GetTransform()->GetModelToWorldCollisionTransform();
                    mShader->SetUniformMatrix3("TRSMatrix", TRSmatrix);
                    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
                    TRSmatrix = obj->GetComponentByType<Pressure>(eComponentType::PRESSURE)->GetCollider2()->GetTransform()->GetModelToWorldCollisionTransform();
                    mShader->SetUniformMatrix3("TRSMatrix", TRSmatrix);
                    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
                }
            }
        }
    }
    mShader->DisableShader();
}

void Graphics::DrawPauseObjects() const
{
    float width, height;
    width  = 1280; //(float)APPLICATION->GetWidth();
    height = 720;  //(float)APPLICATION->GetHeight();

    Matrix3x3<float> camera = MATRIX3x3::BuildIdentity<float>();
    Matrix3x3<float> ndc(2.f / width, 0.f, 0.f, 0.f, 2.f / height, 0.f, 0.f, 0.f, 1.f);

    mShader->EnableShader();
    mShader->SetUniformMatrix3("CameraMatrix", camera);
    mShader->SetUniformMatrix3("NDCMatrix", ndc);

    for (auto obj : OBJECT_MANAGER->GetPauseObjectVector())
    {
        if (obj->GetIsActive() == false)
            continue;

        Sprite *sprite = obj->GetSprite();
        if (!sprite->GetIsVisible())
            continue;

        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, sprite->GetTexture()->GetTextureID());

        Matrix3x3<float> TRSmatrix = obj->GetTransform()->GetModelToWorldTransform();
        mShader->SetUniformMatrix3("TRSMatrix", TRSmatrix);

        Vector4<float> color = sprite->GetColor();
        mShader->SetUniformFloat("alpha", color.w);

        mShader->SetUniformFloat("totalFrameNum", 1.f);
        mShader->SetUniformFloat("currentFrameNum", 1.f);

        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    }
    mShader->DisableShader();
}

void Graphics::DrawTileMap() const
{
    float width, height;
    width  = 1280; //(float)APPLICATION->GetWidth();
    height = 720;  //(float)APPLICATION->GetHeight();

    Matrix3x3<float> camera = mCamera->GetCameraMatrix();
    Matrix3x3<float> ndc(2.f / width, 0.f, 0.f, 0.f, 2.f / height, 0.f, 0.f, 0.f, 1.f);

    mShader->EnableShader();
    mShader->SetUniformMatrix3("CameraMatrix", camera);
    mShader->SetUniformMatrix3("NDCMatrix", ndc);

    for (auto tile : TILE_MAP->GetPhysicsTiles())
    {
        Matrix3x3<float> TRSmatrix = tile.second->transform.GetModelToWorldTransform();
        mShader->SetUniformMatrix3("TRSMatrix", TRSmatrix);

        glBindTexture(GL_TEXTURE_2D, mTextureData->FindTextureWithEnum(tile.second->texture)->GetTextureID());

        mShader->SetUniformFloat("totalFrameNum", 1.f);
        mShader->SetUniformFloat("currentFrameNum", 1.f);
        mShader->SetUniformFloat("alpha", 1.f);

        if (tile.second->isFliped)
        {
            mShader->SetUniformInteger("isFlipped", 1);
        }
        else
        {
            mShader->SetUniformInteger("isFlipped", 0);
        }

        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    }

    // Tiles Show Grid
    if (mIsTileBoxDrawing)
    {
        glBindTexture(GL_TEXTURE_2D, mTextureData->GetAllTexture()[eTexture::BOUNDARY]->GetTextureID());

        for (auto tile : TILE_MAP->GetPhysicsTiles())
        {
            mShader->SetUniformFloat("totalFrameNum", 1.f);
            mShader->SetUniformFloat("currentFrameNum", 1.f);
            Matrix3x3<float> TRSmatrix = tile.second->transform.GetModelToWorldTransform();
            mShader->SetUniformMatrix3("TRSMatrix", TRSmatrix);
            glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
        }
    }

    for (auto tile : TILE_MAP->GetGraphicTiles())
    {
        Matrix3x3<float> TRSmatrix = tile.second->transform.GetModelToWorldTransform();
        mShader->SetUniformMatrix3("TRSMatrix", TRSmatrix);

        glBindTexture(GL_TEXTURE_2D, mTextureData->FindTextureWithEnum(tile.second->texture)->GetTextureID());

        mShader->SetUniformFloat("totalFrameNum", 1.f);
        mShader->SetUniformFloat("currentFrameNum", 1.f);
        mShader->SetUniformFloat("alpha", 1.f);

        if (tile.second->isFliped)
        {
            mShader->SetUniformInteger("isFlipped", 1);
        }
        else
        {
            mShader->SetUniformInteger("isFlipped", 0);
        }

        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    }
    mShader->DisableShader();
}

void Graphics::DrawBackground() const
{
    mBackgroundShader->EnableShader();
    for (auto background : mBackgroundManager->GetBackgrounds())
    {
        glBindTexture(GL_TEXTURE_2D, background->GetSprite()->GetTexture()->GetTextureID());
        float offset = std::abs(mCamera->GetCenter().x) * 0.001f * background->GetSpeed();
        mBackgroundShader->SetUniformFloat("offset", offset);
        mBackgroundShader->SetUniformFloat("texCoordMultiplier", 0.8f);

        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    }
    mBackgroundShader->DisableShader();
}

void Graphics::DrawParticles(float dt) const
{
    float width, height;
    width  = 1280; //(float)APPLICATION->GetWidth();
    height = 720;  //(float)APPLICATION->GetHeight();

    Matrix3x3<float> camera = mCamera->GetCameraMatrix();
    Matrix3x3<float> ndc(2.f / width, 0.f, 0.f, 0.f, 2.f / height, 0.f, 0.f, 0.f, 1.f);

    if (!mParticleEmitters.empty())
    {
        mParticleShader->EnableShader();
        glBlendFunc(GL_SRC_ALPHA, GL_ONE);
        mParticleShader->SetUniformMatrix3("CameraMatrix", camera);
        mParticleShader->SetUniformMatrix3("NDCMatrix", ndc);

        for (auto particles : mParticleEmitters)
        {
            particles->UpdateParticles(dt);
            glBindTexture(GL_TEXTURE_2D, particles->GetTexture()->GetTextureID());
            for (auto &p : particles->GetParticleVector())
            {
                if (p.life > 0.f)
                {
                    mParticleShader->SetUniformVec2("offset", p.position);
                    mParticleShader->SetUniformVec4("color", p.color);
                    mParticleShader->SetUniformFloat("scale", p.scale);
                    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
                }
            }
        }
        mParticleShader->DisableShader();
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    }
}

void Graphics::DrawUIs() const
{
    if (mUIObjectsToDraw.empty() == true)
    {
        return;
    }

    float width, height;
    width  = 1280; //(float)APPLICATION->GetWidth();
    height = 720;  //(float)APPLICATION->GetHeight();

    Matrix3x3<float> camera = MATRIX3x3::BuildIdentity<float>();
    Matrix3x3<float> ndc(2.f / width, 0.f, 0.f, 0.f, 2.f / height, 0.f, 0.f, 0.f, 1.f);

    mShader->EnableShader();
    mShader->SetUniformMatrix3("CameraMatrix", camera);
    mShader->SetUniformMatrix3("NDCMatrix", ndc);

    for (auto obj : mUIObjectsToDraw)
    {
        if (obj->GetIsActive() == false)
            continue;

        Sprite *sprite = obj->GetSprite();

        if (sprite->GetIsVisible() == false || sprite->GetTexture() == nullptr)
            continue;

        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, sprite->GetTexture()->GetTextureID());
        // obj.second.

        Matrix3x3<float> TRSmatrix = obj->GetTransform()->GetModelToWorldTransform();
        mShader->SetUniformMatrix3("TRSMatrix", TRSmatrix);

        Vector4<float> color = sprite->GetColor();
        mShader->SetUniformFloat("alpha", color.w);
        if (sprite->IsAnimation())
        {
            mShader->SetUniformFloat("totalFrameNum", static_cast<float>(sprite->GetAnimationFrameNum()));
            mShader->SetUniformFloat("currentFrameNum", static_cast<float>(sprite->GetCurrentFrame()));
        }
        else
        {
            mShader->SetUniformFloat("totalFrameNum", 1.f);
            mShader->SetUniformFloat("currentFrameNum", 1.f);
        }

        // for players, check flipped
        eObjectType objType = static_cast<eObjectType>(sprite->GetOwner()->GetObjectType());

        if (objType == eObjectType::PLAYER_1 || objType == eObjectType::PLAYER_2 || objType == eObjectType::PLAYER_COMBINED)
        {
            if (sprite->GetOwner()->GetComponentByType<Player>(eComponentType::PLAYER)->GetContollerDirection() == eControllerDirection::CONTROLLER_DIRECTION_LEFT)
            {
                mShader->SetUniformInteger("isFlipped", 0);
            }
            else
            {
                mShader->SetUniformInteger("isFlipped", 1);
            }
        }
        else
        {
            mShader->SetUniformInteger("isFlipped", 0);
        }
        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    }
    mShader->DisableShader();
}

void Graphics::DrawTransition() const
{
    mTransitionShader->EnableShader();
    mTransitionShader->SetUniformFloat("time", std::abs(mTransitionTimer - mTransitionTime));
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    mTransitionShader->DisableShader();
}