/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : Texture.cpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Haewon Shon
Secondary :
- End Header ----------------------------------------------------------------*/

#define STB_IMAGE_IMPLEMENTATION
#define STBI_ONLY_PNG
#include "Texture.hpp"
#include "../Graphics.hpp"
#include "stb_image.h"
#include <iostream>

Texture::Texture()
{
}

Texture::~Texture()
{
    mPixelData.clear();
    glDeleteTextures(1, &mTextureID);
    glBindTexture(GL_TEXTURE_2D, 0);
}

bool Texture::Initialize(std::filesystem::path filePath)
{
    mFileName = filePath.generic_string();

    if (!LoadPixelData(mFileName))
    {
        return false;
    }

    glGenTextures(1, &mTextureID);
    glBindTexture(GL_TEXTURE_2D, mTextureID);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, mWidth, mHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, &mPixelData[0]);

    glBindTexture(GL_TEXTURE_2D, 0);

    if (mTextureID == NULL)
    {
        return false;
    }
    return true;
}

bool Texture::InitializeWithData()
{
    if (mPixelData.size() == 0)
    {
        return false;
    }
    glGenTextures(1, &mTextureID);
    glBindTexture(GL_TEXTURE_2D, mTextureID);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, mWidth, mHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, &mPixelData[0]);

    glBindTexture(GL_TEXTURE_2D, 0);

    if (mTextureID == NULL)
    {
        return false;
    }
    return true;
}

const GLuint Texture::GetTextureID() const
{
    return mTextureID;
}

int Texture::GetWidth()
{
    return mWidth;
}

int Texture::GetHeight()
{
    return mHeight;
}

unsigned char *Texture::GetPixelData()
{
    return &mPixelData[0];
}

bool Texture::LoadPixelData(std::string fileName)
{
    mFileName = fileName;

    int            channels;
    unsigned char *data = stbi_load(fileName.c_str(), &mWidth, &mHeight, &channels, 4);

    if (data == nullptr)
    {
        std::string TextFail = stbi_failure_reason();
        std::cout << TextFail.c_str() << std::endl;
        return false;
    }

    int count = 0;
    while (count != mWidth * mHeight)
    {
        for (int i = 0; i < 4; i++)
        {
            mPixelData.push_back(*data);
            data++;
        }
        count++;
    }
    return true;
}