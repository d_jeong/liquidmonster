/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : Texture.hpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Haewon Shon
Secondary :
- End Header ----------------------------------------------------------------*/

#pragma once
#include <filesystem>
#include <glew.h>
#include <vector>

class Texture
{
public:
    Texture();
    ~Texture();
    bool           Initialize(std::filesystem::path filePath);
    bool           InitializeWithData();
    const GLuint   GetTextureID() const;
    int            GetWidth();
    int            GetHeight();
    unsigned char *GetPixelData();
    bool           LoadPixelData(std::string fileName);

private:
    GLuint                     mTextureID;
    std::string                mFileName;
    std::vector<unsigned char> mPixelData;
    int                        mWidth;
    int                        mHeight;
};
