/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : Battery.cpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Sejeong Park
Secondary :
- End Header ----------------------------------------------------------------*/

#include "../Battery/Battery.hpp"
#include "../../Data/Data.hpp"
#include "../../Object/Object.hpp"
#include "../../TileMap/TileMap.hpp"
#include "../Collision/Collision.hpp"
#include "../Fan/Fan.hpp"
#include "../MovingPlatform/MovingPlatform.hpp"
#include "../Pressure/Pressure.hpp"
#include "../Sprite/Sprite.hpp"

Battery::Battery() : Component(eComponentType::BATTERY)
{
}

Battery::Battery(Object* batteryHome, Object* connectingObject) : Component(eComponentType::BATTERY)
{
    mObjectBatteryHome = batteryHome;
    mObjectConnecting = connectingObject;
}

void Battery::Initialize(Object* object)
{
}

void Battery::Update(float dt)
{
    mWasBatteryInHome = mIsBatteryInHome;
    

    if (mObjectConnecting != nullptr && mObjectBatteryHome != nullptr)
    {
        // in case if there is battery in battery home
        if (mIsBatteryInHome == true)
        {
            UpdateWireGraphics(mIsBatteryInHome);
            mObjectBatteryHome->GetSprite()->LoadTextureFromData(eTexture::BATTERY_HOME);

            if (mOwner->GetVelocity().x != 0.f || mOwner->GetTransform()->GetParentTransform() != nullptr)
            {
                mIsBatteryInHome = false;
                mOwner->GetSprite()->SetVisible();
            }
        }
        // if there is no battery
        else
        {
            UpdateWireGraphics(mIsBatteryInHome);
            mObjectBatteryHome->GetSprite()->LoadTextureFromData(eTexture::OBJECT_BATTERY_HOME_OFF);

            if (std::abs(mOwner->GetPosition().y - mObjectBatteryHome->GetPosition().y) < mRange)
            {
                if (std::abs(mOwner->GetPosition().x - mObjectBatteryHome->GetPosition().x) < mRange)
                {
                    if (mOwner->GetVelocity().x >= 0.f && mOwner->GetPosition().x < mObjectBatteryHome->GetPosition().x)
                    {
                        mIsBatteryInHome = true;
                        mOwner->GetSprite()->SetInVisible();
                        mOwner->SetVelocity({ 0.f, 0.f });
                    }
                    else if (mOwner->GetVelocity().x <= 0.f && mOwner->GetPosition().x > mObjectBatteryHome->GetPosition().x)
                    {
                        mIsBatteryInHome = true;
                        mOwner->GetSprite()->SetInVisible();
                        mOwner->SetVelocity({ 0.f, 0.f });
                    }
                }
            }
        }
    }

    if (mWasBatteryInHome != mIsBatteryInHome)
    {
        if (mIsBatteryInHome)
        {
            FMOD_MANAGER->PlaySoundEffectSound("sound/sfx_battery_in.mp3");

            switch (mObjectConnecting->GetObjectType())
            {
            case eObjectType::FAN:
                mObjectConnecting->GetComponentByType<Fan>(eComponentType::FAN)->SetIsFanActive(true);
                break;
            case eObjectType::MOVING_PLATFORM:
                mObjectConnecting->GetComponentByType<MovingPlatform>(eComponentType::MOVINGPLATFORM)->SetIsMovingPlatformActive(true);
                break;
            case eObjectType::PRESSURE:
                mObjectConnecting->GetComponentByType<Pressure>(eComponentType::PRESSURE)->SetIsPressureActive(true);
                break;
            }
        }
        else
        {
            switch (mObjectConnecting->GetObjectType())
            {
            case eObjectType::FAN:
                mObjectConnecting->GetComponentByType<Fan>(eComponentType::FAN)->SetIsFanActive(false);
                break;
            case eObjectType::MOVING_PLATFORM:
                mObjectConnecting->GetComponentByType<MovingPlatform>(eComponentType::MOVINGPLATFORM)->SetIsMovingPlatformActive(false);
                break;
            case eObjectType::PRESSURE:
                mObjectConnecting->GetComponentByType<Pressure>(eComponentType::PRESSURE)->SetIsPressureActive(false);
                break;
            }
        }
    }
}

void Battery::Delete(void)
{
}

void Battery::SetBattery(bool battery)
{
    mIsBatteryInHome = battery;
}

bool Battery::GetBattery(void)
{
    return mIsBatteryInHome;
}

void Battery::SetBatteryHome(Object* batteryHome)
{
    mObjectBatteryHome = batteryHome;
}

void Battery::SetConnectingObject(Object* connectingObject)
{
    mObjectConnecting = connectingObject;
}

Object* Battery::GetBatteryHome(void)
{
    return mObjectBatteryHome;
}

Object* Battery::GetConnectingObject(void)
{
    return mObjectConnecting;
}

void Battery::InsertGraphicsTile(int grid, eTexture texture)
{
    mWireTiles.insert({ grid, texture });
    TILE_MAP->FindGraphicsTileByGrid(grid)->SetTileConnectedObjectName(mOwner->GetName());
}

void Battery::EraseGraphicsTile(int grid)
{
    mWireTiles.erase(grid);
}

void Battery::UpdateWireGraphics(bool isBattery)
{
    if (isBattery) // true = when the battery is in. BLUE
    {
        for (auto wire : mWireTiles)
        {
            switch (wire.second)
            {
            case eTexture::TILE_WIRE_RED_1:
                TILE_MAP->ChangeGraphicTileTexture(wire.first, eTexture::TILE_WIRE_BLUE_1);
                break;
            case eTexture::TILE_WIRE_RED_2:
                TILE_MAP->ChangeGraphicTileTexture(wire.first, eTexture::TILE_WIRE_BLUE_2);
                break;
            case eTexture::TILE_WIRE_RED_3:
                TILE_MAP->ChangeGraphicTileTexture(wire.first, eTexture::TILE_WIRE_BLUE_3);
                break;
            case eTexture::TILE_WIRE_RED_4:
                TILE_MAP->ChangeGraphicTileTexture(wire.first, eTexture::TILE_WIRE_BLUE_4);
                break;
            case eTexture::TILE_WIRE_RED_5:
                TILE_MAP->ChangeGraphicTileTexture(wire.first, eTexture::TILE_WIRE_BLUE_5);
                break;
            case eTexture::TILE_WIRE_RED_6:
                TILE_MAP->ChangeGraphicTileTexture(wire.first, eTexture::TILE_WIRE_BLUE_6);
                break;
            case eTexture::TILE_WIRE_RED_7:
                TILE_MAP->ChangeGraphicTileTexture(wire.first, eTexture::TILE_WIRE_BLUE_7);
                break;
            case eTexture::TILE_WIRE_RED_8:
                TILE_MAP->ChangeGraphicTileTexture(wire.first, eTexture::TILE_WIRE_BLUE_8);
                break;
            case eTexture::TILE_WIRE_RED_9:
                TILE_MAP->ChangeGraphicTileTexture(wire.first, eTexture::TILE_WIRE_BLUE_9);
                break;
            case eTexture::TILE_WIRE_RED_10:
                TILE_MAP->ChangeGraphicTileTexture(wire.first, eTexture::TILE_WIRE_BLUE_10);
                break;
            case eTexture::TILE_WIRE_BLUE_1:
                TILE_MAP->ChangeGraphicTileTexture(wire.first, eTexture::TILE_WIRE_BLUE_1);
                break;
            case eTexture::TILE_WIRE_BLUE_2:
                TILE_MAP->ChangeGraphicTileTexture(wire.first, eTexture::TILE_WIRE_BLUE_2);
                break;
            case eTexture::TILE_WIRE_BLUE_3:
                TILE_MAP->ChangeGraphicTileTexture(wire.first, eTexture::TILE_WIRE_BLUE_3);
                break;
            case eTexture::TILE_WIRE_BLUE_4:
                TILE_MAP->ChangeGraphicTileTexture(wire.first, eTexture::TILE_WIRE_BLUE_4);
                break;
            case eTexture::TILE_WIRE_BLUE_5:
                TILE_MAP->ChangeGraphicTileTexture(wire.first, eTexture::TILE_WIRE_BLUE_5);
                break;
            case eTexture::TILE_WIRE_BLUE_6:
                TILE_MAP->ChangeGraphicTileTexture(wire.first, eTexture::TILE_WIRE_BLUE_6);
                break;
            case eTexture::TILE_WIRE_BLUE_7:
                TILE_MAP->ChangeGraphicTileTexture(wire.first, eTexture::TILE_WIRE_BLUE_7);
                break;
            case eTexture::TILE_WIRE_BLUE_8:
                TILE_MAP->ChangeGraphicTileTexture(wire.first, eTexture::TILE_WIRE_BLUE_8);
                break;
            case eTexture::TILE_WIRE_BLUE_9:
                TILE_MAP->ChangeGraphicTileTexture(wire.first, eTexture::TILE_WIRE_BLUE_9);
                break;
            case eTexture::TILE_WIRE_BLUE_10:
                TILE_MAP->ChangeGraphicTileTexture(wire.first, eTexture::TILE_WIRE_BLUE_10);
                break;
            }
        }
    }
    else // false = when the battery is not in. RED
    {
        for (auto wire : mWireTiles)
        {
            switch (wire.second)
            {
            case eTexture::TILE_WIRE_BLUE_1:
                TILE_MAP->ChangeGraphicTileTexture(wire.first, eTexture::TILE_WIRE_RED_1);
                break;
            case eTexture::TILE_WIRE_BLUE_2:
                TILE_MAP->ChangeGraphicTileTexture(wire.first, eTexture::TILE_WIRE_RED_2);
                break;
            case eTexture::TILE_WIRE_BLUE_3:
                TILE_MAP->ChangeGraphicTileTexture(wire.first, eTexture::TILE_WIRE_RED_3);
                break;
            case eTexture::TILE_WIRE_BLUE_4:
                TILE_MAP->ChangeGraphicTileTexture(wire.first, eTexture::TILE_WIRE_RED_4);
                break;
            case eTexture::TILE_WIRE_BLUE_5:
                TILE_MAP->ChangeGraphicTileTexture(wire.first, eTexture::TILE_WIRE_RED_5);
                break;
            case eTexture::TILE_WIRE_BLUE_6:
                TILE_MAP->ChangeGraphicTileTexture(wire.first, eTexture::TILE_WIRE_RED_6);
                break;
            case eTexture::TILE_WIRE_BLUE_7:
                TILE_MAP->ChangeGraphicTileTexture(wire.first, eTexture::TILE_WIRE_RED_7);
                break;
            case eTexture::TILE_WIRE_BLUE_8:
                TILE_MAP->ChangeGraphicTileTexture(wire.first, eTexture::TILE_WIRE_RED_8);
                break;
            case eTexture::TILE_WIRE_BLUE_9:
                TILE_MAP->ChangeGraphicTileTexture(wire.first, eTexture::TILE_WIRE_RED_9);
                break;
            case eTexture::TILE_WIRE_BLUE_10:
                TILE_MAP->ChangeGraphicTileTexture(wire.first, eTexture::TILE_WIRE_RED_10);
                break;
            case eTexture::TILE_WIRE_RED_1:
                TILE_MAP->ChangeGraphicTileTexture(wire.first, eTexture::TILE_WIRE_RED_1);
                break;
            case eTexture::TILE_WIRE_RED_2:
                TILE_MAP->ChangeGraphicTileTexture(wire.first, eTexture::TILE_WIRE_RED_2);
                break;
            case eTexture::TILE_WIRE_RED_3:
                TILE_MAP->ChangeGraphicTileTexture(wire.first, eTexture::TILE_WIRE_RED_3);
                break;                                                          
            case eTexture::TILE_WIRE_RED_4:                                     
                TILE_MAP->ChangeGraphicTileTexture(wire.first, eTexture::TILE_WIRE_RED_4);
                break;                                                            
            case eTexture::TILE_WIRE_RED_5:                                       
                TILE_MAP->ChangeGraphicTileTexture(wire.first, eTexture::TILE_WIRE_RED_5);
                break;                                                             
            case eTexture::TILE_WIRE_RED_6:                                        
                TILE_MAP->ChangeGraphicTileTexture(wire.first, eTexture::TILE_WIRE_RED_6);
                break;                                                            
            case eTexture::TILE_WIRE_RED_7:                                       
                TILE_MAP->ChangeGraphicTileTexture(wire.first, eTexture::TILE_WIRE_RED_7);
                break;                                                             
            case eTexture::TILE_WIRE_RED_8:                                        
                TILE_MAP->ChangeGraphicTileTexture(wire.first, eTexture::TILE_WIRE_RED_8);
                break;                                                             
            case eTexture::TILE_WIRE_RED_9:                                        
                TILE_MAP->ChangeGraphicTileTexture(wire.first, eTexture::TILE_WIRE_RED_9);
                break;                                                             
            case eTexture::TILE_WIRE_RED_10:                                       
                TILE_MAP->ChangeGraphicTileTexture(wire.first, eTexture::TILE_WIRE_RED_10);
            }
        }
    }
}
