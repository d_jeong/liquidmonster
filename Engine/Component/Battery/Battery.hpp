/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : Battery.hpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Sejeong Park
Secondary :
- End Header ----------------------------------------------------------------*/
#pragma once

#include <unordered_map>
#include "../Component.hpp"

class Object;
enum class eTexture;

class Battery : public Component
{
public:
    Battery();
    Battery(Object* batteryHome, Object* connectingObject);

    void Initialize(Object* object) override;
    void Update(float dt) override;
    void Delete(void) override;

    void SetBattery(bool battery);
    bool GetBattery(void);

    void SetBatteryHome(Object* batteryHome);
    void SetConnectingObject(Object* connectingObject);

    Object* GetBatteryHome(void);
    Object* GetConnectingObject(void);

    void InsertGraphicsTile(int grid, eTexture texture);
    void EraseGraphicsTile(int grid);

private:
    void    UpdateWireGraphics(bool isBattery);

    Object* mObjectBatteryHome = nullptr;
    Object* mObjectConnecting  = nullptr;

    const float mRange = 50.f;

    bool                              mWasBatteryInHome = false;
    bool                              mIsBatteryInHome = false;
    std::unordered_map<int, eTexture> mWireTiles{};
};