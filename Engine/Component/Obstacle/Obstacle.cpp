/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : Obstacle.cpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Haewon Shon
Secondary :
- End Header ----------------------------------------------------------------*/

#include "Obstacle.hpp"
#include "../../Data/Data.hpp"
#include "../../Object/Object.hpp"
#include "../../Object/ObjectManager/ObjectManager.hpp"
#include "../Collision/Collision.hpp"
#include "../Player/Player.hpp"
#include "../Sprite/Sprite.hpp"
#include "../Respawn/Respawn.hpp"

Obstacle::Obstacle(void) : Component(eComponentType::OBSTACLE)
{
}

void Obstacle::Initialize(Object* object)
{
}

void Obstacle::Update(float dt)
{
    if (this->mOwner->GetSprite()->GetTextureEnum() == eTexture::OBSTACLE)
    {
        if (mIsActive)
        {
            Object* player1        = OBJECT_MANAGER->FindObjectByName("Player1");
            Object* player2        = OBJECT_MANAGER->FindObjectByName("Player2");
            Object* playerCombined = OBJECT_MANAGER->FindObjectByName("PlayerCombined");

            if (mOwner->GetComponentByType<Collision>(eComponentType::COLLISION)->IsCollidingWith(player1))
            {
                player1->GetComponentByType<Player>(eComponentType::PLAYER)->SetPlayerDeath();
            }

            if (mOwner->GetComponentByType<Collision>(eComponentType::COLLISION)->IsCollidingWith(player2))
            {
                player2->GetComponentByType<Player>(eComponentType::PLAYER)->SetPlayerDeath();
            }

            if (mOwner->GetComponentByType<Collision>(eComponentType::COLLISION)->IsCollidingWith(playerCombined))
            {
                playerCombined->GetComponentByType<Player>(eComponentType::PLAYER)->SetPlayerDeath();
            }
        }
    }
    else if (this->mOwner->GetSprite()->GetTextureEnum() == eTexture::LAZOR)
    {
        if (mIsActive)
        {
            Object* player1        = OBJECT_MANAGER->FindObjectByName("Player1");
            Object* player2        = OBJECT_MANAGER->FindObjectByName("Player2");
            Object* playerCombined = OBJECT_MANAGER->FindObjectByName("PlayerCombined");

            if (mOwner->GetComponentByType<Collision>(eComponentType::COLLISION)->IsCollidingWith(player1))
            {
                FMOD_MANAGER->PlaySoundEffectSound("sound/sfx_laser.mp3");
                player1->GetComponentByType<Player>(eComponentType::PLAYER)->SetPlayerDeath();
            }

            if (mOwner->GetComponentByType<Collision>(eComponentType::COLLISION)->IsCollidingWith(player2))
            {
                FMOD_MANAGER->PlaySoundEffectSound("sound/sfx_laser.mp3");
                player2->GetComponentByType<Player>(eComponentType::PLAYER)->SetPlayerDeath();
            }

            if (mOwner->GetComponentByType<Collision>(eComponentType::COLLISION)->IsCollidingWith(playerCombined))
            {
                FMOD_MANAGER->PlaySoundEffectSound("sound/sfx_laser.mp3");
                playerCombined->GetComponentByType<Player>(eComponentType::PLAYER)->SetPlayerDeath();
            }

            for (auto objectToBurn : OBJECT_MANAGER->GetObjectVector())
            {
                if (objectToBurn->GetComponentByType<Collision>(eComponentType::COLLISION) != nullptr)
                {
                    if (mOwner->GetComponentByType<Collision>(eComponentType::COLLISION)->IsCollidingWith(objectToBurn))
                    {
                        if (objectToBurn->GetObjectType() == eObjectType::WOOD_BOX || objectToBurn->GetObjectType() == eObjectType::BATTERY || objectToBurn->GetObjectType() == eObjectType::EATABLE)
                        {
                            Respawn* objectToBurnRespawn = objectToBurn->GetComponentByType<Respawn>(eComponentType::RESPAWN);
                            if (objectToBurnRespawn != nullptr)
                            {
                                FMOD_MANAGER->PlaySoundEffectSound("sound/sfx_laser.mp3");
                                objectToBurnRespawn->SetDeath();
                            }
                        }
                    }
                }
            }
        }
    }
}

void Obstacle::Delete(void)
{
}
