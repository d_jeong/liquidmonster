/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : Obstacle.hpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Haewon Shon
Secondary :
- End Header ----------------------------------------------------------------*/
#pragma once

#include "../Component.hpp"

class Object;

class Obstacle : public Component
{
public:
    Obstacle(void);

    void Initialize(Object *object) override;
    void Update(float dt) override;
    void Delete(void) override;

};