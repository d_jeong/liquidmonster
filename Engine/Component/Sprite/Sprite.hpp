/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : Sprite.hpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Haewon Shon
Secondary :
- End Header ----------------------------------------------------------------*/
#pragma once

#include "../../Component/Component.hpp"
#include "../../MathLibrary/MathLibrary.hpp"
#include <unordered_map>

class Object;
class Texture;
class Transform;
enum class eTexture;

class Sprite : public Component
{
public:
    Sprite();
    ~Sprite() override;

    void Initialize(Object *object) override;
    void Update(float dt) override;
    void Delete(void) override;

    void Draw();

    void SetColor(Vector4<float> color);
    void LoadTexture(const char *fileName);

    void SetVisible(void);
    void SetInVisible(void);
    bool GetIsVisible(void);

    void SetFlip(bool isFlip);
    bool GetIsFlip(void);

    Vector4<float> GetColor() const;
    Transform *    GetTransform() const;
    Texture *      GetTexture() const;

    void LoadTextureFromData(eTexture textureEnum);

    // animation
    bool IsAnimation(void) const;
    void SetAnimation(bool isAni);
    void SetAnimation(bool isAni, unsigned frameNum, float speed);
    void SetAnimationFrame(unsigned frameNum);
    void SetAnimationSpeed(float speed);
    void PauseAnimation(bool set);

    unsigned GetAnimationFrameNum(void) const;
    float GetAnimationSpeed(void) const;
    unsigned GetCurrentFrame(void) const;
    void     GetTexCoords(float *data);
    eTexture GetTextureEnum(void);

private:
    Transform *                                mTransform = nullptr;
    Vector4<float>                             mColor     = {0, 0, 0, 0};
    Texture *                                  mCurrentTexture;
    std::unordered_map<std::string, Texture *> mTextureMap;
    bool                                       mIsVisible;
    bool                                       mIsFlip;
    eTexture                                   mTextureEnum;

    // animation
    bool mIsAnimation = false;
    // How many frames consisting animation
    unsigned mAnimationFrame = 0;
    // Frame delay for current to next frame
    float                       mFrameDelay     = 0.f;
    float                       mAnimationSpeed = 0.f;
    std::vector<Vector2<float>> mTexCoords;
    bool                        mIsAniRunning;

    void     UpdateTexCoord();
    unsigned mCurrentFrame = 0;
    float    mframeTimer   = 0.f;
};