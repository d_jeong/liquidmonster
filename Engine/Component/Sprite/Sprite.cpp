/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : Sprite.cpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Haewon Shon
Secondary :
- End Header ----------------------------------------------------------------*/
#include "Sprite.hpp"
#include "../../Data/Data.hpp"
#include "../../Graphics/Graphics.hpp"
#include "../../Graphics/Texture/Texture.hpp"
#include "../../Object/Object.hpp"
#include "../../Object/ObjectDepth.hpp"
#include <iostream>

Sprite::Sprite() : Component(eComponentType::SPRITE)
{
    mIsVisible = true;
}

Sprite::~Sprite()
{
}

void Sprite::Initialize(Object *object)
{
    mTransform = GetOwner()->GetTransform();
    if (mOwner->GetDepth() == UI)
    {
        GRAPHICS->AddUISprite(GetOwner());
    }
    else
    {
        GRAPHICS->AddSprite(GetOwner());
    }
}

void Sprite::Update(float dt)
{
    if (mIsAnimation && mIsAniRunning)
    {
        mframeTimer += dt;
        if (mframeTimer > mFrameDelay)
        {
            ++mCurrentFrame;
            mframeTimer = 0.f;
        }
        if (mCurrentFrame == mAnimationFrame)
        {
            mCurrentFrame = 0;
        }
    }
}

void Sprite::Delete(void)
{
    for (auto texture : mTextureMap)
    {
        delete texture.second;
    }

    if (mOwner->GetDepth() == UI)
    {
        GRAPHICS->DeleteUISprite(mOwner);
    }
    else
    {
        GRAPHICS->DeleteSprite(mOwner);
    }
}

void Sprite::Draw()
{
}

void Sprite::SetColor(Vector4<float> color)
{
    mColor = color;
}

void Sprite::LoadTexture(const char *fileName)
{
    if (mTextureMap.find(std::string(fileName)) != mTextureMap.end())
    {
        mCurrentTexture = mTextureMap[std::string(fileName)];
    }
    else
    {
         mCurrentTexture = new Texture;

        if (!mCurrentTexture->Initialize(fileName))
        {
            std::cout << "Failed to load texture : " << fileName << std::endl;
        }
        mTextureMap.insert({std::string(fileName), mCurrentTexture});
    }
}

void Sprite::LoadTextureFromData(eTexture textureEnum)
{
    Texture *texture = GRAPHICS->GetTextureFromData()->FindTextureWithEnum(textureEnum);
    if (texture == nullptr)
    {
        std::cout << "There is no texture in data" << std::endl;
    }

    mTextureEnum = textureEnum;

    mCurrentTexture = texture;
}

void Sprite::SetVisible(void)
{
    mIsVisible = true;
}

void Sprite::SetInVisible(void)
{
    mIsVisible = false;
}

bool Sprite::GetIsVisible(void)
{
    return mIsVisible;
}

void Sprite::SetFlip(bool isFlip)
{
    mIsFlip = isFlip;
}

bool Sprite::GetIsFlip(void)
{
    return mIsFlip;
}

Vector4<float> Sprite::GetColor() const
{
    return mColor;
}

Transform *Sprite::GetTransform() const
{
    return mTransform;
}

Texture *Sprite::GetTexture() const
{
    return mCurrentTexture;
}

bool Sprite::IsAnimation(void) const
{
    return mIsAnimation;
}

void Sprite::SetAnimation(bool isAni)
{
    mIsAnimation = isAni;
    if (mIsAnimation)
    {
        mIsAniRunning = true;
    }
}

void Sprite::SetAnimation(bool isAni, unsigned frameNum, float speed)
{
    mIsAnimation    = isAni;
    mAnimationFrame = frameNum;
    mAnimationSpeed = speed;
    mFrameDelay     = 1.f / speed;

    if (mIsAnimation)
    {
        UpdateTexCoord();
        mIsAniRunning = true;
    }
}

void Sprite::SetAnimationFrame(unsigned frameNum)
{
    mAnimationFrame = frameNum;

    if (mIsAnimation)
    {
        UpdateTexCoord();
    }
    UpdateTexCoord();
}

void Sprite::SetAnimationSpeed(float speed)
{
    mAnimationSpeed = speed;

    mFrameDelay = 1.f / speed;

    if (mIsAnimation)
    {
        UpdateTexCoord();
    }
}

void Sprite::PauseAnimation(bool set)
{
    mIsAniRunning = !set;
}

unsigned Sprite::GetAnimationFrameNum(void) const
{
    return mAnimationFrame;
}

float Sprite::GetAnimationSpeed(void) const
{
    return mAnimationSpeed;
}

unsigned Sprite::GetCurrentFrame(void) const
{
    return mCurrentFrame;
}

void Sprite::GetTexCoords(float *data)
{
    data[0] = mTexCoords[(size_t)mCurrentFrame * 4].x;
    data[1] = mTexCoords[(size_t)mCurrentFrame * 4].y;
    data[2] = mTexCoords[(size_t)mCurrentFrame * 4 + 1].x;
    data[3] = mTexCoords[(size_t)mCurrentFrame * 4 + 1].y;
    data[4] = mTexCoords[(size_t)mCurrentFrame * 4 + 2].x;
    data[5] = mTexCoords[(size_t)mCurrentFrame * 4 + 2].y;
    data[6] = mTexCoords[(size_t)mCurrentFrame * 4 + 3].x;
    data[7] = mTexCoords[(size_t)mCurrentFrame * 4 + 3].y;
}

eTexture Sprite::GetTextureEnum(void)
{
    return mTextureEnum;
}

void Sprite::UpdateTexCoord()
{
    mTexCoords.clear();
    mCurrentFrame = 0;

    float xCoordTerm = 1.f / mAnimationFrame;
    for (unsigned i = 0; i < mAnimationFrame; ++i)
    {
        mTexCoords.push_back({(i + 1) * xCoordTerm, 0.f});
        mTexCoords.push_back({(i + 1) * xCoordTerm, 1.f});
        mTexCoords.push_back({i * xCoordTerm, 0.f});
        mTexCoords.push_back({i * xCoordTerm, 1.f});
    }
}