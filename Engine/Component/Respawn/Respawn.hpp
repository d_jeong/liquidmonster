/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : Respawn.hpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Haewon Shon
Secondary :
- End Header ----------------------------------------------------------------*/
#pragma once

#include "../Component.hpp"

class DeadEffect;

class Respawn : public Component
{
public:
    Respawn();

    void Initialize(Object* object) override;
    void Update(float dt) override;
    void Delete(void) override;

    bool IsAlive();
    void SetDeath();
    void CheckRespawn(float dt);

private:
    DeadEffect* mDeadEffectEmitter = nullptr;
    bool        mIsObjectAlive;
    const float mRespawnDelay = 3.f;
    float       mRespawnTime;
};