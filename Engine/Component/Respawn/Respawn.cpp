/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : Respawn.cpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Haewon Shon
Secondary :
- End Header ----------------------------------------------------------------*/

#include "Respawn.hpp"
#include "../../Data/Data.hpp"
#include "../../Graphics/Graphics.hpp"
#include "../../Graphics/Particle/DeadEffect/DeadEffect.hpp"
#include "../Sprite/Sprite.hpp"

Respawn::Respawn() : Component(eComponentType::RESPAWN)
{
}

void Respawn::Initialize(Object* object)
{
    mIsObjectAlive = true;

    eTexture textureEnum;

    switch (object->GetObjectType())
    {
        case eObjectType::PLAYER_1:
            textureEnum = eTexture::PLAYER_1_PARTICLE;
            break;
        case eObjectType::PLAYER_2:
            textureEnum = eTexture::PLAYER_2_PARTICLE;
            break;
        case eObjectType::PLAYER_COMBINED:
            textureEnum = eTexture::PLAYER_COMBINED_PARTICLE;
            break;
        case eObjectType::BATTERY:
            textureEnum = eTexture::BATTERY_PARTICLE;
            break;
        case eObjectType::EATABLE:
            textureEnum = eTexture::KEY_PARTICLE;
            break;
        case eObjectType::WOOD_BOX:
            textureEnum = eTexture::BOX_PARTICLE;
            break;
        default:
            textureEnum = eTexture::KEY_PARTICLE;
            break;
    }

    mDeadEffectEmitter = new DeadEffect(object, GRAPHICS->GetTextureFromData()->FindTextureWithEnum(textureEnum));
    GRAPHICS->AddParticleEmitter(mDeadEffectEmitter);

    mIsObjectAlive = true;
}

void Respawn::Update(float dt)
{
    if (!mIsObjectAlive)
    {
        CheckRespawn(dt);
    }
}

void Respawn::Delete(void)
{
    //delete mDeadEffectEmitter;
}

bool Respawn::IsAlive()
{
    return mIsObjectAlive;
}

void Respawn::SetDeath()
{
    mDeadEffectEmitter->GenerateEffect();

    mIsObjectAlive = false;
    mRespawnTime   = mRespawnDelay;

    mOwner->GetSprite()->SetInVisible();
    mOwner->SetVelocity({0.f, 0.f});

    mOwner->SetPosition(mOwner->GetSpawnPosition());
}

void Respawn::CheckRespawn(float dt)
{
    if (!mIsObjectAlive)
    {
        std::cout << mOwner->GetSprite()->GetIsVisible() << std::endl;
        if (mRespawnTime > 0.f)
        {
            mRespawnTime -= dt;
        }
        else if (mRespawnTime < 0.f && !mIsObjectAlive)
        {
            //respawn object
            mIsObjectAlive = true;
            mOwner->GetSprite()->SetVisible();
        }
    }
}