/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : Pressure.hpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Haewon Shon
Secondary :
- End Header ----------------------------------------------------------------*/
#pragma once

#include "../Component.hpp"

class Object;

class Pressure : public Component
{
public:
    enum class ePressureType
    {
        HORIZONTAL,
        VERTICAL,
        NONE
    };

public:
    Pressure();
    ~Pressure() = default;

    void Initialize(Object *object) override;
    void Update(float dt) override;
    void Delete(void) override;

    void UpdateColliderPosition(void);
    
    void SetPressureType(ePressureType type);
    ePressureType GetPressureType(void);

    void SetIsPressureActive(bool isWorking);
    
    Object *GetCollider1() const;
    Object *GetCollider2() const;

    bool GetIsPressureActive();
    
private:
    void        ActivatePressure(void);
    void        UnactivatePressure(void);
    void        PressingPlayer(float dt, Object *player);

    float       mDelayTimer = 0.f;
    float       mWorkTimer  = 0.f;
    const float mPressDelay = 2.f;
    const float mWorkPeriod = 1.f;

    ePressureType mPressureType;

    // const float mPressingSpeed = 100.f;
    // const float mDepressingSpeed = 30.f;

    bool mIsPressureActive;
    bool mIsPressureWorking;

    Object *mCollider1;
    Object *mCollider2;
};