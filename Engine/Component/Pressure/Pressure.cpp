/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : Pressure.cpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Haewon Shon
Secondary :
- End Header ----------------------------------------------------------------*/

#include "Pressure.hpp"
#include "../../MathLibrary/Vector2.hpp"
#include "../../Object/Object.hpp"
#include "../../Object/ObjectManager/ObjectManager.hpp"
#include "../Collision/Collision.hpp"
#include "../Player/Player.hpp"
#include "../Sprite/Sprite.hpp"
#include "../Transform/Transform.hpp"

Pressure::Pressure() : Component(eComponentType::PRESSURE)
{
    mIsActive     = true;
    mPressureType = ePressureType::NONE;
}

void Pressure::Initialize(Object* object)
{
    mDelayTimer        = mPressDelay;
    mWorkTimer         = mWorkPeriod;
    mIsPressureWorking = false;
    mOwner->GetSprite()->PauseAnimation(true);

    mCollider1 = new Object(mOwner->GetName() + "_collider_1", eObjectType::STATIC);
    mCollider1->AddComponent(new Transform());
    mCollider1->AddComponent(new Collision);

    mCollider2 = new Object(mOwner->GetName() + "_collider_2", eObjectType::STATIC);
    mCollider2->AddComponent(new Transform());
    mCollider2->AddComponent(new Collision);

    if (mPressureType == ePressureType::HORIZONTAL)
    {
        // leftside collider
        mCollider1->SetPosition({mOwner->GetPosition().x - (mOwner->GetScale().y * 0.375f), mOwner->GetPosition().y});
        mCollider1->SetScale({mOwner->GetScale().y * 0.25f - 5.f, mOwner->GetScale().x - 4.f});
        // rightside collider
        mCollider2->SetPosition({mOwner->GetPosition().x + (mOwner->GetScale().x * 0.375f), mOwner->GetPosition().y});
        mCollider2->SetScale({mOwner->GetScale().y * 0.25f - 5.f, mOwner->GetScale().x - 4.f});
    }
    else if (mPressureType == ePressureType::VERTICAL)
    {
        // upside collider
        mCollider1->SetPosition({mOwner->GetPosition().x, mOwner->GetPosition().y + (mOwner->GetScale().y * 0.375f)});
        mCollider1->SetScale({mOwner->GetScale().x - 4.f, mOwner->GetScale().y * 0.25f - 5.f});
        // downside collider
        mCollider2->SetPosition({mOwner->GetPosition().x, mOwner->GetPosition().y - (mOwner->GetScale().y * 0.375f)});
        mCollider2->SetScale({mOwner->GetScale().x - 4.f, mOwner->GetScale().y * 0.25f - 5.f});
    }

    mCollider1->GetComponentByType<Collision>(eComponentType::COLLISION)->Initialize(mCollider1);
    mCollider2->GetComponentByType<Collision>(eComponentType::COLLISION)->Initialize(mCollider2);
}

void Pressure::Update(float dt)
{
    if (mIsPressureActive)
    {
        if (mIsPressureWorking)
        {
            mWorkTimer -= dt;
            UpdateColliderPosition();
            if (mWorkTimer < 0.f)
            {
                if (mOwner->GetSprite()->GetCurrentFrame() == 0)
                {
                    UnactivatePressure();
                }
            }
            else
            {
                PressingPlayer(dt, OBJECT_MANAGER->FindObjectByName("Player1"));
                PressingPlayer(dt, OBJECT_MANAGER->FindObjectByName("Player2"));
                PressingPlayer(dt, OBJECT_MANAGER->FindObjectByName("PlayerCombined"));
            }
        }
        else
        {
            if (mDelayTimer < 0.f)
            {
                mDelayTimer = mPressDelay;
                ActivatePressure();
            }
            else
            {
                mDelayTimer -= dt;
            }
        }
    }
    else
    {
        if (mOwner->GetSprite()->GetCurrentFrame() == 0)
        {
            UpdateColliderPosition();
            UnactivatePressure();
        }
    }
}

void Pressure::Delete(void)
{
}

void Pressure::UpdateColliderPosition(void)
{
    if (mPressureType == ePressureType::HORIZONTAL)
    {
        switch (mOwner->GetSprite()->GetCurrentFrame())
        {
            case 0:
            case 6:
                mCollider1->SetPosition({mOwner->GetPosition().x - (mOwner->GetScale().y * 0.375f), mOwner->GetPosition().y});
                mCollider2->SetPosition({mOwner->GetPosition().x + (mOwner->GetScale().y * 0.375f), mOwner->GetPosition().y});
                break;
            case 1:
            case 5:
                // hardcoded value
                mCollider1->SetPosition({mOwner->GetPosition().x - (mOwner->GetScale().y * (59.f / 200.f)), mOwner->GetPosition().y});
                mCollider2->SetPosition({mOwner->GetPosition().x + (mOwner->GetScale().y * (59.f / 200.f)), mOwner->GetPosition().y});
                break;
            case 2:
            case 4:
                // hardcoded value
                mCollider1->SetPosition({mOwner->GetPosition().x - (mOwner->GetScale().y * (38.f / 200.f)), mOwner->GetPosition().y});
                mCollider2->SetPosition({mOwner->GetPosition().x + (mOwner->GetScale().y * (38.f / 200.f)), mOwner->GetPosition().y});
                break;
            case 3:
                FMOD::Sound* clamperSound = FMOD_MANAGER->GetSound(1, "sound/sfx_clamper.mp3");
                FMOD::Sound* sound        = nullptr;
                FMOD_MANAGER->GetSoundEffectChannel()->getCurrentSound(&sound);

                if (sound != clamperSound)
                {
                    FMOD_MANAGER->PlaySoundEffectSound("sound/sfx_clamper.mp3");
                }
                mCollider1->SetPosition({mOwner->GetPosition().x - (mCollider1->GetScale().x / 2.f), mOwner->GetPosition().y});
                mCollider2->SetPosition({mOwner->GetPosition().x + (mCollider2->GetScale().x / 2.f), mOwner->GetPosition().y});
                break;
        }
    }
    else if (mPressureType == ePressureType::VERTICAL)
    {
        switch (mOwner->GetSprite()->GetCurrentFrame())
        {
            case 0:
            case 6:
                mCollider1->SetPosition({mOwner->GetPosition().x, mOwner->GetPosition().y - (mOwner->GetScale().y * 0.375f)});
                mCollider2->SetPosition({mOwner->GetPosition().x, mOwner->GetPosition().y + (mOwner->GetScale().y * 0.375f)});
                break;
            case 1:
            case 5:
                // hardcoded value
                mCollider1->SetPosition({mOwner->GetPosition().x, mOwner->GetPosition().y - (mOwner->GetScale().y * (59.f / 200.f))});
                mCollider2->SetPosition({mOwner->GetPosition().x, mOwner->GetPosition().y + (mOwner->GetScale().y * (59.f / 200.f))});
                break;
            case 2:
            case 4:
                // hardcoded value
                mCollider1->SetPosition({mOwner->GetPosition().x, mOwner->GetPosition().y - (mOwner->GetScale().y * (38.f / 200.f))});
                mCollider2->SetPosition({mOwner->GetPosition().x, mOwner->GetPosition().y + (mOwner->GetScale().y * (38.f / 200.f))});
                break;
            case 3:
                FMOD::Sound* clamperSound = FMOD_MANAGER->GetSound(1, "sound/sfx_clamper.mp3");
                FMOD::Sound* sound        = nullptr;
                FMOD_MANAGER->GetSoundEffectChannel()->getCurrentSound(&sound);

                if (sound != clamperSound)
                {
                    FMOD_MANAGER->PlaySoundEffectSound("sound/sfx_clamper.mp3");
                }
                mCollider1->SetPosition({mOwner->GetPosition().x, mOwner->GetPosition().y - (mCollider1->GetScale().y / 2.f)});
                mCollider2->SetPosition({mOwner->GetPosition().x, mOwner->GetPosition().y + (mCollider2->GetScale().y / 2.f)});
                break;
        }
    }
}

void Pressure::SetPressureType(ePressureType type)
{
    mPressureType = type;
}

Pressure::ePressureType Pressure::GetPressureType(void)
{
    return mPressureType;
}

void Pressure::SetIsPressureActive(bool isWorking)
{
    mIsPressureActive = isWorking;

    if (mIsPressureActive == true)
    {
        ActivatePressure();
    }
}

void Pressure::ActivatePressure()
{
    mIsPressureWorking = true;
    mOwner->GetComponentByType<Sprite>(eComponentType::SPRITE)->PauseAnimation(false);
    mWorkTimer = mWorkPeriod;
}

void Pressure::UnactivatePressure()
{
    mIsPressureWorking = false;
    mOwner->GetComponentByType<Sprite>(eComponentType::SPRITE)->PauseAnimation(true);
    mWorkTimer = mWorkPeriod;
}

void Pressure::PressingPlayer(float dt, Object* player)
{
    Collision*            playerCollision = player->GetComponentByType<Collision>(eComponentType::COLLISION);
    const Vector2<float>& playerScale     = player->GetScale();

    if (mPressureType == ePressureType::HORIZONTAL)
    {
        if(playerCollision->CheckAndPushObjectToLeftside(dt, mCollider1) || playerCollision->CheckAndPushObjectToRightside(dt, mCollider2))
        {
            switch (mOwner->GetSprite()->GetCurrentFrame())
            {
                case 1:
                case 5:
                    // hardcoded value
                    if (playerScale.x > mOwner->GetScale().y * (68.f / 200.f))
                    {
                        player->SetScale({mOwner->GetScale().y * (68.f / 200.f), playerScale.y});
                    }
                    player->SetPosition({mOwner->GetPosition().x, player->GetPosition().y});
                    break;
                case 2:
                case 4:
                    // hardcoded value
                    if (playerScale.x > mOwner->GetScale().y * (34.f / 200.f))
                    {
                        player->SetScale({mOwner->GetScale().y * (34.f / 200.f), playerScale.y});
                    }
                    player->SetPosition({mOwner->GetPosition().x, player->GetPosition().y});
                    break;
                case 3:
                    player->GetComponentByType<Player>(eComponentType::PLAYER)->SetPlayerDeath();
                    player->SetScale({playerScale.y, playerScale.y});

                    break;
            }
        }
    }
    else if (mPressureType == ePressureType::VERTICAL)
    {
        if (playerCollision->CheckIsOnObject(dt, mCollider1) || playerCollision->CheckIsUnderObject(dt, mCollider2))
        {
            switch (mOwner->GetSprite()->GetCurrentFrame())
            {
                case 1:
                case 5:
                    // hardcoded value
                    if (playerScale.y > mOwner->GetScale().y * (68.f / 200.f))
                    {
                        player->SetScale({playerScale.x, mOwner->GetScale().y * (68.f / 200.f)});
                    }
                    player->SetPosition({player->GetPosition().x, mOwner->GetPosition().y});
                    break;
                case 2:
                case 4:
                    // hardcoded value
                    if (playerScale.y > mOwner->GetScale().y * (34.f / 200.f))
                    {
                        player->SetScale({playerScale.x, mOwner->GetScale().y * (34.f / 200.f)});
                    }
                    player->SetPosition({player->GetPosition().x, mOwner->GetPosition().y});
                    break;
                case 3:
                    player->GetComponentByType<Player>(eComponentType::PLAYER)->SetPlayerDeath();
                    player->SetScale({playerScale.x, playerScale.x});
                    break;
            }
        }
    }
}

Object* Pressure::GetCollider1() const
{
    return mCollider1;
}
Object* Pressure::GetCollider2() const
{
    return mCollider2;
}

bool Pressure::GetIsPressureActive()
{
    return mIsPressureActive;
}
