/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : Component.hpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Daeun Jeong
Secondary :
- End Header ----------------------------------------------------------------*/

#pragma once

#include "ComponentType.hpp"

class Object;

class Component
{
public:
    Component(eComponentType type);
    virtual ~Component(void);

    virtual void Initialize(Object *object) = 0;
    virtual void Update(float dt)           = 0;
    virtual void Delete(void)               = 0;

    Object *GetOwner(void);
    void    SetOwner(Object *owner);

    Object *GetMover(void);
    void    SetMover(Object *owner);

    eComponentType GetType(void) const;

    bool GetIsActive(void);
    void SetIsActive(bool isActive);

protected:
    Object *mOwner    = nullptr;
    Object *mMover    = nullptr;
    bool    mIsActive = true;

private:
    eComponentType mComponentType = eComponentType::NONE;
};