/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : Player.hpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monsters
Primary   : Daeun Jeong
Secondary :
- End Header ----------------------------------------------------------------*/

#pragma once

#include "../../Component/Component.hpp"
#include "../../MathLibrary/MathLibrary.hpp"
#include "../../FMOD/FmodManager.hpp"
#include "SDL.h"

class CustomPhysics;
class MoveEffect;
class DeadEffect;
class Object;
enum class eObjectType;

enum class ePlayerState
{
    PLAYER_STATE_STAND,
    PLAYER_STATE_WALK,
    PLAYER_STATE_JUMP,
    PLAYER_STATE_COUNT
};

enum class ePlayerAction
{
    PLAYER_ACTION_NONE,
    PLAYER_ACTION_EAT,
    PLAYER_ACTION_SPIT,
    PLAYER_ACTION_COUNT,
    PLAYER_ACTION_CANNOT_DO
};

enum class eControllerDirection
{
    CONTROLLER_DIRECTION_LEFT,
    CONTROLLER_DIRECTION_RIGHT,
};

struct KeyInput
{
    SDL_Scancode mLeftKey;
    SDL_Scancode mRightKey;
    SDL_Scancode mUpKey;
    SDL_Scancode mEatKey;
    SDL_Scancode mSpitKey;
};

class Player : public Component
{
public:
    Player();
    ~Player();

    void Initialize(Object *object) override;
    void Update(float dt) override;
    void Delete(void) override;

    Object *GetEattenObject(void);
    bool    GetIsOwnerAteObject(void);
    void    SetEattenObject(Object *obj);

    eControllerDirection GetContollerDirection(void) const;
    void                 SetControllerDirection(eControllerDirection direction);
    
    bool IsPlayerAlive(void) const;
    void SetPlayerDeath(void);
    void CheckRespawn(float dt);

    void SetUIArrow(void);
    void SetInputs(eObjectType objType);

    void UpdateController(float dt);

    void Eat(float dt);
    void Spit(void);

    void UpdateAnimation(void);

    CustomPhysics* GetCustomPhysics(void);

    void SetPlayerControllerForImgui(bool controllerIsWorking);
    bool GetPlayerControllerForImgui(void);

    void PlaySFX(const std::string& file);
    void StopSFX();
    void StopSFX(FMOD::Channel* sfx);
    FMOD::Channel* GetChannelOfInhale();
    FMOD::Channel* GetChannelOfWalking();

    void SetPlayerAction(ePlayerAction action);

private:
    void SetSpawnPositionWithSpawner();
    void    FloatingEatenObject(float dt);

    bool    mIsOwnerAteObject             = false;
    bool    mIsOwnerEattenByAnotherPlayer = false;
    Object *mEattenObject;
    float   mEattenObjectValue;

    float mWalkSpeed;
    float mJumpWalkSpeed;
    float mJumpSpeed;
    float mMinJumpSpeed;
    float mMaxFallingSpeed;

    Vector2<float> mPlayerVelocity;

    KeyInput       mKeyInput;
    CustomPhysics *mCustomPhysics = nullptr;
    ePlayerState   mPlayerState   = ePlayerState::PLAYER_STATE_STAND;
    ePlayerState   mPrevState     = ePlayerState::PLAYER_STATE_STAND;

    ePlayerAction mPlayerAction = ePlayerAction::PLAYER_ACTION_NONE;
    ePlayerAction mPrevAction   = ePlayerAction::PLAYER_ACTION_NONE;

    eControllerDirection mControllerDirection = eControllerDirection::CONTROLLER_DIRECTION_LEFT;

    Vector2<float> mDrawingRange{};
    Vector2<float> mEatingRange{};

    MoveEffect *mMoveEffectEmitter = nullptr;
    DeadEffect *mDeadEffectEmitter = nullptr;

    bool        mIsPlayerAlive;
    const float mRespawnDelay = 3.f;
    float       mRespawnTime;

    bool  mCanPlayerMove;

    Object *mArrow;
    bool    mPlayerControllerForImgui = true;

    FMOD::Channel* mChannelSFX = nullptr;
    FMOD::Channel* mChannelSFXOfInhale = nullptr;
    FMOD::Sound* mSound = nullptr;
};