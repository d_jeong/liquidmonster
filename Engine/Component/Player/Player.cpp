/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : Player.cpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monsters
Primary   : Daeun Jeong
Secondary :
- End Header ----------------------------------------------------------------*/

#include "Player.hpp"
#include "../../Data/Data.hpp"
#include "../../FMOD/FmodManager.hpp"
#include "../../Graphics/Graphics.hpp"
#include "../../Graphics/Particle/DeadEffect/DeadEffect.hpp"
#include "../../Graphics/Particle/MoveEffect/MoveEffect.hpp"
#include "../../Input/Input.hpp"
#include "../../Object/Object.hpp"
#include "../../Object/ObjectDepth.hpp"
#include "../../Object/ObjectManager/ObjectManager.hpp"
#include "../../Object/ObjectType.hpp"
#include "../Collision/Collision.hpp"
#include "../Sprite/Sprite.hpp"
#include "../Transform/Transform.hpp"
#include <iostream>

#define min(a, b) (a < b) ? a : b
#define max(a, b) (a > b) ? a : b

Player::Player()
    : Component(eComponentType::PLAYER), mPlayerState(ePlayerState::PLAYER_STATE_WALK), mWalkSpeed(300.f), mJumpWalkSpeed(300.0f), mJumpSpeed(300.f), mMinJumpSpeed(280.f), mMaxFallingSpeed(-300.0f),
      mPlayerVelocity(0, 0)
{

    mChannelSFXOfInhale = FMOD_MANAGER->PlaySoundEffectForLOOP("sound/sfx_slime_inhale.mp3", mChannelSFXOfInhale);
    FMOD_MANAGER->SetChannelPaused(mChannelSFXOfInhale, true);
}

Player::~Player()
{
    StopSFX(mChannelSFX);
    StopSFX(mChannelSFXOfInhale);
}

void Player::Initialize(Object* object)
{
    mIsOwnerAteObject = false;
    mEattenObject     = nullptr;

    mCustomPhysics = mOwner->GetCustomPhysics();

    SetInputs(mOwner->GetObjectType());

    SetUIArrow();
    SetSpawnPositionWithSpawner();

    eTexture textureEnum = eTexture::PLAYER_1_PARTICLE;

    switch (object->GetObjectType())
    {
        case eObjectType::PLAYER_1:
            textureEnum   = eTexture::PLAYER_1_PARTICLE;
            mDrawingRange = {200.f, 100.f};
            mEatingRange  = {50.f, 20.f};
            break;
        case eObjectType::PLAYER_2:
            textureEnum   = eTexture::PLAYER_2_PARTICLE;
            mDrawingRange = {200.f, 100.f};
            mEatingRange  = {50.f, 20.f};
            break;
        case eObjectType::PLAYER_COMBINED:
            textureEnum   = eTexture::PLAYER_COMBINED_PARTICLE;
            mDrawingRange = {400.f, 100.f};
            mEatingRange  = {100.f, 20.f};
            mArrow        = nullptr;
            break;
    }

    mMoveEffectEmitter = new MoveEffect(object, GRAPHICS->GetTextureFromData()->FindTextureWithEnum(textureEnum));
    mDeadEffectEmitter = new DeadEffect(object, GRAPHICS->GetTextureFromData()->FindTextureWithEnum(textureEnum));

    GRAPHICS->AddParticleEmitter(mMoveEffectEmitter);
    GRAPHICS->AddParticleEmitter(mDeadEffectEmitter);

    mIsPlayerAlive = true;
}

void Player::Update(float dt)
{
    if (mIsPlayerAlive)
    {
        UpdateAnimation();
        UpdateController(dt);
    }
    else
    {
        CheckRespawn(dt);
    }

    const Vector2<float>& scale = mOwner->GetScale();
    // for case if player is pressed by pressure and no longer pressed, recover its original scale
    if (scale.y < scale.x)
    {
        Vector2<float> newScale = {scale.x, min(scale.x, scale.y + 20.f * dt)};
        mOwner->SetScale(newScale);
    }
    if (scale.x < scale.y)
    {
        Vector2<float> newScale = {min(scale.y, scale.x + 20.f * dt), scale.y};
        mOwner->SetScale(newScale);
    }

    if (mIsOwnerAteObject)
    {
        FloatingEatenObject(dt);
    }
}

void Player::Delete(void)
{
}

bool Player::IsPlayerAlive(void) const
{
    return mIsPlayerAlive;
}

void Player::SetPlayerDeath(void)
{
    if (mOwner->GetObjectType() != eObjectType::PLAYER_COMBINED)
    {
        mDeadEffectEmitter->GenerateEffect();

        StopSFX();
        FMOD_MANAGER->SetChannelPaused(mChannelSFXOfInhale, true);

        FMOD_MANAGER->PlaySoundEffectSound("sound/sfx_slime_death.mp3");

        mIsPlayerAlive = false;
        mRespawnTime   = mRespawnDelay;

        mOwner->GetSprite()->SetInVisible();
        mOwner->SetVelocity({0.f, 0.f});
        mArrow->GetSprite()->SetInVisible();

        if (GetIsOwnerAteObject())
        {
            Object* obj = GetEattenObject();
            SetEattenObject(nullptr);
            obj->SetParent(nullptr);
            obj->SetScale({obj->GetScale() * 1.25f});
            obj->SetPosition(obj->GetSpawnPosition());
        }

        mOwner->SetPosition(mOwner->GetSpawnPosition());
    }
    else // When combined player is dead
    {
        mDeadEffectEmitter->GenerateEffect();

        // SOUNDMANAGER->StopSoundEffectSound();
        StopSFX();
        FMOD_MANAGER->SetChannelPaused(mChannelSFXOfInhale, true);

        FMOD_MANAGER->PlaySoundEffectSound("sound/sfx_slime_death.mp3");

        Object* player1 = OBJECT_MANAGER->FindObjectByName("Player1");
        Object* player2 = OBJECT_MANAGER->FindObjectByName("Player2");
        Object* p1Arrow = OBJECT_MANAGER->FindObjectByName("Arrow1");
        Object* p2Arrow = OBJECT_MANAGER->FindObjectByName("Arrow2");

        player1->GetComponentByType<Player>(eComponentType::PLAYER)->mIsPlayerAlive = false;
        player1->GetComponentByType<Player>(eComponentType::PLAYER)->mRespawnTime   = mRespawnDelay;
        player1->GetSprite()->SetInVisible();
        player1->SetVelocity({0.f, 0.f});

        player2->GetComponentByType<Player>(eComponentType::PLAYER)->mIsPlayerAlive = false;
        player2->GetComponentByType<Player>(eComponentType::PLAYER)->mRespawnTime   = mRespawnDelay;
        player2->GetSprite()->SetInVisible();
        player2->SetVelocity({0.f, 0.f});

        if (mArrow != nullptr)
        {
            mArrow->GetSprite()->SetInVisible();
            mArrow->SetParent(nullptr);
            mArrow = nullptr;
        }

        if (p1Arrow->GetIsActive() == true)
        {
            p2Arrow->SetIsActive(true);
            player1->SetIsActive(true);
            player2->SetIsActive(true);
            player2->GetComponentByType<Player>(eComponentType::PLAYER)->SetUIArrow();
            player1->GetComponentByType<Player>(eComponentType::PLAYER)->SetUIArrow();
        }
        else if (p2Arrow->GetIsActive() == true)
        {
            p1Arrow->SetIsActive(true);
            player1->SetIsActive(true);
            player2->SetIsActive(true);
            player1->GetComponentByType<Player>(eComponentType::PLAYER)->SetUIArrow();
            player2->GetComponentByType<Player>(eComponentType::PLAYER)->SetUIArrow();
        }

        if (GetIsOwnerAteObject())
        {
            Object* obj = GetEattenObject();
            SetEattenObject(nullptr);
            obj->SetParent(nullptr);
            obj->SetScale({obj->GetScale() * 1.25f});
            obj->SetPosition(obj->GetSpawnPosition());
        }

        mOwner->SetIsActive(false);
        mOwner->SetPosition({-5000.f, -5000.f});
    }
}

void Player::CheckRespawn(float dt)
{
    if (!mIsPlayerAlive)
    {
        if (mRespawnTime > 0.f)
        {
            mRespawnTime -= dt;
        }
        else if (mRespawnTime < 0.f && !mIsPlayerAlive)
        {
            // respawn player
            mIsPlayerAlive = true;
            mCanPlayerMove = false;

            mOwner->GetSprite()->SetVisible();
            mArrow->GetSprite()->SetVisible();

            FMOD_MANAGER->PlaySoundEffectSound("sound/sfx_spawner.mp3");
            Vector2<float> playerPosition;
            playerPosition = mOwner->GetSpawnPosition();
            mOwner->SetPosition(playerPosition);
        }
    }
}

void Player::SetUIArrow(void)
{
    auto player1        = OBJECT_MANAGER->FindObjectByName("Player1");
    auto player2        = OBJECT_MANAGER->FindObjectByName("Player2");
    auto combinedPlayer = OBJECT_MANAGER->FindObjectByName("PlayerCombined");

    if (mOwner->GetObjectType() == eObjectType::PLAYER_1)
    {
        auto p1Arrow = OBJECT_MANAGER->FindObjectByName("Arrow1");
        p1Arrow->SetParent(mOwner->GetTransform());
        p1Arrow->SetPosition(Vector2<float>(0.f, 1.f));
        mArrow = p1Arrow;
    }
    else if (mOwner->GetObjectType() == eObjectType::PLAYER_2)
    {
        auto p2Arrow = OBJECT_MANAGER->FindObjectByName("Arrow2");
        p2Arrow->SetParent(mOwner->GetTransform());
        p2Arrow->SetPosition(Vector2<float>(0.f, 1.f));
        mArrow = p2Arrow;
    }
    else if (mOwner->GetObjectType() == eObjectType::PLAYER_COMBINED)
    {
        auto p1Arrow = OBJECT_MANAGER->FindObjectByName("Arrow1");
        auto p2Arrow = OBJECT_MANAGER->FindObjectByName("Arrow2");

        if (p1Arrow->GetIsActive() == true && p2Arrow->GetIsActive() == false)
        {
            p1Arrow->SetParent(mOwner->GetTransform());
            mArrow = p1Arrow;
        }
        else if (p1Arrow->GetIsActive() == false && p2Arrow->GetIsActive() == true)
        {
            p2Arrow->SetParent(mOwner->GetTransform());
            mArrow = p2Arrow;
        }
    }
}

void Player::SetInputs(eObjectType objType)
{
    switch (objType)
    {
        case eObjectType::PLAYER_1:
        {
            mKeyInput.mLeftKey  = SDL_SCANCODE_LEFT;
            mKeyInput.mRightKey = SDL_SCANCODE_RIGHT;
            mKeyInput.mUpKey    = SDL_SCANCODE_UP;
            mKeyInput.mEatKey   = SDL_SCANCODE_DOWN;
            mKeyInput.mSpitKey  = SDL_SCANCODE_RSHIFT;
            break;
        }
        case eObjectType::PLAYER_2:
        {
            mKeyInput.mLeftKey  = SDL_SCANCODE_A;
            mKeyInput.mRightKey = SDL_SCANCODE_D;
            mKeyInput.mUpKey    = SDL_SCANCODE_W;
            mKeyInput.mEatKey   = SDL_SCANCODE_S;
            mKeyInput.mSpitKey  = SDL_SCANCODE_LSHIFT;
            break;
        }
        case eObjectType::PLAYER_COMBINED:
        {
            auto p1Arrow = OBJECT_MANAGER->FindObjectByName("Arrow1");
            auto p2Arrow = OBJECT_MANAGER->FindObjectByName("Arrow2");

            if (p1Arrow->GetIsActive() == true && p2Arrow->GetIsActive() == false)
            {
                mKeyInput.mLeftKey  = SDL_SCANCODE_LEFT;
                mKeyInput.mRightKey = SDL_SCANCODE_RIGHT;
                mKeyInput.mUpKey    = SDL_SCANCODE_UP;
                mKeyInput.mEatKey   = SDL_SCANCODE_DOWN;
                mKeyInput.mSpitKey  = SDL_SCANCODE_M;
            }
            else if (p1Arrow->GetIsActive() == false && p2Arrow->GetIsActive() == true)
            {
                mKeyInput.mLeftKey  = SDL_SCANCODE_A;
                mKeyInput.mRightKey = SDL_SCANCODE_D;
                mKeyInput.mUpKey    = SDL_SCANCODE_W;
                mKeyInput.mEatKey   = SDL_SCANCODE_S;
                mKeyInput.mSpitKey  = SDL_SCANCODE_G;
            }
        }
        default:
            break;
    }
}

void Player::Eat(float dt)
{
    auto    p1Arrow        = OBJECT_MANAGER->FindObjectByName("Arrow1");
    auto    p2Arrow        = OBJECT_MANAGER->FindObjectByName("Arrow2");
    Object* combinedPlayer = OBJECT_MANAGER->FindObjectByName("PlayerCombined");

    if (OBJECT_MANAGER->GetObjectVector().empty() == false)
    {
        for (auto objectToCheck : OBJECT_MANAGER->GetObjectVector())
        {
            // skip about itself
            if (objectToCheck == mOwner)
                continue;

            eObjectType objType = objectToCheck->GetObjectType();

            if (objType == eObjectType::PLAYER_1 || objType == eObjectType::PLAYER_2 || objType == eObjectType::EATABLE || objType == eObjectType::BATTERY)
            {
                Vector2<float> objPos   = objectToCheck->GetPosition();
                Vector2<float> ownerPos = mOwner->GetPosition();

                Vector2<float> eattingRange = (objType == eObjectType::PLAYER_1 || objType == eObjectType::PLAYER_2) ? mEatingRange + Vector2<float>{50.f, 0.f} : mEatingRange;

                // 1. is it in eating range?
                if (std::abs(objPos.x - ownerPos.x) < eattingRange.x && std::abs(objPos.y - ownerPos.y) < eattingRange.y)
                {
                    // SOUNDMANAGER->StopSoundEffectSound();

                    // if it almost collide?
                    if (std::abs(objPos.x - ownerPos.x) < 20.f)
                    {
                        // eating object
                        if (mEattenObject == nullptr && (objType == eObjectType::EATABLE || objType == eObjectType::BATTERY || (mOwner->GetObjectType() == eObjectType::PLAYER_COMBINED)))
                        {
                            mEattenObject = objectToCheck;
                            GRAPHICS->ReplaceSpriteWithNewDepth(mEattenObject, GAME_EATEN_OBJECT);
                            objectToCheck->SetParent(mOwner->GetTransform());
                            objectToCheck->SetPosition({0.f, 0.f});
                            objectToCheck->SetVelocity({0.f, 0.f});
                            mIsOwnerAteObject = true;

                            mEattenObjectValue = 0.f;
                            mEattenObject->SetScale({mEattenObject->GetScale() * 0.8f});
                            StopSFX();
                            FMOD_MANAGER->SetChannelPaused(mChannelSFXOfInhale, true);

                            FMOD_MANAGER->PlaySoundEffectSound("sound/sfx_slime_eat.mp3");
                            return;
                        }
                        // eating player
                        else
                        {
                            // in case if opponent has eatable object
                            Player* opponentComp = objectToCheck->GetComponentByType<Player>(eComponentType::PLAYER);
                            opponentComp->StopSFX();
                            FMOD_MANAGER->SetChannelPaused(opponentComp->GetChannelOfInhale(), true);
                            FMOD_MANAGER->SetChannelPaused(GetChannelOfInhale(), true);

                            if (opponentComp->GetIsOwnerAteObject())
                            {
                                Object* eattenObject = opponentComp->GetEattenObject();
                                combinedPlayer->GetComponentByType<Player>(eComponentType::PLAYER)->SetEattenObject(eattenObject);
                                opponentComp->SetEattenObject(nullptr);
                                eattenObject->SetParent(combinedPlayer->GetTransform());
                            }

                            if (objType == eObjectType::PLAYER_1)
                            {
                                p1Arrow->SetIsActive(false);
                            }
                            else
                            {
                                p2Arrow->SetIsActive(false);
                            }
                            combinedPlayer->SetIsActive(true);
                            combinedPlayer->GetComponentByType<Player>(eComponentType::PLAYER)->SetInputs(mOwner->GetObjectType());
                            combinedPlayer->GetComponentByType<Player>(eComponentType::PLAYER)->SetUIArrow();
                            combinedPlayer->GetComponentByType<Player>(eComponentType::PLAYER)
                                ->SetControllerDirection(mOwner->GetComponentByType<Player>(eComponentType::PLAYER)->GetContollerDirection());
                            combinedPlayer->SetPosition(mOwner->GetPosition() + Vector2<float>{0.f, 25.f});
                            mOwner->SetIsActive(false);
                            objectToCheck->SetIsActive(false);
                            StopSFX();
                            // FMOD_MANAGER->SetChannelPaused(mChannelSFXOfInhale, true);
                            FMOD_MANAGER->PlaySoundEffectSound("sound/sfx_slime_eat.mp3");
                            return;
                        }
                    }

                    // is it on right side and player is looking right side?
                    if (objPos.x > ownerPos.x && mControllerDirection == eControllerDirection::CONTROLLER_DIRECTION_RIGHT)
                    {
                        // eating object
                        if (mEattenObject == nullptr && (objType == eObjectType::EATABLE || objType == eObjectType::BATTERY || (mOwner->GetObjectType() == eObjectType::PLAYER_COMBINED)))
                        {
                            mEattenObject = objectToCheck;
                            GRAPHICS->ReplaceSpriteWithNewDepth(mEattenObject, GAME_EATEN_OBJECT);
                            objectToCheck->SetParent(mOwner->GetTransform());
                            objectToCheck->SetPosition({0.f, 0.f});
                            objectToCheck->SetVelocity({0.f, 0.f});
                            mIsOwnerAteObject = true;

                            mEattenObjectValue = 0.f;
                            mEattenObject->SetScale({mEattenObject->GetScale() * 0.8f});
                            StopSFX();
                            FMOD_MANAGER->SetChannelPaused(mChannelSFXOfInhale, true);
                            FMOD_MANAGER->PlaySoundEffectSound("sound/sfx_slime_eat.mp3");
                            return;
                        }
                        // eating player
                        else
                        {
                            // in case if opponent has eatable object
                            Player* opponentComp = objectToCheck->GetComponentByType<Player>(eComponentType::PLAYER);
                            opponentComp->StopSFX();
                            FMOD_MANAGER->SetChannelPaused(opponentComp->GetChannelOfInhale(), true);
                            FMOD_MANAGER->SetChannelPaused(GetChannelOfInhale(), true);

                            if (opponentComp->GetIsOwnerAteObject())
                            {
                                Object* eattenObject = opponentComp->GetEattenObject();
                                combinedPlayer->GetComponentByType<Player>(eComponentType::PLAYER)->SetEattenObject(eattenObject);
                                opponentComp->SetEattenObject(nullptr);
                                eattenObject->SetParent(combinedPlayer->GetTransform());
                            }

                            if (objType == eObjectType::PLAYER_1)
                            {
                                p1Arrow->SetIsActive(false);
                            }
                            else
                            {
                                p2Arrow->SetIsActive(false);
                            }
                            combinedPlayer->SetIsActive(true);
                            combinedPlayer->GetComponentByType<Player>(eComponentType::PLAYER)->SetInputs(mOwner->GetObjectType());
                            combinedPlayer->GetComponentByType<Player>(eComponentType::PLAYER)->SetUIArrow();
                            combinedPlayer->GetComponentByType<Player>(eComponentType::PLAYER)
                                ->SetControllerDirection(mOwner->GetComponentByType<Player>(eComponentType::PLAYER)->GetContollerDirection());
                            combinedPlayer->SetPosition(mOwner->GetPosition() + Vector2<float>{0.f, 25.f});
                            mOwner->SetIsActive(false);
                            objectToCheck->SetIsActive(false);
                            StopSFX();
                            // FMOD_MANAGER->SetChannelPaused(mChannelSFXOfInhale, true);
                            FMOD_MANAGER->PlaySoundEffectSound("sound/sfx_slime_eat.mp3");
                            return;
                        }
                    }

                    // is it on left side and player is looking left side?
                    if (objPos.x < ownerPos.x && mControllerDirection == eControllerDirection::CONTROLLER_DIRECTION_LEFT)
                    {
                        // eating object
                        if (objType == eObjectType::EATABLE || objType == eObjectType::BATTERY || (mOwner->GetObjectType() == eObjectType::PLAYER_COMBINED))
                        {
                            mEattenObject = objectToCheck;
                            GRAPHICS->ReplaceSpriteWithNewDepth(mEattenObject, GAME_EATEN_OBJECT);
                            objectToCheck->SetParent(mOwner->GetTransform());
                            objectToCheck->SetPosition({0.f, 0.f});
                            objectToCheck->SetVelocity({0.f, 0.f});
                            mIsOwnerAteObject = true;

                            mEattenObjectValue = 0.f;
                            mEattenObject->SetScale({mEattenObject->GetScale() * 0.8f});
                            StopSFX();
                            FMOD_MANAGER->SetChannelPaused(mChannelSFXOfInhale, true);
                            FMOD_MANAGER->PlaySoundEffectSound("sound/sfx_slime_eat.mp3");
                            return;
                        }
                        // eating player
                        else
                        {
                            // in case if opponent has eatable object
                            Player* opponentComp = objectToCheck->GetComponentByType<Player>(eComponentType::PLAYER);
                            opponentComp->StopSFX();
                            FMOD_MANAGER->SetChannelPaused(opponentComp->GetChannelOfInhale(), true);
                            FMOD_MANAGER->SetChannelPaused(GetChannelOfInhale(), true);

                            if (opponentComp->GetIsOwnerAteObject())
                            {
                                Object* eattenObject = opponentComp->GetEattenObject();
                                combinedPlayer->GetComponentByType<Player>(eComponentType::PLAYER)->SetEattenObject(eattenObject);
                                opponentComp->SetEattenObject(nullptr);
                                eattenObject->SetParent(combinedPlayer->GetTransform());
                            }

                            if (objType == eObjectType::PLAYER_1)
                            {
                                p1Arrow->SetIsActive(false);
                            }
                            else
                            {
                                p2Arrow->SetIsActive(false);
                            }

                            combinedPlayer->SetIsActive(true);
                            combinedPlayer->GetComponentByType<Player>(eComponentType::PLAYER)->SetInputs(mOwner->GetObjectType());
                            combinedPlayer->GetComponentByType<Player>(eComponentType::PLAYER)->SetUIArrow();
                            combinedPlayer->GetComponentByType<Player>(eComponentType::PLAYER)
                                ->SetControllerDirection(mOwner->GetComponentByType<Player>(eComponentType::PLAYER)->GetContollerDirection());
                            combinedPlayer->SetPosition(mOwner->GetPosition() + Vector2<float>{0.f, 25.f});
                            combinedPlayer->SetMass(3.f);
                            mOwner->SetIsActive(false);
                            objectToCheck->SetIsActive(false);
                            StopSFX();
                            // FMOD_MANAGER->SetChannelPaused(mChannelSFXOfInhale, true);
                            FMOD_MANAGER->PlaySoundEffectSound("sound/sfx_slime_eat.mp3");
                            return;
                        }
                    }
                }
                // 2. it is in drawing range?
                else if ((objType == eObjectType::EATABLE || objType == eObjectType::BATTERY || (mOwner->GetObjectType() == eObjectType::PLAYER_COMBINED)) &&
                         (std::abs(objPos.x - ownerPos.x) < mDrawingRange.x && std::abs(objPos.y - ownerPos.y) < mDrawingRange.y))
                {
                    // is it on right side and player is looking right side?
                    if (objPos.x > ownerPos.x && mControllerDirection == eControllerDirection::CONTROLLER_DIRECTION_RIGHT)
                    {
                        // if object is on upside of player
                        if (objPos.y > ownerPos.y)
                        {
                            objectToCheck->SetVelocity(Vector2<float>{-200.f, -100.f});
                        }
                        // if object is on downside of player
                        else
                        {
                            objectToCheck->SetVelocity(Vector2<float>{-200.f, 100.f});
                        }
                    }

                    // is it on left side and player is looking left side?
                    if (objPos.x < ownerPos.x && mControllerDirection == eControllerDirection::CONTROLLER_DIRECTION_LEFT)
                    {
                        // if object is on upside of player
                        if (objPos.y > ownerPos.y)
                        {
                            objectToCheck->SetVelocity(Vector2<float>{200.f, -100.f});
                        }
                        // if object is on downside of player
                        else
                        {
                            objectToCheck->SetVelocity(Vector2<float>{200.f, 100.f});
                        }
                    }
                }
            }
        }
    }
}

void Player::Spit(void)
{
    auto p1Arrow = OBJECT_MANAGER->FindObjectByName("Arrow1");
    auto p2Arrow = OBJECT_MANAGER->FindObjectByName("Arrow2");

    Object* player1 = OBJECT_MANAGER->FindObjectByName("Player1");
    Object* player2 = OBJECT_MANAGER->FindObjectByName("Player2");

    // spitting eatable object
    if (mEattenObject)
    {
        mEattenObject->SetParent(nullptr);
        float posOffset = 0.f;
        if (mOwner->GetObjectType() == eObjectType::PLAYER_COMBINED)
        {
            posOffset = 50.f;
        }
        if (mControllerDirection == eControllerDirection::CONTROLLER_DIRECTION_RIGHT)
        {
            mEattenObject->SetVelocity({400.f, 0.f});
        }
        else
        {
            mEattenObject->SetVelocity({-400.f, 0.f});
            posOffset = -posOffset;
        }
        mEattenObject->SetPosition(mOwner->GetPosition() + Vector2<float>{posOffset, 0.f});
        mEattenObject->SetScale({mEattenObject->GetScale() * 1.25f});
        GRAPHICS->ReplaceSpriteWithNewDepth(mEattenObject, GAME_UI);
        mEattenObject     = nullptr;
        mIsOwnerAteObject = false;

        if (mOwner->GetObjectType() == eObjectType::PLAYER_1 || mOwner->GetObjectType() == eObjectType::PLAYER_2)
        {
            FMOD_MANAGER->PlaySoundEffectSound("sound/sfx_slime_small_spit_out.mp3");
        }
        else if (mOwner->GetObjectType() == eObjectType::PLAYER_COMBINED)
        {
            FMOD_MANAGER->PlaySoundEffectSound("sound/sfx_slime_combined_spit_out.mp3");
        }
    }
    // spitting other player
    else
    {
        if (mOwner->GetObjectType() != eObjectType::PLAYER_COMBINED)
            return;

        Object* eater    = nullptr;
        Object* opponent = nullptr;

        FMOD_MANAGER->PlaySoundEffectSound("sound/sfx_slime_squish.mp3");
        // in case first player ate second player
        if (p1Arrow->GetIsActive() == true)
        {
            eater    = player1;
            opponent = player2;
            p2Arrow->SetIsActive(true);
            opponent->GetComponentByType<Player>(eComponentType::PLAYER)->SetUIArrow();
            eater->GetComponentByType<Player>(eComponentType::PLAYER)->SetUIArrow();
        }
        // in case second player ate first player
        else if (p2Arrow->GetIsActive() == true)
        {
            eater    = player2;
            opponent = player1;
            p1Arrow->SetIsActive(true);
            opponent->GetComponentByType<Player>(eComponentType::PLAYER)->SetUIArrow();
            eater->GetComponentByType<Player>(eComponentType::PLAYER)->SetUIArrow();
        }

        // in case both are not detected  -> sometime happen, don't know why
        if (eater == nullptr || opponent == nullptr)
            return;

        Vector2<float> pos = mOwner->GetPosition();
        eater->SetIsActive(true);
        opponent->SetIsActive(true);

        eater->SetPosition(pos);

        // spit to right
        if (mControllerDirection == eControllerDirection::CONTROLLER_DIRECTION_RIGHT)
        {
            opponent->SetPosition(pos + Vector2<float>{20.f, 0.f});
            opponent->SetVelocity({300.f, 50.f});
        }
        // spit to left
        else
        {
            opponent->SetPosition(pos + Vector2<float>{-20.f, 0.f});
            opponent->SetVelocity({-300.f, 50.f});
        }
        mOwner->SetIsActive(false);
        StopSFX();
    }
}

void Player::UpdateAnimation(void)
{
    if (mPlayerAction != mPrevAction)
    {
        switch (mOwner->GetObjectType())
        {
            case eObjectType::PLAYER_1:
            {
                if (mPlayerAction == ePlayerAction::PLAYER_ACTION_EAT)
                {
                    // mOwner->GetSprite()->LoadTexture("texture/player1EatingAnimation.png");
                    mOwner->GetSprite()->LoadTextureFromData(eTexture::PLAYER_1_EATING_ANIMATION);
                    mOwner->GetSprite()->SetAnimation(true);
                    mOwner->GetSprite()->SetAnimationFrame(6);
                }
                else if (mPlayerAction == ePlayerAction::PLAYER_ACTION_SPIT)
                {
                    mOwner->GetSprite()->LoadTextureFromData(eTexture::PLAYER_1_SPIT);
                    mOwner->GetSprite()->SetAnimation(false);
                }
                else if (mPlayerAction == ePlayerAction::PLAYER_ACTION_CANNOT_DO)
                {
                    mOwner->GetSprite()->LoadTextureFromData(eTexture::PLAYER_1_ANIMATION_CANNOT_DO);
                    mOwner->GetSprite()->SetAnimation(true);
                    mOwner->GetSprite()->SetAnimationFrame(6);
                    mOwner->GetSprite()->SetAnimationSpeed(5.0f);
                }
                else
                {
                    // mOwner->GetSprite()->LoadTexture("texture/player1Animation.png");
                    mOwner->GetSprite()->LoadTextureFromData(eTexture::PLAYER_1_ANIMATION);
                    mOwner->GetSprite()->SetAnimation(true);

                    mOwner->GetSprite()->SetAnimationFrame(6);
                    mOwner->GetSprite()->SetAnimationSpeed(2.0f);
                }
                break;
            }
            case eObjectType::PLAYER_2:
            {
                if (mPlayerAction == ePlayerAction::PLAYER_ACTION_EAT)
                {
                    // mOwner->GetSprite()->LoadTexture("texture/player2EatingAnimation.png");
                    mOwner->GetSprite()->LoadTextureFromData(eTexture::PLAYER_2_EATING_ANIMATION);
                    mOwner->GetSprite()->SetAnimation(true);
                    mOwner->GetSprite()->SetAnimationFrame(6);
                    mOwner->GetSprite()->SetAnimationSpeed(2.0f);
                }
                else if (mPlayerAction == ePlayerAction::PLAYER_ACTION_SPIT)
                {
                    mOwner->GetSprite()->LoadTextureFromData(eTexture::PLAYER_2_SPIT);
                    mOwner->GetSprite()->SetAnimation(false);
                }
                else if (mPlayerAction == ePlayerAction::PLAYER_ACTION_CANNOT_DO)
                {
                    mOwner->GetSprite()->LoadTextureFromData(eTexture::PLAYER_2_ANIMATION_CANNOT_DO);
                    mOwner->GetSprite()->SetAnimation(true);
                    mOwner->GetSprite()->SetAnimationFrame(6);
                    mOwner->GetSprite()->SetAnimationSpeed(5.0f);
                }
                else
                {
                    // mOwner->GetSprite()->LoadTexture("texture/player2Animation.png");
                    mOwner->GetSprite()->LoadTextureFromData(eTexture::PLAYER_2_ANIMATION);
                    mOwner->GetSprite()->SetAnimation(true);
                    mOwner->GetSprite()->SetAnimationFrame(6);
                    mOwner->GetSprite()->SetAnimationSpeed(2.0f);
                }
                break;
            }
            case eObjectType::PLAYER_COMBINED:
            {
                if (mPlayerAction == ePlayerAction::PLAYER_ACTION_EAT)
                {
                    // mOwner->GetSprite()->LoadTexture("texture/combinedEatingAnimation.png");
                    mOwner->GetSprite()->LoadTextureFromData(eTexture::PLAYER_COMBINED_EATING_ANIMATION);
                    mOwner->GetSprite()->SetAnimation(true);
                    mOwner->GetSprite()->SetAnimationFrame(6);
                    mOwner->GetSprite()->SetAnimationSpeed(2.0f);
                }
                else if (mPlayerAction == ePlayerAction::PLAYER_ACTION_SPIT)
                {
                    mOwner->GetSprite()->LoadTextureFromData(eTexture::PLAYER_COMBINED_SPIT);
                    mOwner->GetSprite()->SetAnimation(false);
                }
                else if (mPlayerAction == ePlayerAction::PLAYER_ACTION_CANNOT_DO)
                {
                    mOwner->GetSprite()->LoadTextureFromData(eTexture::PLAYER_COMBINED_ANIMATION_CANNOT_DO);
                    mOwner->GetSprite()->SetAnimation(true);
                    mOwner->GetSprite()->SetAnimationFrame(6);
                    mOwner->GetSprite()->SetAnimationSpeed(4.0f);
                }
                else
                {
                    // mOwner->GetSprite()->LoadTexture("texture/combinedAnimation.png");
                    mOwner->GetSprite()->LoadTextureFromData(eTexture::PLAYER_COMBINED_ANIMATION);
                    mOwner->GetSprite()->SetAnimation(true);
                    mOwner->GetSprite()->SetAnimationFrame(6);
                    mOwner->GetSprite()->SetAnimationSpeed(2.0f);
                }
                break;
            }
        }
        mPrevAction = mPlayerAction;
    }
}

CustomPhysics* Player::GetCustomPhysics(void)
{
    return mCustomPhysics;
}

void Player::SetPlayerControllerForImgui(bool controllerIsWorking)
{
    mPlayerControllerForImgui = controllerIsWorking;
}

bool Player::GetPlayerControllerForImgui(void)
{
    return mPlayerControllerForImgui;
}

void Player::PlaySFX(const std::string& file)
{
    mChannelSFX = FMOD_MANAGER->PlaySoundEffectForLOOP(file, mChannelSFX);
}

void Player::StopSFX()
{
    mChannelSFX->stop();
}

void Player::StopSFX(FMOD::Channel* sfx)
{
    sfx->stop();
}

FMOD::Channel* Player::GetChannelOfInhale()
{
    return mChannelSFXOfInhale;
}

FMOD::Channel* Player::GetChannelOfWalking()
{
    return mChannelSFX;
}

void Player::SetPlayerAction(ePlayerAction action)
{
    mPlayerAction = action;
}

void Player::UpdateController(float dt)
{
    bool isOnGround = mOwner->GetComponentByType<Collision>(eComponentType::COLLISION)->CanItJump();

    if (mCanPlayerMove == false)
    {
        if (isOnGround == true)
        {
            mCanPlayerMove = true;
        }
        return;
    }

    if (mPlayerControllerForImgui)
    {
        if (Input::IsKeyTriggered(mKeyInput.mEatKey) && mEattenObject == nullptr)
        {
            StopSFX();
            FMOD_MANAGER->SetChannelPaused(mChannelSFXOfInhale, false);
        }

        if (Input::IsKeyReleased(mKeyInput.mEatKey))
        {
            StopSFX();
            FMOD_MANAGER->SetChannelPaused(mChannelSFXOfInhale, true);
        }

        if (Input::IsKeyPressed(mKeyInput.mEatKey) && mEattenObject == nullptr)
        {
            mPlayerAction = ePlayerAction::PLAYER_ACTION_EAT;
            Eat(dt);
        }
        else if (Input::IsKeyTriggered(mKeyInput.mSpitKey))
        {
            if (mEattenObject != nullptr || mOwner->GetObjectType() == eObjectType::PLAYER_COMBINED)
            {
                Spit();
                StopSFX();
                return;
            }
        }
        else if (Input::IsKeyPressed(mKeyInput.mSpitKey))
        {
            mPlayerAction = ePlayerAction::PLAYER_ACTION_SPIT;
        }
        else
        {
            mPlayerAction = ePlayerAction::PLAYER_ACTION_NONE;
        }

        if (Input::IsKeyPressed(mKeyInput.mUpKey))
        {
            if (mPlayerState == ePlayerState::PLAYER_STATE_STAND || mPlayerState == ePlayerState::PLAYER_STATE_WALK)
            {
                if (isOnGround && mOwner->GetObjectType() == eObjectType::PLAYER_COMBINED)
                {
                    mPlayerAction = ePlayerAction::PLAYER_ACTION_CANNOT_DO;
                }
            }
        }

        Vector2<float> newVelocity = mOwner->GetVelocity();

        switch (mPlayerState)
        {
            case ePlayerState::PLAYER_STATE_STAND:
            {
                if (isOnGround == false)
                {
                    mPlayerState = ePlayerState::PLAYER_STATE_JUMP;
                    break;
                }

                // If left or right key is pressed, but not both
                if (Input::IsKeyPressed(mKeyInput.mLeftKey) != Input::IsKeyPressed(mKeyInput.mRightKey))
                {
                    if (mPlayerAction == ePlayerAction::PLAYER_ACTION_EAT)
                        break;
                    mPlayerState = ePlayerState::PLAYER_STATE_WALK;
                    break;
                }
                else if (Input::IsKeyTriggered(mKeyInput.mUpKey))
                {
                    if (isOnGround && mOwner->GetObjectType() != eObjectType::PLAYER_COMBINED)
                    {
                        newVelocity.y = mJumpSpeed - mOwner->GetMass() * 40.f;
                        mPlayerState  = ePlayerState::PLAYER_STATE_JUMP;
                        FMOD_MANAGER->PlaySoundEffectSound("sound/sfx_slime_jump.mp3");
                    }

                    break;
                }
            }
            break;
            case ePlayerState::PLAYER_STATE_WALK:
            {
                if (isOnGround == false)
                {
                    mPlayerState = ePlayerState::PLAYER_STATE_JUMP;
                    StopSFX();
                    // FMOD_MANAGER->PlaySoundEffectSound("sound/sfx_slime_jump.mp3");
                    break;
                }

                if (mPlayerAction == ePlayerAction::PLAYER_ACTION_EAT)
                {
                    mPlayerState  = ePlayerState::PLAYER_STATE_STAND;
                    newVelocity.x = 0.f;
                    break;
                }

                // If both or neither left nor right keys are pressed then stop walking and stand
                if (Input::IsKeyPressed(mKeyInput.mRightKey) == Input::IsKeyPressed(mKeyInput.mLeftKey))
                {
                    mPlayerState  = ePlayerState::PLAYER_STATE_STAND;
                    newVelocity.x = 0.f;
                    break;
                }
                else if (Input::IsKeyPressed(mKeyInput.mRightKey))
                {
                    if (mPlayerAction == ePlayerAction::PLAYER_ACTION_EAT)
                        break;
                    newVelocity.x = mWalkSpeed - mOwner->GetMass() * 40.f;
                }
                else if (Input::IsKeyPressed(mKeyInput.mLeftKey))
                {
                    if (mPlayerAction == ePlayerAction::PLAYER_ACTION_EAT)
                        break;
                    newVelocity.x = -(mWalkSpeed - mOwner->GetMass() * 40.f);
                }
                else
                {
                    mPlayerState  = ePlayerState::PLAYER_STATE_STAND;
                    newVelocity.x = 0.f;
                }

                if (Input::IsKeyTriggered(mKeyInput.mUpKey))
                {
                    if (mPlayerAction == ePlayerAction::PLAYER_ACTION_EAT)
                        break;

                    if (isOnGround && mOwner->GetObjectType() != eObjectType::PLAYER_COMBINED)
                    {
                        newVelocity.y = mJumpSpeed - mOwner->GetMass() * 40.f;
                        mPlayerState  = ePlayerState::PLAYER_STATE_JUMP;
                        FMOD_MANAGER->PlaySoundEffectSound("sound/sfx_slime_jump.mp3");
                        break;
                    }
                }
            }
            break;
            case ePlayerState::PLAYER_STATE_JUMP:
            {
                // newVelocity.y = std::fmax(newVelocity.y, mMaxFallingSpeed);

                if (newVelocity.y > 0)
                {
                    if (newVelocity.y < 500.f && newVelocity.y > mJumpSpeed)
                    {
                        newVelocity.y = mJumpSpeed;
                    }
                }
                // If both or neither left nor right keys are pressed then stop walking and stand
                if (Input::IsKeyPressed(mKeyInput.mRightKey) == Input::IsKeyPressed(mKeyInput.mLeftKey))
                {
                    newVelocity.x = 0.f;
                }
                else if (Input::IsKeyPressed(mKeyInput.mRightKey))
                {
                    if (mPlayerAction == ePlayerAction::PLAYER_ACTION_EAT)
                        break;

                    newVelocity.x = mJumpWalkSpeed - mOwner->GetMass() * 40.f;
                }
                else if (Input::IsKeyPressed(mKeyInput.mLeftKey))
                {
                    if (mPlayerAction == ePlayerAction::PLAYER_ACTION_EAT)
                        break;

                    newVelocity.x = -(mJumpWalkSpeed - mOwner->GetMass() * 40.f);
                }

                if (isOnGround == true)
                {
                    // if there's no movement change state to standing
                    if (Input::IsKeyPressed(mKeyInput.mRightKey) == Input::IsKeyPressed(mKeyInput.mLeftKey))
                    {
                        mPlayerState = ePlayerState::PLAYER_STATE_STAND;
                        mMoveEffectEmitter->GenerateEffect();
                        break;
                    }
                    else // either go right or go left are pressed so we change the state to walk
                    {
                        if (mPlayerAction == ePlayerAction::PLAYER_ACTION_EAT)
                            break;

                        mPlayerState = ePlayerState::PLAYER_STATE_WALK;
                        if (isOnGround == true)
                        {
                            if (FMOD_MANAGER->IsPlaying(mChannelSFX))
                            {
                                StopSFX();
                            }
                            PlaySFX("sound/sfx_slime_walk.mp3");
                        }
                        break;
                    }
                }
            }
            default:
                break;
        }

        if (mPlayerAction != ePlayerAction::PLAYER_ACTION_EAT)
        {
            if (Input::IsKeyPressed(mKeyInput.mLeftKey) && (Input::IsKeyPressed(mKeyInput.mLeftKey) != Input::IsKeyPressed(mKeyInput.mRightKey)))
            {
                SetControllerDirection(eControllerDirection::CONTROLLER_DIRECTION_LEFT);
            }
            if (Input::IsKeyPressed(mKeyInput.mRightKey) && (Input::IsKeyPressed(mKeyInput.mLeftKey) != Input::IsKeyPressed(mKeyInput.mRightKey)))
            {
                SetControllerDirection(eControllerDirection::CONTROLLER_DIRECTION_RIGHT);
            }
        }

        mPlayerVelocity = newVelocity;

        mOwner->SetVelocity(newVelocity);

        if (mPlayerState == ePlayerState::PLAYER_STATE_WALK || mPlayerState == ePlayerState::PLAYER_STATE_JUMP)
        {
            // if (Input::IsKeyTriggered(mKeyInput.mLeftKey) || Input::IsKeyTriggered(mKeyInput.mRightKey))
            if (Input::IsKeyTriggered(mKeyInput.mLeftKey) || Input::IsKeyTriggered(mKeyInput.mRightKey) || Input::IsKeyPressed(mKeyInput.mRightKey) || Input::IsKeyPressed(mKeyInput.mLeftKey))
            {
                if (isOnGround == true)
                {
                    if (FMOD_MANAGER->IsPlaying(mChannelSFX))
                    {
                        // StopSFX();
                    }
                    else
                    {
                        PlaySFX("sound/sfx_slime_walk.mp3");
                    }
                }
            }

            Object* collisionObject = nullptr;
            // right
            if (mPlayerVelocity.x > 0.f && mOwner->GetComponentByType<Collision>(eComponentType::COLLISION)->GetWasPushingRightsideObject())
            {
                collisionObject = mOwner->GetComponentByType<Collision>(eComponentType::COLLISION)->GetPrevRightsideObject();
                if (!collisionObject->GetComponentByType<Collision>(eComponentType::COLLISION)->IsPushableToRightside())
                {
                    if (mOwner->GetObjectType() != eObjectType::PLAYER_COMBINED)
                        mPlayerAction = ePlayerAction::PLAYER_ACTION_CANNOT_DO;
                }
            }
            // left
            if (mPlayerVelocity.x < 0.f && mOwner->GetComponentByType<Collision>(eComponentType::COLLISION)->GetWasPushingLeftsideObject())
            {
                collisionObject = mOwner->GetComponentByType<Collision>(eComponentType::COLLISION)->GetPrevLeftsideObject();
                if (!collisionObject->GetComponentByType<Collision>(eComponentType::COLLISION)->IsPushableToLeftside())
                {
                    if (mOwner->GetObjectType() != eObjectType::PLAYER_COMBINED)
                        mPlayerAction = ePlayerAction::PLAYER_ACTION_CANNOT_DO;
                }
            }
        }

        if (mPlayerState == ePlayerState::PLAYER_STATE_WALK)
        {
            mMoveEffectEmitter->GenerateEffect();
        }

        if (mPlayerState == ePlayerState::PLAYER_STATE_STAND)
        {
            if (Input::IsKeyReleased(mKeyInput.mLeftKey) || Input::IsKeyReleased(mKeyInput.mRightKey))
            {
                StopSFX();
            }
        }
    }
}

eControllerDirection Player::GetContollerDirection(void) const
{
    return mControllerDirection;
}

void Player::SetControllerDirection(eControllerDirection direction)
{
    mControllerDirection = direction;
    Vector2<float> scale = mOwner->GetTransform()->GetScale();
}

Object* Player::GetEattenObject(void)
{
    return mEattenObject;
}

bool Player::GetIsOwnerAteObject(void)
{
    return mIsOwnerAteObject;
}

void Player::SetEattenObject(Object* obj)
{
    mEattenObject = obj;

    if (obj)
    {
        mIsOwnerAteObject = true;
    }
    else
    {
        mIsOwnerAteObject = false;
    }
}

void Player::FloatingEatenObject(float dt)
{
    mEattenObjectValue += dt;

    mEattenObject->SetPosition({-0.1f * std::sin(mEattenObjectValue * 1.5f), 0.05f * std::sin(mEattenObjectValue * 2.f)});
}

void Player::SetSpawnPositionWithSpawner()
{
    if (mOwner->GetObjectType() == eObjectType::PLAYER_1)
    {
        mOwner->SetSpawnPosition(OBJECT_MANAGER->FindObjectByName("P1Spawn")->GetPosition());
    }
    else if (mOwner->GetObjectType() == eObjectType::PLAYER_2)
    {
        mOwner->SetSpawnPosition(OBJECT_MANAGER->FindObjectByName("P2Spawn")->GetPosition());
    }
    mOwner->SetPosition(mOwner->GetSpawnPosition());
    mCanPlayerMove = false;
}