/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : MovingPlatform.cpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Sejeong Park
Secondary :
- End Header ----------------------------------------------------------------*/
#include "MovingPlatform.hpp"
#include "../Engine/Object/Object.hpp"
#include "../Engine/Object/ObjectManager/ObjectManager.hpp"
#include "../Collision/Collision.hpp" // IsUnderObject

MovingPlatform::MovingPlatform() : Component(eComponentType::MOVINGPLATFORM)
{
    mIsMovingPlatformActive = false;
}

MovingPlatform::MovingPlatform(bool isActive, eMoveWay tileType, float startPosition, float endPosition) : Component(eComponentType::MOVINGPLATFORM)
{
    mIsMovingPlatformActive = isActive;
    mMoveWay       = tileType;
    mStartPosition = startPosition;
    mEndPosition   = endPosition;
}

void MovingPlatform::Initialize(Object* object)
{
}

void MovingPlatform::Update(float dt)
{
    if (mMoveWay == eMoveWay::STATIC)
    {
        return;
    }

    if (mIsMovingPlatformActive)
    {
        if (mMoveWay == eMoveWay::MOVE_HORIZONTAL)
        {
            MoveHorizontal(dt);
        }
        else if (mMoveWay == eMoveWay::MOVE_VERTICAL)
        {
            MoveVertical(dt);
        }

        MoveObject(dt);
    }
}

void MovingPlatform::Delete(void)
{
}

void MovingPlatform::MoveHorizontal(float dt)
{
    Vector2<float> tilePosition = GetOwner()->GetPosition();

    if (tilePosition.x > mEndPosition)
    {
        mMoveOppositeWay = false;
    }
    else if (mStartPosition > tilePosition.x)
    {
        mMoveOppositeWay = true;
    }

    if (mMoveOppositeWay)
    {
        tilePosition.x += mMoveSpeed * dt;
    }
    else
    {
        tilePosition.x -= mMoveSpeed * dt;
    }
    GetOwner()->SetPosition(tilePosition);
}

void MovingPlatform::MoveVertical(float dt)
{
    Vector2<float> tilePosition = GetOwner()->GetPosition();

    if (tilePosition.y > mEndPosition)
    {
        mMoveOppositeWay = false;
    }
    else if (mStartPosition > tilePosition.y)
    {
        mMoveOppositeWay = true;
    }

    if (mMoveOppositeWay)
    {
        tilePosition.y += mMoveSpeed * dt;
    }
    else
    {
        tilePosition.y -= mMoveSpeed * dt;
    }

    GetOwner()->SetPosition(tilePosition);
}

void MovingPlatform::MoveObject(float dt)
{
    auto objectMap = OBJECT_MANAGER->GetObjectVector();

    for (auto obj : objectMap)
    {
        if (obj == mOwner)
        {
            continue;
        }

        Collision* objectCollsion = obj->GetComponentByType<Collision>(eComponentType::COLLISION);
        if (objectCollsion)
        {
            if (objectCollsion->GetWasOnObject())
            {
                if (objectCollsion->GetPrevDownsideObject() == mOwner)
                {
                    float posChange = 0.f;

                    if (mMoveOppositeWay)
                    {
                        posChange += mMoveSpeed * dt;
                    }
                    else
                    {
                        posChange -= mMoveSpeed * dt;
                    }

                    Object* objectOnPlatform = obj;
                    while (objectOnPlatform)
                    {
                        if (mMoveWay == eMoveWay::MOVE_HORIZONTAL)
                        {
                            objectOnPlatform->SetPosition(objectOnPlatform->GetPosition() + Vector2<float>{posChange, 0.f});
                        }
                        else if (mMoveWay == eMoveWay::MOVE_VERTICAL)
                        {
                            objectOnPlatform->SetPosition(objectOnPlatform->GetPosition() + Vector2<float>{0.f, posChange});
                        }
                        objectOnPlatform = objectOnPlatform->GetComponentByType<Collision>(eComponentType::COLLISION)->GetPrevUpsideObject();
                    }
                }
            }
        }
    }
}

void MovingPlatform::SetPosition(float startPosition, float endPosition)
{
    mStartPosition = startPosition;
    mEndPosition   = endPosition;
}

Vector2<float> MovingPlatform::GetPosition(void)
{
    return Vector2<float>(mStartPosition, mEndPosition);
}

void MovingPlatform::SetMoveWay(eMoveWay tileType)
{
    mMoveWay = tileType;
}

eMoveWay MovingPlatform::GetMoveWay(void)
{
    return mMoveWay;
}

void MovingPlatform::SetMoveSpeed(float newSpeed)
{
    mMoveSpeed = newSpeed;
}

float MovingPlatform::GetMoveSpeed(void)
{
    return mMoveSpeed;
}

bool MovingPlatform::GetIsMovingPlatformActive(void)
{
    return mIsMovingPlatformActive;
}

void MovingPlatform::SetIsMovingPlatformActive(bool isActive)
{
    mIsMovingPlatformActive = isActive;
}
