/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : MovingPlatform.hpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Sejeong Park
Secondary :
- End Header ----------------------------------------------------------------*/
#pragma once

#include "../Component.hpp"
#include "../../MathLibrary/Vector2.hpp"

enum class eMoveWay
{
    STATIC = 0,
    MOVE_VERTICAL,
    MOVE_HORIZONTAL
};

class MovingPlatform : public Component
{
public:
    MovingPlatform();
    MovingPlatform(bool isActive, eMoveWay tileType, float startPosition, float endPosition);

    void Initialize(Object* object) override;
    void Update(float dt) override;
    void Delete(void) override;

    void MoveHorizontal(float dt);
    void MoveVertical(float dt);

    void MoveObject(float dt);

    void SetPosition(float startPosition, float endPosition);
    Vector2<float> GetPosition(void);

    void SetMoveWay(eMoveWay tileType);
    eMoveWay GetMoveWay(void);

    void SetMoveSpeed(float newSpeed);
    float GetMoveSpeed(void);

    bool GetIsMovingPlatformActive(void);
    void SetIsMovingPlatformActive(bool);

private:
    bool mIsMovingPlatformActive;
    eMoveWay mMoveWay = eMoveWay::STATIC;
    float mStartPosition = 0.f;
    float mEndPosition = 0.f;
    float mMoveSpeed = 100.0f;
    bool mMoveOppositeWay = true;
};