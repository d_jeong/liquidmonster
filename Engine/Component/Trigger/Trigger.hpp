/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : Trigger.hpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Daeun Jeong
Secondary :
- End Header ----------------------------------------------------------------*/

#pragma once

#include "../../Component/Component.hpp"
#include "../../MathLibrary/Vector2.hpp"

class Object;

enum eTriggerType
{
    NONE,
    LAZOR
};

class Trigger : public Component
{
public:
    Trigger();
    void Initialize(Object *object);
    void Update(float dt);
    void Delete();
    ~Trigger();

public:
    void TriggerActivate(float dt);

    void LazorActivate(float dt);

    bool GetIsTriggerd();
    void SetIsTriggerd(bool isTriggered);

    eTriggerType GetTriggerType(void);
    void SetTriggerType(eTriggerType triggerType);

    Vector2<float> GetObjectTranslation();
    void SetObjectTranslation(Vector2<float> translation);
    
    Vector2<float> GetScale();
    void SetScale(Vector2<float> scale);
    
    Object* GetConnectedObject();
    void SetConnectedObject(Object* object);

private:
    Vector2<float> mTranslation;
    Vector2<float> mScale;

    bool mIsTriggered;
    
    Object* mConnectedObject = nullptr;
    eTriggerType mTriggerType;
};