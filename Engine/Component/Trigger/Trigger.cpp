/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : Trigger.cpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Daeun Jeong
Secondary :
- End Header ----------------------------------------------------------------*/

#include "Trigger.hpp"
#include "../../Object/Object.hpp"
#include "../../Object/ObjectManager/ObjectManager.hpp"
#include "../Collision/Collision.hpp"
#include "../Sprite/Sprite.hpp"

Trigger::Trigger() : Component(eComponentType::TRIGGER)
{
}

void Trigger::Initialize(Object* object)
{
}

void Trigger::Update(float dt)
{
    if (mConnectedObject != nullptr)
    {
        TriggerActivate(dt);
    }
}

void Trigger::TriggerActivate(float dt)
{
    switch (mTriggerType)
    {
        case LAZOR:
        {
            LazorActivate(dt);
        }
    }
}

/* Use mIsTriggered, this is shit and can be better. */
void Trigger::LazorActivate(float dt)
{
    bool isCollidingWithSteelBox = false;
    auto objectMap               = OBJECT_MANAGER->GetObjectVector();

    for (auto objectToCheck : objectMap)
    {
        // When laser is colliding with steel box.
        if (objectToCheck->GetComponentByType<Collision>(eComponentType::COLLISION) != nullptr)
        {
            if (objectToCheck->GetComponentByType<Collision>(eComponentType::COLLISION)->IsCollidingWith(mConnectedObject) == true)
            {
                if (objectToCheck->GetObjectType() == eObjectType::STEEL_BOX)
                {
                    isCollidingWithSteelBox = true;
                    continue;
                }
            }
        }
    }

    // in case if lazer is colliding with box, and active
    if (isCollidingWithSteelBox)
    {
        if (mConnectedObject->GetIsActive() == true)
        {
            // turn off the lazor
            mConnectedObject->GetSprite()->SetInVisible();
            mConnectedObject->SetIsActive(false);
            mConnectedObject->SetPosition(mConnectedObject->GetSpawnPosition());
        }
    }
    // in case if lazer is not colliding with box and not active
    else
    {
        if (mConnectedObject->GetIsActive() == false)
        {
            // turn on the lazor
            mConnectedObject->GetSprite()->SetVisible();
            mConnectedObject->SetIsActive(true);
            mConnectedObject->SetPosition(mConnectedObject->GetSpawnPosition());
        }
    }
}

void Trigger::Delete()
{
    // if (mConnectedObject != nullptr)
    //{
    //    delete mConnectedObject;
    //    mConnectedObject = nullptr;
    //}
}

Trigger::~Trigger()
{
}

bool Trigger::GetIsTriggerd()
{
    return mIsTriggered;
}

void Trigger::SetIsTriggerd(bool isTriggered)
{
    mIsTriggered = isTriggered;
}

eTriggerType Trigger::GetTriggerType(void)
{
    return mTriggerType;
}

void Trigger::SetTriggerType(eTriggerType triggerType)
{
    mTriggerType = triggerType;
}

Vector2<float> Trigger::GetObjectTranslation()
{
    return mTranslation;
}

void Trigger::SetObjectTranslation(Vector2<float> translation)
{
    mTranslation = translation;
}

Vector2<float> Trigger::GetScale()
{
    return mScale;
}

void Trigger::SetScale(Vector2<float> scale)
{
    mScale = scale;
}

Object* Trigger::GetConnectedObject()
{
    return mConnectedObject;
}

void Trigger::SetConnectedObject(Object* object)
{
    mConnectedObject = object;
}