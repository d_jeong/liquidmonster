/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : ComponentType.hpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Daeun Jeong
Secondary :
- End Header ----------------------------------------------------------------*/

#pragma once

enum class eComponentType
{
    NONE = 0,
    TRANSFORM,
    SPRITE,
    PLAYER,
    FAN,
    DOOR,
    BUTTON,
    MOVINGPLATFORM,
    OBSTACLE,
    TRIGGER,
    PRESSURE,
    COLLISION,
    BATTERY,
    RESPAWN,
    COUNT
};