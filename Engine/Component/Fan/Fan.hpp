/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : Fan.hpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Haewon Shon
Secondary :
- End Header ----------------------------------------------------------------*/
#pragma once

#include "../Component.hpp"
#include <vector>
#include "../../FMOD/FmodManager.hpp"
#include "../../MathLibrary/Vector2.hpp"

class Object;

enum class eFanType
{
    UP,
    DOWN,
    LEFT,
    RIGHT,
    NONE
};

class Fan : public Component
{
public:
    Fan();
    Fan(bool active, eFanType fantype);

    void Initialize(Object *object) override;
    void Update(float dt) override;
    void Delete(void) override;

    void SetBlowingWindEffect(void);
    void SetIsFanActive(bool active);
    
    bool GetIsFanActive(void) const;

    void SetFanType(eFanType type);
    eFanType GetFanType(void);

    void PlaySFX(const std::string& file);
    void StopSFX();

private:
    bool mIsFanActive;
    eFanType mFanType;
    Object*  mBlowingWindEffect;

    FMOD::Channel* mChannelSFX = nullptr;
    float mObjSensor = 0.f;
    Vector2<float> mObjStopPos = {0.f, 0.f};
    bool mObjStop = false;
};