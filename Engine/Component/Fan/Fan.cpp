/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : Fan.cpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Haewon Shon
Secondary :
- End Header ----------------------------------------------------------------*/

#include "Fan.hpp"
#include "../../Graphics/Graphics.hpp"
#include "../../MathLibrary/Vector2.hpp"
#include "../../Object/Object.hpp"
#include "../../Object/ObjectManager/ObjectManager.hpp"
#include "../../TileMap/TileMap.hpp"

#include "../../Data/Data.hpp"
#include "../../Object/ObjectDepth.hpp"
#include "../Collision/Collision.hpp"
#include "../Sprite/Sprite.hpp"
#include "../Transform/Transform.hpp"

Fan::Fan() : Component(eComponentType::FAN)
{
    mIsFanActive = false;
    mFanType     = eFanType::NONE;
}

Fan::Fan(bool active, eFanType fantype) : Component(eComponentType::FAN)
{
    mIsFanActive = active;
    mFanType     = fantype;
}

void Fan::Initialize(Object* object)
{
    mBlowingWindEffect = new Object("wind", eObjectType::NONE);
    mBlowingWindEffect->AddComponent(new Sprite());
    mBlowingWindEffect->AddComponent(new Transform());
    mBlowingWindEffect->GetSprite()->SetColor(Vector4<float>(1.f, 1.f, 1.f, 0.7f));
    mBlowingWindEffect->GetSprite()->LoadTextureFromData(eTexture::WIND);
    mBlowingWindEffect->GetSprite()->SetAnimation(true);
    mBlowingWindEffect->GetSprite()->SetAnimationFrame(9);
    mBlowingWindEffect->GetSprite()->SetAnimationSpeed(8.f);
    mBlowingWindEffect->SetDepth(GAME_UI);

    SetBlowingWindEffect();

    mBlowingWindEffect->GetSprite()->Initialize(mBlowingWindEffect);
    mBlowingWindEffect->GetTransform()->Initialize(mBlowingWindEffect);

    mOwner->GetComponentByType<Sprite>(eComponentType::SPRITE)->PauseAnimation(!mIsFanActive);

    if (mIsFanActive)
    {
        mBlowingWindEffect->GetSprite()->SetVisible();
        PlaySFX("sound/sfx_fan_move.mp3");
        mOwner->GetSprite()->LoadTextureFromData(eTexture::FAN);
    }
    else
    {
        mBlowingWindEffect->GetSprite()->SetInVisible();
        mOwner->GetSprite()->LoadTextureFromData(eTexture::OBJECT_FAN_OFF);
    }
}

void Fan::Update(float dt)
{
    if (mIsFanActive)
    {
        FMOD_MANAGER->SetSoundEffectForLoop(false);
        mBlowingWindEffect->GetSprite()->Update(dt);
        auto objMap = OBJECT_MANAGER->GetObjectVector();

        Vector2<float> fanPosition = GetOwner()->GetPosition();
        Vector2<float> fanScale    = GetOwner()->GetScale();

        for (auto obj : objMap)
        {
            eObjectType objType = obj->GetObjectType();

            if (objType == eObjectType::PLAYER_1 || objType == eObjectType::PLAYER_2 || objType == eObjectType::DYNAMIC || objType == eObjectType::EATABLE || objType == eObjectType::BATTERY)
            {
                Vector2<float> objPosition  = obj->GetPosition();
                Vector2<float> objScale     = obj->GetScale();
                Vector2<float> objVelocity  = obj->GetVelocity();
                Collision*     objCollision = obj->GetComponentByType<Collision>(eComponentType::COLLISION);

                float objLeftSide  = objPosition.x - objScale.x / 2.f;
                float objRightSide = objPosition.x + objScale.x / 2.f;
                float objUpSide    = objPosition.y + objScale.y / 2.f;
                float objDownSide  = objPosition.y - objScale.y / 2.f;

                if (mFanType == eFanType::UP || mFanType == eFanType::DOWN)
                {
                    if ((objLeftSide >= fanPosition.x - fanScale.x / 2.f && objLeftSide <= fanPosition.x + fanScale.x / 2.f) ||
                        (objRightSide >= fanPosition.x - fanScale.x / 2.f && objRightSide <= fanPosition.x + fanScale.x / 2.f))
                    {
                        if (mFanType == eFanType::UP)
                        {
                            if (objDownSide > fanPosition.y + fanScale.y / 2.f && objDownSide < fanPosition.y + fanScale.y / 2.f + 500.f ||
                                obj->GetComponentByType<Collision>(eComponentType::COLLISION)->IsCollidingWith(mOwner))
                            {
                                // obj->SetPosition(obj->GetPosition() + Vector2<float>{0.f, 400.f * dt});
                                obj->SetVelocity({0.f, 500.f});
                                float cellingY = 0.f;
                                if (objCollision->CheckIsTouchingCelling(dt, cellingY))
                                {
                                    obj->SetPosition({objPosition.x + objVelocity.x * dt, cellingY});
                                    obj->SetVelocity({objVelocity.x, 0.f});
                                }
                            }
                        }
                        else if (mFanType == eFanType::DOWN)
                        {
                            if (objUpSide < fanPosition.y - fanScale.y / 2.f || obj->GetComponentByType<Collision>(eComponentType::COLLISION)->IsCollidingWith(mOwner))
                            {
                                // obj->SetPosition(obj->GetPosition() + Vector2<float>{0.f, -700 * dt});
                                if (obj->GetVelocity().y > -500.f)
                                {
                                    obj->SetVelocity({0.f, -500.f});
                                }
                                else
                                {
                                    obj->AddVelocity({0.f, -500.f * dt});
                                }
                                float groundY = 0.f;
                                if (objCollision->CheckIsOnGround(dt, groundY))
                                {
                                    obj->SetPosition({objPosition.x + objVelocity.x * dt, groundY});
                                    obj->SetVelocity({objVelocity.x, 0.f});
                                }
                            }
                        }
                    }
                }
                else if (mFanType == eFanType::LEFT || mFanType == eFanType::RIGHT)
                {
                    if ((objDownSide <= fanPosition.y + fanScale.x / 2.f && fanPosition.y - fanScale.x / 2.f <= objDownSide) ||
                        (fanPosition.y - fanScale.x / 2.f <= objUpSide && objUpSide <= fanPosition.y + fanScale.x / 2.f))
                    {
                        if (mFanType == eFanType::RIGHT)
                        {
                            if (fanPosition.x + fanScale.y / 2.f <= objLeftSide && objLeftSide <= fanPosition.x + fanScale.y / 2.f + 500.f ||
                                obj->GetComponentByType<Collision>(eComponentType::COLLISION)->IsCollidingWith(mOwner))
                            {
                                if (obj->GetVelocity().x < 0)
                                {
                                    if (mObjStop)
                                    {
                                        obj->SetPosition(mObjStopPos);
                                        return;
                                    }
                                    obj->SetVelocity({500.f, 0.f});
                                }

                                if (!objCollision->CheckIsPushingRightside(dt, mObjSensor))
                                {
                                    obj->SetPosition(obj->GetPosition() + Vector2<float>(400.f * dt, 0.f));
                                    mObjStop = false;
                                }
                                else
                                {
                                    mObjStopPos = objPosition;
                                    mObjStop    = true;
                                }
                            }
                        }
                        else if (mFanType == eFanType::LEFT)
                        {
                            if (fanPosition.x + fanScale.y / 2.f - 500.f <= objRightSide && objRightSide <= fanPosition.x + fanScale.y / 2.f ||
                                obj->GetComponentByType<Collision>(eComponentType::COLLISION)->IsCollidingWith(mOwner))
                            {
                                if (obj->GetVelocity().x > 0)
                                {
                                    if (mObjStop)
                                    {
                                        obj->SetPosition(mObjStopPos);
                                        return;
                                    }
                                    obj->SetVelocity({-500.f, 0.f});
                                }

                                if (!objCollision->CheckIsPushingLeftside(dt, mObjSensor))
                                {
                                    obj->SetPosition(obj->GetPosition() + Vector2<float>(-400.f * dt, 0.f));
                                    mObjStop = false;
                                }
                                else
                                {
                                    mObjStopPos = objPosition;
                                    mObjStop    = true;
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

void Fan::Delete(void)
{
    GRAPHICS->DeleteSprite(mBlowingWindEffect);

    StopSFX();

    for (auto comp : mBlowingWindEffect->GetAllComponents())
    {
        delete comp;
    }

    mBlowingWindEffect->GetAllComponents().clear();

    delete mBlowingWindEffect;
}

void Fan::SetBlowingWindEffect(void)
{
    mBlowingWindEffect->SetRotation(0.f);

    if (mFanType == eFanType::UP)
    {
        mBlowingWindEffect->SetPosition(Vector2<float>{mOwner->GetPosition().x, mOwner->GetPosition().y + 250.f * 1.4f});
        mBlowingWindEffect->SetScale(Vector2<float>{mOwner->GetScale().x, -500.f});
        mBlowingWindEffect->SetRotation(PI);
    }
    else if (mFanType == eFanType::DOWN)
    {
        mBlowingWindEffect->SetPosition(Vector2<float>{mOwner->GetPosition().x, mOwner->GetPosition().y - 250.f * 1.4f});
        mBlowingWindEffect->SetScale(Vector2<float>{mOwner->GetScale().x, 500.f});
        mBlowingWindEffect->SetRotation(PI);
    }
    else if (mFanType == eFanType::RIGHT)
    {
        mBlowingWindEffect->SetPosition(Vector2<float>{mOwner->GetPosition().x + 250.f * 1.4f, mOwner->GetPosition().y});
        mBlowingWindEffect->SetScale(Vector2<float>{mOwner->GetScale().x, 500.f});
        mBlowingWindEffect->SetRotation(-PI / 2.f);
    }
    else if (mFanType == eFanType::LEFT)
    {
        mBlowingWindEffect->SetPosition(Vector2<float>{mOwner->GetPosition().x - 250.f * 1.4f, mOwner->GetPosition().y});
        mBlowingWindEffect->SetScale(Vector2<float>{mOwner->GetScale().x, 500.f});
        mBlowingWindEffect->SetRotation(PI / 2.f);
    }
}

void Fan::SetIsFanActive(bool active)
{
    mIsFanActive = active;

    if (mIsFanActive)
    {
        StopSFX();
        PlaySFX("sound/sfx_fan_move.mp3");
        mBlowingWindEffect->GetSprite()->SetVisible();
        mOwner->GetSprite()->LoadTextureFromData(eTexture::FAN);
    }
    else
    {
        StopSFX();
        mBlowingWindEffect->GetSprite()->SetInVisible();
        mOwner->GetSprite()->LoadTextureFromData(eTexture::OBJECT_FAN_OFF);
    }
}

bool Fan::GetIsFanActive(void) const
{
    return mIsFanActive;
}

void Fan::SetFanType(eFanType type)
{
    mFanType = type;
}

eFanType Fan::GetFanType(void)
{
    return mFanType;
}

void Fan::PlaySFX(const std::string& file)
{
    mChannelSFX = FMOD_MANAGER->PlaySoundEffectForLOOP(file, mChannelSFX);
}

void Fan::StopSFX()
{
    mChannelSFX->stop();
}
