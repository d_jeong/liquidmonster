/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : Button.hpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Sejeong Park
Secondary :
- End Header ----------------------------------------------------------------*/
#pragma once
#include <unordered_map>
#include "../Component.hpp"

class Object;
enum class eTexture;

enum class eButtonDirection
{
    UP,
    LEFT,
    RIGHT,
    NONE
};

class Button : public Component
{
public:
    Button();
    Button(eButtonDirection direction);

    void SetButtonDirection(eButtonDirection direction);
    eButtonDirection GetButtonDirection(void);

    void SetButton(bool setButton, float dt);
    bool GetButton(void);

    void Initialize(Object *object) override;
    void Update(float dt) override;
    void Delete(void) override;

    void ChangeButtonGraphics(void);

    void SetConnectedObject(Object* object);
    Object* GetConnectedObject(void);

    void InsertGraphicsTile(int grid, eTexture texture);
    std::unordered_map<int, eTexture> GetGraphicsTiles(void);
    void EraseGraphicsTile(int grid);

    void InsertPhysicsTile(int grid, eTexture texture);
    std::unordered_map<int, eTexture> GetPhysicsTiles(void);
    void ErasePhysicsTile(int grid);

private:
    void UpdateWireGraphics(bool isButton);

    bool mIsButtonDown = false;
    Object* mConnectedObject = nullptr;
    float mOriginalX = 0;
    bool mPhysicsTileOnce = false;
    bool mOriginalIsActiveOfConnectedObject = false;

    eButtonDirection mButtonDirection;

    std::unordered_map<int, eTexture> mGraphicsTiles{};
    std::unordered_map<int, eTexture> mPhysicsTiles{};
};