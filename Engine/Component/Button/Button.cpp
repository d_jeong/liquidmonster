/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : Button.cpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Sejeong Park
Secondary :
- End Header ----------------------------------------------------------------*/

#include "Button.hpp"
#include "../../Data/Data.hpp"
#include "../../TileMap/TileMap.hpp"
#include "../Engine/Component/Collision/Collision.hpp" // CheckIsUnderObject
#include "../Engine/Object/Object.hpp"
#include "../Engine/Object/ObjectManager/ObjectManager.hpp"
#include "../Fan/Fan.hpp"
#include "../MovingPlatform/MovingPlatform.hpp"
#include "../Sprite/Sprite.hpp"

Button::Button() : Component(eComponentType::BUTTON)
{
    mButtonDirection = eButtonDirection::UP;
}

Button::Button(eButtonDirection direction) : Component(eComponentType::BUTTON)
{
    mButtonDirection = direction;
}

void Button::SetButtonDirection(eButtonDirection direction)
{
    mButtonDirection = direction;

    switch (mButtonDirection)
    {
        case eButtonDirection::UP:
            mOwner->SetRotation(0.f);
            break;

        case eButtonDirection::LEFT:
            mOwner->SetRotation(-PI / 2.f);
            break;

        case eButtonDirection::RIGHT:
            mOwner->SetRotation(PI / 2.f);
            break;
    }
}

eButtonDirection Button::GetButtonDirection(void)
{
    return mButtonDirection;
}

void Button::SetButton(bool setButton, float dt)
{
    if (mIsButtonDown != setButton)
    {
        if (mIsButtonDown == true) // down->up
        {
            FMOD_MANAGER->PlaySoundEffectSound("sound/sfx_component_button_off.mp3");
        }
        else
        {
            FMOD_MANAGER->PlaySoundEffectSound("sound/sfx_component_button_on.mp3");
        }
    }

    mIsButtonDown = setButton;

    if (mConnectedObject != nullptr)
    {
        switch (mConnectedObject->GetObjectType())
        {
            case eObjectType::FAN:
            {
                Fan* fanComponent = mConnectedObject->GetComponentByType<Fan>(eComponentType::FAN);

                if (mIsButtonDown == true)
                {
                    fanComponent->SetIsFanActive(!mOriginalIsActiveOfConnectedObject);
                }
                else // false
                {
                    fanComponent->SetIsFanActive(mOriginalIsActiveOfConnectedObject);
                }

                break;
            }
            case eObjectType::OBSTACLE: // Actually it is a laser.
            {
                if (mOriginalX < mConnectedObject->GetScale().x)
                {
                    mOriginalX = mConnectedObject->GetScale().x;
                }

                int multiplier = 10;

                if (!mIsButtonDown) // when button is not down
                {
                    if (mConnectedObject->GetScale().x >= 10 && mConnectedObject->GetIsActive() == false)
                    {
                        mConnectedObject->SetIsActive(true);
                        FMOD_MANAGER->PlaySoundEffectSound("sound/sfx_laser_on_off.mp3");
                        mConnectedObject->SetPosition(mConnectedObject->GetSpawnPosition());
                    }
                    else if (mOriginalX > mConnectedObject->GetScale().x)
                    {
                        if (mConnectedObject->GetScale().x + mConnectedObject->GetScale().x * dt * multiplier >= mOriginalX)
                        {
                            mConnectedObject->SetScale(Vector2<float>{mOriginalX, mConnectedObject->GetScale().y});
                        }
                        else
                        {
                            mConnectedObject->SetScale(Vector2<float>{mConnectedObject->GetScale().x + mConnectedObject->GetScale().x * dt * multiplier, mConnectedObject->GetScale().y});
                        }
                    }
                }
                else // when button is down
                {
                    if (mConnectedObject->GetScale().x <= 10 && mConnectedObject->GetIsActive() == true)
                    {
                        mConnectedObject->SetIsActive(false);
                        FMOD_MANAGER->PlaySoundEffectSound("sound/sfx_laser_on_off.mp3");
                    }
                    else if (mConnectedObject->GetScale().x >= 10 && mConnectedObject->GetIsActive() == true)
                    {
                        mConnectedObject->SetScale(Vector2<float>{mConnectedObject->GetScale().x - mConnectedObject->GetScale().x * dt * multiplier, mConnectedObject->GetScale().y});
                    }
                }

                break;
            }
            case eObjectType::MOVING_PLATFORM:
            {
                MovingPlatform* movingComponent = mConnectedObject->GetComponentByType<MovingPlatform>(eComponentType::MOVINGPLATFORM);
                movingComponent->SetIsMovingPlatformActive(mIsButtonDown);
                break;
            }
        }
    }

    if (!mPhysicsTiles.empty())
    {
        if (mIsButtonDown)
        {
            for (auto tile : mPhysicsTiles)
            {
                TILE_MAP->ErasePhysicsTile(tile.first);
            }
        }
        else
        {
            if (mPhysicsTileOnce)
            {
                for (auto tile : mPhysicsTiles)
                {
                    TILE_MAP->CreatePhysicsTile(tile.first, tile.second, false);
                }
            }
        }
    }
}


bool Button::GetButton(void)
{
    return mIsButtonDown;
}

void Button::Initialize(Object* object)
{
}

void Button::Update(float dt)
{
    auto objectMap = OBJECT_MANAGER->GetObjectVector();

    if (mIsButtonDown) // when button down
    {
        for (auto obj : objectMap)
        {
            if (obj == mOwner)
            {
                continue;
            }

            eObjectType objType = obj->GetObjectType();

            if (objType == eObjectType::PLAYER_1 || objType == eObjectType::PLAYER_2 || objType == eObjectType::PLAYER_COMBINED || objType == eObjectType::DYNAMIC ||
                objType == eObjectType::WOOD_BOX || objType == eObjectType::STEEL_BOX)
            {
                if (obj->GetIsActive() == true)
                {
                    if (mButtonDirection == eButtonDirection::UP)
                    {
                        if (mOwner->GetComponentByType<Collision>(eComponentType::COLLISION)->CheckIsUnderObject(dt, obj))
                        {
                            mPhysicsTileOnce = true;
                            SetButton(true, dt);
                            return;
                        }
                    }
                    else if (mButtonDirection == eButtonDirection::RIGHT)
                    {
                        if (mOwner->GetComponentByType<Collision>(eComponentType::COLLISION)->CheckAndPushObjectToLeftside(dt, obj))
                        {
                            mPhysicsTileOnce = true;
                            SetButton(true, dt);
                            return;
                        }
                    }
                    else if (mButtonDirection == eButtonDirection::LEFT)
                    {
                        if (mOwner->GetComponentByType<Collision>(eComponentType::COLLISION)->CheckAndPushObjectToRightside(dt, obj))
                        {
                            mPhysicsTileOnce = true;
                            SetButton(true, dt);
                            return;
                        }
                    }
                }
            }
        }

        SetButton(false, dt);
        mPhysicsTileOnce = false;
        ChangeButtonGraphics();
    }
    else
    {
        for (auto obj : objectMap)
        {
            if (obj == mOwner)
            {
                continue;
            }

            eObjectType objType = obj->GetObjectType();

            if (objType == eObjectType::PLAYER_1 || objType == eObjectType::PLAYER_2 || objType == eObjectType::PLAYER_COMBINED || objType == eObjectType::DYNAMIC ||
                objType == eObjectType::WOOD_BOX || objType == eObjectType::STEEL_BOX)
            {
                if (mButtonDirection == eButtonDirection::UP)
                {
                    if (mOwner->GetComponentByType<Collision>(eComponentType::COLLISION)->CheckIsUnderObject(dt, obj))
                    {
                        mPhysicsTileOnce = true;
                        SetButton(true, dt);
                        ChangeButtonGraphics();
                        return;
                    }
                }
                else if (mButtonDirection == eButtonDirection::RIGHT)
                {
                    if (mOwner->GetComponentByType<Collision>(eComponentType::COLLISION)->CheckAndPushObjectToLeftside(dt, obj))
                    {
                        mPhysicsTileOnce = true;
                        SetButton(true, dt);
                        ChangeButtonGraphics();
                        return;
                    }
                }
                else if (mButtonDirection == eButtonDirection::LEFT)
                {
                    if (mOwner->GetComponentByType<Collision>(eComponentType::COLLISION)->CheckAndPushObjectToRightside(dt, obj))
                    {
                        mPhysicsTileOnce = true;
                        SetButton(true, dt);
                        ChangeButtonGraphics();
                        return;
                    }
                }
            }
        }

        SetButton(false, dt);
        mPhysicsTileOnce = false;
    }
}

void Button::Delete(void)
{
}

void Button::ChangeButtonGraphics(void)
{
    Object* button = GetOwner();
    if (GetButton())
    {
        button->GetSprite()->LoadTextureFromData(eTexture::BLUE_BUTTON);
        UpdateWireGraphics(true);
    }
    else
    {
        button->GetSprite()->LoadTextureFromData(eTexture::RED_BUTTON);
        UpdateWireGraphics(false);
    }
}

void Button::SetConnectedObject(Object* object)
{
    mConnectedObject = object;

    if (mConnectedObject->GetObjectType() == eObjectType::FAN)
    {
        Fan* fanComponent                  = mConnectedObject->GetComponentByType<Fan>(eComponentType::FAN);
        mOriginalIsActiveOfConnectedObject = fanComponent->GetIsFanActive();
    }
}

Object* Button::GetConnectedObject(void)
{
    return mConnectedObject;
}

void Button::InsertGraphicsTile(int grid, eTexture texture)
{
    mGraphicsTiles.insert({grid, texture});
    TILE_MAP->FindGraphicsTileByGrid(grid)->SetTileConnectedObjectName(mOwner->GetName());
}

std::unordered_map<int, eTexture> Button::GetGraphicsTiles(void)
{
    return mGraphicsTiles;
}

void Button::EraseGraphicsTile(int grid)
{
    mGraphicsTiles.erase(grid);
}

void Button::InsertPhysicsTile(int grid, eTexture texture)
{
    mPhysicsTiles.insert({grid, texture});
    TILE_MAP->FindPhysicsTileByGrid(grid)->SetTileConnectedObjectName(mOwner->GetName());
}

std::unordered_map<int, eTexture> Button::GetPhysicsTiles(void)
{
    return mPhysicsTiles;
}

void Button::ErasePhysicsTile(int grid)
{
    mPhysicsTiles.erase(grid);
}

void Button::UpdateWireGraphics(bool isButton)
{
    if (isButton)
    {
        for (auto wire : mGraphicsTiles)
        {
            switch (wire.second)
            {
                case eTexture::TILE_WIRE_RED_1:
                    TILE_MAP->ChangeGraphicTileTexture(wire.first, eTexture::TILE_WIRE_BLUE_1);
                    break;
                case eTexture::TILE_WIRE_RED_2:
                    TILE_MAP->ChangeGraphicTileTexture(wire.first, eTexture::TILE_WIRE_BLUE_2);
                    break;
                case eTexture::TILE_WIRE_RED_3:
                    TILE_MAP->ChangeGraphicTileTexture(wire.first, eTexture::TILE_WIRE_BLUE_3);
                    break;
                case eTexture::TILE_WIRE_RED_4:
                    TILE_MAP->ChangeGraphicTileTexture(wire.first, eTexture::TILE_WIRE_BLUE_4);
                    break;
                case eTexture::TILE_WIRE_RED_5:
                    TILE_MAP->ChangeGraphicTileTexture(wire.first, eTexture::TILE_WIRE_BLUE_5);
                    break;
                case eTexture::TILE_WIRE_RED_6:
                    TILE_MAP->ChangeGraphicTileTexture(wire.first, eTexture::TILE_WIRE_BLUE_6);
                    break;
                case eTexture::TILE_WIRE_RED_7:
                    TILE_MAP->ChangeGraphicTileTexture(wire.first, eTexture::TILE_WIRE_BLUE_7);
                    break;
                case eTexture::TILE_WIRE_RED_8:
                    TILE_MAP->ChangeGraphicTileTexture(wire.first, eTexture::TILE_WIRE_BLUE_8);
                    break;
                case eTexture::TILE_WIRE_RED_9:
                    TILE_MAP->ChangeGraphicTileTexture(wire.first, eTexture::TILE_WIRE_BLUE_9);
                    break;
                case eTexture::TILE_WIRE_RED_10:
                    TILE_MAP->ChangeGraphicTileTexture(wire.first, eTexture::TILE_WIRE_BLUE_10);
                    break;
            }
        }
    }
    else
    {
        for (auto wire : mGraphicsTiles)
        {
            TILE_MAP->ChangeGraphicTileTexture(wire.first, wire.second);
        }
    }
}
