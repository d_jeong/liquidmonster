/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : Collision.cpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Yewon Lim
Secondary :
- End Header ----------------------------------------------------------------*/

#include "Collision.hpp"
#include "../../Object/Object.hpp"
#include "../../Physics/Physics.hpp"
#include "../../TileMap/TileMap.hpp"
#include <iostream>

Collision::Collision(void) : Component(eComponentType::COLLISION)
{
    mIsOnGround         = false;
    mIsTouchingCelling  = false;
    mIsPushingLeftside  = false;
    mIsPushingRightside = false;

    mWasOnGround         = false;
    mWasTouchingCelling  = false;
    mWasPushingLeftside  = false;
    mWasPushingRightside = false;

    mIsPushingLeftsideObject  = false;
    mIsPushingRightsideObject = false;
    mIsOnObject               = false;
    mIsUnderObject            = false;

    mWasPushingLeftsideObject  = false;
    mWasPushingRightsideObject = false;
    mWasOnObject               = false;
    mWasUnderObject            = false;

    mUpsideObject    = nullptr;
    mDownsideObject  = nullptr;
    mLeftsideObject  = nullptr;
    mRightsideObject = nullptr;

    mIsGhost = false;
}

void Collision::Initialize(Object* object)
{
    PHYSICS->AddToCollisionList(mOwner);
    mGround = mOwner->GetPosition().y;
}

void Collision::Update(float dt)
{
    mWasOnGround         = mIsOnGround;
    mWasTouchingCelling  = mIsTouchingCelling;
    mWasPushingLeftside  = mIsPushingLeftside;
    mWasPushingRightside = mIsPushingRightside;

    mWasPushingLeftsideObject  = mIsPushingLeftsideObject;
    mWasPushingRightsideObject = mIsPushingRightsideObject;
    mWasOnObject               = mIsOnObject;
    mWasUnderObject            = mIsUnderObject;

    mIsOnGround         = false;
    mIsTouchingCelling  = false;
    mIsPushingLeftside  = false;
    mIsPushingRightside = false;

    mIsPushingLeftsideObject  = false;
    mIsPushingRightsideObject = false;
    mIsOnObject               = false;
    mIsUnderObject            = false;

    mPrevUpsideObject    = mUpsideObject;
    mPrevDownsideObject  = mDownsideObject;
    mPrevLeftsideObject  = mLeftsideObject;
    mPrevRightsideObject = mRightsideObject;

    mUpsideObject    = nullptr;
    mDownsideObject  = nullptr;
    mLeftsideObject  = nullptr;
    mRightsideObject = nullptr;
}

void Collision::Delete()
{
}

bool Collision::CanItJump(void) const
{
    return mWasOnGround || mWasOnObject;
}

bool Collision::GetIsOnGround(void) const
{
    return mIsOnGround;
}

bool Collision::GetIsTouchingCelling(void) const
{
    return mIsTouchingCelling;
}

bool Collision::GetIsPushingLeftside(void) const
{
    return mIsPushingLeftside;
}

bool Collision::GetIsPushingRightside(void) const
{
    return mIsPushingRightside;
}

bool Collision::GetWasOnGround(void) const
{
    return mWasOnGround;
}

bool Collision::GetWasTouchingCelling(void) const
{
    return mWasTouchingCelling;
}

bool Collision::GetWasPushingLeftside(void) const
{
    return mWasPushingLeftside;
}

bool Collision::GetWasPushingRightside(void) const
{
    return mWasPushingRightside;
}

void Collision::SetIsOnGround(bool set)
{
    mIsOnGround = set;
}

void Collision::SetIsTouchingCelling(bool set)
{
    mIsTouchingCelling = set;
}

void Collision::SetIsPushingLeftside(bool set)
{
    mIsPushingLeftside = set;
}

void Collision::SetIsPushingRightside(bool set)
{
    mIsPushingRightside = set;
}

bool Collision::IsCollidingWith(Object* object) const
{
    return IsCollidingWithAABB(object->GetPosition(), object->GetScale());
}

bool Collision::IsCollidingWith(Tile* tile) const
{
    return IsCollidingWithAABB(tile->transform.GetPosition(), tile->transform.GetScale());
}

bool Collision::IsCollidingWithAABB(Vector2<float> pos, Vector2<float> scale) const
{
    Vector3<float> rotatedScale = MATRIX3x3::BuildRotation<float>(std::abs(mOwner->GetRotation())) * Vector3<float>{mOwner->GetScale().x, mOwner->GetScale().y, 1.f};
    rotatedScale.x              = std::abs(rotatedScale.x);
    rotatedScale.y              = std::abs(rotatedScale.y);
    Vector2<float> thisMax      = mOwner->GetPosition() + Vector2<float>{rotatedScale.x / 2.f, rotatedScale.y / 2.f};
    Vector2<float> thisMin      = mOwner->GetPosition() - Vector2<float>{rotatedScale.x / 2.f, rotatedScale.y / 2.f};

    Vector2<float> opponentMax = pos + Vector2<float>{scale.x / 2.f, scale.y / 2.f};
    Vector2<float> opponentMin = pos - Vector2<float>{scale.x / 2.f, scale.y / 2.f};

    return !(thisMin.x > opponentMax.x || thisMax.x < opponentMin.x || thisMin.y > opponentMax.y || thisMax.y < opponentMin.y);
}

bool Collision::IsCollidingWithPoint(Vector2<float> pos)
{
    Vector2<float> thisMax = mOwner->GetPosition() + Vector2<float>{mOwner->GetScale().x / 2.f, mOwner->GetScale().y / 2.f};
    Vector2<float> thisMin = mOwner->GetPosition() - Vector2<float>{mOwner->GetScale().x / 2.f, mOwner->GetScale().y / 2.f};

    if (pos.x >= thisMin.x && pos.x <= thisMax.x)
    {
        if (pos.y >= thisMin.y && pos.y <= thisMax.y)
        {
            return true;
        }
    }
    return false;
}

Tile* Collision::CheckIsOnGround(float dt, float& groundY)
{
    if (mOwner->GetVelocity().y <= 0.f)
    {
        Vector2<float> pos              = mOwner->GetPosition();
        Vector2<float> velocity         = mOwner->GetVelocity();
        float          deltaTimePerTile = std::abs(static_cast<float>(TILE_SIZE) / velocity.y);

        for (; pos.y >= mOwner->GetPosition().y + velocity.y * dt; pos += velocity * deltaTimePerTile)
        {
            Vector2<float> bottomSensorMax = pos + Vector2<float>{mOwner->GetScale().x / 2.f - 3.f, -(mOwner->GetScale().y / 2.f + 0.5f)};
            Vector2<float> bottomSensorMin = pos + Vector2<float>{-mOwner->GetScale().x / 2.f + 3.f, -(mOwner->GetScale().y / 2.f + 0.5f)};

            for (float x = bottomSensorMin.x;; x += TILE_SIZE)
            {
                float xPos = std::fmin(x, bottomSensorMax.x);
                Tile* tile = TILE_MAP->FindPhysicsTileOnPosition({xPos, bottomSensorMax.y});
                if (tile)
                {
                    groundY     = tile->transform.GetPosition().y + (TILE_SIZE + mOwner->GetScale().y) / 2.f;
                    mIsOnGround = true;
                    mGround     = groundY;
                    return tile;
                }

                if (xPos == bottomSensorMax.x)
                {
                    break;
                }
            }
        }
    }

    return nullptr;
}


Tile* Collision::CheckIsTouchingCelling(float dt, float& cellingY)
{
    if (mOwner->GetVelocity().y >= 0.f)
    {
        Vector2<float> pos              = mOwner->GetPosition();
        Vector2<float> velocity         = mOwner->GetVelocity();
        float          deltaTimePerTile = std::abs(static_cast<float>(TILE_SIZE) / velocity.y);

        for (; pos.y <= mOwner->GetPosition().y + velocity.y * dt; pos += velocity * deltaTimePerTile)
        {
            Vector2<float> topSensorMax = pos + Vector2<float>{mOwner->GetScale().x / 2.f - 1.f, (mOwner->GetScale().y / 2.f + 0.5f)};
            Vector2<float> topSensorMin = pos + Vector2<float>{-mOwner->GetScale().x / 2.f + 1.f, (mOwner->GetScale().y / 2.f + 0.5f)};

            for (float x = topSensorMin.x;; x += TILE_SIZE)
            {
                float xPos = std::fmin(x, topSensorMax.x);
                Tile* tile = TILE_MAP->FindPhysicsTileOnPosition({xPos, topSensorMax.y});
                if (tile)
                {
                    cellingY           = tile->transform.GetPosition().y - (TILE_SIZE + mOwner->GetScale().y) / 2.f;
                    mIsTouchingCelling = true;
                    return tile;
                }

                if (xPos == topSensorMax.x)
                {
                    break;
                }
            }
        }
    }
    return nullptr;
}


Tile* Collision::CheckIsPushingLeftside(float dt, float& leftX)
{
    mIsPushingLeftside = false;

    if (mOwner->GetVelocity().x <= 0.f)
    {
        Vector2<float> pos              = mOwner->GetPosition();
        Vector2<float> velocity         = mOwner->GetVelocity();
        float          deltaTimePerTile = std::abs(static_cast<float>(TILE_SIZE) / velocity.x);

        for (; pos.x >= mOwner->GetPosition().x + velocity.x * dt; pos += velocity * deltaTimePerTile)
        {
            Vector2<float> leftSensorMin = pos + Vector2<float>{-(mOwner->GetScale().x / 2.f + 1.f), -(mOwner->GetScale().y / 2.f - 0.5f)};
            Vector2<float> leftSensorMax = pos + Vector2<float>{-(mOwner->GetScale().x / 2.f + 1.f), (mOwner->GetScale().y / 2.f - 0.5f)};

            for (float y = leftSensorMin.y;; y += TILE_SIZE)
            {
                float yPos = std::fmin(y, leftSensorMax.y);
                Tile* tile = TILE_MAP->FindPhysicsTileOnPosition({leftSensorMax.x, yPos});
                if (tile)
                {
                    leftX              = tile->transform.GetPosition().x + (TILE_SIZE + mOwner->GetScale().x) / 2.f;
                    mIsPushingLeftside = true;
                    return tile;
                }
                if (yPos == leftSensorMax.y)
                {
                    break;
                }
            }
        }
    }
    return nullptr;
}

Tile* Collision::CheckIsPushingRightside(float dt, float& rightX)
{
    mIsPushingRightside = false;

    if (mOwner->GetVelocity().x >= 0.f)
    {
        Vector2<float> pos              = mOwner->GetPosition();
        Vector2<float> velocity         = mOwner->GetVelocity();
        float          deltaTimePerTile = std::abs(static_cast<float>(TILE_SIZE) / velocity.x);

        for (; pos.x <= mOwner->GetPosition().x + velocity.x * dt; pos += velocity * deltaTimePerTile)
        {
            Vector2<float> rightSensorMin = pos + Vector2<float>{(mOwner->GetScale().x / 2.f + 0.5f), -(mOwner->GetScale().y / 2.f - 0.5f)};
            Vector2<float> rightSensorMax = pos + Vector2<float>{(mOwner->GetScale().x / 2.f + 0.5f), (mOwner->GetScale().y / 2.f - 0.5f)};

            for (float y = rightSensorMin.y;; y += TILE_SIZE)
            {
                float yPos = std::fmin(y, rightSensorMax.y);
                Tile* tile = TILE_MAP->FindPhysicsTileOnPosition({rightSensorMax.x, yPos});
                if (tile)
                {
                    rightX              = tile->transform.GetPosition().x - (TILE_SIZE + mOwner->GetScale().x) / 2.f;
                    mIsPushingRightside = true;
                    return tile;
                }
                if (yPos == rightSensorMax.y)
                {
                    break;
                }
            }
        }
    }
    return nullptr;
}

bool Collision::GetIsPushingLeftsideObject(void) const
{
    // return mIsPushingLeftsideObject;
    return mLeftsideObject != nullptr;
}

bool Collision::GetIsPushingRightsideObject(void) const
{
    // return mWasPushingRightside;
    return mRightsideObject != nullptr;
}

bool Collision::GetIsOnObject(void) const
{
    return mIsOnObject;
}

bool Collision::GetIsUnderObject(void) const
{
    return mIsUnderObject;
}

bool Collision::GetWasPushingLeftsideObject(void) const
{
    return mWasPushingLeftsideObject;
}

bool Collision::GetWasPushingRightsideObject(void) const
{
    return mWasPushingRightsideObject;
}

bool Collision::GetWasOnObject(void) const
{
    return mWasOnObject;
}

bool Collision::GetWasUnderObject(void) const
{
    return mWasUnderObject;
}

void Collision::SetIsPushingLeftsideObject(bool set)
{
    mIsPushingLeftsideObject = set;
}

void Collision::SetIsPushingRightsideObject(bool set)
{
    mIsPushingRightsideObject = set;
}

void Collision::SetIsOnObject(bool set)
{
    mIsOnObject = set;
}

void Collision::SetIsUnderObject(bool set)
{
    mIsUnderObject = set;
}

bool Collision::GetIsGhost(void) const
{
    return mIsGhost;
}

void Collision::SetIsGhost(bool set)
{
    mIsGhost = set;
}

bool Collision::CheckAndPushObjectToLeftside(float dt, Object* opponent)
{
    if (mOwner->GetVelocity().x <= 0.f)
    {
        Collision*     opponentCollision = opponent->GetComponentByType<Collision>(eComponentType::COLLISION);
        Vector2<float> pos               = mOwner->GetPosition() + mOwner->GetVelocity() * dt;

        Vector3<float> rotatedScale    = MATRIX3x3::BuildRotation<float>(std::abs(mOwner->GetRotation())) * Vector3<float>{mOwner->GetScale().x, mOwner->GetScale().y, 1.f};
        rotatedScale.x                 = std::abs(rotatedScale.x);
        rotatedScale.y                 = std::abs(rotatedScale.y);
        Vector2<float> leftSensorPos   = pos + Vector2<float>{-(rotatedScale.x / 2.f + 1.f), 0.f};
        Vector2<float> leftSensorScale = Vector2<float>{2.f, rotatedScale.y - 2.f};

        if (opponentCollision->IsCollidingWithAABB(leftSensorPos, leftSensorScale))
        {
            mIsPushingLeftsideObject = true;
            if (mLeftsideObject == nullptr)
            {
                mLeftsideObject = opponent;
            }
            opponentCollision->SetRightsideObject(mOwner);
            opponentCollision->SetIsPushingRightsideObject(true);
            return true;
        }
    }
    return false;
}

bool Collision::CheckAndPushObjectToRightside(float dt, Object* opponent)
{
    if (mOwner->GetVelocity().x >= 0.f)
    {
        Collision*     opponentCollision = opponent->GetComponentByType<Collision>(eComponentType::COLLISION);
        Vector2<float> pos               = mOwner->GetPosition() + mOwner->GetVelocity() * dt;

        Vector3<float> rotatedScale     = MATRIX3x3::BuildRotation<float>(std::abs(mOwner->GetRotation())) * Vector3<float>{mOwner->GetScale().x, mOwner->GetScale().y, 1.f};
        rotatedScale.x                  = std::abs(rotatedScale.x);
        rotatedScale.y                  = std::abs(rotatedScale.y);
        Vector2<float> rightSensorPos   = pos + Vector2<float>{(rotatedScale.x / 2.f + 1.f), 0.f};
        Vector2<float> rightSensorScale = Vector2<float>{2.f, rotatedScale.y - 2.f};

        if (opponentCollision->IsCollidingWithAABB(rightSensorPos, rightSensorScale))
        {
            mIsPushingRightsideObject = true;
            if (mRightsideObject == nullptr)
            {
                mRightsideObject = opponent;
            }
            opponentCollision->SetLeftsideObject(mOwner);
            opponentCollision->SetIsPushingLeftsideObject(true);
            return true;
        }
    }
    return false;
}

bool Collision::CheckIsOnObject(float dt, Object* opponent)
{
    // if (!mIsOnObject && !mIsPushingLeftsideObject && !mIsPushingRightsideObject)
    {
        if (mOwner->GetVelocity().y <= 0.f)
        {
            Vector2<float> pos               = mOwner->GetPosition() + mOwner->GetVelocity() * dt;
            Vector3<float> rotatedScale      = MATRIX3x3::BuildRotation<float>(std::abs(mOwner->GetRotation())) * Vector3<float>{mOwner->GetScale().x, mOwner->GetScale().y, 1.f};
            rotatedScale.x                   = std::abs(rotatedScale.x);
            rotatedScale.y                   = std::abs(rotatedScale.y);
            Vector2<float> bottomSensorPos   = pos + Vector2<float>{0.f, -(rotatedScale.y / 2.f + 1.f)};
            Vector2<float> bottomSensorScale = Vector2<float>{rotatedScale.x - 1.f, 2.f};

            if (opponent->GetComponentByType<Collision>(eComponentType::COLLISION)->IsCollidingWithAABB(bottomSensorPos, bottomSensorScale))
            {
                mIsOnObject = true;
                if (mDownsideObject == nullptr)
                {
                    mDownsideObject = opponent;
                }
                opponent->GetComponentByType<Collision>(eComponentType::COLLISION)->SetUpsideObject(mOwner);
                return true;
            }
        }
    }
    return false;
}

bool Collision::CheckIsUnderObject(float dt, Object* opponent)
{
    if (!mIsOnObject && !mIsPushingLeftsideObject && !mIsPushingRightsideObject)
    {
        if (mOwner->GetVelocity().y >= 0.f)
        {
            Vector2<float> pos               = mOwner->GetPosition();
            Vector3<float> rotatedScale      = MATRIX3x3::BuildRotation<float>(std::abs(mOwner->GetRotation())) * Vector3<float>{mOwner->GetScale().x, mOwner->GetScale().y, 1.f};
            rotatedScale.x                   = std::abs(rotatedScale.x);
            rotatedScale.y                   = std::abs(rotatedScale.y);
            Vector2<float> bottomSensorPos   = pos + Vector2<float>{0.f, (rotatedScale.y / 2.f + 1.f)};
            Vector2<float> bottomSensorScale = Vector2<float>{rotatedScale.x - 1.f, 2.f};

            if (opponent->GetComponentByType<Collision>(eComponentType::COLLISION)->IsCollidingWithAABB(bottomSensorPos, bottomSensorScale))
            {
                mIsUnderObject = true;
                if (mUpsideObject == nullptr)
                {
                    mUpsideObject = opponent;
                }
                opponent->GetComponentByType<Collision>(eComponentType::COLLISION)->SetDownsideObject(mOwner);
                return true;
            }
        }
    }
    return false;
}

Object* Collision::GetUpsideObject(void) const
{
    return mUpsideObject;
}

Object* Collision::GetDownsideObject(void) const
{
    return mDownsideObject;
}

Object* Collision::GetLeftsideObject(void) const
{
    return mLeftsideObject;
}

Object* Collision::GetRightsideObject(void) const
{
    return mRightsideObject;
}

Object* Collision::GetPrevUpsideObject(void) const
{
    return mPrevUpsideObject;
}

Object* Collision::GetPrevDownsideObject(void) const
{
    return mPrevDownsideObject;
}

Object* Collision::GetPrevLeftsideObject(void) const
{
    return mPrevLeftsideObject;
}

Object* Collision::GetPrevRightsideObject(void) const
{
    return mPrevRightsideObject;
}

void Collision::SetUpsideObject(Object* obj)
{
    mUpsideObject = obj;
}

void Collision::SetDownsideObject(Object* obj)
{
    mDownsideObject = obj;
}

void Collision::SetLeftsideObject(Object* obj)
{
    mLeftsideObject = obj;
}

void Collision::SetRightsideObject(Object* obj)
{
    mRightsideObject = obj;
}

bool Collision::IsPushableToLeftside()
{
    if (mRightsideObject == nullptr && mPrevRightsideObject == nullptr)
        return true;
    Object*     leftObject   = mPrevLeftsideObject;
    eObjectType ownerType    = mOwner->GetObjectType();
    eObjectType opponentType = (mRightsideObject == nullptr) ? mPrevRightsideObject->GetObjectType() : mRightsideObject->GetObjectType();

    if (ownerType == eObjectType::STEEL_BOX)
    {
        if (opponentType == eObjectType::PLAYER_1 || opponentType == eObjectType::PLAYER_2 || opponentType == eObjectType::WOOD_BOX)
        {
            return false;
        }
    }

    if (mWasPushingLeftside == true || mIsPushingLeftside == true)
    {
        return false;
    }

    while (leftObject != nullptr)
    {
        Collision* leftCollision = leftObject->GetComponentByType<Collision>(eComponentType::COLLISION);
        if (leftCollision->GetPrevLeftsideObject() != nullptr)
        {
            if (leftCollision->GetPrevLeftsideObject()->GetComponentByType<Collision>(eComponentType::COLLISION)->GetPrevLeftsideObject() == leftObject)
            {
                return true;
            }
        }
        if (leftCollision->GetWasPushingLeftside() == true || leftCollision->GetIsPushingLeftside() == true || leftObject->GetVelocity().x > 0.f)
        {
            return false;
        }
        else
        {
            // not pushable objects(static)
            if (leftObject->GetObjectType() == eObjectType::BUTTON)
            {
                return false;
            }

            if (opponentType != eObjectType::STEEL_BOX && opponentType != eObjectType::PLAYER_COMBINED)
            {
                if (leftObject->GetObjectType() == eObjectType::STEEL_BOX)
                {
                    return false;
                }
            }
            else if (leftObject->GetObjectType() == eObjectType::PLAYER_1 || leftObject->GetObjectType() == eObjectType::PLAYER_2)
            {
                return false;
            }
            else if (leftObject->GetVelocity().x > 0.f)
            {
                return false;
            }
            leftObject = leftCollision->GetPrevLeftsideObject();
        }
    }
    return true;
}

bool Collision::IsPushableToRightside()
{
    if (mLeftsideObject == nullptr && mPrevLeftsideObject == nullptr)
        return true;
    Object*     rightObject  = mPrevRightsideObject;
    eObjectType ownerType    = mOwner->GetObjectType();
    eObjectType opponentType = (mLeftsideObject== nullptr) ? mPrevLeftsideObject->GetObjectType() : mLeftsideObject->GetObjectType();

    if (ownerType == eObjectType::STEEL_BOX)
    {
        if (opponentType == eObjectType::PLAYER_1 || opponentType == eObjectType::PLAYER_2 || opponentType == eObjectType::WOOD_BOX)
        {
            return false;
        }
    }

    if (mWasPushingRightside == true || mIsPushingRightside == true)
    {
        return false;
    }

    while (rightObject != nullptr)
    {
        Collision* rightCollision = rightObject->GetComponentByType<Collision>(eComponentType::COLLISION);
        if (rightCollision->GetPrevRightsideObject() != nullptr)
        {
            if (rightCollision->GetPrevRightsideObject()->GetComponentByType<Collision>(eComponentType::COLLISION)->GetPrevRightsideObject() == rightObject)
            {
                return true;
            }
        }

        if (rightCollision->GetWasPushingRightside() == true || rightCollision->GetIsPushingRightside() == true || rightObject->GetVelocity().x < 0.f)
        {
            return false;
        }
        else
        {
            // not pushable objects(static)
            if (rightObject->GetObjectType() == eObjectType::BUTTON)
            {
                return false;
            }

            if (opponentType != eObjectType::STEEL_BOX && opponentType != eObjectType::PLAYER_COMBINED)
            {
                if (rightObject->GetObjectType() == eObjectType::STEEL_BOX)
                {
                    return false;
                }
            }
            else if (rightObject->GetObjectType() == eObjectType::PLAYER_1 || rightObject->GetObjectType() == eObjectType::PLAYER_2)
            {
                return false;
            }
            else if (rightObject->GetVelocity().x < 0.f)
            {
                return false;
            }
            rightObject = rightCollision->GetPrevRightsideObject();
        }
    }
    return true;
}

float Collision::GetGround()
{
    return mGround;
}

void Collision::SetGround(float ground)
{
    mGround = ground;
}