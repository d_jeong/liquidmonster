/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : Collision.hpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Yewon Lim
Secondary :
- End Header ----------------------------------------------------------------*/
#pragma once

#include "../../MathLibrary/MathLibrary.hpp"
#include "../Component.hpp"
#include <vector>

struct Tile;
class Object;

const float sensorOffset = 2.f;

class Collision : public Component
{
public:
    Collision(void);

    void Initialize(Object* obj) override;
    void Update(float dt) override;
    void Delete(void) override;

    bool CanItJump(void) const;

    bool GetIsOnGround(void) const;
    bool GetIsTouchingCelling(void) const;
    bool GetIsPushingLeftside(void) const;
    bool GetIsPushingRightside(void) const;

    bool GetWasOnGround(void) const;
    bool GetWasTouchingCelling(void) const;
    bool GetWasPushingLeftside(void) const;
    bool GetWasPushingRightside(void) const;

    void SetIsOnGround(bool set);
    void SetIsTouchingCelling(bool set);
    void SetIsPushingLeftside(bool set);
    void SetIsPushingRightside(bool set);

    bool IsCollidingWith(Object* object) const;
    bool IsCollidingWith(Tile* tile) const;
    bool IsCollidingWithAABB(Vector2<float> pos, Vector2<float> scale) const;
    bool IsCollidingWithPoint(Vector2<float> pos);

    Tile* CheckIsOnGround(float dt, float& groundY);
    Tile* CheckIsTouchingCelling(float dt, float& cellingY);
    Tile* CheckIsPushingLeftside(float dt, float& leftX);
    Tile* CheckIsPushingRightside(float dt, float& rightX);

    bool GetIsPushingLeftsideObject(void) const;
    bool GetIsPushingRightsideObject(void) const;
    bool GetIsOnObject(void) const;
    bool GetIsUnderObject(void) const;

    bool GetWasPushingLeftsideObject(void) const;
    bool GetWasPushingRightsideObject(void) const;
    bool GetWasOnObject(void) const;
    bool GetWasUnderObject(void) const;

    void SetIsPushingLeftsideObject(bool set);
    void SetIsPushingRightsideObject(bool set);
    void SetIsOnObject(bool set);
    void SetIsUnderObject(bool set);

    bool GetIsGhost(void) const;
    void SetIsGhost(bool set);

    bool CheckAndPushObjectToLeftside(float dt, Object* opponent);
    bool CheckAndPushObjectToRightside(float dt, Object* opponent);
    bool CheckIsOnObject(float dt, Object* opponent);
    bool CheckIsUnderObject(float dt, Object* opponent);

    Object* GetUpsideObject(void) const;
    Object* GetDownsideObject(void) const;
    Object* GetLeftsideObject(void) const;
    Object* GetRightsideObject(void) const;

    Object* GetPrevUpsideObject(void) const;
    Object* GetPrevDownsideObject(void) const;
    Object* GetPrevLeftsideObject(void) const;
    Object* GetPrevRightsideObject(void) const;

    void SetUpsideObject(Object* obj);
    void SetDownsideObject(Object* obj);
    void SetLeftsideObject(Object* obj);
    void SetRightsideObject(Object* obj);

    bool IsPushableToLeftside();
    bool IsPushableToRightside();

    float GetGround();
    void  SetGround(float ground);

private:
    bool mIsOnGround;
    bool mIsTouchingCelling;
    bool mIsPushingLeftside;
    bool mIsPushingRightside;

    bool mWasOnGround;
    bool mWasTouchingCelling;
    bool mWasPushingLeftside;
    bool mWasPushingRightside;

    bool mIsPushingLeftsideObject;
    bool mIsPushingRightsideObject;
    bool mIsOnObject;
    bool mIsUnderObject;

    bool mWasPushingLeftsideObject;
    bool mWasPushingRightsideObject;
    bool mWasOnObject;
    bool mWasUnderObject;

    Object* mUpsideObject;
    Object* mDownsideObject;
    Object* mLeftsideObject;
    Object* mRightsideObject;

    Object* mPrevUpsideObject;
    Object* mPrevDownsideObject;
    Object* mPrevLeftsideObject;
    Object* mPrevRightsideObject;

    bool mIsGhost;
    float mGround;
};