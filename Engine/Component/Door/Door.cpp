/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : Door.cpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Sejeong Park
Secondary :
- End Header ----------------------------------------------------------------*/

#include "Door.hpp"
#include "../../Data/Data.hpp"
#include "../Collision/Collision.hpp"
#include "../Engine/FMOD/FmodManager.hpp"
#include "../Engine/Object/Object.hpp"
#include "../Engine/Object/ObjectManager/ObjectManager.hpp"
#include "../Game/State/State.hpp"
#include "../Game/State/StateManager/StateManager.hpp"
#include "../Player/Player.hpp"
#include "../Sprite/Sprite.hpp"

Door::Door() : Component(eComponentType::DOOR)
{
    mIsDoorWorking = false;
}

void Door::Initialize(Object* object)
{
    mIsDoorWorking = false;
}

void Door::Update(float dt)
{
    if (STATE_MANAGER->GetCurrentState()->GetCurrentStateType() == eStateType::CHAPTER2_LEVEL_6)
    {
        mOwner->GetSprite()->LoadTextureFromData(eTexture::TWINCLE);
    }

    if (mIsDoorWorking == true)
    {
        return;
    }

    Object* obj = OBJECT_MANAGER->FindObjectByName("PlayerCombined");
    Object* obj1 = OBJECT_MANAGER->FindObjectByName("Player1");
    Object* obj2 = OBJECT_MANAGER->FindObjectByName("Player2");

    // When it is combined player
    if (obj->GetComponentByType<Collision>(eComponentType::COLLISION)->IsCollidingWith(mOwner) == true)
    {
        Player* player_component = obj->GetComponentByType<Player>(eComponentType::PLAYER);

        if (player_component->GetIsOwnerAteObject())
        {
            Object* object_key = player_component->GetEattenObject();

            if (object_key->GetName() == "Key")
            {
                FMOD_MANAGER->StopSoundEffectForLoop();
                FMOD_MANAGER->StopSoundEffectSound();

                STATE_MANAGER->SetNextState(STATE_MANAGER->GetCurrentState()->GetNextStateType());
                STATE_MANAGER->SetStateChangeTimer(2.f);

                if (STATE_MANAGER->GetCurrentState()->GetCurrentStateType() != eStateType::CHAPTER2_LEVEL_6)
                {
                    FMOD_MANAGER->PlaySoundEffectSound("sound/sfx_key_open.mp3");

                    mOwner->GetSprite()->LoadTextureFromData(eTexture::OBJECT_DOOR_OPENED);
                }
                else
                {
                    mOwner->SetRotation(mOwner->GetRotation() * dt);
                }
#ifdef _DEBUG
#else
                STATE_MANAGER->GetNextState()->SaveLevelLock();
#endif

                mIsDoorWorking = true;
                return;
            }
        }
    }
    // When it is player1, 2
    else if (obj1->GetComponentByType<Collision>(eComponentType::COLLISION)->IsCollidingWith(mOwner) == true &&
        obj2->GetComponentByType<Collision>(eComponentType::COLLISION)->IsCollidingWith(mOwner) == true)
    {
        Player* player_component = obj1->GetComponentByType<Player>(eComponentType::PLAYER);

        if (player_component->GetIsOwnerAteObject())
        {
            Object* object_key = player_component->GetEattenObject();

            if (object_key->GetName() == "Key")
            {
                FMOD_MANAGER->StopSoundEffectForLoop();
                FMOD_MANAGER->StopSoundEffectSound();

                STATE_MANAGER->SetNextState(STATE_MANAGER->GetCurrentState()->GetNextStateType());
                STATE_MANAGER->SetStateChangeTimer(2.f);

                if (STATE_MANAGER->GetCurrentState()->GetCurrentStateType() != eStateType::CHAPTER2_LEVEL_6)
                {
                    FMOD_MANAGER->PlaySoundEffectSound("sound/sfx_key_open.mp3");

                    mOwner->GetSprite()->LoadTextureFromData(eTexture::OBJECT_DOOR_OPENED);
                }
                else
                {
                    mOwner->SetRotation(mOwner->GetRotation() * dt);
                }
#ifdef _DEBUG
#else
                STATE_MANAGER->GetNextState()->SaveLevelLock();
#endif

                mIsDoorWorking = true;
                return;
            }
        }

        player_component = obj2->GetComponentByType<Player>(eComponentType::PLAYER);

        if (player_component->GetIsOwnerAteObject())
        {
            Object* object_key = player_component->GetEattenObject();

            if (object_key->GetName() == "Key")
            {
                FMOD_MANAGER->StopSoundEffectForLoop();
                FMOD_MANAGER->StopSoundEffectSound();

                STATE_MANAGER->SetNextState(STATE_MANAGER->GetCurrentState()->GetNextStateType());
                STATE_MANAGER->SetStateChangeTimer(2.f);

                if (STATE_MANAGER->GetCurrentState()->GetCurrentStateType() != eStateType::CHAPTER2_LEVEL_6)
                {
                    FMOD_MANAGER->PlaySoundEffectSound("sound/sfx_key_open.mp3");

                    mOwner->GetSprite()->LoadTextureFromData(eTexture::OBJECT_DOOR_OPENED);
                }
                else
                {
                    mOwner->SetRotation(mOwner->GetRotation() * dt);
                }
#ifdef _DEBUG
#else
                STATE_MANAGER->GetNextState()->SaveLevelLock();
#endif

                mIsDoorWorking = true;

                return;
            }
        }
    }
}

void Door::Delete(void)
{
}
