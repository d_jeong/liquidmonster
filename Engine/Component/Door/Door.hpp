/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : Door.hpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Sejeong Park
Secondary :
- End Header ----------------------------------------------------------------*/
#pragma once

#include "../Component.hpp"

class Door : public Component
{
public:
    Door();
    void Initialize(Object *object) override;
    void Update(float dt) override;
    void Delete(void) override;

private:
    bool mIsDoorWorking;
};