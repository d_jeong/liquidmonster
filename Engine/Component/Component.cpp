/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : Component.cpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Daeun Jeong
Secondary :
- End Header ----------------------------------------------------------------*/

#include "Component.hpp"

Component::Component(eComponentType type) : mOwner(nullptr), mMover(nullptr), mComponentType(type)
{
}

Component::~Component()
{
}

void Component::SetOwner(Object *owner)
{
    mOwner = owner;
}

Object *Component::GetOwner()
{
    return mOwner;
}

void Component::SetMover(Object *owner)
{
    mMover = owner;
}

Object *Component::GetMover()
{
    return mMover;
}
eComponentType Component::GetType() const
{
    return mComponentType;
}

bool Component::GetIsActive(void)
{
    return mIsActive;
}

void Component::SetIsActive(bool isActive)
{
    mIsActive = isActive;
}