/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : Transform.hpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Yewon Lim
Secondary :
- End Header ----------------------------------------------------------------*/

#pragma once
#include "../../MathLibrary/Matrix3x3.hpp"
#include "../../MathLibrary/Vector2.hpp"
#include "../Component.hpp"
#include "../ComponentType.hpp"

class Transform : public Component
{
public:
    Transform();
    ~Transform() = default;

    void Initialize(Object *object) override;
    void Update(float dt) override;
    void Delete(void) override;

    Vector2<float> GetPosition(void) const;
    void           SetPosition(Vector2<float> pos);

    Vector2<float> GetScale(void) const;
    void           SetScale(Vector2<float> scale);

    float GetRotation(void) const;
    void  SetRotation(float angle);

    Transform *GetParentTransform(void) const;
    void       SetParentTransform(Transform *parent);

    Matrix3x3<float> GetModelToWorldTransform(void) const;
    Matrix3x3<float> GetModelToWorldCollisionTransform(void) const;

private:
    Vector2<float> mTranslation;
    Vector2<float> mScale;
    float          mRotation;

    Transform *mParentTransform;
};