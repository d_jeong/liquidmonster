/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : Transform.cpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Yewon Lim
Secondary :
- End Header ----------------------------------------------------------------*/

#include "Transform.hpp"
#include "../Collision/Collision.hpp"

Transform::Transform() : Component(eComponentType::TRANSFORM)
{
    mTranslation     = {0.f, 0.f};
    mScale           = {1.f, 1.f};
    mRotation        = 0.f;
    mParentTransform = nullptr;
}

void Transform::Initialize(Object *object)
{
}

void Transform::Update(float dt)
{
}

void Transform::Delete(void)
{
}

Vector2<float> Transform::GetPosition() const
{
    return mTranslation;
}

void Transform::SetPosition(Vector2<float> position)
{
    mTranslation = position;
}

Vector2<float> Transform::GetScale() const
{
    return mScale;
}

void Transform::SetScale(Vector2<float> scale)
{
    mScale = scale;
}

float Transform::GetRotation() const
{
    return mRotation;
}

void Transform::SetRotation(float angle)
{
    mRotation = angle;
}

Transform *Transform::GetParentTransform() const
{
    return mParentTransform;
}

void Transform::SetParentTransform(Transform *parent)
{
    mParentTransform = parent;
}

Matrix3x3<float> Transform::GetModelToWorldTransform(void) const
{
    if (mParentTransform == nullptr)
    {
        Matrix3x3<float> translation     = MATRIX3x3::BuildTranslation<float>(Vector2<float>{mTranslation.x, mTranslation.y});
        Matrix3x3<float> rotation        = MATRIX3x3::BuildRotation<float>(mRotation);
        Matrix3x3<float> scale           = MATRIX3x3::BuildScale<float>(Vector2<float>{mScale.x, mScale.y});
        Matrix3x3<float> transformMatrix = translation * rotation * scale;
        return transformMatrix;
    }
    else
    {
        Matrix3x3<float> translation     = MATRIX3x3::BuildTranslation<float>(Vector2<float>{mTranslation.x, mTranslation.y});
        Matrix3x3<float> rotation        = MATRIX3x3::BuildRotation<float>(mRotation);
        Matrix3x3<float> scale           = MATRIX3x3::BuildScale<float>({mScale.x / mParentTransform->GetScale().x, mScale.y / mParentTransform->GetScale().y});
        Matrix3x3<float> transformMatrix = translation * rotation * scale;
        return mParentTransform->GetModelToWorldTransform() * transformMatrix;
    }
}

Matrix3x3<float> Transform::GetModelToWorldCollisionTransform(void) const
{
    if (mParentTransform == nullptr)
    {
        Matrix3x3<float> translation     = MATRIX3x3::BuildTranslation<float>(Vector2<float>{mTranslation.x, mTranslation.y});
        Matrix3x3<float> rotation        = MATRIX3x3::BuildRotation<float>(mRotation);
        Matrix3x3<float> scale           = MATRIX3x3::BuildScale<float>({mScale.x, mScale.y});
        Matrix3x3<float> transformMatrix = translation * rotation * scale;
        return transformMatrix;
    }
    else
    {
        Matrix3x3<float> translation     = MATRIX3x3::BuildTranslation<float>(Vector2<float>{mTranslation.x, mTranslation.y});
        Matrix3x3<float> rotation        = MATRIX3x3::BuildRotation<float>(mRotation);
        Matrix3x3<float> scale           = MATRIX3x3::BuildScale<float>({mScale.x, mScale.y});
        Matrix3x3<float> transformMatrix = translation * rotation * scale;
        return mParentTransform->GetModelToWorldTransform() * transformMatrix;
    }
}