/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : AllComponents.hpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Daeun Jeong
Secondary :
- End Header ----------------------------------------------------------------*/
#pragma once

#include "ComponentType.hpp"
#include "Component.hpp"
#include "Sprite/Sprite.hpp"
#include "Pressure/Pressure.hpp"
#include "MovingPlatform/MovingPlatform.hpp"
#include "Player/Player.hpp"
#include "Trigger/Trigger.hpp"
#include "Transform/Transform.hpp"
#include "Obstacle/Obstacle.hpp"
#include "Fan/Fan.hpp"
#include "Door/Door.hpp"
#include "Collision/Collision.hpp"
#include "Button/Button.hpp"
#include "Battery/Battery.hpp"
#include "Respawn/Respawn.hpp"