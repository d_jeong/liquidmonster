/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : Logger.cpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Daeun Jeong
Secondary :
- End Header ----------------------------------------------------------------*/

#include "Logger.hpp"

#ifdef _DEBUG
    Logger LOGGER(Logger::eSeverity::DEBUG, true);

#else
    Logger LOGGER(Logger::eSeverity::EVENT, false);

#endif

void Logger::Log(eSeverity severity, std::string displayText)
{
    if (mSeverityLevel <= severity)
    {
        std::string severityType;

        switch (severity)
        {
        case eSeverity::DEBUG:
        {
            severityType = "Debug";
            break;
        }
        case eSeverity::ERROR:
        {
            severityType = "Error";
            break;
        }
        case eSeverity::EVENT:
        {
            severityType = "Event";
            break;
        }
        case eSeverity::VERBOSE:
        {
            severityType = "Verb ";
            break;
        }
        default:
        {
            break;
        }
        }
        
        mOutStream << "[" << TIMER->GetDeltaTime() << "]\t";
        mOutStream << severityType << "\t" << displayText << std::endl;
    }
}