/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : Logger.hpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Daeun Jeong
Secondary :
- End Header ----------------------------------------------------------------*/

#pragma once
#include <string>
#include <fstream>
#include <iostream>
#include "../Tool/Timer/Timer.hpp"

class Logger
{
public:
    enum class eSeverity
    {
        VERBOSE,    // This option is for minor messages, which could be useful 
        DEBUG,	    // This should only be used when you are currently trying to debug something
        EVENT,	    // These are general events, such as key press, exit state, enter state, enter state finish
        ERROR,	    // This is for an error, such as an asset is not found
    };

    Logger(eSeverity severity, bool shouldUseConsole) : mSeverityLevel(severity), mOutStream("Trace.log")
    {
        if (shouldUseConsole == true)
        {
            mOutStream.set_rdbuf(std::cout.rdbuf());
        }
    }

    void LogError(std::string text) { Log(eSeverity::ERROR, text); }
    void LogEvent(std::string text) { Log(eSeverity::EVENT, text); }
    void LogDebug(std::string text) { Log(eSeverity::DEBUG, text); }
    void LogVerbose(std::string text) { Log(eSeverity::VERBOSE, text); }

private:
    void Log(eSeverity, std::string displayText);

    std::ofstream mOutStream;
    eSeverity mSeverityLevel;
};

extern Logger LOGGER;