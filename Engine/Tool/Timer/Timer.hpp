/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : Timer.hpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Haewon Shon
Secondary :
- End Header ----------------------------------------------------------------*/
#pragma once

#include <chrono>

class Timer
{
public:
    Timer();
    ~Timer();

    void  Initialize();
    void  ResetTimeStamp();
    float GetDeltaTime();
    float GetElapsedSeconds() const;

private:
    using clock_t = std::chrono::high_resolution_clock;
    using second_t = std::chrono::duration<float>;

    std::chrono::time_point<clock_t> mTimeStamp;

    std::chrono::time_point<clock_t> mPreviousTime;
    std::chrono::time_point<clock_t> mCurrentTime;
    std::chrono::time_point<clock_t> mStartTime;
};

extern Timer* TIMER;