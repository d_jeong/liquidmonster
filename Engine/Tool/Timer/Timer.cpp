/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : Timer.cpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Haewon Shon
Secondary :
- End Header ----------------------------------------------------------------*/
#include "Timer.hpp"

Timer* TIMER = new Timer();

Timer::Timer() : mTimeStamp(clock_t::now())
{
    TIMER = this;
}

Timer::~Timer()
{
    delete TIMER;
    TIMER = nullptr;
}

void Timer::Initialize()
{
    mStartTime    = clock_t::now();
    mPreviousTime = mStartTime;
    mCurrentTime  = mStartTime;
}

void Timer::ResetTimeStamp()
{
    mTimeStamp = clock_t::now();
}

float Timer::GetDeltaTime()
{
    mCurrentTime = clock_t::now();
    std::chrono::duration<float> deltaTime(mCurrentTime - mPreviousTime);
    mPreviousTime = clock_t::now();

    return deltaTime.count();
}

float Timer::GetElapsedSeconds() const
{
    return std::chrono::duration_cast<second_t>(clock_t::now() - mTimeStamp).count();
}