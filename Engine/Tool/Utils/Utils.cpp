/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : Utils.cpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Haewon Shon
Secondary :
- End Header ----------------------------------------------------------------*/

#include <cstdlib>
#include <ctime>
#include <random>

bool initFlag = false;
// Random value initialize with current time
void RandomInit()
{
    initFlag = true;
}

// Get Random integer value in range
int GetRandomInt(int min, int max)
{
    if (!initFlag)
    {
        RandomInit();
    }
    return rand() % (max - min + 1) + min;
}

// Get Random float value in range
float GetRandomFloat(float min, float max)
{
    if (!initFlag)
    {
        RandomInit();
    }
    return static_cast<float>(rand()) / (static_cast<float>(RAND_MAX / (max - min)) + min);
}