/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : Screenshot.cpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Haewon Shon
Secondary :
- End Header ----------------------------------------------------------------*/

#define STB_IMAGE_IMPLEMENTATION
#define STBI_ONLY_PNG
#include "../Engine/MathLibrary/Vector4.hpp"
#include <filesystem>
#include <glew.h>
#include <vector>

#define STB_IMAGE_WRITE_IMPLEMENTATION
#define STBI_MSC_SECURE_CRT

#pragma warning(push)
#pragma warning(disable : 26451) // disable arithmetic overlow warning in stb_image_write.h
#include <stb_image_write.h>
#pragma warning(pop)

void CaptureScreenshot(int pixelWidth, int pixelHeight, int left, int bottom, const std::filesystem::path filePath) noexcept
{
    std::vector<Vector4<unsigned char>> data;
    data.resize((size_t)pixelWidth * pixelHeight);

    glReadBuffer(GL_BACK_LEFT);
    glReadnPixels(left, bottom, pixelWidth, pixelHeight, GL_RGBA, GL_UNSIGNED_BYTE, pixelWidth * pixelHeight * 4, &data[0]);

    for (int i = 0; i < pixelHeight / 2; ++i)
    {
        for (int j = 0; j < pixelWidth; ++j)
        {
            Vector4<unsigned char> temp                  = data[i * pixelWidth + j];
            data[i * pixelWidth + j]                     = data[(pixelHeight - 1 - i) * pixelWidth + j];
            data[(pixelHeight - 1 - i) * pixelWidth + j] = temp;
        }
    }
    std::string str = filePath.generic_string();
    stbi_write_png(str.c_str(), pixelWidth, pixelHeight, 4, &data[0], 4 * pixelWidth);
}