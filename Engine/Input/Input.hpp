/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : Input.hpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Haewon Shon
Secondary :
- End Header ----------------------------------------------------------------*/

#pragma once

#include "../MathLibrary/Vector2.hpp"
#include <SDL.h>
#include <bitset>

#define SDL_NUM_GAMEPAD 16

enum eMouseButton
{
    MOUSE_BUTTON_NONE = 0,
    MOUSE_BUTTON_LEFT,
    MOUSE_BUTTON_WHEEL,
    MOUSE_BUTTON_RIGHT,
    MOUSE_BUTTON_WHEEL_UP,
    MOUSE_BUTTON_WHEEL_DOWN,
    MOUSE_BUTTON_MOUSE_NUM
};

class Input
{
public:
    Input();
    ~Input();

    static void Update(SDL_Event &inputEvent);

    static bool IsKeyPressed(SDL_Scancode scancode);
    static bool IsKeyReleased(SDL_Scancode scancode);
    static bool IsKeyTriggered(SDL_Scancode scancode);

    static bool IsMousePressed(eMouseButton mouseButton);
    static bool IsMouseReleased(eMouseButton mouseButton);
    static bool IsMouseTriggered(eMouseButton mouseButton);

    static bool IsGamePadPressed(SDL_GameControllerButton GamepadButton);
    static bool IsGamePadTriggered(SDL_GameControllerButton GamepadButton);
    static bool IsGamePadReleased(SDL_GameControllerButton GamepadButton);
    static bool IsGamePadAnyTriggered(void);

    static Vector2<float> GetMousePosition(void);

    static void Reset();

private:
    static void SetPressedKey(SDL_Scancode scancode);
    static void SetReleasedKey(SDL_Scancode scancode);

    static void SetPressedMouse(eMouseButton mouseButton);
    static void SetReleasedMouse(eMouseButton mouseButton);

    static void SetMousePosition(Vector2<float> position);
    static void SetMouseWheel(SDL_MouseWheelEvent wheelEvent);

    static void SetPressedGamepad(int GamepadButton);

    static std::bitset<SDL_NUM_SCANCODES> mKeyPressed;
    static std::bitset<SDL_NUM_SCANCODES> mKeyReleased;
    static std::bitset<SDL_NUM_SCANCODES> mKeyTriggered;

    static std::bitset<MOUSE_BUTTON_MOUSE_NUM> mMousePressed;
    static std::bitset<MOUSE_BUTTON_MOUSE_NUM> mMouseReleased;
    static std::bitset<MOUSE_BUTTON_MOUSE_NUM> mMouseTriggered;

    static std::bitset<SDL_NUM_GAMEPAD> mGamePadPressed;
    static std::bitset<SDL_NUM_GAMEPAD> mGamePadReleased;
    static std::bitset<SDL_NUM_GAMEPAD> mGamePadTriggered;

    static Vector2<float> mMouse;

    static Vector2<float> mRightPadPosition;
    static Vector2<float> mLeftPadPosition;
};