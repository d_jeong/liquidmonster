/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : Input.cpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Haewon Shon
Secondary :
- End Header ----------------------------------------------------------------*/

#include "Input.hpp"
#include "../Application/Application.hpp"
#include <iostream>

std::bitset<SDL_NUM_SCANCODES> Input::mKeyPressed;
std::bitset<SDL_NUM_SCANCODES> Input::mKeyReleased;
std::bitset<SDL_NUM_SCANCODES> Input::mKeyTriggered;

std::bitset<MOUSE_BUTTON_MOUSE_NUM> Input::mMousePressed;
std::bitset<MOUSE_BUTTON_MOUSE_NUM> Input::mMouseReleased;
std::bitset<MOUSE_BUTTON_MOUSE_NUM> Input::mMouseTriggered;

std::bitset<SDL_NUM_GAMEPAD> Input::mGamePadPressed;
std::bitset<SDL_NUM_GAMEPAD> Input::mGamePadTriggered;
std::bitset<SDL_NUM_GAMEPAD> Input::mGamePadReleased;

Vector2<float> Input::mMouse;

Vector2<float> Input::mRightPadPosition;
Vector2<float> Input::mLeftPadPosition;

Input::Input()
{
    mKeyPressed.reset();
    mKeyReleased.reset();
    mKeyTriggered.reset();

    mMousePressed.reset();
    mMouseReleased.reset();
    mMouseTriggered.reset();

    mGamePadPressed.reset();
    mGamePadTriggered.reset();
    mGamePadReleased.reset();

    mMouse = Vector2<float>{0.0f, 0.0f};

    mRightPadPosition = Vector2<float>{0.0f, 0.0f};
    mLeftPadPosition  = Vector2<float>{0.0f, 0.0f};
}

Input::~Input()
{
}

void Input::Update(SDL_Event &inputEvent)
{
    switch (inputEvent.type)
    {
        case SDL_KEYDOWN:
            SetPressedKey(inputEvent.key.keysym.scancode);
            break;
        case SDL_KEYUP:
            SetReleasedKey(inputEvent.key.keysym.scancode);
            break;
        case SDL_MOUSEMOTION:
            SetMousePosition(Vector2<float>{(float)inputEvent.motion.x, (float)inputEvent.motion.y});
            break;
        case SDL_MOUSEBUTTONDOWN:
            SetPressedMouse(static_cast<eMouseButton>(inputEvent.button.button));
            break;
        case SDL_MOUSEBUTTONUP:
            SetReleasedMouse(static_cast<eMouseButton>(inputEvent.button.button));
            break;
        case SDL_MOUSEWHEEL:
            SetMouseWheel(inputEvent.wheel);
            break;
        case SDL_CONTROLLERBUTTONDOWN:
            SetPressedGamepad(inputEvent.cbutton.button);
            break;
        case SDL_CONTROLLERBUTTONUP:
            SetPressedGamepad(inputEvent.cbutton.button);
            break;
        case SDL_CONTROLLERAXISMOTION:
            SetPressedGamepad(inputEvent.cbutton.button);
            break;
        default:
            break;
    }

    // std::cout << "GamePad : " << (int)inputEvent.cbutton.button << std::endl;

    // std::cout << "JoyStick : " << (int)inputEvent.jaxis.value << std::endl;

    // JoyStick
    if (inputEvent.jaxis.value < -800 || inputEvent.jaxis.value > 800)
    {
        if (inputEvent.jaxis.axis == 2)
        {
            mRightPadPosition.x = inputEvent.jaxis.value;
        }
        else if (inputEvent.jaxis.axis == 4)
        {
            mRightPadPosition.y = inputEvent.jaxis.value;
        }
        if (inputEvent.jaxis.value < -800 || inputEvent.jaxis.value > 800)
        {
            if (inputEvent.jaxis.axis == 0)
            {
                mLeftPadPosition.x = inputEvent.jaxis.value;
            }
            else if (inputEvent.jaxis.axis == 1)
            {
                mLeftPadPosition.y = inputEvent.jaxis.value;
            }
        }
    }
}

bool Input::IsKeyPressed(SDL_Scancode scancode)
{
    return mKeyPressed[scancode];
}

bool Input::IsKeyReleased(SDL_Scancode scancode)
{
    return mKeyReleased[scancode];
}

bool Input::IsKeyTriggered(SDL_Scancode scancode)
{
    return mKeyTriggered[scancode];
}

bool Input::IsMousePressed(eMouseButton mouseButton)
{
    return mMousePressed[mouseButton];
}

bool Input::IsMouseReleased(eMouseButton mouseButton)
{
    return mMouseReleased[mouseButton];
}

bool Input::IsMouseTriggered(eMouseButton mouseButton)
{
    return mMouseTriggered[mouseButton];
}

bool Input::IsGamePadPressed(SDL_GameControllerButton GamepadButton)
{
    return mGamePadPressed[GamepadButton];
}

bool Input::IsGamePadTriggered(SDL_GameControllerButton GamepadButton)
{
    return mGamePadTriggered[GamepadButton];
}

bool Input::IsGamePadReleased(SDL_GameControllerButton GamepadButton)
{
    return mGamePadReleased[GamepadButton];
}

bool Input::IsGamePadAnyTriggered(void)
{
    return mGamePadTriggered.any();
}

Vector2<float> Input::GetMousePosition(void)
{
    return mMouse;
}

void Input::SetPressedKey(SDL_Scancode scancode)
{
    if (mKeyPressed[scancode] != true)
    {
        mKeyPressed[scancode]   = true;
        mKeyTriggered[scancode] = true;
    }
}

void Input::SetReleasedKey(SDL_Scancode scancode)
{
    mKeyPressed[scancode]  = false;
    mKeyReleased[scancode] = true;
}

void Input::SetPressedMouse(eMouseButton mouseButton)
{
    if (mMousePressed[mouseButton] != true)
    {
        mMousePressed[mouseButton]   = true;
        mMouseTriggered[mouseButton] = true;
    }
}

void Input::SetReleasedMouse(eMouseButton mouseButton)
{
    mMousePressed[mouseButton]  = false;
    mMouseReleased[mouseButton] = true;
}

void Input::SetMousePosition(Vector2<float> position)
{
    mMouse.x = position.x - APPLICATION->GetWidth() / 2.f;
    mMouse.y = -(position.y - APPLICATION->GetHeight() / 2.f);
}

void Input::SetMouseWheel(SDL_MouseWheelEvent wheelEvent)
{
    if (wheelEvent.y > 0)
    {
        mMouseTriggered[eMouseButton::MOUSE_BUTTON_WHEEL_UP] = true;
    }
    else if (wheelEvent.y < 0)
    {
        mMouseTriggered[eMouseButton::MOUSE_BUTTON_WHEEL_DOWN] = true;
    }
}

void Input::SetPressedGamepad(int GamepadButton)
{
    if (mGamePadPressed[GamepadButton] != true)
    {
        mGamePadPressed[GamepadButton]   = true;
        mGamePadTriggered[GamepadButton] = true;
    }
}

void Input::Reset()
{
    mKeyReleased.reset();
    mKeyTriggered.reset();

    mMouseReleased.reset();
    mMouseTriggered.reset();

    mGamePadReleased.reset();
    mGamePadTriggered.reset();
}