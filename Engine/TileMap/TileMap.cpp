/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : TileMap.cpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Haewon Shon
Secondary :
- End Header ----------------------------------------------------------------*/

#include "TileMap.hpp"
#include <iostream>

TileMap* TILE_MAP = new TileMap;

TileMap::TileMap()
{
    TILE_MAP       = this;
    mWidthTileNum  = MAX_WIDTH / TILE_SIZE;
    mHeightTileNum = MAX_HEIGHT / TILE_SIZE;
}

void TileMap::Initialize(void)
{
}

void TileMap::Update(float dt)
{
}

void TileMap::Delete()
{
    ClearAllTiles();
    delete TILE_MAP;
    TILE_MAP = nullptr;
}

void TileMap::CreateGraphicTile(int grid, eTexture texture, bool isFlip)
{
    int x = grid % mWidthTileNum;
    int y = grid / mWidthTileNum;

    CreateGraphicTile(x, y, texture, isFlip);
}

void TileMap::CreateGraphicTile(int x, int y, eTexture texture, bool isFlip)
{
    if (x < 0 || x >= mWidthTileNum || y < 0 || y >= mHeightTileNum)
    {
        std::cout << "FAILED TO CREATE TILE - POSITION IS NOT VALID" << std::endl;
        std::cout << "Pos : " << x << ", " << y << std::endl;
        return;
    }

    Tile* tile = new Tile;
    tile->type = eTileType::GRAPHICS;
    tile->transform.SetPosition(TileToWorldPos({x, y}));
    if (isFlip)
    {
        tile->transform.SetScale({static_cast<float>(-TILE_SIZE), static_cast<float>(TILE_SIZE)});
    }
    else
    {
        tile->transform.SetScale({static_cast<float>(TILE_SIZE), static_cast<float>(TILE_SIZE)});
    }
    tile->texture  = texture;
    tile->isFliped = false;

    mGraphicTiles.insert({x + (mWidthTileNum * y), tile});
}

void TileMap::CreatePhysicsTile(int grid, eTexture texture, bool isFlip)
{
    int x = grid % mWidthTileNum;
    int y = grid / mWidthTileNum;

    CreatePhysicsTile(x, y, texture, isFlip);
}

void TileMap::CreatePhysicsTile(int x, int y, eTexture texture, bool isFlip)
{
    if (x < 0 || x >= mWidthTileNum || y < 0 || y >= mHeightTileNum)
    {
        std::cout << "FAILED TO CREATE TILE" << std::endl;
        return;
    }

    if (mPhysicsTileGrid[y][x])
    {
        std::cout << "FAILED TO CREATE TILE - TILE IS ALREADY EXISTS" << std::endl;
        std::cout << "Pos : " << x << ", " << y << ", Index : " << x + y * mWidthTileNum << std::endl;
    }

    Tile* tile = new Tile;
    tile->type = eTileType::PHYSICS;
    tile->transform.SetPosition(TileToWorldPos({x, y}));
    if (isFlip)
    {
        tile->transform.SetScale({static_cast<float>(-TILE_SIZE), static_cast<float>(TILE_SIZE)});
    }
    else
    {
        tile->transform.SetScale({static_cast<float>(TILE_SIZE), static_cast<float>(TILE_SIZE)});
    }
    tile->texture  = texture;
    tile->isFliped = false;

    mPhysicsTiles.insert({x + (mWidthTileNum * y), tile});
    mPhysicsTileGrid[y][x] = true;
}

bool TileMap::IsTherePhysicsTileOnPosition(const Vector2<int>& pos) const
{
    if (pos.x < 0 || pos.x >= mWidthTileNum || pos.y < 0 || pos.y >= mHeightTileNum)
    {
        return false;
    }
    else
    {
        return mPhysicsTileGrid[pos.y][pos.x];
    }
}

bool TileMap::IsThereGraphicsTileOnPosition(const Vector2<int>& pos) const
{
    if (pos.x < 0 || pos.x >= mWidthTileNum || pos.y < 0 || pos.y >= mHeightTileNum)
    {
        return false;
    }
    else
    {
        return mGraphicsTileGrid[pos.y][pos.x];
    }
}

Tile* TileMap::FindPhysicsTileOnPosition(const Vector2<float>& pos)
{
    Vector2<int> tilePos = WorldToTilePos(pos);
    if (IsTherePhysicsTileOnPosition(tilePos))
    {
        return mPhysicsTiles[tilePos.x + tilePos.y * mWidthTileNum];
    }
    return nullptr;
}

Tile* TileMap::FindGraphicsTileOnPosition(const Vector2<float>& pos)
{
    Vector2<int> tilePos = WorldToTilePos(pos);
    if (IsThereGraphicsTileOnPosition(tilePos))
    {
        return mGraphicTiles[tilePos.x + tilePos.y * mWidthTileNum];
    }
    return nullptr;
}

Tile* TileMap::FindTileOnMousePosition()
{
    return nullptr;
}

void TileMap::ClearAllTiles(void)
{
    ClearGraphicTiles();
    ClearPhysicsTiles();
}

void TileMap::ClearGraphicTiles(void)
{
    for (auto tile : mGraphicTiles)
    {
        delete tile.second;
    }
    mGraphicTiles.clear();
    for (int i = 0; i < mHeightTileNum; ++i)
    {
        for (int j = 0; j < mWidthTileNum; ++j)
        {
            mGraphicsTileGrid[i][j] = false;
        }
    }
}

void TileMap::ClearPhysicsTiles(void)
{
    for (auto tile : mPhysicsTiles)
    {
        delete tile.second;
    }
    mPhysicsTiles.clear();
    for (int i = 0; i < mHeightTileNum; ++i)
    {
        for (int j = 0; j < mWidthTileNum; ++j)
        {
            mPhysicsTileGrid[i][j] = false;
        }
    }
}

void TileMap::EraseGraphicTile(const Vector2<int>& pos)
{
    std::unordered_map<int, Tile*>::iterator tile = mGraphicTiles.find(pos.x + (mWidthTileNum * pos.y));

    if (tile != mGraphicTiles.end())
    {
        mGraphicTiles.erase(pos.x + (pos.y * mWidthTileNum));
    }
}

void TileMap::EraseGraphicTile(const int pos)
{
    Vector2<int> position = {pos % mWidthTileNum, pos / mWidthTileNum};

    if (IsThereGraphicsTileOnPosition(position))
    {
        mGraphicTiles.erase(pos);
        mGraphicsTileGrid[position.y][position.x] = false;
    }
}

void TileMap::ErasePhysicsTile(const Vector2<int>& pos)
{
    if (IsTherePhysicsTileOnPosition(pos))
    {
        mPhysicsTiles.erase(pos.x + (pos.y * mWidthTileNum));
        mPhysicsTileGrid[pos.y][pos.x] = false;
    }
}

void TileMap::ErasePhysicsTile(const int pos)
{
    Vector2<int> position = {pos % mWidthTileNum, pos / mWidthTileNum};

    if (IsTherePhysicsTileOnPosition(position))
    {
        mPhysicsTiles.erase(pos);
        mPhysicsTileGrid[position.y][position.x] = false;
    }
}

std::unordered_map<int, Tile*> TileMap::GetGraphicTiles(void) const
{
    return mGraphicTiles;
}

std::unordered_map<int, Tile*> TileMap::GetPhysicsTiles(void) const
{
    return mPhysicsTiles;
}

int TileMap::GetWidthTileNum(void) const
{
    return mWidthTileNum;
}

int TileMap::GetHeightTileNum(void) const
{
    return mHeightTileNum;
}

Vector2<int> TileMap::WorldToTilePos(const Vector2<float>& worldPos)
{
    int newX = static_cast<int>((worldPos.x + (MAX_WIDTH / 2))) / TILE_SIZE;
    int newY = static_cast<int>(-((worldPos.y) - MAX_HEIGHT / 2)) / TILE_SIZE;
    return {newX, newY};
}

Vector2<float> TileMap::TileToWorldPos(const Vector2<int>& tilePos)
{
    float          newXPos = static_cast<float>(tilePos.x * TILE_SIZE - MAX_WIDTH / 2);
    float          newYPos = static_cast<float>(-tilePos.y * TILE_SIZE + MAX_HEIGHT / 2);
    Vector2<float> pos     = {newXPos, newYPos};
    pos += {TILE_SIZE / 2, -TILE_SIZE / 2};
    return pos;
}

void TileMap::ChangeGraphicTileTexture(int grid, eTexture texture)
{
    mGraphicTiles[grid]->texture = texture;
}

void TileMap::ChangeGraphicTileTexture(int x, int y, eTexture texture)
{
    ChangeGraphicTileTexture(x + y * mWidthTileNum, texture);
}

// return middle of the tilemap, bottomRight, topLeft
Vector2<float> TileMap::GetTileMapInfo(Vector2<float>& bottomRight, Vector2<float>& topLeft)
{
    int widthTileNum  = MAX_WIDTH / TILE_SIZE;
    int heightTileNum = MAX_HEIGHT / TILE_SIZE;

    int top    = heightTileNum;
    int bottom = 0;
    int left   = widthTileNum;
    int right  = 0;

    for (int i = 0; i < heightTileNum; ++i)
    {
        for (int j = 0; j < widthTileNum; ++j)
        {
            if (IsTherePhysicsTileOnPosition({j, i}))
            {
                top    = (top > i) ? i : top;
                bottom = (bottom < i) ? i : bottom;
                left   = (left > j) ? j : left;
                right  = (right < j) ? j : right;
            }
        }
    }

    bottomRight = TileToWorldPos({right, bottom});
    topLeft     = TileToWorldPos({left, top});

    bottomRight += {25.f, -25.f};
    topLeft += {-25.f, 25.f};

    return (topLeft + bottomRight) / 2.f;
}

Tile* TileMap::FindGraphicsTileByGrid(int grid)
{
    return mGraphicTiles.find(grid)->second;
}

Tile* TileMap::FindPhysicsTileByGrid(int grid)
{
    return mPhysicsTiles.find(grid)->second;
}

std::string Tile::GetTileConnectedObjectName(void)
{
    return connectedObjectName;
}

void Tile::SetTileConnectedObjectName(std::string name)
{
    connectedObjectName = name;
}
