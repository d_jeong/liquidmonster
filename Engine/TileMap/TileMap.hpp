/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : TileMap.hpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Haewon Shon
Secondary :
- End Header ----------------------------------------------------------------*/
#pragma once

#include <unordered_map>
#include "../MathLibrary/Vector2.hpp"
#include "../Component/Transform/Transform.hpp"

const int TILE_SIZE  = 50;
const int MAX_WIDTH  = 8000;
const int MAX_HEIGHT = 4000;

enum class eTexture;

enum class eTileType
{
    PHYSICS,
    GRAPHICS
};

struct Tile
{
    eTileType type;
    Transform transform;
    eTexture  texture;
    bool      isFliped;
    std::string connectedObjectName = "";

    std::string GetTileConnectedObjectName(void);
    void SetTileConnectedObjectName(std::string name);
};

class TileMap
{
public:
    TileMap();

    void Initialize(void);
    void Update(float dt);
    void Delete();

    void CreateGraphicTile(int grid, eTexture texture, bool isFlip);
    void CreateGraphicTile(int x, int y, eTexture texture, bool isFlip);
    void CreatePhysicsTile(int grid, eTexture texture, bool isFlip);
    void CreatePhysicsTile(int x, int y, eTexture texture, bool isFlip);

    bool  IsTherePhysicsTileOnPosition(const Vector2<int>& pos) const;
    bool  IsThereGraphicsTileOnPosition(const Vector2<int>& pos) const;
    Tile* FindPhysicsTileOnPosition(const Vector2<float>& pos);
    Tile* FindGraphicsTileOnPosition(const Vector2<float>& pos);
    Tile* FindTileOnMousePosition(void);

    void ClearAllTiles(void);
    void ClearGraphicTiles(void);
    void ClearPhysicsTiles(void);

    void EraseGraphicTile(const Vector2<int>& pos);
    void EraseGraphicTile(const int pos);
    void ErasePhysicsTile(const Vector2<int>& pos);
    void ErasePhysicsTile(const int pos);

    std::unordered_map<int, Tile*> GetGraphicTiles(void) const;
    std::unordered_map<int, Tile*> GetPhysicsTiles(void) const;

    int GetWidthTileNum(void) const;
    int GetHeightTileNum(void) const;

    Vector2<int>   WorldToTilePos(const Vector2<float>& worldPos);
    Vector2<float> TileToWorldPos(const Vector2<int>& tilePos);

    void ChangeGraphicTileTexture(int grid, eTexture texture);
    void ChangeGraphicTileTexture(int x, int y, eTexture texture);

    Vector2<float> GetTileMapInfo(Vector2<float>& bottomRight, Vector2<float>& topLeft);

    Tile* FindGraphicsTileByGrid(int grid);
    Tile* FindPhysicsTileByGrid(int grid);

private:
    std::unordered_map<int, Tile*> mGraphicTiles;
    std::unordered_map<int, Tile*> mPhysicsTiles;
    bool                           mPhysicsTileGrid[MAX_HEIGHT / TILE_SIZE][MAX_WIDTH / TILE_SIZE] = {false};
    bool                           mGraphicsTileGrid[MAX_HEIGHT / TILE_SIZE][MAX_WIDTH / TILE_SIZE] = {false};

    int mWidthTileNum;
    int mHeightTileNum;
};

extern TileMap* TILE_MAP;