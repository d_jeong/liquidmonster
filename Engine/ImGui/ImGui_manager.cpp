/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : ImGui_manager.cpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Sejeong Park
Secondary :
- End Header ----------------------------------------------------------------*/

#include "ImGui_manager.hpp"
#include "..//Application/Application.hpp"
#include "../Graphics/Graphics.hpp"
#include "../Object/Object.hpp"
#include "../Object/ObjectDepth.hpp"
#include "../Object/ObjectManager/ObjectManager.hpp"
#include "helper/imgui_internal.h"
#include "../Game/State/State.hpp"
#include "../Game/State/StateManager/StateManager.hpp"
#include "../Data/Data.hpp"
#include "../Object/CustomPhysics/CustomPhysics.hpp"
#include "../Component/Transform/Transform.hpp"
#include "../Game/State/StateManager/StateManager.hpp"

ImGui_manager* IMGUI = nullptr;

ImGui_manager::ImGui_manager(void)
{
    IMGUI      = this;
    mGlContext = SDL_GL_GetCurrentContext();

    mLevelEditor       = false;
    mTileEditor        = false;
    mPlayerInformation = false;

    mTileDrawing          = false;
    mObjectComponentCheck = false;

    mIsLoaded = false;
}

void ImGui_manager::LevelEditor(bool showWindow)
{
    if (showWindow == false)
    {
        mObjectComponentCheck = false;
        return;
    }
    {
        ImGui::TextDisabled("(Level tip)");
        if (ImGui::IsItemHovered())
        {
            ImGui::BeginTooltip();
            ImGui::PushTextWrapPos(ImGui::GetFontSize() * 35.0f);
            ImGui::TextUnformatted("Press Tab for starting this editor : stop the players movement");
            ImGui::PopTextWrapPos();
            ImGui::EndTooltip();
        }
    }

    static char name[128] = "";
    ImGui::InputTextWithHint("Name", "enter name here", name, IM_ARRAYSIZE(name));

    const char* objectTypes[] = {"Steel Box", "Wood Box", /*"Door", */ "Fan", "Trigger", "Laser", "Moving Platform", "Pressure", "Button", /*"Key",*/ "Battery", "Battery Home"};

    static int currentObjectType = 0;
    ImGui::Combo("Object type list", &currentObjectType, objectTypes, IM_ARRAYSIZE(objectTypes));


    if (ImGui::Button("Add Object"))
    {
        mObjectComponentCheck = true;
        HelperFunctionAddObject(name, currentObjectType);
    }

    if (ImGui::Button("Delete Object"))
    {
        if (mClickObject != nullptr)
        {
            OBJECT_MANAGER->DeleteObject(mClickObject);
            mClickObject = nullptr;
        }
    }

    std::string clickObject_name;

    if (mClickObject != nullptr)
    {
        clickObject_name = mClickObject->GetName();
    }

    ImGui::Text("Click Object name : %s", clickObject_name.c_str());

    if (mClickObject)
    {
        ImGui::BulletText("Object components");
        ImGui::Indent();

        static bool selected[10] = {false, false, false, false, false, false, false, false, false, false};

        if (mObjectComponentCheck)
        {
            ComponentCheck(selected);
            mObjectComponentCheck = false;
        }

        ImGui::Selectable("Sprite", &selected[0]);
        ImGui::Selectable("Transform", &selected[1]);
        ImGui::Selectable("Collision", &selected[2]);
        ImGui::Selectable("Fan", &selected[3]);
        ImGui::Selectable("MovingPlatform", &selected[4]);
        ImGui::Selectable("Button", &selected[5]);
        ImGui::Selectable("Trigger", &selected[6]);
        ImGui::Selectable("Laser", &selected[7]);
        ImGui::Selectable("Pressure", &selected[8]);
        ImGui::Selectable("Battery", &selected[9]);

        ImGui::Unindent();
        ImGui::Separator();
        HelperFunctionObjectComponent(selected);
    }
}

void ImGui_manager::TileEditor(bool showWindow)
{
    if (showWindow == false)
    {
        mTileDrawing             = false;
        mConnectedWithObjectTile = nullptr;
        return;
    }

    static int     test          = 0;
    Vector3<float> calculatedPos = GRAPHICS->GetInverseCameraMatrix() * Vector3<float>{Input::GetMousePosition().x, Input::GetMousePosition().y, 1.f};
    Vector2<int>   tilePos       = TILE_MAP->WorldToTilePos({calculatedPos.x, calculatedPos.y});
    ImGui::Text("Tile Position : (%d, %d)", tilePos.x, tilePos.y);

    static bool showGrid = false;

    if (ImGui::Checkbox("Show Grid", &showGrid))
    {
        if (showGrid)
        {
            GRAPHICS->SetTileBoxDrawing(true);
        }
        else
        {
            GRAPHICS->SetTileBoxDrawing(false);
        }
    }

    ImGui::Separator();

    {
        static bool connectingObjectTile = false;
        ImGui::Text("<For connecting with tiles of button or battery>");
        ImGui::Text("Press B on object that you want to connect");

        if (mConnectedWithObjectTile != nullptr)
        {
            if (mConnectedWithObjectTile->IsGetComponent(eComponentType::BATTERY))
            {
                ImGui::Text("Connected with : Battery");
            }
            else if (mConnectedWithObjectTile->IsGetComponent(eComponentType::BUTTON))
            {
                ImGui::Text("Connected with : Button");
            }
        }

        if (ImGui::Button("Make Connected object to nullptr"))
        {
            mConnectedWithObjectTile = nullptr;
        }
    }

    ImGui::NewLine();
    ImGui::Separator();
    {
        ImGui::TextDisabled("(Tile tip)");

        if (ImGui::IsItemHovered())
        {
            ImGui::BeginTooltip();
            ImGui::PushTextWrapPos(ImGui::GetFontSize() * 35.0f);
            ImGui::TextUnformatted("Press C : Create physics tile\nPress V : Create graphics tile\nPress Z : delete physics tile\nPress X : delete graphics tile");
            ImGui::PopTextWrapPos();
            ImGui::EndTooltip();
        }
    }

    for (auto& temp : mTileTextures)
    {
        if (ImGui::ImageButton(temp.second, ImVec2(32, 32)))
        {
            mCurrentTexture = temp.first;
            mTileDrawing    = true;
            ++test;
        }

        if (temp.first == eTexture::TILE_EXAMPLE_3 || temp.first == eTexture::TILE_WIRE_RED_10 || temp.first == eTexture::TILE_WIRE_BLUE_10)
        {
            continue;
        }

        ImGui::SameLine();
    }

    ImGui::NewLine();
    ImGui::Separator();

    ImGui::Text("pressed %d times", test);

    if (ImGui::Button("Clear Graphic Tile"))
    {
        TILE_MAP->ClearGraphicTiles();
    }

    ImGui::SameLine();
    if (ImGui::Button("clear Physics Tile"))
    {
        TILE_MAP->ClearPhysicsTiles();
    }
}

void ImGui_manager::PlayerInformation(bool showWindow)
{
    if (showWindow == false)
        return;

    static bool collisionBox = false;

    if (ImGui::Checkbox("Show collision box", &collisionBox))
    {
        if (collisionBox)
        {
            GRAPHICS->SetBoxDrawing(true);
        }
        else
        {
            GRAPHICS->SetBoxDrawing(false);
        }
    }

    auto objectMap = OBJECT_MANAGER->GetObjectVector();

    for (auto obj : objectMap)
    {
        if (eObjectType::PLAYER_1 == obj->GetObjectType())
        {
            CustomPhysics* playerPhysics   = obj->GetComponentByType<Player>(eComponentType::PLAYER)->GetCustomPhysics();
            Transform*     playerTransform = obj->GetComponentByType<Transform>(eComponentType::TRANSFORM);
            ImGui::BulletText("Player1 Information");
            ImGui::Indent();
            ImGui::Text("Velocity x : %f", playerPhysics->GetVelocity().x);
            ImGui::Text("Velocity y : %f", playerPhysics->GetVelocity().y);
            ImGui::Text("Position x : %f", playerTransform->GetPosition().x);
            ImGui::Text("Position y : %f", playerTransform->GetPosition().y);
            ImGui::Text("Collision with : %s", CollisionCheck(obj).c_str());
            ImGui::Unindent();
            ImGui::Separator();
        }

        if (eObjectType::PLAYER_2 == obj->GetObjectType())
        {
            CustomPhysics* playerPhysics   = obj->GetComponentByType<Player>(eComponentType::PLAYER)->GetCustomPhysics();
            Transform*     playerTransform = obj->GetComponentByType<Transform>(eComponentType::TRANSFORM);
            ImGui::BulletText("Player2 Information");
            ImGui::Indent();
            ImGui::Text("Velocity x : %f", playerPhysics->GetVelocity().x);
            ImGui::Text("Velocity y : %f", playerPhysics->GetVelocity().y);
            ImGui::Text("Position x : %f", playerTransform->GetPosition().x);
            ImGui::Text("Position y : %f", playerTransform->GetPosition().y);
            ImGui::Text("Collision with : %s", CollisionCheck(obj).c_str());
            ImGui::Unindent();
            ImGui::Separator();
        }

        if (eObjectType::PLAYER_COMBINED == obj->GetObjectType())
        {
            CustomPhysics* playerPhysics   = obj->GetComponentByType<Player>(eComponentType::PLAYER)->GetCustomPhysics();
            Transform*     playerTransform = obj->GetComponentByType<Transform>(eComponentType::TRANSFORM);
            ImGui::BulletText("Combined Player Information");
            ImGui::Indent();
            ImGui::Text("Velocity x : %f", playerPhysics->GetVelocity().x);
            ImGui::Text("Velocity y : %f", playerPhysics->GetVelocity().y);
            ImGui::Text("Position x : %f", playerTransform->GetPosition().x);
            ImGui::Text("Position y : %f", playerTransform->GetPosition().y);
            ImGui::Text("Collision with : %s", CollisionCheck(obj).c_str());
            ImGui::Unindent();
        }
    }
}

void ImGui_manager::Initialize(void)
{
    const char* glsl_version = "#version 130";

    mWindow = SDL_GL_GetCurrentWindow();
    // gl_context = SDL_GL_GetCurrentContext();

    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGuiIO& io = ImGui::GetIO();
    (void)io;
    ImGui::StyleColorsDark();

    ImGui_ImplSDL2_InitForOpenGL(mWindow, mGlContext);
    ImGui_ImplOpenGL3_Init(glsl_version);
    TileTextureFromData();
    ObjectTextureFromData();

    mIsLoaded = true;
}

void ImGui_manager::Delete(void)
{
    ImGui_ImplOpenGL3_Shutdown();
    ImGui_ImplSDL2_Shutdown();
    ImGui::DestroyContext();

    SDL_GL_DeleteContext(mGlContext);
    // SDL_DestroyWindow(pWindow);
    SDL_Quit();
}

void ImGui_manager::Update(void)
{
    if (mTileDrawing)
    {
        Vector3<float> calculatedPos = GRAPHICS->GetInverseCameraMatrix() * Vector3<float>{Input::GetMousePosition().x, Input::GetMousePosition().y, 1.f};
        Vector2<int>   tilePos       = TILE_MAP->WorldToTilePos({calculatedPos.x, calculatedPos.y});
        int            grid          = tilePos.x + tilePos.y * TILE_MAP->GetWidthTileNum();

        if (Input::IsKeyPressed(SDL_SCANCODE_C))
        {
            TILE_MAP->CreatePhysicsTile(tilePos.x, tilePos.y, mCurrentTexture, false);

            if (mConnectedWithObjectTile != nullptr)
            {
                if (mConnectedWithObjectTile->IsGetComponent(eComponentType::BUTTON))
                {
                    mConnectedWithObjectTile->GetComponentByType<Button>(eComponentType::BUTTON)->InsertPhysicsTile(grid, mCurrentTexture);
                }
            }
        }

        if (Input::IsKeyPressed(SDL_SCANCODE_V))
        {
            TILE_MAP->CreateGraphicTile(tilePos.x, tilePos.y, mCurrentTexture, false);
            
            if (mConnectedWithObjectTile != nullptr)
            {
                if (mConnectedWithObjectTile->IsGetComponent(eComponentType::BATTERY))
                {
                    mConnectedWithObjectTile->GetComponentByType<Battery>(eComponentType::BATTERY)->InsertGraphicsTile(grid, mCurrentTexture);
                }
                else
                {
                    mConnectedWithObjectTile->GetComponentByType<Button>(eComponentType::BUTTON)->InsertGraphicsTile(grid, mCurrentTexture);
                }
            }
        }

        if (Input::IsKeyPressed(SDL_SCANCODE_Z))
        {
            TILE_MAP->ErasePhysicsTile(tilePos);

            if (mConnectedWithObjectTile != nullptr)
            {
                if (mConnectedWithObjectTile->IsGetComponent(eComponentType::BUTTON))
                {
                    mConnectedWithObjectTile->GetComponentByType<Button>(eComponentType::BUTTON)->ErasePhysicsTile(grid);
                }
            }
        }

        if (Input::IsKeyPressed(SDL_SCANCODE_X))
        {
            TILE_MAP->EraseGraphicTile(tilePos);

            if (mConnectedWithObjectTile != nullptr)
            {
                if (mConnectedWithObjectTile->IsGetComponent(eComponentType::BATTERY))
                {
                    mConnectedWithObjectTile->GetComponentByType<Battery>(eComponentType::BATTERY)->EraseGraphicsTile(grid);
                }
                else
                {
                    mConnectedWithObjectTile->GetComponentByType<Button>(eComponentType::BUTTON)->EraseGraphicsTile(grid);
                }
            }
        }

        if (Input::IsKeyPressed(SDL_SCANCODE_B))
        {
            Object* object_ = GetSelectedObject();

            if (object_->IsGetComponent(eComponentType::BATTERY) || object_->IsGetComponent(eComponentType::BUTTON))
            {
                mConnectedWithObjectTile = object_;
            }
        }
    }

    if (mLevelEditor)
    {
        if (Input::IsKeyTriggered(SDL_SCANCODE_TAB))
        {
            eStateType currState = STATE_MANAGER->GetCurrentState()->GetCurrentStateType();
            if (eStateType::LEVEL_1<=currState && currState <= eStateType::CHAPTER2_LEVEL_6)
            {
                STATE_MANAGER->SetIsKeyAvailable(!STATE_MANAGER->GetIsKeyAvailable());
                APPLICATION->SetIsKeyAvailable(!APPLICATION->GetIsKeyAvailable());
                GRAPHICS->SetIsZoomAvailable(!GRAPHICS->GetIsZoomAvailable());

                mPlayer1 = OBJECT_MANAGER->FindObjectByName("Player1");
                mPlayer2 = OBJECT_MANAGER->FindObjectByName("Player2");
                mPlayerCombined = OBJECT_MANAGER->FindObjectByName("PlayerCombined");

                if (mCanPlayerMovement)
                {
                    mPlayer1->GetComponentByType<Player>(eComponentType::PLAYER)->SetPlayerControllerForImgui(false);
                    mPlayer2->GetComponentByType<Player>(eComponentType::PLAYER)->SetPlayerControllerForImgui(false);
                    mPlayerCombined->GetComponentByType<Player>(eComponentType::PLAYER)->SetPlayerControllerForImgui(false);
                    mCanPlayerMovement = false;
                }
                else
                {
                    mPlayer1->GetComponentByType<Player>(eComponentType::PLAYER)->SetPlayerControllerForImgui(true);
                    mPlayer2->GetComponentByType<Player>(eComponentType::PLAYER)->SetPlayerControllerForImgui(true);
                    mPlayerCombined->GetComponentByType<Player>(eComponentType::PLAYER)->SetPlayerControllerForImgui(true);
                    mCanPlayerMovement = true;
                }
            }
        }

        if (Input::IsMouseTriggered(eMouseButton::MOUSE_BUTTON_RIGHT))
        {
            if (mClickObject != nullptr)
            {
                mClickObject = GetSelectedObject();
            }
            if (mClickObject != GetSelectedObject()) // when click the other object
            {
                mClickObject = GetSelectedObject();
            }

            mObjectComponentCheck = true;
        }
        if (Input::IsMouseTriggered(eMouseButton::MOUSE_BUTTON_WHEEL))
        {
            mClickObject = nullptr;
        }
        if (Input::IsMousePressed(eMouseButton::MOUSE_BUTTON_RIGHT))
        {
            if (mClickObject)
            {
                Vector3<float> newPos = GRAPHICS->GetInverseCameraMatrix() * Vector3<float>{Input::GetMousePosition().x, Input::GetMousePosition().y, 1.f};
                mClickObject->GetComponentByType<Transform>(eComponentType::TRANSFORM)->SetPosition({newPos.x, newPos.y});
            }
        }
        if (Input::IsKeyPressed(SDL_SCANCODE_Z))
        {
            mSelectObject = GetSelectedObject();
        }
    }
}

void ImGui_manager::Render(void)
{
    ImGui_ImplOpenGL3_NewFrame();
    ImGui_ImplSDL2_NewFrame(mWindow);
    ImGui::NewFrame();

    bool     show_demo_window = true;
    ImVec4   clear_color      = ImVec4(1.00f, 1.00f, 1.00f, 1.00f);
    ImGuiIO& io               = ImGui::GetIO();

    const char* editors[]     = {"Level Editor", "Tile Editor", "Player Information"};
    static int  currentEditor = 0;

    ImGui::Combo("editors", &currentEditor, editors, IM_ARRAYSIZE(editors));

    ImGui::Separator();

    switch (currentEditor)
    {
        case 0:
            mLevelEditor       = true;
            mTileEditor        = false;
            mPlayerInformation = false;
            break;
        case 1:
            mLevelEditor       = false;
            mTileEditor        = true;
            mPlayerInformation = false;
            break;
        case 2:
            mLevelEditor       = false;
            mTileEditor        = false;
            mPlayerInformation = true;
            break;
    }

    LevelEditor(mLevelEditor);
    TileEditor(mTileEditor);
    PlayerInformation(mPlayerInformation);

    ImGui::Separator();

    if (ImGui::Button("Save Level"))
    {
        STATE_MANAGER->GetCurrentState()->SaveLevel();
    }

    if (ImGui::Button("Save Object"))
    {
        STATE_MANAGER->GetCurrentState()->SaveLevel();
    }

    if (ImGui::Button("Clear Objects"))
    {
        OBJECT_MANAGER->GetObjectVector().clear();
    }

    // ImGui::End();

    // Rendering
    ImGui::Render();
    // glViewport(0, 0, (int)APPLICATION->GetWidth(), (int)APPLICATION->GetHeight());
    glClearColor(clear_color.x, clear_color.y, clear_color.z, clear_color.w);
    // glClear(GL_COLOR_BUFFER_BIT);
    ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
    // SDL_GL_SwapWindow(pWindow);
}

///////////////////////////////////////helper function/////////////////////////////////////////////
void ImGui_manager::TileTextureFromData(void)
{
    mTileTextures.insert(std::make_pair(eTexture::TILE_EXAMPLE_1, (ImTextureID)GRAPHICS->GetTextureFromData()->FindTextureWithEnum(eTexture::TILE_EXAMPLE_1)->GetTextureID()));
    mTileTextures.insert(std::make_pair(eTexture::TILE_EXAMPLE_2, (ImTextureID)GRAPHICS->GetTextureFromData()->FindTextureWithEnum(eTexture::TILE_EXAMPLE_2)->GetTextureID()));
    mTileTextures.insert(std::make_pair(eTexture::TILE_EXAMPLE_3, (ImTextureID)GRAPHICS->GetTextureFromData()->FindTextureWithEnum(eTexture::TILE_EXAMPLE_3)->GetTextureID()));
    mTileTextures.insert(std::make_pair(eTexture::TILE_WIRE_RED_1, (ImTextureID)GRAPHICS->GetTextureFromData()->FindTextureWithEnum(eTexture::TILE_WIRE_RED_1)->GetTextureID()));
    mTileTextures.insert(std::make_pair(eTexture::TILE_WIRE_RED_2, (ImTextureID)GRAPHICS->GetTextureFromData()->FindTextureWithEnum(eTexture::TILE_WIRE_RED_2)->GetTextureID()));
    mTileTextures.insert(std::make_pair(eTexture::TILE_WIRE_RED_3, (ImTextureID)GRAPHICS->GetTextureFromData()->FindTextureWithEnum(eTexture::TILE_WIRE_RED_3)->GetTextureID()));
    mTileTextures.insert(std::make_pair(eTexture::TILE_WIRE_RED_4, (ImTextureID)GRAPHICS->GetTextureFromData()->FindTextureWithEnum(eTexture::TILE_WIRE_RED_4)->GetTextureID()));
    mTileTextures.insert(std::make_pair(eTexture::TILE_WIRE_RED_5, (ImTextureID)GRAPHICS->GetTextureFromData()->FindTextureWithEnum(eTexture::TILE_WIRE_RED_5)->GetTextureID()));
    mTileTextures.insert(std::make_pair(eTexture::TILE_WIRE_RED_6, (ImTextureID)GRAPHICS->GetTextureFromData()->FindTextureWithEnum(eTexture::TILE_WIRE_RED_6)->GetTextureID()));
    mTileTextures.insert(std::make_pair(eTexture::TILE_WIRE_RED_7, (ImTextureID)GRAPHICS->GetTextureFromData()->FindTextureWithEnum(eTexture::TILE_WIRE_RED_7)->GetTextureID()));
    mTileTextures.insert(std::make_pair(eTexture::TILE_WIRE_RED_8, (ImTextureID)GRAPHICS->GetTextureFromData()->FindTextureWithEnum(eTexture::TILE_WIRE_RED_8)->GetTextureID()));
    mTileTextures.insert(std::make_pair(eTexture::TILE_WIRE_RED_9, (ImTextureID)GRAPHICS->GetTextureFromData()->FindTextureWithEnum(eTexture::TILE_WIRE_RED_9)->GetTextureID()));
    mTileTextures.insert(std::make_pair(eTexture::TILE_WIRE_RED_10, (ImTextureID)GRAPHICS->GetTextureFromData()->FindTextureWithEnum(eTexture::TILE_WIRE_RED_10)->GetTextureID()));

    mTileTextures.insert(std::make_pair(eTexture::TILE_WIRE_BLUE_1, (ImTextureID)GRAPHICS->GetTextureFromData()->FindTextureWithEnum(eTexture::TILE_WIRE_BLUE_1)->GetTextureID()));
    mTileTextures.insert(std::make_pair(eTexture::TILE_WIRE_BLUE_2, (ImTextureID)GRAPHICS->GetTextureFromData()->FindTextureWithEnum(eTexture::TILE_WIRE_BLUE_2)->GetTextureID()));
    mTileTextures.insert(std::make_pair(eTexture::TILE_WIRE_BLUE_3, (ImTextureID)GRAPHICS->GetTextureFromData()->FindTextureWithEnum(eTexture::TILE_WIRE_BLUE_3)->GetTextureID()));
    mTileTextures.insert(std::make_pair(eTexture::TILE_WIRE_BLUE_4, (ImTextureID)GRAPHICS->GetTextureFromData()->FindTextureWithEnum(eTexture::TILE_WIRE_BLUE_4)->GetTextureID()));
    mTileTextures.insert(std::make_pair(eTexture::TILE_WIRE_BLUE_5, (ImTextureID)GRAPHICS->GetTextureFromData()->FindTextureWithEnum(eTexture::TILE_WIRE_BLUE_5)->GetTextureID()));
    mTileTextures.insert(std::make_pair(eTexture::TILE_WIRE_BLUE_6, (ImTextureID)GRAPHICS->GetTextureFromData()->FindTextureWithEnum(eTexture::TILE_WIRE_BLUE_6)->GetTextureID()));
    mTileTextures.insert(std::make_pair(eTexture::TILE_WIRE_BLUE_7, (ImTextureID)GRAPHICS->GetTextureFromData()->FindTextureWithEnum(eTexture::TILE_WIRE_BLUE_7)->GetTextureID()));
    mTileTextures.insert(std::make_pair(eTexture::TILE_WIRE_BLUE_8, (ImTextureID)GRAPHICS->GetTextureFromData()->FindTextureWithEnum(eTexture::TILE_WIRE_BLUE_8)->GetTextureID()));
    mTileTextures.insert(std::make_pair(eTexture::TILE_WIRE_BLUE_9, (ImTextureID)GRAPHICS->GetTextureFromData()->FindTextureWithEnum(eTexture::TILE_WIRE_BLUE_9)->GetTextureID()));
    mTileTextures.insert(std::make_pair(eTexture::TILE_WIRE_BLUE_10, (ImTextureID)GRAPHICS->GetTextureFromData()->FindTextureWithEnum(eTexture::TILE_WIRE_BLUE_10)->GetTextureID()));
}

void ImGui_manager::ObjectTextureFromData(void)
{
    mObjectTextures.insert(std::make_pair(eTexture::STEEL_BOX, (ImTextureID)GRAPHICS->GetTextureFromData()->FindTextureWithEnum(eTexture::STEEL_BOX)->GetTextureID()));
    mObjectTextures.insert(std::make_pair(eTexture::WOOD_BOX, (ImTextureID)GRAPHICS->GetTextureFromData()->FindTextureWithEnum(eTexture::WOOD_BOX)->GetTextureID()));
    mObjectTextures.insert(std::make_pair(eTexture::FAN, (ImTextureID)GRAPHICS->GetTextureFromData()->FindTextureWithEnum(eTexture::FAN)->GetTextureID()));
    mObjectTextures.insert(std::make_pair(eTexture::DOOR, (ImTextureID)GRAPHICS->GetTextureFromData()->FindTextureWithEnum(eTexture::DOOR)->GetTextureID()));
    mObjectTextures.insert(std::make_pair(eTexture::KEY, (ImTextureID)GRAPHICS->GetTextureFromData()->FindTextureWithEnum(eTexture::KEY)->GetTextureID()));

    mObjectTextures.insert(std::make_pair(eTexture::LAZOR, (ImTextureID)GRAPHICS->GetTextureFromData()->FindTextureWithEnum(eTexture::LAZOR)->GetTextureID()));
    mObjectTextures.insert(std::make_pair(eTexture::OBSTACLE, (ImTextureID)GRAPHICS->GetTextureFromData()->FindTextureWithEnum(eTexture::OBSTACLE)->GetTextureID()));
    mObjectTextures.insert(std::make_pair(eTexture::BATTERY, (ImTextureID)GRAPHICS->GetTextureFromData()->FindTextureWithEnum(eTexture::BATTERY)->GetTextureID()));
    mObjectTextures.insert(std::make_pair(eTexture::BATTERY_HOME, (ImTextureID)GRAPHICS->GetTextureFromData()->FindTextureWithEnum(eTexture::BATTERY_HOME)->GetTextureID()));

    mObjectTextures.insert(std::make_pair(eTexture::PLAYER_1_SPAWN_ANIMATION, (ImTextureID)GRAPHICS->GetTextureFromData()->FindTextureWithEnum(eTexture::PLAYER_1_SPAWN_ANIMATION)->GetTextureID()));
    mObjectTextures.insert(std::make_pair(eTexture::PLAYER_2_SPAWN_ANIMATION, (ImTextureID)GRAPHICS->GetTextureFromData()->FindTextureWithEnum(eTexture::PLAYER_2_SPAWN_ANIMATION)->GetTextureID()));
}

// for sprite and transform component
void ImGui_manager::HelperFunctionObjectComponent(bool* component)
{
    if (mClickObject != nullptr)
    {
        // sprite
        {
            if (component[0] == true)
            {
                // sprite
                ImGui::Text("Sprite");
                static float color[4] = {255.f / 255.f, 255.f / 255.f, 255.f / 255.f, 255.f / 255.f};

                ImGui::ColorEdit4("color", color, ImGuiColorEditFlags_Float);
                mClickObject->GetSprite()->SetColor(mClickObject->GetSprite()->GetColor());

                int i = 0;
                for (auto& temp : mObjectTextures)
                {
                    ++i;
                    if (ImGui::ImageButton(temp.second, ImVec2(32, 32)))
                    {
                        mClickObject->GetSprite()->LoadTextureFromData(temp.first);
                    }

                    if (i % 5 == 0)
                    {
                        continue;
                    }

                    ImGui::SameLine();
                }

                ImGui::NewLine();

                static bool  animationComponent = false;
                static int   frame              = 1;
                static float speed              = 2.0f;

                animationComponent = mClickObject->GetSprite()->IsAnimation();
                if (ImGui::Checkbox("animation", &animationComponent))
                {
                }

                if (animationComponent)
                {
                    frame = mClickObject->GetSprite()->GetAnimationFrameNum();
                    speed = mClickObject->GetSprite()->GetAnimationSpeed();
                    ImGui::SliderInt("frame", &frame, 1, 10);
                    ImGui::SliderFloat("speed", &speed, 2.0f, 4.0f);
                    mClickObject->GetSprite()->SetAnimation(true);
                    mClickObject->GetSprite()->SetAnimationFrame(frame);
                    mClickObject->GetSprite()->SetAnimationSpeed(speed);
                }
                else
                {
                    mClickObject->GetSprite()->SetAnimation(false);
                }

                ImGui::Separator();
            }
        }

        // transform
        {
            if (component[1] == true)
            {
                // transform
                ImGui::Text("Transform");
                static float x = 0.f, y = 0.f;

                x = mClickObject->GetTransform()->GetScale().x;
                y = mClickObject->GetTransform()->GetScale().y;
                ImGui::SliderFloat("Scale X", &x, 0.0f, 500.f);
                ImGui::SliderFloat("Scale Y", &y, 0.0f, 500.f);
                ImGui::InputFloat("Input Scale X", &x, 0.01f, 1.f, "%.3f");
                ImGui::InputFloat("Input Scale Y", &y, 0.01f, 1.f, "%.3f");
                mClickObject->GetTransform()->SetScale(Vector2<float>(x, y));
                ImGui::Separator();

                static float pX = 0.f, pY = 0.f;
                pX = mClickObject->GetTransform()->GetPosition().x;
                pY = mClickObject->GetTransform()->GetPosition().y;
                ImGui::SliderFloat("Position X", &pX, -30.f, 500.f);
                ImGui::SliderFloat("Position Y", &pY, -30.f, 500.f);
                ImGui::InputFloat("Input Position X", &pX, 0.01f, 1.f, "%.3f");
                ImGui::InputFloat("Input Position Y", &pY, 0.01f, 1.f, "%.3f");

                mClickObject->GetTransform()->SetPosition(Vector2<float>(pX, pY));
                ImGui::Separator();

                static float r = 0.f;
                r = mClickObject->GetTransform()->GetRotation();
                ImGui::SliderFloat("Rotation", &r, 0.f, 2 * PI);
                if (ImGui::Button("Clockwise Rotation : 90 degree"))
                {
                    r += (-PI / 2.f);
                }

                if (ImGui::Button("Counter ClockWise Rotation : 90 degree"))
                {
                    r += (PI / 2.f);
                }
                mClickObject->GetTransform()->SetRotation(r);

                ImGui::Separator();
            }
        }

        // collision
        {
            if (component[2] == true)
            {
                ImGui::Text("Collision");
                ImGui::Separator();
            }
        }

        // fan
        {
            if (component[3] == true)
            {
                ImGui::Text("Fan");
                static int fanType = 0;
                static bool fanIsActive = false;

                ImGui::Checkbox("fanActive", &fanIsActive);

                ImGui::RadioButton("Up", &fanType, 0);
                ImGui::SameLine();
                ImGui::RadioButton("Down", &fanType, 1);
                ImGui::SameLine();
                ImGui::RadioButton("Left", &fanType, 2);
                ImGui::SameLine();
                ImGui::RadioButton("Right", &fanType, 3);

                if (ImGui::Button("Apply"))
                {
                    Fan* fancomponent = mClickObject->GetComponentByType<Fan>(eComponentType::FAN);
                    if (fanType == 0)
                    {
                        fancomponent->SetFanType(eFanType::UP);
                        mClickObject->SetRotation(0.f);
                    }
                    else if (fanType == 1)
                    {
                        fancomponent->SetFanType(eFanType::DOWN);
                        mClickObject->SetRotation(PI);
                    }
                    else if (fanType == 2)
                    {
                        fancomponent->SetFanType(eFanType::LEFT);
                        mClickObject->SetRotation(PI / 2.f);
                    }
                    else if (fanType == 3)
                    {
                        fancomponent->SetFanType(eFanType::RIGHT);
                        mClickObject->SetRotation(-PI / 2.f);
                    }

                    if (fanIsActive)
                    {
                        fancomponent->SetIsFanActive(true);
                    }
                    else
                    {
                        fancomponent->SetIsFanActive(false);
                    }


                    fancomponent->SetBlowingWindEffect();
                }

                ImGui::Separator();
            }
        }

        // moving platform
        {
            if (component[4] == true)
            {
                ImGui::Text("Moving platform");

                static bool movingPlatformIsActive = false;
                ImGui::Checkbox("Moving Platform Is active", &movingPlatformIsActive);

                static int moveWay = 0;
                ImGui::RadioButton("Vertical", &moveWay, 0);
                ImGui::SameLine();
                ImGui::RadioButton("Horizontal", &moveWay, 1);

                ImGui::Separator();
                static float startPos = 0.f, endPos = 0.f;
                ImGui::SliderFloat("Start Position", &startPos, -500.f, 500.f);
                ImGui::SliderFloat("End Position", &endPos, -500, 500.f);

                ImGui::InputFloat("Input Start Position", &startPos, 0.01f, 1.f, "%.3f");
                ImGui::InputFloat("Input End Position", &endPos, 0.01f, 1.f, "%.3f");
                ImGui::Separator();

                if (ImGui::Button("Apply"))
                {
                    MovingPlatform* movingplatform = mClickObject->GetComponentByType<MovingPlatform>(eComponentType::MOVINGPLATFORM);
                    if (moveWay == 0)
                    {
                        movingplatform->SetMoveWay(eMoveWay::MOVE_VERTICAL);
                    }
                    else
                    {
                        movingplatform->SetMoveWay(eMoveWay::MOVE_HORIZONTAL);
                    }

                    if (movingPlatformIsActive)
                    {
                        movingplatform->SetIsMovingPlatformActive(true);
                    }
                    else
                    {
                        movingplatform->SetIsMovingPlatformActive(false);
                    }

                    movingplatform->SetPosition(startPos, endPos);
                }
                ImGui::Separator();
            }
        }

        // button
        {
            if (component[5] == true)
            {
                ImGui::Text("Button");

                static int buttonDirection = 0;

                ImGui::RadioButton("Up", &buttonDirection, 0);
                ImGui::SameLine();
                ImGui::RadioButton("Left", &buttonDirection, 1);
                ImGui::SameLine();
                ImGui::RadioButton("Right", &buttonDirection, 2);

                if (ImGui::Button("Apply"))
                {
                    Button* buttonComponent = mClickObject->GetComponentByType<Button>(eComponentType::BUTTON);
                    
                    if (mSelectObject != nullptr)
                    {
                        buttonComponent->SetConnectedObject(mSelectObject);

                        if (mSelectObject->GetObjectType() == eObjectType::FAN)
                        {
                            mSelectObject->GetComponentByType<Fan>(eComponentType::FAN)->SetIsFanActive(false);
                        }

                        mSelectObject = nullptr;
                    }
                    switch (buttonDirection)
                    {
                    case 0:
                        buttonComponent->SetButtonDirection(eButtonDirection::UP);
                        break;
                    case 1:
                        buttonComponent->SetButtonDirection(eButtonDirection::LEFT);
                        break;
                    case 2:
                        buttonComponent->SetButtonDirection(eButtonDirection::RIGHT);
                        break;
                    }

                    

                }

                ImGui::Separator();
            }
        }

        // door
        /*{
            if (component[6] == true)
            {
                ImGui::Text("Door");

                const char* levels[] = { "Level 1", "Level 2", "Level 3", "Level 4" };
                static int currentLevel = 0;
                ImGui::ListBox("Next Levels", &currentLevel, levels, IM_ARRAYSIZE(levels));

                if (ImGui::Button("Set Next Level"))
                {
                    Door* doorComponent = mClickObject->GetComponentByType<Door>(eComponentType::DOOR);
                    if (currentLevel == 0)
                    {
                        doorComponent->SetNextState(eStateList::LEVEL_1);
                    }
                    else if (currentLevel == 1)
                    {
                        doorComponent->SetNextState(eStateList::LEVEL_2);
                    }
                    else if (currentLevel == 2)
                    {
                        doorComponent->SetNextState(eStateList::LEVEL_3);
                    }
                    else
                    {
                        doorComponent->SetNextState(eStateList::LEVEL_4);
                    }
                }
                ImGui::Separator();
            }
        }*/

        // trigger
        {
            if (component[6] == true)
            {
                ImGui::Text("Trigger");

                if (mSelectObject != nullptr)
                {
                    if (ImGui::Button("Apply"))
                    {
                        mClickObject->GetComponentByType<Trigger>(eComponentType::TRIGGER)->SetConnectedObject(mSelectObject);
                        mSelectObject = nullptr;
                    }
                }

                ImGui::Separator();
            }
        }

        // lazor
        {
            if (component[7] == true)
            {
                ImGui::Text("Laser");
                ImGui::Separator();
            }
        }

        // pressure
        {
            if (component[8] == true)
            {
                ImGui::Text("Pressure");

                static bool pressureIsActive = false;
                ImGui::Checkbox("Pressure Is Active", &pressureIsActive);

                static int pressureType = 0;

                ImGui::RadioButton("Vertical", &pressureType, 0);
                ImGui::SameLine();
                ImGui::RadioButton("Horizontal", &pressureType, 1);

                if (ImGui::Button("Apply"))
                {
                    Pressure* pressure = mClickObject->GetComponentByType<Pressure>(eComponentType::PRESSURE);
                    if (pressureType == 0)
                    {
                        pressure->SetPressureType(Pressure::ePressureType::VERTICAL);
                        mClickObject->SetRotation(0.f);
                    }
                    else
                    {
                        pressure->SetPressureType(Pressure::ePressureType::HORIZONTAL);
                        mClickObject->SetRotation(PI / 2.f);
                    }

                    if (pressureIsActive)
                    {
                        pressure->SetIsPressureActive(true);
                    }
                    else
                    {
                        pressure->SetIsPressureActive(false);
                    }
                }

                ImGui::Separator();
            }
        }

        // battery
        {
            if (component[9] == true)
            {
                ImGui::Text("Battery");

                if (mSelectObject != nullptr)
                {
                    if (ImGui::Button("ObjectHome"))
                    {
                        mClickObject->GetComponentByType<Battery>(eComponentType::BATTERY)->SetBatteryHome(mSelectObject);
                        mSelectObject = nullptr;
                    }
                    if (ImGui::Button("Object Connecting"))
                    {
                        mClickObject->GetComponentByType<Battery>(eComponentType::BATTERY)->SetConnectingObject(mSelectObject);
                        mSelectObject = nullptr;
                    }
                }

                ImGui::Separator();
            }
        }
    }
}

void ImGui_manager::ComponentCheck(bool* component)
{
    {
        if (mClickObject->IsGetComponent(eComponentType::SPRITE))
        {
            component[0] = true;
        }
        else
        {
            component[0] = false;
        }
    }

    {
        if (mClickObject->IsGetComponent(eComponentType::TRANSFORM))
        {
            component[1] = true;
        }
        else
        {
            component[1] = false;
        }
    }

    {
        if (mClickObject->IsGetComponent(eComponentType::COLLISION))
        {
            component[2] = true;
        }
        else
        {
            component[2] = false;
        }
    }

    {
        if (mClickObject->IsGetComponent(eComponentType::FAN))
        {
            component[3] = true;
        }
        else
        {
            component[3] = false;
        }
    }

    {
        if (mClickObject->IsGetComponent(eComponentType::MOVINGPLATFORM))
        {
            component[4] = true;
        }
        else
        {
            component[4] = false;
        }
    }

    {
        if (mClickObject->IsGetComponent(eComponentType::BUTTON))
        {
            component[5] = true;
        }
        else
        {
            component[5] = false;
        }
    }

    {
        if (mClickObject->IsGetComponent(eComponentType::TRIGGER))
        {
            component[6] = true;
        }
        else
        {
            component[6] = false;
        }
    }


    {
        if (mClickObject->IsGetComponent(eComponentType::PRESSURE))
        {
            component[8] = true;
        }
        else
        {
            component[8] = false;
        }
    }

    {
        if (mClickObject->IsGetComponent(eComponentType::BATTERY))
        {
            component[9] = true;
        }
        else
        {
            component[9] = false;
        }
    }
}

// for checking collision with object
std::string ImGui_manager::CollisionCheck(Object* object)
{
    std::string collisionNames = "";

    Collision* collisionComponent = object->GetComponentByType<Collision>(eComponentType::COLLISION);

    auto objectMap = OBJECT_MANAGER->GetObjectVector();

    for (auto obj : objectMap)
    {
        if (obj->GetObjectType() == object->GetObjectType())
            continue;

        if (collisionComponent->IsCollidingWith(obj))
        {
            collisionNames += obj->GetName();
            collisionNames += " ";
        }
    }
    return collisionNames;
}

// for moving clicked object
Object* ImGui_manager::GetSelectedObject(void)
{
    Vector2<float> mousePos = Input::GetMousePosition();

    Vector3<float> calculatedPos = {mousePos.x, mousePos.y, 1.f};
    calculatedPos                = GRAPHICS->GetInverseCameraMatrix() * calculatedPos;

    for (auto obj : OBJECT_MANAGER->GetObjectVector())
    {
        if (obj->GetTransform()->GetParentTransform() == nullptr)
        {
            if (CollisionCheckWithPoint(obj, {calculatedPos.x, calculatedPos.y}))
            {
                return obj;
            }
        }
    }
    return nullptr;
}

bool ImGui_manager::CollisionCheckWithPoint(Object* object, Vector2<float> pos)
{
    Vector2<float> scale   = object->GetScale();
    Vector2<float> thisMax = object->GetPosition() + Vector2<float>{scale.x / 2.f, scale.y / 2.f};
    Vector2<float> thisMin = object->GetPosition() - Vector2<float>{scale.x / 2.f, scale.y / 2.f};

    if (pos.x >= thisMin.x && pos.x <= thisMax.x)
    {
        if (pos.y >= thisMin.y && pos.y <= thisMax.y)
        {
            return true;
        }
    }
    return false;
}
//

void ImGui_manager::HelperFunctionAddObject(std::string objectName, int objectType)
{
    Object* object = new Object();
    switch (objectType)
    {
        case 0: // steel box
        {
            object->SetObjectType(eObjectType::STEEL_BOX);
            object->SetName(objectName);
            object->AddComponent(new Sprite());
            object->AddComponent(new Transform());
            object->AddComponent(new Collision());
            object->GetSprite()->SetColor({1.f, 1.f, 1.f, 1.f});
            object->GetSprite()->LoadTextureFromData(eTexture::STEEL_BOX);
            object->SetScale(Vector2<float>(150, 150));
            object->SetPosition(Vector2<float>(0, 0));
            object->SetDepth(GAME_OBJECT);
            break;
        }

        case 1: // wood box
        {
            object->SetObjectType(eObjectType::WOOD_BOX);
            object->SetName(objectName);
            object->AddComponent(new Sprite());
            object->AddComponent(new Transform());
            object->AddComponent(new Collision());
            object->GetSprite()->SetColor({1.f, 1.f, 1.f, 1.f});
            object->GetSprite()->LoadTextureFromData(eTexture::WOOD_BOX);
            object->SetScale(Vector2<float>(100, 100));
            object->SetPosition(Vector2<float>(0, 0));
            object->SetDepth(GAME_OBJECT);
            break;
        }

        case 2: // fan
        {
            object->SetObjectType(eObjectType::FAN);
            object->SetName(objectName);
            object->AddComponent(new Sprite());
            object->AddComponent(new Transform());
            object->AddComponent(new Fan());
            object->GetSprite()->SetColor({1.f, 1.f, 1.f, 1.f});
            object->GetSprite()->LoadTextureFromData(eTexture::FAN);
            object->SetScale(Vector2<float>(150.f, 150.f));
            object->SetPosition(Vector2<float>(0.f, 0.f));
            object->SetDepth(GAME_OBJECT);
            break;
        }

        case 3: // trigger
        {
            object->SetObjectType(eObjectType::TRIGGER);
            object->SetName(objectName);
            object->AddComponent(new Sprite());
            object->AddComponent(new Transform());
            object->AddComponent(new Trigger());
            object->GetComponentByType<Trigger>(eComponentType::TRIGGER)->SetTriggerType(LAZOR);
            break;
        }

        case 4: // laser
        {
            object->SetObjectType(eObjectType::OBSTACLE);
            object->SetName(objectName);
            object->AddComponent(new Sprite());
            object->AddComponent(new Transform());
            object->AddComponent(new Collision());
            object->AddComponent(new Obstacle());
            object->GetSprite()->SetColor({1.f, 1.f, 1.f, 1.f});
            object->GetSprite()->LoadTextureFromData(eTexture::LAZOR);
            object->GetSprite()->SetAnimation(true, 4, 3.f);
            object->SetScale(Vector2<float>(50.f, 295.f));
            object->SetPosition(Vector2<float>(0.f, 0.f));
            object->GetComponentByType<Collision>(eComponentType::COLLISION)->SetIsGhost(true);
            object->SetDepth(GAME_OBJECT);
            break;
        }

        case 5: // moving platform
        {
            object->SetObjectType(eObjectType::MOVING_PLATFORM);
            object->SetName(objectName);
            object->AddComponent(new Sprite());
            object->AddComponent(new Transform());
            object->AddComponent(new Collision());
            object->AddComponent(new MovingPlatform());
            object->GetSprite()->SetColor({1.f, 1.f, 1.f, 1.f});
            object->GetSprite()->LoadTextureFromData(eTexture::PLATFORM);
            object->SetScale(Vector2<float>(200.f, 20.f));
            object->SetPosition(Vector2<float>(0.f, 0.f));
            object->SetDepth(GAME_OBJECT);
            break;
        }

        case 6: // pressure
        {
            object->SetObjectType(eObjectType::PRESSURE);
            object->SetName(objectName);
            object->AddComponent(new Sprite());
            object->AddComponent(new Transform());
            object->AddComponent(new Pressure());
            object->GetSprite()->SetColor({1.f, 1.f, 1.f, 1.f});
            object->GetSprite()->LoadTextureFromData(eTexture::PRESSURE);
            object->GetSprite()->SetAnimation(true, 7, 6.5f);
            object->SetScale({100.f, 250.f});
            object->SetPosition({0, 0});
            object->SetDepth(GAME_OBJECT);
            break;
        }

        case 7: // button
        {
            object->SetObjectType(eObjectType::BUTTON);
            object->SetName(objectName);
            object->AddComponent(new Sprite());
            object->AddComponent(new Transform());
            object->AddComponent(new Collision());
            object->AddComponent(new Button());
            object->GetSprite()->SetColor({1.f, 1.f, 1.f, 1.f});
            object->GetSprite()->LoadTextureFromData(eTexture::RED_BUTTON);
            object->SetScale(Vector2<float>(100, 50));
            object->SetPosition(Vector2<float>(0, 0));
            object->SetDepth(GAME_OBJECT);
            break;
        }

            // case 9: //key
            //{
            //    object->SetObjectType(eObjectType::EATABLE);
            //    object->SetName(objectName);
            //    object->AddComponent(new Sprite());
            //    object->AddComponent(new Transform());
            //    object->AddComponent(new Collision());
            //    object->GetSprite()->SetColor({ 1,1,1,1 });
            //    object->GetSprite()->LoadTextureFromData(eTexture::KEY);
            //    object->SetScale(Vector2<float>(80, 80));
            //    object->SetPosition(Vector2<float>(0, 0));
            //    object->SetDepth(GAME_OBJECT);
            //    break;
            //}

        case 8: // battery
        {
            object->SetObjectType(eObjectType::BATTERY);
            object->SetName(objectName);
            object->AddComponent(new Sprite());
            object->AddComponent(new Transform());
            object->AddComponent(new Collision());
            object->AddComponent(new Battery());
            object->GetSprite()->SetColor({1.f, 1.f, 1.f, 1.f});
            object->GetSprite()->LoadTextureFromData(eTexture::BATTERY);
            object->SetScale(Vector2<float>(80, 80));
            object->SetPosition(Vector2<float>(0, 0));
            object->SetDepth(GAME_OBJECT);
            break;
        }

        case 9: // battery home
        {
            object->SetObjectType(eObjectType::NONE);
            object->SetName(objectName);
            object->AddComponent(new Sprite());
            object->AddComponent(new Transform());
            object->AddComponent(new Collision());
            object->GetSprite()->SetColor({1.f, 1.f, 1.f, 0.7f});
            object->GetSprite()->LoadTextureFromData(eTexture::BATTERY_HOME);
            object->SetScale(Vector2<float>(80, 80));
            object->SetPosition(Vector2<float>(0, 0));
            object->SetDepth(GAME_OBJECT);
            break;
        }
    }
    mClickObject = object;

    std::vector<Component*> allComponents = object->GetAllComponents();
    for (const auto& component : allComponents)
    {
        component->Initialize(object);
    }

    OBJECT_MANAGER->AddObject(object);
}

bool ImGui_manager::IsLoadDone() const
{
    return mIsLoaded;
}

bool ImGui_manager::GetIsLevelEditor()
{
    return mLevelEditor;
}

void ImGui_manager::ResetClickedObject()
{
    mClickObject = nullptr;
}