/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : ImGui_manager.hpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Sejeong Park
Secondary :
- End Header ----------------------------------------------------------------*/

#pragma once
#include "../ImGui/helper/imgui.h"
#include "../ImGui/helper/imgui_impl_opengl3.h"
#include "../ImGui/helper/imgui_impl_sdl.h"
#include "../Input/Input.hpp"
#include <unordered_map>
#include <string>
#include <SDL.h>
#include <glew.h>
#include <stdio.h>

#include "../Engine/Graphics/Graphics.hpp"
#include "../Game/State/StateManager/StateManager.hpp"
#include "../Engine/Graphics/Texture/Texture.hpp"
#include "../Engine/Object/ObjectManager/ObjectManager.hpp"
#include "../Engine/TileMap/TileMap.hpp"

#include "../Component/AllComponents.hpp"

class ImGui_manager
{
public:
    ImGui_manager(void);

    void LevelEditor(bool showWindow);
    void TileEditor(bool showWindow);
    void PlayerInformation(bool showWindow);

    void Initialize(void);
    void Delete(void);
    void Update(void);
    void Render(void);

    void TileTextureFromData(void);
    void ObjectTextureFromData(void);

    void HelperFunctionObjectComponent(bool* component);
    void ComponentCheck(bool* component);

    std::string CollisionCheck(Object* object);
    Object* GetSelectedObject(void);
    bool CollisionCheckWithPoint(Object* object, Vector2<float> pos);
    void HelperFunctionAddObject(std::string objectName, int objectType);

    bool IsLoadDone() const;
    bool GetIsLevelEditor();

    void ResetClickedObject();

private:
    bool mLevelEditor;
    bool mTileEditor;
    bool mPlayerInformation;

    bool mCanPlayerMovement = true;
    Object* mPlayer1 = nullptr;
    Object* mPlayer2 = nullptr;
    Object* mPlayerCombined = nullptr;

    Object* mClickObject = nullptr;
    Object* mSelectObject = nullptr;

    std::unordered_map<eTexture, ImTextureID> mObjectTextures;

    bool        mObjectComponentCheck;

    std::unordered_map<eTexture, ImTextureID> mTileTextures;
    eTexture      mCurrentTexture;
    bool          mTileDrawing;
    Object*       mConnectedWithObjectTile = nullptr;

    SDL_Window    *mWindow = nullptr;
    SDL_GLContext mGlContext;

    bool mIsLoaded;
};
extern ImGui_manager *IMGUI;