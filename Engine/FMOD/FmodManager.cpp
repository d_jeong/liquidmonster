/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : FmodManager.cpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Sejeong Park
Secondary :
- End Header ----------------------------------------------------------------*/

#include "FmodManager.hpp"

FmodManager* FMOD_MANAGER = new FmodManager;

FmodManager::FmodManager()
{
    m_result     = FMOD_OK;
    FMOD_MANAGER = this;
}

FmodManager::~FmodManager()
{
    delete FMOD_MANAGER;
}

void FmodManager::Initialize()
{
    m_result = FMOD::System_Create(&mSystem);
    CheckError(m_result);

    m_result = mSystem->init(32, FMOD_INIT_NORMAL, 0);
    CheckError(m_result);

    m_result = mSystem->getMasterChannelGroup(&mMaster);
    CheckError(m_result);
    m_result = mSystem->getMasterChannelGroup(&mMasterUI);
    CheckError(m_result);

    m_result = mSystem->createChannelGroup("Background", &mGroups[0]);
    CheckError(m_result);
    m_result = mSystem->createChannelGroup("SoundEffect", &mGroups[1]);
    CheckError(m_result);
    m_result = mSystem->createChannelGroup("UISound", &mGroups[2]);
    CheckError(m_result);
    m_result = mSystem->createChannelGroup("InGames", &mGroups[3]);
    CheckError(m_result);

    m_result = mMaster->addGroup(mGroups[0]);
    CheckError(m_result);
    m_result = mMaster->addGroup(mGroups[1]);
    CheckError(m_result);
    m_result = mMasterUI->addGroup(mGroups[2]);
    CheckError(m_result);
    m_result = mMaster->addGroup(mGroups[3]);
    CheckError(m_result);

    CreateBackgroundSound("sound/background_chapter_1.mp3");
    CreateBackgroundSound("sound/background_2.mp3");
    CreateBackgroundSound("sound/background_3.mp3");
    CreateBackgroundSound("sound/background_4.mp3");
    CreateBackgroundSound("sound/background_5.mp3");

    // PlayBackgroundSound("sound/background_chapter_1.mp3");

    // add create sound
    CreateSoundEffectSound("sound/sfx_key_open.mp3");
    CreateSoundEffectSound("sound/sfx_button_click.mp3");
    CreateSoundEffectSound("sound/sfx_button_change.mp3");
    CreateSoundEffectSound("sound/sfx_jump.mp3");

    CreateSoundEffectSound("sound/sfx_fan_move.mp3", FMOD_LOOP_NORMAL);

    CreateSoundEffectSound("sound/sfx_slime_eat.mp3");
    CreateSoundEffectSound("sound/sfx_slime_jump.mp3");
    CreateSoundEffectSound("sound/sfx_slime_walk.mp3", FMOD_LOOP_NORMAL);
    CreateSoundEffectSound("sound/sfx_slime_inhale.mp3", FMOD_LOOP_NORMAL);
    CreateSoundEffectSound("sound/sfx_slime_death.mp3");
    CreateSoundEffectSound("sound/sfx_slime_combined_spit_out.mp3");
    CreateSoundEffectSound("sound/sfx_slime_small_spit_out.mp3");
    CreateSoundEffectSound("sound/sfx_laser.mp3");
    CreateSoundEffectSound("sound/sfx_battery_in.mp3");
    CreateSoundEffectSound("sound/sfx_clamper.mp3");
    CreateSoundEffectSound("sound/sfx_spawner.mp3");
    CreateSoundEffectSound("sound/sfx_component_button_on.mp3");
    CreateSoundEffectSound("sound/sfx_component_button_off.mp3");
    CreateSoundEffectSound("sound/sfx_slime_squish.mp3");
    CreateSoundEffectSound("sound/sfx_laser_on_off.mp3");
    CreateSoundEffectSound("sound/sfx_box_move.mp3", FMOD_LOOP_NORMAL);

    // add ui sound
    CreateSoundEffectSound("sound/sfx_BAM_monkeySound.mp3");
    CreateSoundEffectSound("sound/sfx_forest.mp3");
    CreateUISound("sound/background_1.mp3");

    mPhysicsSFX = PlaySoundEffectForLOOP("sound/sfx_box_move.mp3", mPhysicsSFX);
    SetChannelPaused(mPhysicsSFX, true);
}

void FmodManager::Update()
{
    m_result = mSystem->update();
    CheckError(m_result);
}

void FmodManager::Delete()
{
    for (int i = 0; i < 3; ++i)
    {
        for (auto sounds : mSounds[i])
        {
            sounds.second->release();
        }
        mSounds[i].clear();
    }
    mSystem->close();
    m_result = mSystem->release();
    CheckError(m_result);
}

FMOD::Channel* FmodManager::GetBackgroundChannel()
{
    return mChannel;
    ;
}

FMOD::Channel* FmodManager::GetSoundEffectChannel()
{
    return mSoundEffectChannel;
}

FMOD::Channel* FmodManager::GetUISoundChannel()
{
    return mUIChannel;
}

void FmodManager::CreateBackgroundSound(const std::string& file)
{
    FMOD::Sound* sound;
    m_result = mSystem->createSound(file.c_str(), FMOD_LOOP_NORMAL, 0, &sound);
    CheckError(m_result);
    mSounds[0].insert(std::make_pair(file, sound));
}

void FmodManager::CreateSoundEffectSound(const std::string& file)
{
    FMOD::Sound* sound;
    m_result = mSystem->createSound(file.c_str(), FMOD_LOOP_OFF, 0, &sound);
    CheckError(m_result);

    mSounds[1].insert(std::make_pair(file, sound));
}

void FmodManager::CreateSoundEffectSound(const std::string& file, FMOD_MODE channel)
{
    FMOD::Sound* sound;
    m_result = mSystem->createSound(file.c_str(), channel, 0, &sound);
    CheckError(m_result);

    mSounds[1].insert(std::make_pair(file, sound));
}

void FmodManager::CreateUISound(const std::string& file)
{
    FMOD::Sound* sound;
    m_result = mSystem->createSound(file.c_str(), FMOD_LOOP_NORMAL, 0, &sound);
    CheckError(m_result);

    mSounds[2].insert(std::make_pair(file, sound));
}

void FmodManager::PlayBackgroundSound(const std::string& file)
{
    std::unordered_map<std::string, FMOD::Sound*>::iterator sound = mSounds[0].find(file);

    if (sound != mSounds[0].end())
    {
        m_result = mSystem->playSound(sound->second, NULL, false, &mChannel);
        CheckError(m_result);
        m_result = mChannel->setChannelGroup(mGroups[0]);
        CheckError(m_result);
        mChannel->setPaused(false);
    }
}

void FmodManager::PlaySoundEffectSound(const std::string& file)
{
    std::unordered_map<std::string, FMOD::Sound*>::iterator sound = mSounds[1].find(file);

    if (sound != mSounds[1].end())
    {
        mSystem->playSound(sound->second, NULL, true, &mSoundEffectChannel);
        mSoundEffectChannel->setChannelGroup(mGroups[1]);
        mSoundEffectChannel->setPaused(false);
    }
}

FMOD::Channel* FmodManager::PlaySoundEffectSound(const std::string& file, FMOD::Channel* channel)
{
    std::unordered_map<std::string, FMOD::Sound*>::iterator sound = mSounds[1].find(file);

    if (sound != mSounds[1].end())
    {
        mSystem->playSound(sound->second, NULL, true, &channel);
        channel->setChannelGroup(mGroups[1]);
        channel->setPaused(false);
    }
    return channel;
}

void FmodManager::PlayUISound(const std::string& file)
{
    std::unordered_map<std::string, FMOD::Sound*>::iterator sound = mSounds[2].find(file);

    if (sound != mSounds[2].end())
    {
        mSystem->playSound(sound->second, NULL, true, &mUIChannel);
        mUIChannel->setChannelGroup(mGroups[2]);
        mUIChannel->setPaused(false);
    }
}

void FmodManager::StopAllGameSound()
{
    mMaster->stop();
}

void FmodManager::StopAllUISound()
{
    mMasterUI->stop();
}

void FmodManager::StopBackgroundSound()
{
    mGroups[0]->stop();
}

void FmodManager::StopSoundEffectSound()
{
    mGroups[1]->stop();
}

void FmodManager::StopUISound()
{
    mGroups[2]->stop();
}

void FmodManager::SetAllGameSoundPaused(bool paused)
{
    mMaster->setPaused(paused);
}

void FmodManager::SetAllUISoundPaused(bool paused)
{
    mMasterUI->setPaused(paused);
}

void FmodManager::SetBackgroundSoundPaused(bool paused)
{
    mGroups[0]->setPaused(paused);
}

void FmodManager::SetSoundEffectSoundPaused(bool paused)
{
    mGroups[1]->setPaused(paused);
}

void FmodManager::SetUISoundPaused(bool paused)
{
    mGroups[2]->setPaused(paused);
}

void FmodManager::SetChannelPaused(FMOD::Channel* channel, bool paused)
{
    channel->setPaused(paused);
}

void FmodManager::SetPhysicsSFXPaused(bool paused)
{
    mPhysicsSFX->setPaused(paused);
}

void FmodManager::SetGroupOfSFXLoopPaused(bool paused)
{
    int numChannel = 0;
    mGroups[3]->getNumChannels(&numChannel);

    for (int i = 0; i < numChannel; ++i)
    {
        FMOD::Channel* pChannel = nullptr;
        mGroups[3]->getChannel(i, &pChannel);

        FMOD::Sound* sound = nullptr;
        pChannel->getCurrentSound(&sound);

        pChannel->setPaused(paused);
    }
}

FMOD::Channel* FmodManager::FindChannel(int groupIndex, FMOD::Sound* findSound)
{
    int numChannel = 0;
    mGroups[groupIndex]->getNumChannels(&numChannel);

    for (int i = 0; i < numChannel; ++i)
    {
        FMOD::Channel* pChannel = nullptr;
        mGroups[groupIndex]->getChannel(i, &pChannel);

        FMOD::Sound* sound = nullptr;
        pChannel->getCurrentSound(&sound);

        if (findSound == sound)
        {
            return pChannel;
        }
    }
    return nullptr;
}

void FmodManager::SetVolumeAllGameSound(float volume)
{
    mMaster->setVolume(volume);
}

void FmodManager::SetVolumeAllUISound(float volume)
{
    mMasterUI->setVolume(volume);
}

void FmodManager::SetVolumeBackgroundSound(float volume)
{
    mGroups[0]->setVolume(volume);
}

void FmodManager::SetVolumeSoundEffectSound(float volume)
{
    mGroups[1]->setVolume(volume);
}

void FmodManager::SetVolumeUISound(float volume)
{
    mGroups[2]->setVolume(volume);
}

void FmodManager::CheckError(FMOD_RESULT result)
{
    if (result != FMOD_OK)
        std::cout << "Sound error\n";
}

bool FmodManager::IsPlaying(FMOD::Channel* channel)
{
    if (channel == nullptr)
    {
        return false;
    }
    bool isplaying;
    m_result = channel->isPlaying(&isplaying);

    if (m_result == FMOD_RESULT::FMOD_ERR_INVALID_HANDLE || m_result == FMOD_RESULT::FMOD_ERR_CHANNEL_STOLEN) // If you get FMOD.ERR_INVALID_HANDLE, that means it is not playing.
    {
        return isplaying;
    }

    CheckError(m_result);
    return isplaying;
}

FMOD::Channel* FmodManager::PlaySoundEffectForLOOP(const std::string& file, FMOD::Channel* channel)
{
    std::unordered_map<std::string, FMOD::Sound*>::iterator sound = mSounds[1].find(file);

    if (sound != mSounds[1].end())
    {
        mSystem->playSound(sound->second, NULL, true, &channel);
        channel->setChannelGroup(mGroups[3]);
        channel->setPaused(false);
    }
    return channel;
}

void FmodManager::SetSoundEffectForLoop(bool paused)
{
    mGroups[3]->setPaused(paused);
}

void FmodManager::StopSoundEffectForLoop()
{
    mGroups[3]->stop();
}

FMOD::Sound* FmodManager::GetSound(int index, const std::string& file)
{
    std::unordered_map<std::string, FMOD::Sound*>::iterator sound = mSounds[index].find(file);
    if (sound != mSounds[index].end())
    {
        return mSounds[index].at(file);
    }

    return nullptr;
}
