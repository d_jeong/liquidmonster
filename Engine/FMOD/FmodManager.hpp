/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : FmodManager.hpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Sejeong Park
Secondary :
- End Header ----------------------------------------------------------------*/

#pragma once
#include <fmod.hpp>
#include <fmod.h>
#include <iostream>
#include <string>
#include <unordered_map>

class FmodManager
{
public:
    FmodManager();
    ~FmodManager();

    void Initialize();
    void Update();
    void Delete();

    FMOD::Channel* GetBackgroundChannel();
    FMOD::Channel* GetSoundEffectChannel();
    FMOD::Channel* GetUISoundChannel();

    void CreateBackgroundSound(const std::string &file);
    void CreateSoundEffectSound(const std::string &file);
    void CreateSoundEffectSound(const std::string& file, FMOD_MODE channel);
    void CreateUISound(const std::string& file);

    void PlayBackgroundSound(const std::string &file);
    void PlaySoundEffectSound(const std::string &file);
    FMOD::Channel* PlaySoundEffectSound(const std::string& file, FMOD::Channel* channel);
    void PlayUISound(const std::string& file);

    void StopAllGameSound();
    void StopAllUISound();

    void StopBackgroundSound();
    void StopSoundEffectSound();
    void StopUISound();

    void SetAllGameSoundPaused(bool paused);
    void SetAllUISoundPaused(bool paused);

    void SetBackgroundSoundPaused(bool paused);
    void SetSoundEffectSoundPaused(bool paused);
    void SetUISoundPaused(bool paused);

    void SetChannelPaused(FMOD::Channel* channel, bool paused);
    void SetPhysicsSFXPaused(bool paused);
    void SetGroupOfSFXLoopPaused(bool paused);
    FMOD::Channel* FindChannel(int groupIndex, FMOD::Sound* findSound);

    void SetVolumeAllGameSound(float volume);
    void SetVolumeAllUISound(float volume);

    void SetVolumeBackgroundSound(float volume);
    void SetVolumeSoundEffectSound(float volume);
    void SetVolumeUISound(float volume);

    void CheckError(FMOD_RESULT result);
    bool IsPlaying(FMOD::Channel* channel);

    FMOD::Channel* PlaySoundEffectForLOOP(const std::string& file, FMOD::Channel* channel);
    void SetSoundEffectForLoop(bool paused);
    void StopSoundEffectForLoop();

    FMOD::Sound* GetSound(int index, const std::string& file);


private:
    FMOD::System *mSystem = nullptr;
    FMOD_RESULT   m_result;

    std::unordered_map<std::string, FMOD::Sound *> mSounds[3]; // background_sounds, sound_effects, UI_sounds
    FMOD::ChannelGroup *                           mMaster = nullptr;
    FMOD::ChannelGroup *                           mMasterUI = nullptr;
    FMOD::ChannelGroup *                           mGroups[4]; // background_sounds, sound_effects, UI_sounds, ingames

    FMOD::Channel *mChannel             = nullptr;
    FMOD::Channel *mSoundEffectChannel = nullptr;
    FMOD::Channel* mUIChannel = nullptr;

    FMOD::Channel* mPhysicsSFX = nullptr;

    // FMOD_SYSTEM* p_System= nullptr;
    /*FMOD_SOUND* p_BackgroundSound = nullptr;
    FMOD_CHANNEL* p_BackgroundChannel = nullptr;

    FMOD_SOUND* p_SoundEffect = nullptr;
    FMOD_CHANNEL* p_SoundEffectChannel = nullptr;

    int m_numberEffectSound =0;
    int m_numberBackgroundSound =0;*/
};
extern FmodManager *FMOD_MANAGER;