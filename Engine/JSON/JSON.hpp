/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : json.hpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Daeun Jeong
Secondary :
- End Header ----------------------------------------------------------------*/

#pragma once

#include "../../external/rapidjson/document.h"
#include "../../external/rapidjson/writer.h"
#include "../../external/rapidjson/stringbuffer.h"
#include "../../external/rapidjson/filewritestream.h"
#include "../../external/rapidjson/filereadstream.h"
#include "../../external/rapidjson/prettywriter.h"
#include "../Object/ObjectManager/ObjectManager.hpp"
#include "../../Game/State/StateManager/StateManager.hpp"
#include "../../Game/State/State.hpp"
#include "../TileMap/TileMap.hpp"

const int MAX_BUFFER_SIZE = 15384;

using namespace rapidjson;

enum class eTileType;

class Json
{
public:
    Json();
    ~Json();

    void Initialize(void);
    void Update(float dt);

    void ObjectsToDocument(Object* obj, const std::string& file, const std::string& path);
    void SaveObjectsToJson(const std::string& file, const std::string& path);
    void LoadObjectFromJson(const std::string& file, const std::string& path);
    Document LoadObjectDocumentFromJson(const std::string& file, const std::string& path);

    void SaveLevelObject(Object* obj, const std::string& file, const std::string& path);
    void LoadLevel(const std::string& file, const std::string& path);

    void TilesToDocument(int grid_, Tile* obj, eTileType type, const std::string& path);
    void SaveTilesToJson(eTileType type, const std::string& path);
    void LoadTilesFromJson(eTileType type, const std::string& file);
    void SaveLevelTiles(int grid, Tile* tiles, eTileType type, const std::string& file, const std::string& path);
    Document LoadTilesDocumentFromJson(eTileType type, const std::string& file);

    Value ButtonToValue(Object* object);
    Value BatteryToValue(Object* object);
    Value CollisionToValue(Object* object);
    Value FanToValue(Object* object);
    Value PressureToValue(Object* object);
    Value SpriteToValue(Object* object);
    Value TransformToValue(Object* object);
    Value TriggerToValue(Object* object);
    Value MovingPlatformToValue(Object* object);
    Value PlayerToValue(Object* object);

    void ValueToLevel(Value&);
    void ValueToObjectMember(Object* object, Value&);
    void ValueToTransform(Object* object, Value&);
    void ValueToSprite(Object* object, Value&);
    void ValueToCollision(Object* object, Value&);
    void ValueToTrigger(Object* object, Value&);
    void ValueToBattery(Object* object, Value&);
    void ValueToButton(Object* object, Value&);
    void ValueToFan(Object* object, Value&);
    void ValueToPressure(Object* object, Value&);
    void ValueToMovingPlatform(Object* object, Value&);
    void ValueToPlayer(Object* object, Value&);

    Document& GetObjectDocument(void);
    Document& GetTileDocument(void);

private:
    Document mObjectDocument{};
    Document mTileDocument{};

    StringBuffer mObjectBuffer{};
    StringBuffer mTileBuffer{};

    std::string mFilePath = "levels/";
};

extern Json* JSON;