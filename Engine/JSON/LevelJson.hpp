/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : LevelJson.hpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Daeun Jeong
Secondary :
- End Header ----------------------------------------------------------------*/

#pragma once

#include "../../external/rapidjson/document.h"
#include "../../external/rapidjson/writer.h"
#include "../../external/rapidjson/stringbuffer.h"
#include "../../external/rapidjson/filewritestream.h"
#include "../../external/rapidjson/filereadstream.h"
#include "../../external/rapidjson/prettywriter.h"
#include <string>
#include <unordered_map>
#include "SDL.h"

using namespace rapidjson;

class LevelJson
{
public:
    LevelJson();
    ~LevelJson();

    void Initialize(void);
    void Update(float dt);

    void LevelToDocument(int index);
    void SaveLevelLockToJson();
    Document LoadLevelLockDocumentFromJson() const;
    Document& GetLevelLockDocument(void);

    void LoadLevelLockFromJson();
    void ClearLevelLockFile();

private:
    Document mLevelDocument{};
    StringBuffer mLevelBuffer{};
    
    std::wstring mLevelLockPath = L"levels/";
};

extern LevelJson *LEVEL_JSON;