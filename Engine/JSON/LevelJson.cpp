/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : LevelJson.cpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Daeun Jeong
Secondary :
- End Header ----------------------------------------------------------------*/

#include "LevelJson.hpp"
#include "../Game/State/LevelSelect/LevelSelect.hpp"
#include "../Game/State/StateManager/StateManager.hpp"
#include "Json.hpp"
#include <filesystem>
#include <iostream>

LevelJson* LEVEL_JSON = new LevelJson();

LevelJson::LevelJson()
{
    LEVEL_JSON = this;
    mLevelDocument.SetObject();
}

LevelJson::~LevelJson()
{
    delete LEVEL_JSON;
    LEVEL_JSON = nullptr;
}

void LevelJson::Initialize()
{
    mLevelDocument.SetObject();

    char* path = SDL_GetPrefPath("BAM", "LiquidMonsters");


    if (path != nullptr)
    {
        mLevelLockPath = (wchar_t*)SDL_iconv_utf8_ucs2(path);

        std::wstring fileName(mLevelLockPath);
        fileName.append(L"LevelLock.json");
        try
        {
            if (std::filesystem::exists(fileName) == true)
            {
                SDL_free(path);
                return;
            }
            else
            {
                ClearLevelLockFile();
                SDL_free(path);
            }
        }
        catch (std::system_error& error)
        {
            std::string str(error.what());
            std::cout << str << std::endl;
        }
        
        SDL_free(path);
    }
}

void LevelJson::Update(float dt)
{
}

void LevelJson::SaveLevelLockToJson()
{
    std::wstring fileName(mLevelLockPath);
    fileName.append(L"LevelLock.json");

    FILE* filePointer = nullptr;

    /* w+
    write/update: Create an empty file and open it for update (both for input and output).
    If a file with the same name already exists its contents are discarded
    and the file is treated as a new empty file.*/
    errno_t error = _wfopen_s(&filePointer, fileName.c_str(), L"w");

    if (error == 0)
    {
        char writeBuffer[MAX_BUFFER_SIZE];

        FileWriteStream outStream(filePointer, writeBuffer, sizeof(writeBuffer));

        outStream.Flush();

        PrettyWriter<FileWriteStream> writer(outStream);

        mLevelDocument.Accept(writer);
    }
    else
    {
        std::cout << "Failed to open file. [SaveLevelLockToJson]" << std::endl;
        std::cout << std::filesystem::absolute(fileName) << std::endl;
        std::cout << "SaveLevelLockToJson : " << static_cast<int>(error) << std::endl;

        return;
    }

    fclose(filePointer);
}

Document LevelJson::LoadLevelLockDocumentFromJson() const
{
    std::wstring fileName(mLevelLockPath);
    fileName.append(L"LevelLock.json");

    FILE* filePointer = nullptr;

    /* r+
    read/update: Open a file for update (both for input and output).
    The file must exist. */
    errno_t error = _wfopen_s(&filePointer, fileName.c_str(), L"r");

    if (error == 0)
    {
        char           readBuffer[MAX_BUFFER_SIZE];
        FileReadStream inStream(filePointer, readBuffer, sizeof(readBuffer));

        Document LevelLockDoc;
        LevelLockDoc.ParseStream(inStream);

        fclose(filePointer);

        return LevelLockDoc;
    }
    else
    {
        std::cout << "Failed to open file. [OpenLevelLockInformation]" << std::endl;

        return NULL;
    }

    return NULL;
}

Document& LevelJson::GetLevelLockDocument(void)
{
    return mLevelDocument;
}

void LevelJson::LevelToDocument(int index)
{
    Value levelTree(kArrayType);
    levelTree.SetObject();

    Value isAvailable, id;

    id.SetInt(index);
    levelTree.AddMember("ID", id, mLevelDocument.GetAllocator());

    isAvailable.SetBool(LEVEL_SELECT->GetIsAvailable(index).first);
    levelTree.AddMember("IsAvailable", isAvailable, mLevelDocument.GetAllocator());

    mLevelDocument.AddMember("Level", levelTree, mLevelDocument.GetAllocator());

    mLevelDocument.Parse(mLevelBuffer.GetString());

    SaveLevelLockToJson();
}

void LevelJson::LoadLevelLockFromJson()
{
    mLevelDocument = LoadLevelLockDocumentFromJson();

    for (auto& temp : mLevelDocument.GetObject())
    {
        Value& lock_array = temp.value;
        Value  levelId, isAvailable;

        levelId.SetObject();
        isAvailable.SetObject();

        if (lock_array.HasMember("ID") == true && lock_array.HasMember("IsAvailable") == true)
        {
            levelId     = lock_array.FindMember("ID")->value;
            isAvailable = lock_array.FindMember("IsAvailable")->value;

            int  id     = levelId.GetInt();
            bool status = isAvailable.GetBool();

            LEVEL_SELECT->SetIsAvailable(status, id);
        }
    }

    mLevelDocument.SetObject();
}

void LevelJson::ClearLevelLockFile()
{
    std::wstring fileName(mLevelLockPath);
    fileName.append(L"LevelLock.json");

    if (std::filesystem::exists(fileName) == true)
    {
        std::filesystem::remove(fileName);
        std::cout << "Removed LevelLock.json." << std::endl;
    }

    STATE_MANAGER->GetCurrentState()->SaveLevelLock();
}