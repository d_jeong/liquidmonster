/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : Json.cpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Daeun Jeong
Secondary :
- End Header ----------------------------------------------------------------*/

#include "Json.hpp"
#include "../Component/AllComponents.hpp"
#include "../Data/Data.hpp"
#include "../Graphics/Background/BackgroundManager.hpp"
#include "../Graphics/Graphics.hpp"
#include "../Object/Object.hpp"
#include "../Object/ObjectDepth.hpp"
#include "../Object/ObjectType.hpp"
#include <iostream>
#include <filesystem>

Json* JSON = new Json();

Json::Json()
{
    JSON = this;
}

Json::~Json()
{
    delete JSON;
    JSON = nullptr;
}

void Json::Initialize()
{
    /* Make a document */
    mObjectDocument.SetObject();
    mTileDocument.SetObject();
}

void Json::Update(float /*dt*/)
{
}

void Json::ObjectsToDocument(Object* object, const std::string& file, const std::string& path)
{
    if (object->GetName() == "StateNumber")
    {
        return;
    }

    /* Value to save */
    Value objectTree(kArrayType);
    Value levelTree(kArrayType);

    Value componentBatteryTree(kArrayType);
    Value componentPlayerTree(kArrayType);
    Value componentButtonTree(kArrayType);
    Value componentMovingPlatformTree(kArrayType);
    Value componentCollisionTree(kArrayType);
    Value componentFanTree(kArrayType);
    Value componentPressureTree(kArrayType);
    Value componentSpriteTree(kArrayType);
    Value componentTransformTree(kArrayType);
    Value componentTriggerTree(kArrayType);
    Value depth, objectType, name, isActive;

    Value levelSize, backgroundType;

    /* Level Setting */
    levelTree.SetObject();

    levelSize.SetObject();
    levelSize.AddMember("x", GRAPHICS->GetLevelWidthAndHeight().x, mObjectDocument.GetAllocator());
    levelSize.AddMember("y", GRAPHICS->GetLevelWidthAndHeight().y, mObjectDocument.GetAllocator());

    backgroundType.SetInt(static_cast<int>(GRAPHICS->GetBackground()));

    /* Object Setting */
    objectTree.SetObject();

    objectType.SetInt(static_cast<int>(object->GetObjectType()));

    name.SetString(object->GetName().c_str(), static_cast<rapidjson::SizeType>(object->GetName().length()), mObjectDocument.GetAllocator());

    isActive.SetBool(object->GetIsActive());

    /* Component Setting */
    componentBatteryTree.SetObject();
    componentPlayerTree.SetObject();
    componentButtonTree.SetObject();
    componentMovingPlatformTree.SetObject();
    componentCollisionTree.SetObject();
    componentFanTree.SetObject();
    componentPressureTree.SetObject();
    componentSpriteTree.SetObject();
    componentTransformTree.SetObject();
    componentTriggerTree.SetObject();

    componentTransformTree = TransformToValue(object);

    /* SET COMPONENT */
    if (object->GetComponentByType<Battery>(eComponentType::BATTERY) != nullptr)
    {
        componentBatteryTree = BatteryToValue(object);
    }
    if (object->GetComponentByType<Collision>(eComponentType::COLLISION) != nullptr)
    {
        componentCollisionTree = CollisionToValue(object);
    }
    if (object->GetComponentByType<Fan>(eComponentType::FAN) != nullptr)
    {
        componentFanTree = FanToValue(object);
    }
    if (object->GetComponentByType<Pressure>(eComponentType::PRESSURE) != nullptr)
    {
        componentPressureTree = PressureToValue(object);
    }
    if (object->GetComponentByType<Sprite>(eComponentType::SPRITE) != nullptr)
    {
        componentSpriteTree = SpriteToValue(object);
    }
    if (object->GetComponentByType<Trigger>(eComponentType::TRIGGER) != nullptr)
    {
        componentTriggerTree = TriggerToValue(object);
    }
    if (object->GetComponentByType<Button>(eComponentType::BUTTON) != nullptr)
    {
        componentButtonTree = ButtonToValue(object);
    }
    if (object->GetComponentByType<MovingPlatform>(eComponentType::MOVINGPLATFORM) != nullptr)
    {
        componentMovingPlatformTree = MovingPlatformToValue(object);
    }
    if (object->GetComponentByType<Player>(eComponentType::PLAYER) != nullptr)
    {
        componentPlayerTree = PlayerToValue(object);
    }

    /* SET DEPTH */
    if (object->GetObjectType() == eObjectType::BACKGROUND)
    {
        depth.SetFloat(GAME_BACKGROUND);
    }
    else if (object->GetObjectType() == eObjectType::DOOR)
    {
        depth.SetFloat(GAME_DOOR);
    }
    else if ((object->GetObjectType() == eObjectType::PLAYER_1)
        || (object->GetObjectType() == eObjectType::PLAYER_2)
        || (object->GetObjectType() == eObjectType::PLAYER_COMBINED))
    {
        depth.SetFloat(GAME_PLAYER);
    }
    else if ((object->GetObjectType() == eObjectType::ARROW_1)
        || (object->GetObjectType() == eObjectType::ARROW_2)
        || (object->GetObjectType() == eObjectType::FAN)
        || (object->GetObjectType() == eObjectType::EATABLE)
        || (object->GetObjectType() == eObjectType::BATTERY))
    {
        depth.SetFloat(GAME_UI);
    }
    else
    {
        depth.SetFloat(GAME_OBJECT);
    }

    /* Add Object Value to Json Document Object Tree (DOM) */
    objectTree.AddMember("Name", name, mObjectDocument.GetAllocator());
    objectTree.AddMember("IsActive", isActive, mObjectDocument.GetAllocator());
    objectTree.AddMember("ObjectType", objectType, mObjectDocument.GetAllocator());
    objectTree.AddMember("Depth", depth, mObjectDocument.GetAllocator());
    objectTree.AddMember("Transform", componentTransformTree, mObjectDocument.GetAllocator());
    objectTree.AddMember("Sprite", componentSpriteTree, mObjectDocument.GetAllocator());
    objectTree.AddMember("Collision", componentCollisionTree, mObjectDocument.GetAllocator());
    objectTree.AddMember("Trigger", componentTriggerTree, mObjectDocument.GetAllocator());
    objectTree.AddMember("Pressure", componentPressureTree, mObjectDocument.GetAllocator());
    objectTree.AddMember("Fan", componentFanTree, mObjectDocument.GetAllocator());
    objectTree.AddMember("Button", componentButtonTree, mObjectDocument.GetAllocator());
    objectTree.AddMember("MovingPlatform", componentMovingPlatformTree, mObjectDocument.GetAllocator());
    objectTree.AddMember("Battery", componentBatteryTree, mObjectDocument.GetAllocator());
    objectTree.AddMember("Player", componentPlayerTree, mObjectDocument.GetAllocator());

    /* Add Level Value to Json Document Object Tree (DOM) */
    if (mObjectDocument.HasMember("Level") == false)
    {
        levelTree.AddMember("LevelSize", levelSize, mObjectDocument.GetAllocator());
        levelTree.AddMember("BackgroundType", backgroundType, mObjectDocument.GetAllocator());
        mObjectDocument.AddMember("Level", levelTree, mObjectDocument.GetAllocator());
    }

    mObjectDocument.AddMember("Object", objectTree, mObjectDocument.GetAllocator());

    mObjectDocument.Parse(mObjectBuffer.GetString());

    SaveObjectsToJson(file, path);
}

Value Json::TransformToValue(Object* object)
{
    Value transform(kArrayType);
    Value translation, scale, rotation;

    transform.SetObject();
    translation.SetObject();
    scale.SetObject();
    rotation.SetObject();

    translation.AddMember("x", object->GetTransform()->GetPosition().x, mObjectDocument.GetAllocator());
    translation.AddMember("y", object->GetTransform()->GetPosition().y, mObjectDocument.GetAllocator());

    scale.AddMember("x", object->GetTransform()->GetScale().x, mObjectDocument.GetAllocator());
    scale.AddMember("y", object->GetTransform()->GetScale().y, mObjectDocument.GetAllocator());

    rotation.SetFloat(object->GetTransform()->GetRotation());

    transform.AddMember("position", translation, mObjectDocument.GetAllocator());
    transform.AddMember("scale", scale, mObjectDocument.GetAllocator());
    transform.AddMember("rotation", rotation, mObjectDocument.GetAllocator());

    return transform;
}

Value Json::TriggerToValue(Object* obj)
{
    Value trigger(kArrayType);

    if (obj->GetComponentByType<Trigger>(eComponentType::TRIGGER) != nullptr)
    {
        Value triggerType, connectedObjectName;

        trigger.SetObject();

        Trigger*    triggerPointer = obj->GetComponentByType<Trigger>(eComponentType::TRIGGER);
        std::string name           = triggerPointer->GetConnectedObject()->GetName();

        triggerType.SetInt(static_cast<int>(triggerPointer->GetTriggerType()));
        connectedObjectName.SetString(name.c_str(), static_cast<rapidjson::SizeType>(name.length()), mObjectDocument.GetAllocator());

        trigger.AddMember("triggerType", triggerType, mObjectDocument.GetAllocator());
        trigger.AddMember("connectedObjectName", connectedObjectName, mObjectDocument.GetAllocator());
    }

    return trigger;
}

Value Json::MovingPlatformToValue(Object* object)
{
    Value movingPlatform(kArrayType);

    Value moveWayType, movePosition, isActive;

    movingPlatform.SetObject();
    movePosition.SetObject();
    isActive.SetObject();

    MovingPlatform* movingPointer = object->GetComponentByType<MovingPlatform>(eComponentType::MOVINGPLATFORM);

    moveWayType.SetInt(static_cast<int>(movingPointer->GetMoveWay()));
    movePosition.AddMember("x", movingPointer->GetPosition().x, mObjectDocument.GetAllocator());
    movePosition.AddMember("y", movingPointer->GetPosition().y, mObjectDocument.GetAllocator());
    isActive.SetBool(movingPointer->GetIsMovingPlatformActive());

    movingPlatform.AddMember("movePosition", movePosition, mObjectDocument.GetAllocator());
    movingPlatform.AddMember("moveWayType", moveWayType, mObjectDocument.GetAllocator());
    movingPlatform.AddMember("isActive", isActive, mObjectDocument.GetAllocator());

    return movingPlatform;
}

Value Json::PlayerToValue(Object* object)
{
    Value player(kArrayType);
    Value direction;

    player.SetObject();

    direction.SetInt(static_cast<int>(object->GetComponentByType<Player>(eComponentType::PLAYER)->GetContollerDirection()));
    player.AddMember("direction", direction, mObjectDocument.GetAllocator());

    return player;
}

Value Json::FanToValue(Object* obj)
{
    Value fan(kArrayType);
    Value fanType, isActive;

    fan.SetObject();
    fanType.SetObject();

    isActive.SetBool(static_cast<int>(obj->GetComponentByType<Fan>(eComponentType::FAN)->GetIsFanActive()));
    fanType.SetInt(static_cast<int>(obj->GetComponentByType<Fan>(eComponentType::FAN)->GetFanType()));
    fan.AddMember("fanType", fanType, mObjectDocument.GetAllocator());
    fan.AddMember("isActive", isActive, mObjectDocument.GetAllocator());

    return fan;
}

Value Json::PressureToValue(Object* obj)
{
    Value pressureType, typeEnum, isActive;

    pressureType.SetObject();
    isActive.SetObject();

    typeEnum.SetInt(static_cast<int>(obj->GetComponentByType<Pressure>(eComponentType::PRESSURE)->GetPressureType()));
    isActive.SetBool(obj->GetComponentByType<Pressure>(eComponentType::PRESSURE)->GetIsPressureActive());
    pressureType.AddMember("pressureType", typeEnum, mObjectDocument.GetAllocator());
    pressureType.AddMember("isActive", isActive, mObjectDocument.GetAllocator());

    return pressureType;
}

Value Json::SpriteToValue(Object* object)
{
    Value sprite(kArrayType);
    Value textureEnum, isFlip, animationFrame, animationSpeed, isAnimation, color;

    sprite.SetObject();

    textureEnum.SetInt(static_cast<int>(object->GetSprite()->GetTextureEnum()));
    isFlip.SetBool(object->GetSprite()->GetIsFlip());
    animationFrame.SetFloat(static_cast<float>(object->GetSprite()->GetAnimationFrameNum()));
    animationSpeed.SetFloat(object->GetSprite()->GetAnimationSpeed());
    isAnimation.SetBool(object->GetSprite()->IsAnimation());

    color.SetObject();
    color.AddMember("r", object->GetSprite()->GetColor().x, mObjectDocument.GetAllocator());
    color.AddMember("g", object->GetSprite()->GetColor().y, mObjectDocument.GetAllocator());
    color.AddMember("b", object->GetSprite()->GetColor().z, mObjectDocument.GetAllocator());
    color.AddMember("a", object->GetSprite()->GetColor().w, mObjectDocument.GetAllocator());

    sprite.AddMember("textureEnum", textureEnum, mObjectDocument.GetAllocator());
    sprite.AddMember("isFlip", isFlip, mObjectDocument.GetAllocator());
    sprite.AddMember("animationFrame", animationFrame, mObjectDocument.GetAllocator());
    sprite.AddMember("animationSpeed", animationSpeed, mObjectDocument.GetAllocator());
    sprite.AddMember("isAnimation", isAnimation, mObjectDocument.GetAllocator());
    sprite.AddMember("color", color, mObjectDocument.GetAllocator());

    return sprite;
}

Value Json::ButtonToValue(Object* object)
{
    Value button(kArrayType);

    button.SetObject();

    Value connectedObjectName, direction;

    std::string name = "";

    if (object->GetComponentByType<Button>(eComponentType::BUTTON)->GetConnectedObject() != nullptr)
    {
        name = object->GetComponentByType<Button>(eComponentType::BUTTON)->GetConnectedObject()->GetName();
    }

    connectedObjectName.SetString(name.c_str(), static_cast<rapidjson::SizeType>(name.length()), mObjectDocument.GetAllocator());

    direction.SetInt(static_cast<int>(object->GetComponentByType<Button>(eComponentType::BUTTON)->GetButtonDirection()));

    button.AddMember("connectedObjectName", connectedObjectName, mObjectDocument.GetAllocator());
    button.AddMember("direction", direction, mObjectDocument.GetAllocator());

    return button;
}

Value Json::BatteryToValue(Object* object)
{
    Value battery(kArrayType);

    if (object->GetComponentByType<Battery>(eComponentType::BATTERY) != nullptr)
    {
        Value connectedObjectName, batteryHomeName;

        battery.SetObject();

        std::string name1 = object->GetComponentByType<Battery>(eComponentType::BATTERY)->GetConnectingObject()->GetName();
        std::string name2 = object->GetComponentByType<Battery>(eComponentType::BATTERY)->GetBatteryHome()->GetName();

        connectedObjectName.SetString(name1.c_str(), static_cast<rapidjson::SizeType>(name1.length()), mObjectDocument.GetAllocator());
        batteryHomeName.SetString(name2.c_str(), static_cast<rapidjson::SizeType>(name2.length()), mObjectDocument.GetAllocator());

        battery.AddMember("connectedObjectName", connectedObjectName, mObjectDocument.GetAllocator());
        battery.AddMember("batteryHomeName", batteryHomeName, mObjectDocument.GetAllocator());
    }

    return battery;
}

Value Json::CollisionToValue(Object* object)
{
    Value collision(kArrayType);
    Value isGhost, scale;

    collision.SetObject();

    isGhost.SetBool(object->GetComponentByType<Collision>(eComponentType::COLLISION)->GetIsGhost());

    scale.SetObject();
    scale.AddMember("x", object->GetScale().x, mObjectDocument.GetAllocator());
    scale.AddMember("y", object->GetScale().y, mObjectDocument.GetAllocator());

    collision.AddMember("isGhost", isGhost, mObjectDocument.GetAllocator());
    collision.AddMember("scale", scale, mObjectDocument.GetAllocator());

    return collision;
}

void Json::ValueToLevel(Value& objectTree)
{
    if (objectTree.HasMember("LevelSize") == true)
    {
        Vector2<int> size = {objectTree.FindMember("LevelSize")->value.FindMember("x")->value.GetInt(), objectTree.FindMember("LevelSize")->value.FindMember("y")->value.GetInt()};

        GRAPHICS->SetLevelWidthAndHeight(size);
    }

    if (objectTree.HasMember("BackgroundType") == true)
    {
        GRAPHICS->SetBackground(static_cast<BackgroundManager::eBackground>(objectTree.FindMember("BackgroundType")->value.GetInt()));
    }
}

void Json::ValueToObjectMember(Object* object, Value& objectTree)
{
    Value depth;

    if (objectTree.HasMember("Name") == true)
    {
        object->SetName(objectTree.FindMember("Name")->value.GetString());
    }

    if (objectTree.HasMember("ObjectType") == true)
    {
        object->SetObjectType(static_cast<eObjectType>(objectTree.FindMember("ObjectType")->value.GetInt()));
    }

    if (objectTree.HasMember("IsActive") == true)
    {
        object->SetIsActive(objectTree.FindMember("IsActive")->value.GetBool());
    }

    if ((objectTree.HasMember("Depth") == true) && (objectTree.FindMember("Depth")->value.GetFloat() != NULL))
    {
        depth.SetFloat(objectTree.FindMember("Depth")->value.GetFloat());

        object->SetDepth(depth.GetFloat());
    }
}

void Json::ValueToTransform(Object* object, Value& objectTree)
{
    Value transform;

    if (objectTree.HasMember("Transform") == true)
    {
        transform.SetObject();

        transform = objectTree.FindMember("Transform")->value;

        if (object->GetComponentByType<Transform>(eComponentType::TRANSFORM) == nullptr)
        {
            object->AddComponent(new Transform());
        }

        Vector2<float> translation, scale;
        float          rotation;

        if (transform.HasMember("position") == true)
        {
            translation.x = transform.FindMember("position")->value.FindMember("x")->value.GetFloat();
            translation.y = transform.FindMember("position")->value.FindMember("y")->value.GetFloat();
            object->SetPosition(translation);
            object->SetSpawnPosition(translation);
        }

        if (transform.HasMember("scale") == true)
        {
            scale.x = transform.FindMember("scale")->value.FindMember("x")->value.GetFloat();
            scale.y = transform.FindMember("scale")->value.FindMember("y")->value.GetFloat();
            object->SetScale(scale);
        }

        if (transform.HasMember("rotation") == true)
        {
            rotation = transform.FindMember("rotation")->value.GetFloat();
            object->SetRotation(rotation);
        }
    }
}

void Json::ValueToSprite(Object* object, Value& objectTree)
{
    Value sprite;

    if (objectTree.HasMember("Sprite") == true)
    {
        sprite.SetObject();

        sprite = objectTree.FindMember("Sprite")->value;

        if (object->GetComponentByType<Sprite>(eComponentType::SPRITE) == nullptr)
        {
            object->AddComponent(new Sprite());
        }

        Sprite* spritePointer = object->GetSprite();

        int   textureEnum;
        float animationFrame, animationSpeed;
        bool  isFlip, isAnimation;

        if (sprite.HasMember("textureEnum") == true)
        {
            textureEnum = sprite.FindMember("textureEnum")->value.GetInt();
            spritePointer->LoadTextureFromData(static_cast<eTexture>(textureEnum));
        }

        if (sprite.HasMember("isFlip") == true)
        {
            isFlip = sprite.FindMember("isFlip")->value.GetBool();

            spritePointer->SetFlip(isFlip);
        }

        if (sprite.HasMember("isAnimation") == true)
        {
            isAnimation = sprite.FindMember("isAnimation")->value.GetBool();

            spritePointer->SetAnimation(isAnimation);
        }

        if (sprite.HasMember("animationFrame") == true)
        {
            animationFrame = sprite.FindMember("animationFrame")->value.GetFloat();

            spritePointer->SetAnimationFrame(static_cast<unsigned int>(animationFrame));
        }

        if (sprite.HasMember("animationSpeed") == true)
        {
            animationSpeed = sprite.FindMember("animationSpeed")->value.GetFloat();

            spritePointer->SetAnimationSpeed(animationSpeed);
        }

        if (sprite.HasMember("color") == true)
        {
            Vector4<float> color = {sprite.FindMember("color")->value.FindMember("r")->value.GetFloat(), sprite.FindMember("color")->value.FindMember("g")->value.GetFloat(),
                                    sprite.FindMember("color")->value.FindMember("b")->value.GetFloat(), sprite.FindMember("color")->value.FindMember("a")->value.GetFloat()};

            spritePointer->SetColor(color);
        }
    }
}

void Json::ValueToCollision(Object* object, Value& objectTree)
{
    Value collision;

    if (objectTree.HasMember("Collision") == true)
    {
        collision.SetObject();

        collision = objectTree.FindMember("Collision")->value;

        bool           isGhost;
        Vector2<float> scale;

        if (object->GetComponentByType<Collision>(eComponentType::COLLISION) == nullptr)
        {
            object->AddComponent(new Collision());
        }

        object->SetVelocity(Vector2<float>(0, 0));

        if (collision.HasMember("isGhost") == true)
        {
            isGhost = collision.FindMember("isGhost")->value.GetBool();

            object->GetComponentByType<Collision>(eComponentType::COLLISION)->SetIsGhost(isGhost);
        }

        if (collision.HasMember("scale") == true)
        {
            scale.x = collision.FindMember("scale")->value.FindMember("x")->value.GetFloat();
            scale.y = collision.FindMember("scale")->value.FindMember("y")->value.GetFloat();
        }
    }
}

void Json::ValueToTrigger(Object* object, Value& objectTree)
{
    Value trigger;

    if (objectTree.HasMember("Trigger") == true)
    {
        trigger.SetObject();

        trigger = objectTree.FindMember("Trigger")->value;

        int         triggerType;
        std::string connectedObjectName;

        if ((trigger.HasMember("triggerType") == true) && (trigger.HasMember("connectedObjectName") == true))
        {
            triggerType = trigger.FindMember("triggerType")->value.GetInt();

            connectedObjectName = trigger.FindMember("connectedObjectName")->value.GetString();

            if (object->GetComponentByType<Trigger>(eComponentType::TRIGGER) == nullptr)
            {
                object->AddComponent(new Trigger());
            }

            Trigger* triggerPointer = object->GetComponentByType<Trigger>(eComponentType::TRIGGER);

            triggerPointer->SetTriggerType(static_cast<eTriggerType>(triggerType));

            if (OBJECT_MANAGER->FindObjectByName(connectedObjectName) == nullptr)
            {
                Object* connectedObject = new Object(connectedObjectName, eObjectType::NONE);
                OBJECT_MANAGER->AddObject(connectedObject);
            }

            triggerPointer->SetConnectedObject(OBJECT_MANAGER->FindObjectByName(connectedObjectName));
        }
    }
}

void Json::ValueToBattery(Object* object, Value& objectTree)
{
    Value battery;

    if (objectTree.HasMember("Battery") == true)
    {
        battery.SetObject();

        battery = objectTree.FindMember("Battery")->value;

        std::string connectedObjectName, batteryHomeName;

        if ((battery.HasMember("connectedObjectName") == true) && (battery.HasMember("batteryHomeName") == true))
        {
            batteryHomeName     = battery.FindMember("batteryHomeName")->value.GetString();
            connectedObjectName = battery.FindMember("connectedObjectName")->value.GetString();

            if (OBJECT_MANAGER->FindObjectByName(batteryHomeName) == nullptr)
            {
                Object* batteryHomeObject = new Object(batteryHomeName, eObjectType::NONE);
                OBJECT_MANAGER->AddObject(batteryHomeObject);
            }

            if (OBJECT_MANAGER->FindObjectByName(connectedObjectName) == nullptr)
            {
                Object* connectedObject = new Object(connectedObjectName, eObjectType::NONE);
                OBJECT_MANAGER->AddObject(connectedObject);
            }

            if (object->GetComponentByType<Battery>(eComponentType::BATTERY) == nullptr)
            {
                object->AddComponent(new Battery(OBJECT_MANAGER->FindObjectByName(batteryHomeName), OBJECT_MANAGER->FindObjectByName(connectedObjectName)));
            }
        }
    }
}

void Json::ValueToButton(Object* object, Value& objectTree)
{
    Value button;

    if (objectTree.HasMember("Button") == true)
    {
        button.SetObject();

        button = objectTree.FindMember("Button")->value;

        std::string      connectedObjectName;
        eButtonDirection direction;

        if (button.HasMember("direction") == true)
        {
            direction = static_cast<eButtonDirection>(button.FindMember("direction")->value.GetInt());

            if (object->GetComponentByType<Button>(eComponentType::BUTTON) == nullptr)
            {
                object->AddComponent(new Button());
            }

            Button* buttonPointer = object->GetComponentByType<Button>(eComponentType::BUTTON);
            buttonPointer->SetButtonDirection(direction);
        }

        if (button.HasMember("connectedObjectName") == true)
        {
            connectedObjectName = button.FindMember("connectedObjectName")->value.GetString();

            if (object->GetComponentByType<Button>(eComponentType::BUTTON) == nullptr)
            {
                object->AddComponent(new Button());
            }

            Button* buttonPointer = object->GetComponentByType<Button>(eComponentType::BUTTON);

            if (OBJECT_MANAGER->FindObjectByName(connectedObjectName) == nullptr && connectedObjectName != "")
            {
                Object* connectedObject = new Object(connectedObjectName, eObjectType::NONE);
                OBJECT_MANAGER->AddObject(connectedObject);
            }

            if (connectedObjectName != "")
            {
                buttonPointer->SetConnectedObject(OBJECT_MANAGER->FindObjectByName(connectedObjectName));
            }
        }
    }
}

void Json::ValueToFan(Object* object, Value& objectTree)
{
    Value fan;

    if (objectTree.HasMember("Fan") == true)
    {
        fan.SetObject();

        fan = objectTree.FindMember("Fan")->value;

        int  fanType;
        bool isActive;

        if ((fan.HasMember("fanType") == true) && (fan.HasMember("isActive") == true))
        {
            fanType  = fan.FindMember("fanType")->value.GetInt();
            isActive = fan.FindMember("isActive")->value.GetBool();

            if (object->GetComponentByType<Fan>(eComponentType::FAN) == nullptr)
            {
                object->AddComponent(new Fan(isActive, static_cast<eFanType>(fanType)));
            }
        }
    }
}

void Json::ValueToPressure(Object* object, Value& objectTree)
{
    Value pressure;

    if (objectTree.HasMember("Pressure") == true)
    {
        pressure.SetObject();

        pressure = objectTree.FindMember("Pressure")->value;

        int  pressureType;
        bool isActive;

        if (pressure.HasMember("pressureType") == true && pressure.HasMember("isActive") == true)
        {
            pressureType = pressure.FindMember("pressureType")->value.GetInt();
            isActive     = pressure.FindMember("isActive")->value.GetBool();

            if (object->GetComponentByType<Pressure>(eComponentType::PRESSURE) == nullptr)
            {
                object->AddComponent(new Pressure());
            }

            object->GetComponentByType<Pressure>(eComponentType::PRESSURE)->SetPressureType(static_cast<Pressure::ePressureType>(pressureType));
            object->GetComponentByType<Pressure>(eComponentType::PRESSURE)->SetIsPressureActive(isActive);
        }
    }
}

void Json::ValueToMovingPlatform(Object* object, Value& objectTree)
{
    Value movingPlatform;

    if (objectTree.HasMember("MovingPlatform") == true)
    {
        movingPlatform.SetObject();

        movingPlatform = objectTree.FindMember("MovingPlatform")->value;

        int          moveWayType;
        Vector2<int> movePosition;
        bool         isActive;

        if ((movingPlatform.HasMember("movePosition") == true) && (movingPlatform.HasMember("moveWayType") == true) && (movingPlatform.HasMember("isActive") == true))
        {
            movePosition.x = static_cast<int>(movingPlatform.FindMember("movePosition")->value.FindMember("x")->value.GetFloat());
            movePosition.y = static_cast<int>(movingPlatform.FindMember("movePosition")->value.FindMember("y")->value.GetFloat());

            moveWayType = movingPlatform.FindMember("moveWayType")->value.GetInt();

            isActive = movingPlatform.FindMember("isActive")->value.GetBool();

            if (object->GetComponentByType<MovingPlatform>(eComponentType::MOVINGPLATFORM) == nullptr)
            {
                object->AddComponent(new MovingPlatform(isActive, static_cast<eMoveWay>(moveWayType), static_cast<float>(movePosition.x), static_cast<float>(movePosition.y)));
            }
        }
    }
}

void Json::ValueToPlayer(Object* object, Value& objectTree)
{
    Value player;

    if (objectTree.HasMember("Player") == true)
    {
        player.SetObject();

        player = objectTree.FindMember("Player")->value;

        int direction;

        if (player.HasMember("direction") == true)
        {
            direction = player.FindMember("direction")->value.GetInt();

            if (object->GetComponentByType<Player>(eComponentType::PLAYER) == nullptr)
            {
                object->AddComponent(new Player());
            }

            Player* playerPointer = object->GetComponentByType<Player>(eComponentType::PLAYER);
            playerPointer->SetControllerDirection(static_cast<eControllerDirection>(direction));
        }
    }
}

void Json::SaveObjectsToJson(const std::string& file, const std::string& path)
{
    std::string fileName(mFilePath);

    fileName.append(file);
    fileName.append(path + ".json");

    FILE* filePointer = nullptr;

    /* w+
    write/update: Create an empty file and open it for update (both for input and output).
    If a file with the same name already exists its contents are discarded
    and the file is treated as a new empty file.*/
    errno_t error = fopen_s(&filePointer, fileName.c_str(), "w+");

    if (filePointer == nullptr)
    {
        return;
    }

    if (error == 0)
    {
        char writeBuffer[MAX_BUFFER_SIZE];

        FileWriteStream stream(filePointer, writeBuffer, sizeof(writeBuffer));

        PrettyWriter<FileWriteStream> writer(stream);

        mObjectDocument.Accept(writer);
    }
    else
    {
        std::cout << "Failed to open file. [SaveObjectsToJson]" << std::endl;
        std::cout << std::filesystem::absolute(fileName) << std::endl;

        return;
    }

    fclose(filePointer);
}

Document Json::LoadObjectDocumentFromJson(const std::string& file, const std::string& path)
{
    std::filesystem::path fileName(mFilePath);

    fileName.append(file);
    fileName.append(path + ".json");
    //fileName.append(".json");

    FILE* filePointer = nullptr;

    /* r+
    read/update: Open a file for update (both for input and output).
    The file must exist. */
    errno_t error = fopen_s(&filePointer, fileName.string().c_str(), "r");
    
    if (filePointer != nullptr)
    {
        char readBuffer[MAX_BUFFER_SIZE];

        FileReadStream is(filePointer, readBuffer, sizeof(readBuffer));

        Document objectDocument;

        objectDocument.ParseStream(is);

        fclose(filePointer);

        return objectDocument;
    }
    else
    {
        std::cout << "Failed to open file. [LoadObjectDocumentFromJson]" << std::endl;
        std::cout << std::filesystem::absolute(fileName) << std::endl;
        return NULL;
    }

    return NULL;
}

Document& Json::GetObjectDocument(void)
{
    return mObjectDocument;
}

Document& Json::GetTileDocument(void)
{
    return mTileDocument;
}

void Json::SaveLevelObject(Object* object, const std::string& file, const std::string& path)
{
    ObjectsToDocument(object, file, path);
}

void Json::LoadLevel(const std::string& file, const std::string& path)
{
    LoadObjectFromJson(file, path);
    LoadTilesFromJson(eTileType::GRAPHICS, file);
    LoadTilesFromJson(eTileType::PHYSICS, file);
}

void Json::TilesToDocument(int grid_, Tile* tile, eTileType type, const std::string& path)
{
    Value tileTree(kArrayType);

    Value grid, tile_type, textureEnum, connectedObjectName;

    tileTree.SetObject();

    grid.SetInt(grid_);

    tile_type.SetInt(static_cast<int>(type));

    textureEnum.SetInt(static_cast<int>(tile->texture));

    connectedObjectName.SetString(tile->GetTileConnectedObjectName().c_str(), tile->GetTileConnectedObjectName().length(), mTileDocument.GetAllocator());

    tileTree.AddMember("grid", grid, mTileDocument.GetAllocator());
    tileTree.AddMember("tile_type", tile_type, mTileDocument.GetAllocator());
    tileTree.AddMember("textureEnum", textureEnum, mTileDocument.GetAllocator());
    tileTree.AddMember("connectedObjectName", connectedObjectName, mTileDocument.GetAllocator());

    mTileDocument.AddMember("Tile", tileTree, mTileDocument.GetAllocator());

    mTileDocument.Parse(mTileBuffer.GetString());

    SaveTilesToJson(type, path);
}

void Json::SaveTilesToJson(eTileType type, const std::string& path)
{
    std::string fileName(mFilePath);
    fileName.append(path);

    if (type == eTileType::PHYSICS)
    {
        fileName.append("PhysicsTiles.json");
    }
    else // type == eTileType:::GRAPHICS
    {
        fileName.append("GraphicsTiles.json");
    }

    FILE* filePointer = nullptr;

    /* w+
    write/update: Create an empty file and open it for update (both for input and output).
    If a file with the same name already exists its contents are discarded
    and the file is treated as a new empty file.*/
    errno_t error = fopen_s(&filePointer, fileName.c_str(), "wb+");
    std::cout << std::filesystem::absolute(fileName) << std::endl;
    std::cout << "SaveTilesToJson : " << static_cast<int>(error) << std::endl;

    if (error == 0)
    {
        char            writeBuffer[MAX_BUFFER_SIZE];
        FileWriteStream outStream(filePointer, writeBuffer, sizeof(writeBuffer));

        PrettyWriter<FileWriteStream> writer(outStream);
        mTileDocument.Accept(writer);

        fclose(filePointer);
    }
    else
    {
        std::cout << "Failed to open file. [SaveTilesToJson]" << std::endl;

        return;
    }
}

void Json::LoadTilesFromJson(eTileType type, const std::string& file)
{
    mTileDocument = LoadTilesDocumentFromJson(type, file);

    for (auto& temp : mTileDocument.GetObject())
    {
        Value& tile_array = temp.value;

        Value tile_type, grid, textureEnum, connectedObjectName;

        Object* connectedObject = nullptr;

        tile_type.SetObject();
        grid.SetObject();
        textureEnum.SetObject();
        connectedObjectName.SetObject();

        grid        = tile_array.FindMember("grid")->value;
        tile_type   = tile_array.FindMember("tile_type")->value;
        textureEnum = tile_array.FindMember("textureEnum")->value;

        if (tile_array.HasMember("connectedObjectName") == true)
        {
            connectedObjectName = tile_array.FindMember("connectedObjectName")->value;

            if (connectedObjectName.GetString() != "")
            {
                connectedObject = OBJECT_MANAGER->FindObjectByName(connectedObjectName.GetString());
            }
        }

        if (static_cast<int>(tile_type.GetInt()) == 0) // PHYSICS
        {
            TILE_MAP->CreatePhysicsTile(grid.GetInt(), static_cast<eTexture>(textureEnum.GetInt()), false);

            if (connectedObject != nullptr)
            {
                if (connectedObject->GetObjectType() == eObjectType::BUTTON)
                {
                    connectedObject->GetComponentByType<Button>(eComponentType::BUTTON)->InsertPhysicsTile(grid.GetInt(), static_cast<eTexture>(textureEnum.GetInt()));
                }
            }
        }
        else // GRAPHICS
        {
            TILE_MAP->CreateGraphicTile(grid.GetInt(), static_cast<eTexture>(textureEnum.GetInt()), false);

            if (connectedObject != nullptr)
            {
                if (connectedObject->GetObjectType() == eObjectType::BUTTON)
                {
                    connectedObject->GetComponentByType<Button>(eComponentType::BUTTON)->InsertGraphicsTile(grid.GetInt(), static_cast<eTexture>(textureEnum.GetInt()));
                }
                else if (connectedObject->GetObjectType() == eObjectType::BATTERY)
                {
                    connectedObject->GetComponentByType<Battery>(eComponentType::BATTERY)->InsertGraphicsTile(grid.GetInt(), static_cast<eTexture>(textureEnum.GetInt()));
                }
            }
        }
    }

    mTileDocument.SetObject();
}

void Json::SaveLevelTiles(int grid, Tile* tiles, eTileType type, const std::string& file, const std::string& path)
{
    TilesToDocument(grid, tiles, type, path);
}

Document Json::LoadTilesDocumentFromJson(eTileType type, const std::string& file)
{
    std::string fileName(mFilePath);
    fileName.append(file);

    if (type == eTileType::PHYSICS)
    {
        fileName.append("PhysicsTiles.json");
    }
    else // type == eTileType:::GRAPHICS
    {
        fileName.append("GraphicsTiles.json");
    }

    FILE* filePointer = nullptr;

    /* r+
    read/update: Open a file for update (both for input and output).
    The file must exist. */
    errno_t error = fopen_s(&filePointer, fileName.c_str(), "r");

    if (error == 0)
    {
        char           readBuffer[MAX_BUFFER_SIZE];
        FileReadStream inStream(filePointer, readBuffer, sizeof(readBuffer));

        Document tileDocument;
        tileDocument.ParseStream(inStream);

        fclose(filePointer);

        return tileDocument;
    }
    else
    {
        std::cout << "Failed to open file. [LoadTilesDocumentFromJson]" << std::endl;
        std::cout << std::filesystem::absolute(fileName) << std::endl;

        return NULL;
    }

    return NULL;
}

void Json::LoadObjectFromJson(const std::string& file, const std::string& path)
{
    mObjectDocument = LoadObjectDocumentFromJson(file, path);

    for (auto& documentObject : mObjectDocument.GetObject())
    {
        Value& objectTree = documentObject.value;

        Object* object = new Object("object", eObjectType::NONE);

        ValueToLevel(objectTree);
        ValueToTransform(object, objectTree);
        ValueToObjectMember(object, objectTree);
        ValueToSprite(object, objectTree);
        ValueToCollision(object, objectTree);

        if (object->GetComponentByType<Sprite>(eComponentType::SPRITE) != nullptr)
        {
            if (object->GetComponentByType<Sprite>(eComponentType::SPRITE)->GetTextureEnum() == eTexture::BATTERY)
            {
                ValueToBattery(object, objectTree);
            }
        }

        switch (object->GetObjectType())
        {
            case eObjectType::TRIGGER:
            {
                ValueToTrigger(object, objectTree);
                break;
            }
            case eObjectType::BUTTON:
            {
                ValueToButton(object, objectTree);
                break;
            }
            case eObjectType::PRESSURE:
            {
                ValueToPressure(object, objectTree);
                break;
            }
            case eObjectType::FAN:
            {
                ValueToFan(object, objectTree);
                break;
            }
            case eObjectType::MOVING_PLATFORM:
            {
                ValueToMovingPlatform(object, objectTree);
                break;
            }
            case eObjectType::OBSTACLE:
            {
                object->AddComponent(new Obstacle());

                break;
            }
            case eObjectType::DOOR:
            {
                object->AddComponent(new Door());
                break;
            }
            case eObjectType::PLAYER_1:
            case eObjectType::PLAYER_2:
            {
                ValueToPlayer(object, objectTree);
                break;
            }
        }

        /* When the current loading object is already exist in object vector. */
        if (OBJECT_MANAGER->FindObjectByName(object->GetName()) != nullptr)
        {
            Object* setUpObject = OBJECT_MANAGER->FindObjectByName(object->GetName());

            setUpObject->SetIsActive(object->GetIsActive());
            setUpObject->SetObjectType(object->GetObjectType());
            setUpObject->SetDepth(object->GetDepth());

            if (object->GetComponentByType<Transform>(eComponentType::TRANSFORM) != nullptr)
            {
                if (setUpObject->GetComponentByType<Transform>(eComponentType::TRANSFORM) == nullptr)
                {
                    setUpObject->AddComponent(new Transform());
                }

                setUpObject->SetSpawnPosition(object->GetPosition());
                setUpObject->SetPosition(object->GetPosition());
                setUpObject->SetScale(object->GetScale());
                setUpObject->SetRotation(object->GetRotation());
            }

            if (object->GetComponentByType<Sprite>(eComponentType::SPRITE) != nullptr)
            {
                if (setUpObject->GetComponentByType<Sprite>(eComponentType::SPRITE) == nullptr)
                {
                    setUpObject->AddComponent(new Sprite());
                }

                Sprite* spritePointer = setUpObject->GetSprite();

                spritePointer->LoadTextureFromData(object->GetSprite()->GetTextureEnum());
                spritePointer->SetFlip(object->GetSprite()->GetIsFlip());
                spritePointer->SetAnimation(object->GetSprite()->IsAnimation());
                spritePointer->SetAnimationFrame(object->GetSprite()->GetAnimationFrameNum());
                spritePointer->SetAnimationSpeed(object->GetSprite()->GetAnimationSpeed());
                spritePointer->SetColor(object->GetSprite()->GetColor());
            }

            if (object->GetComponentByType<Collision>(eComponentType::COLLISION) != nullptr)
            {
                if (setUpObject->GetComponentByType<Collision>(eComponentType::COLLISION) == nullptr)
                {
                    setUpObject->AddComponent(new Collision());
                }

                Collision* collisionPointer = setUpObject->GetComponentByType<Collision>(eComponentType::COLLISION);

                setUpObject->SetVelocity(Vector2<float>(0, 0));
                collisionPointer->SetIsGhost(object->GetComponentByType<Collision>(eComponentType::COLLISION)->GetIsGhost());
            }

            switch (object->GetObjectType())
            {
                case eObjectType::FAN:
                {
                    if (setUpObject->GetComponentByType<Fan>(eComponentType::FAN) == nullptr)
                    {
                        bool isFanActive = object->GetComponentByType<Fan>(eComponentType::FAN)->GetIsFanActive();
                        setUpObject->AddComponent(new Fan(isFanActive, object->GetComponentByType<Fan>(eComponentType::FAN)->GetFanType()));
                    }
                    break;
                }
                case eObjectType::PRESSURE:
                {
                    if (setUpObject->GetComponentByType<Pressure>(eComponentType::PRESSURE) == nullptr)
                    {
                        setUpObject->AddComponent(new Pressure());
                    }

                    setUpObject->GetComponentByType<Pressure>(eComponentType::PRESSURE)
                        ->SetPressureType(static_cast<Pressure::ePressureType>(object->GetComponentByType<Pressure>(eComponentType::PRESSURE)->GetPressureType()));
                    setUpObject->GetComponentByType<Pressure>(eComponentType::PRESSURE)->SetIsPressureActive(object->GetComponentByType<Pressure>(eComponentType::PRESSURE)->GetIsPressureActive());
                    break;
                }
                case eObjectType::OBSTACLE:
                {
                    if (setUpObject->GetComponentByType<Obstacle>(eComponentType::OBSTACLE) == nullptr)
                    {
                        setUpObject->AddComponent(new Obstacle());
                    }
                    break;
                }
                case eObjectType::MOVING_PLATFORM:
                {
                    if (setUpObject->GetComponentByType<MovingPlatform>(eComponentType::MOVINGPLATFORM) == nullptr)
                    {
                        MovingPlatform* movingPointer = object->GetComponentByType<MovingPlatform>(eComponentType::MOVINGPLATFORM);
                        bool            isActive      = movingPointer->GetIsMovingPlatformActive();
                        setUpObject->AddComponent(new MovingPlatform(isActive, movingPointer->GetMoveWay(), movingPointer->GetPosition().x, movingPointer->GetPosition().y));
                    }
                    break;
                }
                case eObjectType::PLAYER_1:
                case eObjectType::PLAYER_2:
                {
                    if (setUpObject->GetComponentByType<Player>(eComponentType::PLAYER) != nullptr)
                    {
                        Player*              playerPointer = object->GetComponentByType<Player>(eComponentType::PLAYER);
                        eControllerDirection direction     = playerPointer->GetContollerDirection();

                        setUpObject->GetComponentByType<Player>(eComponentType::PLAYER)->SetControllerDirection(direction);
                    }
                    else
                    {
                        setUpObject->AddComponent(new Player());
                        Player*              playerPointer = object->GetComponentByType<Player>(eComponentType::PLAYER);
                        eControllerDirection direction     = playerPointer->GetContollerDirection();

                        setUpObject->GetComponentByType<Player>(eComponentType::PLAYER)->SetControllerDirection(direction);
                    }

                    setUpObject->GetSprite()->SetColor(Vector4<float>{1.f, 1.f, 1.f, 0.7f});
                    break;
                }
                case eObjectType::PLAYER_COMBINED:
                {
                    setUpObject->GetSprite()->SetColor(Vector4<float>{1.f, 1.f, 1.f, 0.7f});
                    setUpObject->SetPosition(Vector2<float>{-5000.f, 5000.f});
                    setUpObject->SetSpawnPosition(Vector2<float>{-5000.f, 5000.f});
                    break;
                }
            }
        }

        /* if object name is not "object", it is level setting. And do not add set up objects. */
        if (object->GetName() != "object")
        {
            if (OBJECT_MANAGER->FindObjectByName(object->GetName()) == nullptr)
            {
                OBJECT_MANAGER->AddObject(object);
            }
        }
    }

    mObjectDocument.SetObject();
}