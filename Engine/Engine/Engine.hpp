/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : Engine.hpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Haewon Shon
Secondary :
- End Header ----------------------------------------------------------------*/

#pragma once
#include <vector>

class System;

class Engine
{
public:
    Engine();
    ~Engine();

    void AddSystem(System* system);
    void DestroyAllSystems();
    void Initialize();
    void Update();
    void Quit();

private:
    friend class Application;

    std::vector<System*> mEngineSystems;
    bool                 mIsGameRunning;
    float                mDeltaTime = 0.0f;
};

extern Engine* ENGINE;