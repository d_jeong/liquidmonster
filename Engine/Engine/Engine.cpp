/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : Engine.cpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Haewon Shon
Secondary :
- End Header ----------------------------------------------------------------*/

#include "Engine.hpp"
#include "../Application/Application.hpp"
#include "../Engine/FMOD/FmodManager.hpp"
#include "../Engine/Json/Json.hpp"
#include "../Engine/Json/LevelJson.hpp"
#include "../Graphics/Graphics.hpp"
#include "../ImGui/ImGui_manager.hpp"
#include "../Object/ObjectManager/ObjectManager.hpp"
#include "../Physics/Physics.hpp"
#include "../TileMap/TileMap.hpp"
#include "../Tool/Timer/Timer.hpp"
#include <SDL.h>
#include <iostream>

#include "../Object/Object.hpp"
#include "../Game/State/LevelSelect/LevelSelect.hpp"

Engine* ENGINE = nullptr;

Engine::Engine()
{
    ENGINE = this;
}

Engine::~Engine()
{
    ENGINE = nullptr;
}

void Engine::AddSystem(System* system)
{
    mEngineSystems.push_back(system);
}

void Engine::DestroyAllSystems()
{
    // Remove system in reverse for order in engineSystems.
    for (auto allSystems = mEngineSystems.rbegin(); allSystems != mEngineSystems.rend(); ++allSystems)
    {
        delete *allSystems;
    }

    LEVEL_SELECT->ClearIsAvailable();
    FMOD_MANAGER->Delete();
    STATE_MANAGER->Delete();
    TILE_MAP->Delete();
}

void Engine::Initialize()
{
    // Adding system order is important.
    AddSystem(new Application());
    AddSystem(new Graphics());
    AddSystem(new ObjectManager());
    AddSystem(new Physics());
    
    for (auto allSystems : mEngineSystems)
    {
        allSystems->Initialize();
    }

    mIsGameRunning = false;
    
    FMOD_MANAGER->Initialize();
    STATE_MANAGER->Initialize();
    TILE_MAP->Initialize();
    JSON->Initialize();
    TIMER->Initialize();
    
    mIsGameRunning = true;
}

void Engine::Update()
{
    SDL_ShowCursor(false);

    while (mIsGameRunning)
    {
        TIMER->ResetTimeStamp();
        mDeltaTime = TIMER->GetDeltaTime();
        mDeltaTime = (mDeltaTime > 0.033333f) ? 0.033333f : mDeltaTime;
        FMOD_MANAGER->Update();

        for (auto allSystems : mEngineSystems)
        {
            if (!mIsGameRunning)
            {
                return;
            }
            allSystems->Update(mDeltaTime);
        }

        IMGUI->Update();
    }
}

void Engine::Quit()
{
    mIsGameRunning = false;
}