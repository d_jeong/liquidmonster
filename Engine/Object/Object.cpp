/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : Object.cpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Daeun Jeong
Secondary :
- End Header ----------------------------------------------------------------*/

#include "Object.hpp"
#include "../Component/Respawn/Respawn.hpp"
#include "../Component/Sprite/Sprite.hpp"
#include "../Component/Transform/Transform.hpp"
#include "CustomPhysics/CustomPhysics.hpp"
#include "ObjectDepth.hpp"
#include <iostream>
Object::Object()
{
    mDepth         = GAME_OBJECT;
    mIsActive      = true;
    mCustomPhysics = new CustomPhysics();
}

Object::Object(ObjectName name, eObjectType objectType)
{
    mName          = name;
    mObjectType    = objectType;
    mDepth         = GAME_OBJECT;
    mIsActive      = true;
    mCustomPhysics = new CustomPhysics();
    if (objectType == eObjectType::WOOD_BOX || objectType == eObjectType::EATABLE || objectType == eObjectType::BATTERY)
    {
        AddComponent(new Respawn());
    }
}

Object::~Object()
{
    if (mCustomPhysics)
    {
        delete mCustomPhysics;
    }

    for (auto component : mComponents)
    {
        delete component;
    }
}

void Object::Initialize()
{
    if (mTransform != nullptr)
    {
        mTransform->Initialize(this);
    }

    if (mSprite != nullptr)
    {
        mSprite->Initialize(this);
    }
}

std::vector<Component*>& Object::GetAllComponents(void)
{
    return mComponents;
}

bool Object::ImGuiAddComponent(Component* pComponent)
{
    if (IsGetComponent(pComponent->GetType()))
    {
        delete pComponent;
        return false;
    }

    pComponent->SetOwner(this);
    mComponents.push_back(pComponent);
    pComponent->Initialize(this);
    return true;
}

void Object::AddComponent(Component* pComponent)
{
    pComponent->SetOwner(this);

    if (pComponent->GetType() == eComponentType::SPRITE)
    {
        this->mSprite = (Sprite*)pComponent;
    }
    else if (pComponent->GetType() == eComponentType::TRANSFORM)
    {
        this->mTransform = (Transform*)pComponent;
    }

    mComponents.push_back(pComponent);
}

void Object::DeleteComponent(Component* pComponent)
{
    for (int i = 0; i < mComponents.size(); ++i)
    {
        if (mComponents[i] == pComponent)
        {
            mComponents.erase(mComponents.begin() + i);
            break;
        }
    }
}

bool Object::IsGetComponent(eComponentType type)
{
    for (auto component : mComponents)
    {
        if (component->GetType() == type)
        {
            return true;
        }
    }
    return false;
}

std::string Object::GetName(void) const
{
    return mName;
}

void Object::SetName(const std::string& name)
{
    mName = name;
}

eObjectType Object::GetObjectType(void) const
{
    return mObjectType;
}

void Object::SetObjectType(eObjectType type)
{
    mObjectType = type;

    if (type == eObjectType::WOOD_BOX || type == eObjectType::EATABLE || type == eObjectType::BATTERY)
    {
        if (this->GetComponentByType<Respawn>(eComponentType::RESPAWN) == nullptr)
        {
            AddComponent(new Respawn());
        }
    }
}

Transform* Object::GetTransform(void) const
{
    return mTransform;
}

void Object::SetParent(Transform* transform)
{
    mTransform->SetParentTransform(transform);
}

Sprite* Object::GetSprite(void) const
{
    return mSprite;
}

void Object::SetSprite(Sprite newSprite)
{
    mSprite = &newSprite;
}


CustomPhysics* Object::GetCustomPhysics(void) const
{
    return mCustomPhysics;
}

Vector2<float> Object::GetPosition(void) const
{
    return mTransform->GetPosition();
}

void Object::SetPosition(Vector2<float> position)
{
    mTransform->SetPosition(position);
}

Vector2<float> Object::GetSpawnPosition(void) const
{
    return mSpawnPosition;
}

void Object::SetSpawnPosition(Vector2<float> translation)
{
    mSpawnPosition = translation;
}


Vector2<float> Object::GetScale(void) const
{
    return mTransform->GetScale();
}

void Object::SetScale(Vector2<float> scale)
{
    mTransform->SetScale(scale);
}

Vector2<float> Object::GetVelocity(void) const
{
    return mCustomPhysics->GetVelocity();
}

void Object::AddVelocity(Vector2<float> velocity)
{
    mCustomPhysics->AddVelocity(velocity);
}

void Object::SetVelocity(Vector2<float> velocity)
{
    mCustomPhysics->SetVelocity(velocity);
}


float Object::GetMass(void) const
{
    return mCustomPhysics->GetMass();
}

void Object::SetMass(float mass)
{
    mCustomPhysics->SetMass(mass);
}

float Object::GetDepth(void) const
{
    return mDepth;
}

void Object::SetDepth(float depth)
{
    mDepth = depth;
}

void Object::SetDead(void)
{
    mIsDead = true;
}

bool Object::GetIsDead(void)
{
    return mIsDead;
}

float Object::GetRotation(void) const
{
    return mTransform->GetRotation();
}

void Object::SetRotation(float angle)
{
    mTransform->SetRotation(angle);
}

bool Object::GetIsActive(void)
{
    return mIsActive;
}

void Object::SetIsActive(bool isActive)
{
    mIsActive = isActive;

    if (!isActive)
    {
        SetPosition({-500.f, -500.f});
    }
}