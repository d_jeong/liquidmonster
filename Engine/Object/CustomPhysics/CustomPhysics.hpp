/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : CustomPhysics.hpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Yewon Lim
Secondary :
- End Header ----------------------------------------------------------------*/

#pragma once
#include "../../MathLibrary/Vector2.hpp"
#include "../Object.hpp"

class CustomPhysics
{
    friend class Object;
public:
    CustomPhysics();

    void  SetMass(float mass);
    float GetMass(void) const;

    void           SetVelocity(Vector2<float> velocity);
    void           AddVelocity(Vector2<float> velocity);
    Vector2<float> GetVelocity(void) const;

    void AddForce(Vector2<float> force);

private:
    Vector2<float>  mVelocity;
    float           mMass;
};