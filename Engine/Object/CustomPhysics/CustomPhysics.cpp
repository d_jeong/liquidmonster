/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : CustomPhysics.cpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Yewon Lim
Secondary :
- End Header ----------------------------------------------------------------*/

#include "CustomPhysics.hpp"

CustomPhysics::CustomPhysics()
{
    mMass             = 1.f;
}
void CustomPhysics::SetMass(float mass)
{
    mMass = mass;
}

float CustomPhysics::GetMass(void) const
{
    return mMass;
}

void CustomPhysics::SetVelocity(Vector2<float> velocity)
{
    mVelocity = velocity;
}

void CustomPhysics::AddVelocity(Vector2<float> velocity)
{
    mVelocity += velocity;
}

Vector2<float> CustomPhysics::GetVelocity(void) const
{
    return mVelocity;
}

void CustomPhysics::AddForce(Vector2<float> force)
{
    mVelocity += force / mMass;
}