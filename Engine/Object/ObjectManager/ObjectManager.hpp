/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : ObjectManager.hpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Daeun Jeong
Secondary :
- End Header ----------------------------------------------------------------*/

#pragma once

#include "../../System.hpp"
#include <string>
#include <vector>

class Object;

using ObjectName = std::string;

class ObjectManager : public System
{
public:
    ObjectManager();
    ~ObjectManager();

    void Initialize() override;
    void Update(float dt) override;

    void    AddObject(Object* object);
    void    AddPauseObject(Object* object);
    Object* FindObjectByName(ObjectName name);

    void ClearObjects();
    void ClearPauseObjects();
    void DeleteObject(Object* object);
    void DeletePauseObject(Object* object);

    std::vector<Object*> GetObjectVector(void);
    std::vector<Object*> GetPauseObjectVector(void);

    int GetNumberOfObjects(void);

    void ReplaceTextureWithFileName(std::string fileName);

private:
    std::vector<Object*> mObjectVector;
    std::vector<Object*> mPauseObjectVector;
};

extern ObjectManager* OBJECT_MANAGER;