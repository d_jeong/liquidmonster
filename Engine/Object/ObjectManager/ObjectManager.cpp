/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : ObjectManager.cpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Daeun Jeong
Secondary :
- End Header ----------------------------------------------------------------*/

#include "ObjectManager.hpp"
#include "../../Component/Collision/Collision.hpp"
#include "../../Component/Component.hpp"
#include "../../Component/Sprite/Sprite.hpp"
#include "../../Input/Input.hpp"
#include "../Engine/Graphics/Graphics.hpp"
#include "../Engine/Physics/Physics.hpp"
#include "../Game/State/StateManager/StateManager.hpp"
#include "../Object.hpp"
#include "../ObjectType.hpp"
#include <algorithm>
#include <iostream>

ObjectManager* OBJECT_MANAGER = nullptr;

ObjectManager::ObjectManager()
{
    OBJECT_MANAGER = this;
}

ObjectManager::~ObjectManager()
{
    ClearObjects();
    ClearPauseObjects();

    OBJECT_MANAGER = nullptr;
}

void ObjectManager::Initialize(void)
{
    for (const auto object : mObjectVector)
    {
        if (object != nullptr)
        {
            std::vector<Component*> allComponents = object->GetAllComponents();

            for (const auto& component : allComponents)
            {
                if (component != nullptr)
                {
                    component->Initialize(object);
                }
            }
        }
    }
}

void ObjectManager::Update(float dt)
{
    if (STATE_MANAGER->IsGameStopped())
    {
        return;
    }

    for (const auto object : mObjectVector)
    {
        if (object != nullptr)
        {
            if (object->GetIsActive() == true)
            {
                std::vector<Component*> allComponents = object->GetAllComponents();

                for (const auto& component : allComponents)
                {
                    if (component != nullptr)
                    {
                        component->Update(dt);
                    }
                }
            }
        }
    }
}

void ObjectManager::AddObject(Object* object)
{
    mObjectVector.push_back(object);
    std::sort(begin(mObjectVector), end(mObjectVector), [](Object* obj1, Object* obj2) { return obj1->GetDepth() > obj2->GetDepth(); });
}

void ObjectManager::AddPauseObject(Object* object)
{
    mPauseObjectVector.push_back(object);
}

Object* ObjectManager::FindObjectByName(ObjectName name)
{
    for (auto object : mObjectVector)
    {
        if (object->GetName() == name)
        {
            return object;
        }
    }

    return nullptr;
}

void ObjectManager::ClearObjects()
{
    for (auto object : mObjectVector)
    {
        for (auto comp : object->GetAllComponents())
        {
            comp->Delete();
            delete comp;
        }

        object->GetAllComponents().clear();
        delete object;
    }

    if (PHYSICS != nullptr)
    {
        PHYSICS->ClearCollisionList();
    }

    mObjectVector.clear();
}

void ObjectManager::ClearPauseObjects()
{
    for (auto object : mPauseObjectVector)
    {
        GRAPHICS->DeleteSprite(object);
        for (auto comp : object->GetAllComponents())
        {
            delete comp;
        }

        object->GetAllComponents().clear();
        delete object;
    }

    mPauseObjectVector.clear();
}

void ObjectManager::DeleteObject(Object* object)
{
    bool isObjectExist = false;
    // GRAPHICS->DeleteSprite(object);
    PHYSICS->DeleteObjectFromList(object);

    int i = 0;
    for (i = 0; i < mObjectVector.size(); ++i)
    {
        if (mObjectVector[i] == object)
        {
            isObjectExist = true;
            break;
        }
    }
    if (isObjectExist)
    {
        for (auto comp : object->GetAllComponents())
        {
            comp->Delete();
            delete comp;
        }

        object->GetAllComponents().clear();
        delete object;
        mObjectVector.erase(mObjectVector.begin() + i);
    }
}

void ObjectManager::DeletePauseObject(Object* object)
{
    bool isObjectExist = false;
    int i = 0;
    for (i = 0; i < mPauseObjectVector.size(); ++i)
    {
        if (mPauseObjectVector[i] == object)
        {
            isObjectExist = true;
            break;
        }
    }
    if (isObjectExist)
    {
        for (auto comp : object->GetAllComponents())
        {
            comp->Delete();
            delete comp;
        }

        object->GetAllComponents().clear();
        delete object;
        mPauseObjectVector.erase(mPauseObjectVector.begin() + i);
    }
}

std::vector<Object*> ObjectManager::GetObjectVector(void)
{
    return mObjectVector;
}

std::vector<Object*> ObjectManager::GetPauseObjectVector(void)
{
    return mPauseObjectVector;
}

int ObjectManager::GetNumberOfObjects(void)
{
    int numberOfObjects = 0;

    for (const auto object : mObjectVector)
    {
        if (object != nullptr)
        {
            ++numberOfObjects;
        }
    }

    return numberOfObjects;
}

void ObjectManager::ReplaceTextureWithFileName(std::string fileName)
{
    std::cout << "REPLACE obj with " << fileName << std::endl;
    Vector2<float> mousePos = Input::GetMousePosition();

    Vector3<float> calculatedPos = {mousePos.x, mousePos.y, 1.f};
    calculatedPos                = GRAPHICS->GetInverseCameraMatrix() * calculatedPos;

    for (auto obj : mObjectVector)
    {
        Collision* objCollision = obj->GetComponentByType<Collision>(eComponentType::COLLISION);
        if (objCollision != nullptr)
        {
            if (objCollision->IsCollidingWithPoint({calculatedPos.x, calculatedPos.y}))
            {
                obj->GetSprite()->LoadTexture(fileName.c_str());
                break;
            }
        }
    }
}