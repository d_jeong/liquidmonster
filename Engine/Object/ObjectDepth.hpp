/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : ObjectDepth.hpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Daeun Jeong
Secondary :
- End Header ----------------------------------------------------------------*/

#pragma once

// From -1.0f(front) to +1.0f(back)

// (temp) 1(front) to 0(back)

const float UI                = 1.1f;
const float GAME_UI           = 1.0f;
const float GAME_OBJECT       = 0.5f;
const float GAME_PLAYER       = 0.4f;
const float GAME_EATEN_OBJECT = 0.3f;
const float GAME_DOOR         = 0.2f;
const float GAME_BACKGROUND   = 0.1f;