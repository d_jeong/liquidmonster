/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : ObjectType.hpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Daeun Jeong
Secondary :
- End Header ----------------------------------------------------------------*/

#pragma once

enum class eObjectType
{
    NONE = 0,

    PLAYER_1, //1
    PLAYER_2, //2
    PLAYER_COMBINED, //3
    ARROW_1, //4
    ARROW_2, //5

    OBSTACLE, //6
    STEEL_BOX, //7
    WOOD_BOX, //8
    DOOR, //9
    FAN, //10
     
    TRIGGER, //11
    MOVING_PLATFORM, //12
    PRESSURE, //13
    BUTTON, //14
    BATTERY, //15

    STATIC, //16
    DYNAMIC, //17 
    EATABLE, //18

    BACKGROUND,
    GAME_LOGO,
    TEAM_LOGO,
    FMOD_LOGO,
    START,
    CREDIT,
    OPTION,
    QUIT,
    PRESS_SPACE,
    HOW_TO_PLAY,

    HOW_OPEN_DOOR,//29
    BATTERY_MOVING,       // 30
    BATTERY_WIND,         // 31
    BATTERY_PRESSURE,     // 32
    BOX_LASER,            // 33
    BOX_MOVING,           // 34
    HOW_MOVING,           // 35
    PRESSURE_DEAD,        // 36
    PRESSURE_PASS,        // 37
    COMBINE_BOX_PUSH,     // 38
    BOX_REMOVE_LASER,     // 39
    COMBINE_NO_JUMP,      // 40
    LASER_DANGER,         // 41
    OBSTACLE_DANGER,      // 42
    SLIME_EAT_RANGE,      // 43
    SMALL_SLIME_NO_PUSH,  // 44
    SMALL_SLIME_PUSH,     // 45
    SLIME_BATTERY_REMOVE, // 46
    SLIME_LASER_OFF,      // 47
    SWITCH_MOVING,        // 48
    SWITCH_WIND,          // 49
    TILE_REMOVE,          // 50
    WIND_RIDE,            // 51

    LASER_ANIMATION,      //52

    COUNT
};