/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : Object.hpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Daeun Jeong
Secondary :
- End Header ----------------------------------------------------------------*/

#pragma once

#include "../Component/Component.hpp"
#include "../MathLibrary/MathLibrary.hpp"
#include "ObjectType.hpp"
#include <string>
#include <vector>

using ObjectName = std::string;

enum class eComponentType;
enum class eObjectType;
class Transform;
class Sprite;
class CustomPhysics;

class Object
{
public:
    Object();
    Object(ObjectName name, eObjectType objectType);
    ~Object();

    void Initialize(void);

    template <typename T> T* GetComponentByType(eComponentType type);

    std::vector<Component*>& GetAllComponents(void);

    bool ImGuiAddComponent(Component* pComponent);
    void AddComponent(Component* pComponent);
    void DeleteComponent(Component* pComponent);
    bool IsGetComponent(eComponentType type);

    std::string GetName(void) const;
    void        SetName(const std::string& name);

    eObjectType GetObjectType(void) const;
    void        SetObjectType(eObjectType type);

    Transform* GetTransform(void) const;
    void       SetParent(Transform* transform);

    Sprite* GetSprite(void) const;
    void    SetSprite(Sprite sprite);

    CustomPhysics* GetCustomPhysics(void) const;

    Vector2<float> GetPosition(void) const;
    void           SetPosition(Vector2<float> translation);

    Vector2<float> GetSpawnPosition(void) const;
    void           SetSpawnPosition(Vector2<float> translation);

    Vector2<float> GetScale(void) const;
    void           SetScale(Vector2<float> scale);

    Vector2<float> GetVelocity(void) const;
    void           AddVelocity(Vector2<float> velocity);
    void           SetVelocity(Vector2<float> velocity);

    float GetMass(void) const;
    void  SetMass(float mass);

    float GetDepth(void) const;
    void  SetDepth(float detph);

    void SetDead(void);
    bool GetIsDead(void);

    float GetRotation() const;
    void  SetRotation(float angle);

    bool GetIsActive(void);
    void SetIsActive(bool isActive);

private:
    bool  mIsDead;
    bool  mIsActive;
    float mDepth;

    ObjectName  mName       = "";
    eObjectType mObjectType = eObjectType::NONE;

    Transform*              mTransform;
    Sprite*                 mSprite;
    Vector2<float>          mSpawnPosition;
    std::vector<Component*> mComponents;

    CustomPhysics* mCustomPhysics;
};

template <typename T> T* Object::GetComponentByType(eComponentType type)
{
    for (auto component : mComponents)
    {
        if (component->GetType() == type)
        {
            return dynamic_cast<T*>(component);
        }
    }

    return nullptr;
}