/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : Data.cpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Sejeong Park
Secondary :
- End Header ----------------------------------------------------------------*/

#include "Data.hpp"
#include "../ImGui/ImGui_manager.hpp"

Data::Data()
{
    mTextureFileName = "All_Sprite.dat";
    mLogoFileName    = "All_Logos.dat";
    mTextureEnum     = 0;
    mIsDataLoadDone  = false;
}

Data::~Data()
{
    for (auto iterator : mAllTexture)
    {
        delete iterator.second;
    }
    workerThread[0].join();
    mAllTexture.clear();
}

void Data::Initialize_Logos()
{
    std::ifstream sprite_dat(mLogoFileName.c_str());
    std::string   line;
    if (sprite_dat.is_open())
    {
        while (std::getline(sprite_dat, line))
        {
            size_t index = line.find(delimiter);
            // std::string sprite_name = line.substr(0, index);
            eTexture sprite_name = static_cast<eTexture>(mTextureEnum);

            std::string sprite_path = line.substr(index + 1, -1);
            Texture *   texture     = new Texture;
            texture->Initialize(sprite_path.c_str());

            mAllTexture.insert(std::make_pair(sprite_name, texture));
            ++mTextureEnum;
        }
    }
    sprite_dat.close();
}

void Data::Initialize_Texture()
{
    std::ifstream sprite_dat(mTextureFileName.c_str());
    std::string   line;
    if (sprite_dat.is_open())
    {
        while (std::getline(sprite_dat, line))
        {
            size_t index = line.find(delimiter);
            // std::string sprite_name = line.substr(0, index);
            eTexture sprite_name = static_cast<eTexture>(mTextureEnum);

            std::string sprite_path = line.substr(index + 1, -1);
            mLoadQueue.push({sprite_name, sprite_path});
            // Texture *   texture     = new Texture;
            // texture->Initialize(sprite_path.c_str());

            // mAllTexture.insert(std::make_pair(sprite_name, texture));
            ++mTextureEnum;
        }
    }
    sprite_dat.close();

    workerThread.push_back(std::thread{&Data::LoaderThread, this});
}

void Data::UpdateTextureLoad()
{
    if (!mTextureQueue.empty())
    {
        eTexture textureEnum = mTextureQueue.front().first;
        Texture *texture     = mTextureQueue.front().second;

        texture->InitializeWithData();
        mAllTexture.insert(std::make_pair(textureEnum, texture));
        mTextureQueue.pop();
    }
    else if (mTextureQueue.empty() && mLoadQueue.empty())
    {
        mIsDataLoadDone = true;
#ifdef _DEBUG
        IMGUI->Initialize();
#endif
    }
}

void Data::LoaderThread()
{
    while (!mIsDataLoadDone)
    {
        if (!mLoadQueue.empty())
        {
            eTexture    textureEnum = mLoadQueue.front().first;
            std::string texturePath = mLoadQueue.front().second;

            Texture *texture = new Texture;
            texture->LoadPixelData(texturePath);

            mLoadQueue.pop();
            mTextureQueue.push({textureEnum, texture});
        }
    }
}

std::map<eTexture, Texture *> Data::GetAllTexture()
{
    return mAllTexture;
}

Texture *Data::FindTextureWithEnum(eTexture texture)
{
    return mAllTexture[texture];
}

bool Data::IsDataLoadDone() const
{
    return mIsDataLoadDone;
}

int Data::GetLoadingProcess()
{
    return (100 * mAllTexture.size()) / static_cast<int>(eTexture::PARTICLE);
}