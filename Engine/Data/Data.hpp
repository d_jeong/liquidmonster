/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : Data.hpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Sejeong Park
Secondary :
- End Header ----------------------------------------------------------------*/

#pragma once
#include "../Graphics/Texture/Texture.hpp"
#include <fstream>
#include <iostream>
#include <map>
#include <queue>
#include <string>
#include <thread>
#include <vector>

enum class eTexture
{
    DP_LOGO = 0,
    GAME_LOGO,     // 1
    TEAM_LOGO,     // 2
    FMOD_LOGO,     // 3
    LOADING_SCENE, // 4

    PLAYER_1_ANIMATION,               // 5
    PLAYER_1_EATING_ANIMATION,        // 6
    ARROW_1,                          // 7
    PLAYER_1_SPIT,                    // 8
    PLAYER_1_SPAWN_ANIMATION,         // 9
    PLAYER_1_PARTICLE,                // 10
    PLAYER_2_ANIMATION,               // 11
    PLAYER_2_EATING_ANIMATION,        // 12
    ARROW_2,                          // 13
    PLAYER_2_SPIT,                    // 14
    PLAYER_2_SPAWN_ANIMATION,         // 15
    PLAYER_2_PARTICLE,                // 16
    PLAYER_COMBINED_ANIMATION,        // 17
    PLAYER_COMBINED_EATING_ANIMATION, // 18
    PLAYER_COMBINED_SPIT,             // 19
    PLAYER_COMBINED_PARTICLE,         // 20

    DOOR,        // 21
    WOOD_BOX,    // 22
    STEEL_BOX,   // 23
    KEY,         // 24
    FAN,         // 25
    BOUNDARY,    // 26
    PLATFORM,    // 27
    OBSTACLE,    // 28
    RED_BUTTON,  // 29
    BLUE_BUTTON, // 30
    LAZOR,       // 31

    BATTERY,      // 32
    BATTERY_HOME, // 33
    WIND,         // 34
    PRESSURE,     // 35
    BACKGROUND,   // 36

    TEST_BACKGROUND,
    TILE_EXAMPLE_1,
    TILE_EXAMPLE_2,
    TILE_EXAMPLE_3,//40

    TILE_WIRE_RED_1,
    TILE_WIRE_RED_2,
    TILE_WIRE_RED_3,
    TILE_WIRE_RED_4,
    TILE_WIRE_RED_5,
    TILE_WIRE_RED_6,//46
    TILE_WIRE_BLUE_1,
    TILE_WIRE_BLUE_2,
    TILE_WIRE_BLUE_3,
    TILE_WIRE_BLUE_4,//50
    TILE_WIRE_BLUE_5,
    TILE_WIRE_BLUE_6,

    LEVEL_SELECT_CHAPTER_1,
    LEVEL_SELECT_CHAPTER_2,

    LEVEL_SELECT_1,
    LEVEL_SELECT_2,
    LEVEL_SELECT_3,
    LEVEL_SELECT_4,
    LEVEL_SELECT_5,
    LEVEL_SELECT_6,//60
    LEVEL_SELECT_7,
    LEVEL_SELECT_8,
    LEVEL_SELECT_BACK,
    LEVEL_SELECT_ARROW,

    MENU_CREDIT,
    MENU_OPTION,
    MENU_QUIT,
    MENU_PLAY,
    MENU_PAUSE,
    MENU_RESUME,//70
    MENU_MENU,
    MENU_FULLSCREEN,

    BACKGROUND_MACHINE,
    BACKGROUND_CONVEY,
    BACKGROUND_WINDOW,
    BACKGROUND_DAY_BUILDING_FRONT,
    BACKGROUND_DAY_BUILDING_BACK,
    BACKGROUND_DAY_LIGHT,
    BACKGROUND_DAY_BACKGROUND,
    BACKGROUND_SUNSET_BUILDING_FRONT,//80
    BACKGROUND_SUNSET_BUILDING_BACK,
    BACKGROUND_SUNSET_LIGHT,
    BACKGROUND_SUNSET_BACKGROUND,
    BACKGROUND_NIGHT_BUILDING_FRONT,
    BACKGROUND_NIGHT_BUILDING_BACK,
    BACKGROUND_NIGHT_LIGHT,
    BACKGROUND_NIGHT_BACKGROUND,

    MENU_PRESS_SPACE,

    OBJECT_DOOR_OPENED,
    CREDIT,//90
    CREDIT_SPACE,
    FULL_SCREEN,
    ON_BUTTON,
    OFF_BUTTON,
    SOUND_OPTION,
    SOUND_MUTE,
    SOUND_100,
    CUTSCENE_BEGIN,
    CUTSCENE_END,
    PLAYER_1_ANIMATION_CANNOT_DO,//100
    PLAYER_2_ANIMATION_CANNOT_DO,
    PLAYER_COMBINED_ANIMATION_CANNOT_DO,
    PLAYER_1_HOW_TO_PLAY,
    PLAYER_1_HOW_TO_MOVE,
    PLAYER_1_HOW_TO_JUMP,
    PLAYER_1_HOW_TO_EAT,
    PLAYER_1_HOW_TO_SPIT,
    PLAYER_2_HOW_TO_MOVE,
    PLAYER_2_HOW_TO_JUMP,
    PLAYER_2_HOW_TO_EAT,//110
    PLAYER_2_HOW_TO_SPIT,
    PRESS_SPACE_TO_BACK,
    PRESS_SPACE_TO_SKIP,
    MENU_HOW_TO_PLAY,
    R_KEY_TO_RESPAWN,
    BOX_PARTICLE,
    KEY_PARTICLE,
    BATTERY_PARTICLE,
    BLACK,
    LEVEL_SELECT_BG,//120
    LEVEL_SELECT_EDGE,
    DOOR_EXIT,
    DOOR_EXIT_OPENED,
    CREDIT_TWO,
    CREDIT_THREE,
    CREDIT_FOUR,
    CREDIT_FIVE,

    LEVEL1_1,
    LEVEL1_2,
    LEVEL1_3,//130
    LEVEL1_4,
    LEVEL1_5,

    LEVEL2_1,
    LEVEL2_2,
    LEVEL2_3,
    LEVEL2_4,

    LEVEL3_1,
    LEVEL3_2,

    TILE_WIRE_RED_7,
    TILE_WIRE_RED_8,//140
    TILE_WIRE_RED_9,
    TILE_WIRE_RED_10,
    TILE_WIRE_BLUE_7,
    TILE_WIRE_BLUE_8,
    TILE_WIRE_BLUE_9,
    TILE_WIRE_BLUE_10,

    STAFF_ONLY,
    BACK,
    PLAYER_2_HOW_TO_PLAY,
    HOW_OPEN_DOOR,//150
    BATTERY_MOVING,//151
    BATTERY_WIND,//152
    BATTERY_PRESSURE,//153
    BOX_LASER,//154
    BOX_MOVING,//155
    HOW_MOVING,//156
    PRESSURE_DEAD,//157
    PRESSURE_PASS,//158
    COMBINE_BOX_PUSH,//159
    BOX_REMOVE_LASER,//160
    COMBINE_NO_JUMP,//161
    LASER_DANGER,//162
    OBSTACLE_DANGER,//163
    SLIME_EAT_RANGE,//164
    SMALL_SLIME_NO_PUSH,//165
    SMALL_SLIME_PUSH,//166
    SLIME_BATTERY_REMOVE,//167
    SLIME_LASER_OFF,//168
    SWITCH_MOVING,//169
    SWITCH_WIND,//170
    TILE_REMOVE,//171
    WIND_RIDE,//172
    DATA_RESET,
    OPTION_LOGO,

    DESTRUCTIVE_QUIT,
    DESTURCTIVE_DATA_RESET,
    DESTRUCTIVE_CANCEL,

    LASER_ANIMATION,//178

    OPTION_LANGUAGE,
    OPTION_ENGLISH,
    OPTION_KOREAN,

    K_PLAYER_1_HOW_TO_MOVE,
    K_PLAYER_1_HOW_TO_JUMP,
    K_PLAYER_1_HOW_TO_EAT,
    K_PLAYER_1_HOW_TO_SPIT,
    K_PLAYER_2_HOW_TO_MOVE,
    K_PLAYER_2_HOW_TO_JUMP,
    K_PLAYER_2_HOW_TO_EAT,
    K_PLAYER_2_HOW_TO_SPIT,
    K_BACK,
    K_CANCLE,
    K_DATA_RESET,
    K_DESTRUCTIVE_DATA,
    K_DESTRUCTIVE_QUIT,
    K_FULL_SCREEN,
    K_MENU_CREDIT,
    K_MENU_HOWTO,
    K_GO_MAIN,
    K_MENU_OPTION,
    K_MENU_PLAY,
    K_MENU_QUIT,
    K_MENU_RESUME,
    K_MUTE,
    K_OFF_BUTTON,
    K_ON_BUTTON,
    K_OPTION_LANGUAGE,
    K_OPTION_SOUND,
    K_PRESS_GO_BACK,
    K_PRESS_SKIP,
    K_R_RESTART,
    K_STAFF_ONLY,

    OBJECT_BATTERY_HOME_OFF,
    OBJECT_FAN_OFF,
    TWINCLE,

    K_CREDIT_1,
    K_CREDIT_2,
    K_CREDIT_3,
    K_CREDIT_4,
    K_CREDIT_5,

    WHITE_STAR,
    K_DEST_QUIT,
    K_OPTION_LOGO,
    K_PAUSE_LOGO,

    SOUND_25,
    SOUND_50,
    SOUND_75,

    _1P_KEY_ENG,
    _2P_KEY_ENG,
    _1P_KEY_KOR,
    _2P_KEY_KOR,

    PARTICLE,
};

const char delimiter = '\t';

class Data
{
public:
    Data();
    ~Data();

    void Initialize_Texture();
    void Initialize_Logos();
    void UpdateTextureLoad();

    std::map<eTexture, Texture*> GetAllTexture();
    Texture* FindTextureWithEnum(eTexture texture);

    bool IsDataLoadDone() const;

    int GetLoadingProcess();

private:
    void LoaderThread();

    std::string                   mTextureFileName;
    std::string                   mLogoFileName;
    std::map<eTexture, Texture*> mAllTexture;
    int                           mTextureEnum;

    std::vector<std::thread>                     workerThread;
    std::queue<std::pair<eTexture, std::string>> mLoadQueue;
    std::queue<std::pair<eTexture, Texture*>>   mTextureQueue;

    bool mIsDataLoadDone;
};