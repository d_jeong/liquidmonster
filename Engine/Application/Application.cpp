/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : Application.cpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Haewon Shon
Secondary :
- End Header ----------------------------------------------------------------*/

#include "Application.hpp"
#include "../Engine/Engine.hpp"
#include "../Graphics/Texture/Texture.hpp" // Icon texture
#include "../ImGui/ImGui_manager.hpp"
#include <glew.h>
#include <iostream>
#include <string>

Application *APPLICATION = nullptr;

Application::Application() : mWidth(1280), mHeight(720)
{
    APPLICATION       = this;
    mShouldQuit       = true;
    mIsFullscreen     = false;
    mIsVSyncOn        = true;
    mScreenshotNumber = 0;
    mFrameCount       = 0;
    mInitialFocusFlag = true;
}

Application::~Application()
{
    SDL_DestroyWindow(mWindow);
    SDL_Quit();

    mWindow     = nullptr;
    APPLICATION = nullptr;
}

void Application::Initialize()
{
    /* BUILD WINDOW */
    if (SDL_Init(SDL_INIT_VIDEO < 0))
    {
        std::cout << "[ERROR] SDL init fail" << std::endl;

        return;
    }

    glewExperimental = GL_TRUE;

    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, SDL_GL_CONTEXT_FORWARD_COMPATIBLE_FLAG);
    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 16);

    mWindow = SDL_CreateWindow("LiquidMonsters", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, mWidth, mHeight, SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE | SDL_WINDOW_INPUT_FOCUS);

    if (!mWindow)
    {
        std::cout << "[ERROR] window init fail" << std::endl;
        return;
    }

    SDL_GLContext glContext = SDL_GL_CreateContext(mWindow);

    if (glContext == NULL)
    {
        std::cout << "[ERROR] glContext init fail" << std::endl;
        return;
    }

    SDL_EventState(SDL_DROPFILE, SDL_ENABLE);
    
    /* ICON SETTING */
    SDL_Surface *icon;
    Texture *    iconTexture = new Texture();
    iconTexture->Initialize("texture/Level/icon.png");
    int iconWidth  = iconTexture->GetWidth();
    int iconHeight = iconTexture->GetHeight();
    icon           = SDL_CreateRGBSurfaceFrom((void *)iconTexture->GetPixelData(), iconWidth, iconHeight, 32, iconWidth * 4, 0xff, 0xff << 8, 0xff << 16, 0xff << 24);

    if (icon == nullptr)
    {
        std::cout << "[ERROR] failed to generate window icon" << std::endl;
    }
    else
    {
        SDL_SetWindowIcon(mWindow, icon);
    }
    SDL_FreeSurface(icon);
    delete iconTexture;

    std::cout << "APPLICATION INITIALIZED SUCCESSFULLY" << std::endl;

    SDL_GL_SetSwapInterval(1);

    /* NO FULLSCREEN IN DEBUG  */
    #ifdef _DEBUG

    #else
    ToggleFullscreen(true);
#endif // !_DEBUG

}

void Application::Update(float dt)
{
    Input::Reset();
    SDL_Event mEvent;

    while (SDL_PollEvent(&mEvent) != 0)
    {
        Input::Update(mEvent);

        if (IMGUI->IsLoadDone())
        {
            ImGui_ImplSDL2_ProcessEvent(&mEvent);
        }

        if (mEvent.type == SDL_QUIT)
        {
            STATE_MANAGER->CreateDestructiveMessage();
        }
        if (mEvent.type == SDL_WINDOWEVENT)
        {
            if (mEvent.window.event == SDL_WINDOWEVENT_RESIZED)
            {
                WindowResize(mEvent.window.data1, mEvent.window.data2);
            }
            if (mEvent.window.event == SDL_WINDOWEVENT_FOCUS_LOST)
            {
                STATE_MANAGER->PauseGame();
            }
        }
        if (mEvent.type == SDL_DROPFILE)
        {
            ReplaceTexture(std::string(mEvent.drop.file));
        }
    }

    STATE_MANAGER->Update(dt);
}

bool Application::ShouldQuit() const
{
    return mShouldQuit;
}

void Application::ToggleFullscreen(Uint32 flags)
{
    if (mIsFullscreen)
    {
        SDL_SetWindowFullscreen(mWindow, flags);//0
    }
    else
    {
        SDL_SetWindowFullscreen(mWindow, flags);//SDL_WINDOW_FULLSCREEN
    }
    mIsFullscreen = !mIsFullscreen;
}

void Application::ToggleVSync()
{
    if (mIsVSyncOn)
    {
        SDL_GL_SetSwapInterval(0);
    }
    else
    {
        SDL_GL_SetSwapInterval(1);
    }
    mIsVSyncOn = !mIsVSyncOn;
}

void Application::WindowResize(int newWidth, int newHeight)
{
    mWidth  = newWidth;
    mHeight = newHeight;
    glViewport(0, 0, newWidth, newHeight);
}

bool Application::IsUsingGamePad()
{
    for (int i = 0; i < SDL_NumJoysticks(); ++i)
    {
        if (SDL_IsGameController(i))
        {
            return true;
        }
    }
    return false;
}

float Application::GetFPS(void) const
{
    return mFps;
}

int Application::GetWidth(void) const
{
    return mWidth;
}

int Application::GetHeight(void) const
{
    return mHeight;
}

void Application::ReplaceTexture(std::string fileName)
{
    std::string filedir;

    if (fileName.find("Game") == std::string::npos)
    {
        std::cout << "[DEBUG] Invalid file directory : file must be in Game folder" << std::endl;
    }
    else
    {
        filedir = fileName.substr(fileName.find("Game") + 5);

        /* texture */
        if (filedir.find(".png") != std::string::npos)
        {
            std::cout << "[DEBUG] " << filedir << std::endl;
            OBJECT_MANAGER->ReplaceTextureWithFileName(filedir);
        }
        /* sound */
        else if (filedir.find(".mp3") != std::string::npos)
        {
            FMOD_MANAGER->StopBackgroundSound();
            FMOD_MANAGER->CreateBackgroundSound(filedir);
            FMOD_MANAGER->PlayBackgroundSound(filedir);
        }
        /* invalid file input */
        else
        {
            std::cout << "[DEBUG] Not valid file." << std::endl;
        }
    }
}

bool Application::GetIsKeyAvailable(void)
{
    return mIsKeyAvailable;
}

void Application::SetIsKeyAvailable(bool isActive)
{
    mIsKeyAvailable = isActive;
}
