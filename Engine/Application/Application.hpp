/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : Application.hpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Haewon Shon
Secondary :
- End Header ----------------------------------------------------------------*/

#pragma once

#include "../System.hpp"
#include "SDL.h"
#include <string>

class Application : public System
{
public:
    Application();
    ~Application();

    void Initialize() override;
    void Update(float dt) override;
    bool ShouldQuit() const;

    int GetWidth() const;
    int GetHeight() const;

    void ToggleFullscreen(Uint32 flags);
    void ToggleVSync();
    void WindowResize(int newWidth, int newHeight);

    bool IsUsingGamePad();

    float GetFPS(void) const;

    void ReplaceTexture(std::string fileName);

    bool GetIsKeyAvailable(void);
    void SetIsKeyAvailable(bool isActive);

private:
    SDL_Window *        mWindow   = nullptr;

    int           mWidth, mHeight;
    bool          mShouldQuit;
    bool          mIsFullscreen;
    bool          mIsVSyncOn;
    unsigned char mScreenshotNumber;
    bool mIsKeyAvailable = true;

    bool mInitialFocusFlag;

    unsigned int mFrameCount;
    float        mFps = 0.f;
};

extern Application *APPLICATION;