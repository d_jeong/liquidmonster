/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : Vector4.hpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Haewon Shon
Secondary :
- End Header ----------------------------------------------------------------*/
#pragma once

template <typename T> class Vector4
{
public:
    Vector4() : x(0), y(0), z(0), w(0)
    {
    }
    Vector4(T x, T y, T z, T w) : x(x), y(y), z(z), w(w)
    {
    }
    constexpr void Add(const Vector4 &vector) noexcept
    {
        x += vector.x;
        y += vector.y;
        z += vector.z;
        w += vector.w;
    }
    constexpr void Subtract(const Vector4 &vector) noexcept
    {
        x -= vector.x;
        y -= vector.y;
        z -= vector.z;
        w -= vector.w;
    }
    constexpr void Multiply(T value) noexcept
    {
        x *= value;
        y *= value;
        z *= value;
        w *= value;
    }

    constexpr void Divide(T value) noexcept
    {
        x /= value;
        y /= value;
        z /= value;
        w /= value;
    }
    constexpr Vector4 &operator+=(const Vector4 &vector) noexcept
    {
        this->Add(vector);
        return *this;
    }
    constexpr Vector4 &operator-=(const Vector4 &vector) noexcept
    {
        this->Subtract(vector);
        return *this;
    }

    constexpr Vector4 &operator*=(T value) noexcept
    {
        this->Multiply(value);
        return *this;
    }
    constexpr Vector4 &operator/=(T value) noexcept
    {
        this->Divide(value);
        return *this;
    }
    constexpr Vector4 operator+(const Vector4 &vector) noexcept
    {
        Vector4 result(*this);
        result.Add(vector);
        return result;
    }

    constexpr Vector4 operator-(const Vector4 &vector) noexcept
    {
        Vector4 result(*this);
        result.Subtract(vector);
        return result;
    }
    constexpr Vector4 operator*(T value) noexcept
    {
        Vector4 result(*this);
        result.Multiply(value);
        return result;
    }
    constexpr Vector4 operator/(T value) noexcept
    {
        Vector4 result(*this);
        result.Divide(value);
        return result;
    }

public:
    T x;
    T y;
    ;
    T z;
    T w;
};