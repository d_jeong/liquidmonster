/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : Matrix3x3.hpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Haewon Shon
Secondary :
- End Header ----------------------------------------------------------------*/
#pragma once
#include "Vector2.hpp"
#include "Vector3.hpp"

template <typename T> class Matrix3x3
{
public:
    constexpr Matrix3x3() noexcept
    {
        for (int i = 0; i < 3; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                elements[i][j] = 0.f;
            }
        }
        elements[0][0] = 1.f;
        elements[1][1] = 1.f;
        elements[2][2] = 1.f;
    }
    constexpr Matrix3x3(Vector3<T> column0, Vector3<T> column1, Vector3<T> column2) noexcept
    {
        columns[0] = column0;
        columns[1] = column1;
        columns[2] = column2;
    }
    constexpr Matrix3x3(T e0, T e1, T e2, T e3, T e4, T e5, T e6, T e7, T e8) noexcept
    {
        elements[0][0] = e0;
        elements[0][1] = e1;
        elements[0][2] = e2;
        elements[1][0] = e3;
        elements[1][1] = e4;
        elements[1][2] = e5;
        elements[2][0] = e6;
        elements[2][1] = e7;
        elements[2][2] = e8;
    }

public:
    union {
        Vector3<T> columns[3];
        T          elements[3][3];
    };
};

template <typename T> constexpr Matrix3x3<T> operator*(const Matrix3x3<float> &lhs, const Matrix3x3<T> &rhs) noexcept
{
    Matrix3x3<T> result;

    result.elements[0][0] = lhs.elements[0][0] * rhs.elements[0][0] + lhs.elements[1][0] * rhs.elements[0][1] + lhs.elements[2][0] * rhs.elements[0][2];
    result.elements[0][1] = lhs.elements[0][1] * rhs.elements[0][0] + lhs.elements[1][1] * rhs.elements[0][1] + lhs.elements[2][1] * rhs.elements[0][2];
    result.elements[0][2] = lhs.elements[0][2] * rhs.elements[0][0] + lhs.elements[1][2] * rhs.elements[0][1] + lhs.elements[2][2] * rhs.elements[0][2];

    result.elements[1][0] = lhs.elements[0][0] * rhs.elements[1][0] + lhs.elements[1][0] * rhs.elements[1][1] + lhs.elements[2][0] * rhs.elements[1][2];
    result.elements[1][1] = lhs.elements[0][1] * rhs.elements[1][0] + lhs.elements[1][1] * rhs.elements[1][1] + lhs.elements[2][1] * rhs.elements[1][2];
    result.elements[1][2] = lhs.elements[0][2] * rhs.elements[1][0] + lhs.elements[1][2] * rhs.elements[1][1] + lhs.elements[2][2] * rhs.elements[1][2];

    result.elements[2][0] = lhs.elements[0][0] * rhs.elements[2][0] + lhs.elements[1][0] * rhs.elements[2][1] + lhs.elements[2][0] * rhs.elements[2][2];
    result.elements[2][1] = lhs.elements[0][1] * rhs.elements[2][0] + lhs.elements[1][1] * rhs.elements[2][1] + lhs.elements[2][1] * rhs.elements[2][2];
    result.elements[2][2] = lhs.elements[0][2] * rhs.elements[2][0] + lhs.elements[1][2] * rhs.elements[2][1] + lhs.elements[2][2] * rhs.elements[2][2];
    return result;
}

template <typename T> constexpr Matrix3x3<T> &operator*=(const Matrix3x3<T> &lhs, const Matrix3x3<T> &rhs) noexcept
{
    Matrix3x3<T> result = lhs * rhs;
    return result;
}

template <typename T> constexpr Vector3<T> operator*(const Matrix3x3<T> &lhs, const Vector3<T> &rhs) noexcept
{
    Vector3<T> result;
    result.x = lhs.elements[0][0] * rhs.x + lhs.elements[1][0] * rhs.y + lhs.elements[2][0] * rhs.z;
    result.y = lhs.elements[0][1] * rhs.x + lhs.elements[1][1] * rhs.y + lhs.elements[2][1] * rhs.z;
    result.z = lhs.elements[0][2] * rhs.x + lhs.elements[1][2] * rhs.y + lhs.elements[2][2] * rhs.z;
    return result;
}

namespace MATRIX3x3
{
    template <typename T> constexpr Matrix3x3<T> BuildTranslation(const Vector2<T> &translation)
    {
        Matrix3x3<T> translationMatrix;
        translationMatrix.elements[2][0] = translation.x;
        translationMatrix.elements[2][1] = translation.y;
        return translationMatrix;
    }
    template <typename T> constexpr Matrix3x3<T> BuildRotation(float angle)
    {
        Matrix3x3<T> rotationMatrix;
        float        cosVal           = (float)std::cos((double)angle);
        float        sinVal           = (float)std::sin((double)angle);
        rotationMatrix.elements[0][0] = cosVal;
        rotationMatrix.elements[0][1] = sinVal;
        rotationMatrix.elements[1][0] = -sinVal;
        rotationMatrix.elements[1][1] = cosVal;
        return rotationMatrix;
    }
    template <typename T> constexpr Matrix3x3<T> BuildScale(const Vector2<T> &scale)
    {
        Matrix3x3<T> scaleMatrix;
        scaleMatrix.elements[0][0] = scale.x;
        scaleMatrix.elements[1][1] = scale.y;
        return scaleMatrix;
    }
    template <typename T> constexpr Matrix3x3<T> BuildTranspose(const Matrix3x3<T> &matrix)
    {
        Matrix3x3<T> transposeMatrix(matrix);
        transposeMatrix.elements[0][1] = matrix.elements[1][0];
        transposeMatrix.elements[0][2] = matrix.elements[2][0];
        transposeMatrix.elements[1][0] = matrix.elements[0][1];
        transposeMatrix.elements[1][2] = matrix.elements[2][1];
        transposeMatrix.elements[2][0] = matrix.elements[0][2];
        transposeMatrix.elements[2][1] = matrix.elements[1][2];
        return transposeMatrix;
    }
    template <typename T> constexpr Matrix3x3<T> BuildIdentity()
    {
        Matrix3x3<T> identity;
        return identity;
    }
}