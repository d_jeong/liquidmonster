/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : Vector3.hpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Haewon Shon
Secondary :
- End Header ----------------------------------------------------------------*/
#pragma once

template <typename T> class Vector3
{
public:
    Vector3() : x(0.f), y(0.f), z(0.f)
    {
    }
    Vector3(T x, T y, T z) : x(x), y(y), z(z)
    {
    }
    constexpr void Add(const Vector3 &vector) noexcept
    {
        x += vector.x;
        y += vector.y;
        z += vector.z;
    }
    constexpr void Subtract(const Vector3 &vector) noexcept
    {
        x -= vector.x;
        y -= vector.y;
        z -= vector.z;
    }
    constexpr void Multiply(T value) noexcept
    {
        x *= value;
        y *= value;
        z *= value;
    }
    constexpr void Divide(T value) noexcept
    {
        x /= value;
        y /= value;
        z /= value;
    }
    constexpr Vector3 &operator+=(const Vector3 &vector) noexcept
    {
        this->Add(vector);
        return *this;
    }
    constexpr Vector3 &operator-=(const Vector3 &vector) noexcept
    {
        this->Subtract(vector);
        return *this;
    }
    constexpr Vector3 &operator*=(T value) noexcept
    {
        this->Multiply(value);
        return *this;
    }
    constexpr Vector3 &operator/=(T value) noexcept
    {
        this->Divide(value);
        return *this;
    }
    constexpr Vector3 operator+(const Vector3 &vector) noexcept
    {
        Vector3 result(*this);
        result.Add(vector);
        return result;
    }
    constexpr Vector3 operator-(const Vector3 &vector) noexcept
    {
        Vector3 result(*this);
        result.Subtract(vector);
        return result;
    }
    constexpr Vector3 operator*(T value) noexcept
    {
        Vector3 result(*this);
        result.Multiply(value);
        return result;
    }
    constexpr Vector3 operator/(T value) noexcept
    {
        Vector3 result(*this);
        result.Divide(value);
        return result;
    }

public:
    T x;
    T y;
    T z;
};