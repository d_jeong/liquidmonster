/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : Angles.hpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Rudy Castan (from cs200 assignment)
Secondary :
- End Header ----------------------------------------------------------------*/
#pragma once

constexpr float PI         = 3.1415926535897932384626433832795f;
constexpr float HALF_PI    = PI / 2.0f;
constexpr float QUARTER_PI = PI / 4.0f;
constexpr float TWO_PI     = 2.0f * PI;

constexpr float DegreeToRadian(float degree)
{
    return degree * PI / 180.0f;
}
constexpr float RadianToDegree(float radian)
{
    return radian * 180.0f / PI;
}
