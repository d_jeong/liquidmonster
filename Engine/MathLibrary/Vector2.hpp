/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : Vector2.hpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Haewon Shon
Secondary :
- End Header ----------------------------------------------------------------*/
#pragma once
#include <cassert>
#include <cmath>

template <typename T> class Vector2
{
public:
    Vector2() : x(0), y(0)
    {
    }
    Vector2(T x, T y) : x(x), y(y)
    {
    }
    constexpr void Add(const Vector2 &vector) noexcept
    {
         x += vector.x;
        y += vector.y;
    }
    constexpr void Subtract(const Vector2 &vector) noexcept
    {
        x -= vector.x;
        y -= vector.y;
    }
    constexpr void Multiply(T value) noexcept
    {
        x *= value;
        y *= value;
    }
    constexpr void Divide(T value) noexcept
    {
        x /= value;
        y /= value;
    }
    constexpr Vector2 &operator+=(const Vector2 &vector) noexcept
    {
        this->Add(vector);
        return *this;
    }
    constexpr Vector2 &operator-=(const Vector2 &vector) noexcept
    {
        this->Subtract(vector);
        return *this;
    }
    constexpr Vector2 &operator*=(T value) noexcept
    {
        this->Multiply(value);
        return *this;
    }
    constexpr Vector2 &operator/=(T value) noexcept
    {
        this->Divide(value);
        return *this;
    }
    constexpr Vector2 operator+(const Vector2 &vector) noexcept
    {
        Vector2 result(*this);
        result.Add(vector);
        return result;
    }
    constexpr Vector2 operator-(const Vector2 &vector) noexcept
    {
        Vector2 result(*this);
        result.Subtract(vector);
        return result;
    }
    constexpr Vector2 operator*(T value) noexcept
    {
        Vector2 result(*this);
        result.Multiply(value);
        return result;
    }
    constexpr Vector2 operator/(T value) noexcept
    {
        Vector2 result(*this);
        result.Divide(value);
        return result;
    }
    constexpr Vector2 Rotate(float angle) noexcept
    {
        Vector2<float> rotate;
        float sinVal = static_cast<float>(std::sin(static_cast<double>(angle)));
        float cosVal = static_cast<float>(std::cos(static_cast<double>(angle)));
        rotate.x = cosVal * static_cast<float>(x) - sinVal * static_cast<float>(y);
        rotate.y = sinVal * static_cast<float>(x) + cosVal * static_cast<float>(y);
        return rotate;
    }

public:
    T x;
    T y;
};