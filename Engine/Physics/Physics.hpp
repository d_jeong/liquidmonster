/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : Physics.hpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Yewon Lim
Secondary :
- End Header ----------------------------------------------------------------*/

#pragma once
#include "../System.hpp"
#include <vector>
#include "../FMOD/FmodManager.hpp"

class Object;

class Physics : public System
{
public:
    Physics();
    ~Physics() override;
    void Initialize() override;
    void Update(float dt) override;

    void AddToCollisionList(Object *object);
    void ClearCollisionList(void);

    void DeleteObjectFromList(Object* object);

private:
    void CollisionSolve(float dt);
    void ApplyFriction(float dt);
    void SolveCollisionWithTile(Object *obj, float dt);
    void SolveStaticCollision(float dt, Object *staticObj, Object *dynamicObj);
    void SolveDynamics(float dt, Object *obj1, Object *obj2);

    const float mGravity         = -300.f;
    const float mDynamicFriction = 10000.f;
    const float mEatableFriction = 500.f;

    std::vector<Object *> mDynamics;
    std::vector<Object *> mEatables;
    std::vector<Object *> mStatics;

    FMOD::Channel* mChannelSFXBox = nullptr;
};
extern Physics *PHYSICS;
