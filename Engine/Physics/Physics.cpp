/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : Physics.cpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Haewon Shon
Secondary :
- End Header ----------------------------------------------------------------*/

#include "Physics.hpp"
#include "../Component/Collision/Collision.hpp"
#include "../Component/Player/Player.hpp"
#include "../Component/Sprite/Sprite.hpp"
#include "../Game/State/StateManager/StateManager.hpp"
#include "../Object/Object.hpp"
#include "../Object/ObjectManager/ObjectManager.hpp"
#include "../Object/ObjectType.hpp"
#include "../TileMap/TileMap.hpp"
#include <algorithm>

Physics* PHYSICS = nullptr;

Physics::Physics()
{
    PHYSICS = this;
}

Physics::~Physics()
{
    PHYSICS = nullptr;
    mChannelSFXBox->stop();
}

void Physics::Initialize()
{
}

void Physics::Update(float dt)
{
    // if the game is paused, do not update physics
    if (STATE_MANAGER->IsGameStopped())
    {
        return;
    }

    float minFallingSpeed = -60.f;
    float maxFallingSpeed = -300.f;

    // apply gravity
    for (auto obj : mDynamics)
    {
        if (obj->GetIsActive() == false)
        {
            continue;
        }
        if (obj->GetTransform()->GetParentTransform() == nullptr)
        {
            if (obj->GetComponentByType<Collision>(eComponentType::COLLISION)->GetIsOnGround() || obj->GetComponentByType<Collision>(eComponentType::COLLISION)->GetIsOnObject())
            {
            }
            else
            {
                if (obj->GetComponentByType<Sprite>(eComponentType::SPRITE)->GetIsVisible() == true)
                {
                    obj->AddVelocity({0.f, mGravity * dt});
                }
            }
        }
    }
    for (auto obj : mEatables)
    {
        if (obj->GetIsActive() == false)
        {
            continue;
        }
        if (obj->GetTransform()->GetParentTransform() == nullptr)
        {
            obj->AddVelocity({0.f, mGravity * dt});
        }
    }

    // static collisions
    for (auto staticObj : mStatics)
    {
        if (staticObj->GetIsActive() == false)
        {
            continue;
        }
        for (auto dynamicObj : mDynamics)
        {
            if (dynamicObj->GetIsActive() == false)
            {
                continue;
            }
            SolveStaticCollision(dt, staticObj, dynamicObj);
        }
        for (auto eatableObj : mEatables)
        {
            if (eatableObj->GetIsActive() == false)
            {
                continue;
            }
            if (eatableObj->GetTransform()->GetParentTransform() == nullptr)
            {
                SolveStaticCollision(dt, staticObj, eatableObj);
            }
        }
    }

    if (!mDynamics.empty())
    {
        for (int obj1Index = 0; obj1Index < mDynamics.size(); ++obj1Index)
        {
            if (mDynamics[obj1Index]->GetIsActive() == false)
            {
                continue;
            }
            for (int obj2Index = obj1Index + 1; obj2Index < mDynamics.size(); ++obj2Index)
            {
                if (mDynamics[obj2Index]->GetIsActive() == false)
                {
                    continue;
                }
                SolveDynamics(dt, mDynamics[obj1Index], mDynamics[obj2Index]);
            }
        }
    }

    // solve collisions vs tile
    for (auto obj : mDynamics)
    {
        if (obj->GetIsActive() == false)
        {
            continue;
        }
        SolveCollisionWithTile(obj, dt);
    }
    for (auto obj : mEatables)
    {
        if (obj->GetIsActive() == false)
        {
            continue;
        }
        if (obj->GetTransform()->GetParentTransform() == nullptr)
        {
            SolveCollisionWithTile(obj, dt);
        }
    }

    for (auto obj : mDynamics)
    {
        if (obj->GetIsActive() == false)
        {
            continue;
        }
        obj->SetPosition(obj->GetPosition() + obj->GetVelocity() * dt);
    }
    for (auto obj : mEatables)
    {
        if (obj->GetIsActive() == false)
        {
            continue;
        }
        if (obj->GetTransform()->GetParentTransform() == nullptr)
        {
            obj->SetPosition(obj->GetPosition() + obj->GetVelocity() * dt);
        }
    }

    ApplyFriction(dt);
}

void Physics::CollisionSolve(float dt)
{
}

void Physics::ApplyFriction(float dt)
{
    for (auto obj : mDynamics)
    {

        Vector2<float> velocity = obj->GetVelocity();
        if (velocity.x > 0.f)
        {
            // obj->SetVelocity({std::fmax(velocity.x - mDynamicFriction * dt, 0.f), velocity.y});
            obj->SetVelocity({0.f, velocity.y});
        }
        else if (velocity.x < 0.f)
        {
            // obj->SetVelocity({std::fmin(velocity.x + mDynamicFriction * dt, 0.f), velocity.y});
            obj->SetVelocity({0.f, velocity.y});
        }
    }
    for (auto obj : mEatables)
    {
        Vector2<float> velocity = obj->GetVelocity();
        if (velocity.x > 0.f)
        {
            obj->SetVelocity({std::fmax(velocity.x - mEatableFriction * dt, 0.f), velocity.y});
        }
        else if (velocity.x < 0.f)
        {
            obj->SetVelocity({std::fmin(velocity.x + mEatableFriction * dt, 0.f), velocity.y});
        }
    }
}

void Physics::AddToCollisionList(Object* object)
{
    switch (object->GetObjectType())
    {
        case eObjectType::DYNAMIC:
        case eObjectType::STEEL_BOX:
        case eObjectType::WOOD_BOX:
        case eObjectType::PLAYER_1:
        case eObjectType::PLAYER_2:
        case eObjectType::PLAYER_COMBINED:
            mDynamics.push_back(object);
            std::sort(begin(mDynamics), end(mDynamics), [](Object* obj1, Object* obj2) { return obj1->GetObjectType() > obj2->GetObjectType(); });
            break;
        case eObjectType::BATTERY:
        case eObjectType::EATABLE:
            mEatables.push_back(object);
            break;
        case eObjectType::STATIC:
        case eObjectType::MOVING_PLATFORM:
        case eObjectType::BUTTON:
            mStatics.push_back(object);
    }
}

void Physics::ClearCollisionList(void)
{
    mDynamics.clear();
    mEatables.clear();
    mStatics.clear();
}

void Physics::DeleteObjectFromList(Object* object)
{
    switch (object->GetObjectType())
    {
        case eObjectType::DYNAMIC:
        case eObjectType::STEEL_BOX:
        case eObjectType::WOOD_BOX:
        case eObjectType::PLAYER_1:
        case eObjectType::PLAYER_2:
        case eObjectType::PLAYER_COMBINED:
            for (auto it = begin(mDynamics); it != end(mDynamics); ++it)
            {
                if (*it == object)
                {
                    mDynamics.erase(it);
                    break;
                }
            }
            break;
        case eObjectType::BATTERY:
        case eObjectType::EATABLE:
            for (auto it = begin(mEatables); it != end(mEatables); ++it)
            {
                if (*it == object)
                {
                    mEatables.erase(it);
                    break;
                }
            }
            break;
        case eObjectType::STATIC:
        case eObjectType::MOVING_PLATFORM:
        case eObjectType::BUTTON:
            for (auto it = begin(mStatics); it != end(mStatics); ++it)
            {
                if (*it == object)
                {
                    mStatics.erase(it);
                    break;
                }
            }
            break;
    }
}

void Physics::SolveCollisionWithTile(Object* obj, float dt)
{
    Collision* collision = obj->GetComponentByType<Collision>(eComponentType::COLLISION);
    if (collision->GetIsGhost())
    {
        return;
    }

    float groundY, cellingY, leftX, rightX;
    Tile* left   = collision->CheckIsPushingLeftside(dt, leftX);
    Tile* right  = collision->CheckIsPushingRightside(dt, rightX);
    Tile* bottom = collision->CheckIsOnGround(dt, groundY);
    Tile* top    = collision->CheckIsTouchingCelling(dt, cellingY);

    Vector2<float> objVelocity = obj->GetVelocity();
    Vector2<float> objPosition = obj->GetPosition();

    if (left != nullptr && bottom != nullptr)
    {
        if (objVelocity.x == 0.f)
        {
            // bottom only
            obj->SetPosition({obj->GetPosition().x, groundY});
            obj->SetVelocity({obj->GetVelocity().x, 0.f});
            collision->SetIsPushingLeftside(false);
            collision->SetGround(groundY - obj->GetScale().y / 2.f);

            // if left, adjust
            if (collision->CheckIsPushingLeftside(dt, leftX))
            {
                obj->SetPosition({leftX, obj->GetPosition().y});
                obj->SetVelocity({0.f, obj->GetVelocity().y});
                collision->SetIsOnGround(true);
            }
        }
        else if (objVelocity.y == 0.f)
        {
            // left only
            obj->SetPosition({leftX, obj->GetPosition().y});
            obj->SetVelocity({0.f, obj->GetVelocity().y});
            collision->SetIsOnGround(false);

            if (collision->CheckIsOnGround(dt, groundY))
            {
                obj->SetPosition({obj->GetPosition().x, groundY});
                obj->SetVelocity({obj->GetVelocity().x, 0.f});
                collision->SetIsPushingLeftside(true);
                collision->SetGround(groundY - obj->GetScale().y / 2.f);
            }
        }
        else
        {
            float xArriveTime = std::abs(objPosition.x - leftX) / std::abs(objVelocity.x);
            float yArriveTime = std::abs(objPosition.y - groundY) / std::abs(objVelocity.y);
            if (yArriveTime < xArriveTime)
            {
                // bottom first
                obj->SetPosition({obj->GetPosition().x, groundY});
                obj->SetVelocity({obj->GetVelocity().x, 0.f});
                collision->SetIsPushingLeftside(false);
                collision->SetGround(groundY - obj->GetScale().y / 2.f);

                // if left, adjust
                if (collision->CheckIsPushingLeftside(dt, leftX))
                {
                    obj->SetPosition({leftX, obj->GetPosition().y});
                    obj->SetVelocity({0.f, obj->GetVelocity().y});
                    collision->SetIsOnGround(true);
                }
            }
            else
            {
                // left first
                obj->SetPosition({leftX, obj->GetPosition().y});
                obj->SetVelocity({0.f, obj->GetVelocity().y});
                collision->SetIsOnGround(false);

                // if bottom, adjust
                if (collision->CheckIsOnGround(dt, groundY))
                {
                    obj->SetPosition({obj->GetPosition().x, groundY});
                    obj->SetVelocity({obj->GetVelocity().x, 0.f});
                    collision->SetIsPushingLeftside(true);
                    collision->SetGround(groundY - obj->GetScale().y / 2.f);
                }
            }
        }
    }
    else if (left != nullptr && top != nullptr)
    {
        if (objVelocity.x == 0.f)
        {
            obj->SetPosition({obj->GetPosition().x, cellingY});
            obj->SetVelocity({obj->GetVelocity().x, 0.f});
            collision->SetIsPushingLeftside(false);

            if (collision->CheckIsPushingLeftside(dt, leftX))
            {
                obj->SetPosition({leftX, obj->GetPosition().y});
                obj->SetVelocity({0.f, obj->GetVelocity().y});
                collision->SetIsPushingLeftside(true);
            }
        }
        else if (objVelocity.y == 0.f)
        {
            obj->SetPosition({leftX, obj->GetPosition().y});
            obj->SetVelocity({0.f, obj->GetVelocity().y});
            collision->SetIsTouchingCelling(false);

            if (collision->CheckIsTouchingCelling(dt, cellingY))
            {
                obj->SetPosition({obj->GetPosition().x, cellingY});
                obj->SetVelocity({obj->GetVelocity().x, 0.f});
                collision->SetIsTouchingCelling(true);
            }
        }
        else
        {
            float xArriveTime = std::abs(objPosition.x - leftX) / std::abs(objVelocity.x);
            float yArriveTime = std::abs(objPosition.y - cellingY) / std::abs(objVelocity.y);
            if (yArriveTime < xArriveTime)
            {
                // top first
                obj->SetPosition({obj->GetPosition().x, cellingY});
                obj->SetVelocity({obj->GetVelocity().x, 0.f});
                collision->SetIsPushingLeftside(false);

                // if left, adjust
                if (collision->CheckIsPushingLeftside(dt, leftX))
                {
                    obj->SetPosition({leftX, obj->GetPosition().y});
                    obj->SetVelocity({0.f, obj->GetVelocity().y});
                    collision->SetIsTouchingCelling(true);
                }
            }
            else
            {
                // left first
                obj->SetPosition({leftX, obj->GetPosition().y});
                obj->SetVelocity({0.f, obj->GetVelocity().y});
                collision->SetIsTouchingCelling(false);

                // if top, adjust
                if (collision->CheckIsTouchingCelling(dt, cellingY))
                {
                    obj->SetPosition({obj->GetPosition().x, cellingY});
                    obj->SetVelocity({obj->GetVelocity().x, 0.f});
                    collision->SetIsPushingLeftside(true);
                }
            }
        }
    }
    else if (left)
    {
        obj->SetPosition({leftX, obj->GetPosition().y});
        obj->SetVelocity({0.f, obj->GetVelocity().y});
    }

    if (right != nullptr && bottom != nullptr)
    {
        if (objVelocity.x == 0.f)
        {
            obj->SetPosition({objPosition.x, groundY});
            obj->SetVelocity({obj->GetVelocity().x, 0.f});
            collision->SetIsPushingRightside(false);
            collision->SetGround(groundY - obj->GetScale().y / 2.f);

            if (collision->CheckIsPushingRightside(dt, rightX))
            {
                obj->SetPosition({rightX, obj->GetPosition().y});
                obj->SetVelocity({0.f, obj->GetVelocity().y});
                collision->SetIsPushingRightside(true);
            }
        }
        else if (objVelocity.y == 0.f)
        {
            obj->SetPosition({rightX, obj->GetPosition().y});
            obj->SetVelocity({0.f, obj->GetVelocity().y});
            collision->SetIsOnGround(false);

            if (collision->CheckIsOnGround(dt, groundY))
            {
                obj->SetPosition({objPosition.x, groundY});
                obj->SetVelocity({obj->GetVelocity().x, 0.f});
                collision->SetIsOnGround(true);
                collision->SetGround(groundY - obj->GetScale().y / 2.f);
            }
        }
        else
        {
            float xArriveTime = std::abs(objPosition.x - rightX) / std::abs(objVelocity.x);
            float yArriveTime = std::abs(objPosition.y - groundY) / std::abs(objVelocity.y);
            if (yArriveTime < xArriveTime)
            {
                // bottom first
                obj->SetPosition({objPosition.x, groundY});
                obj->SetVelocity({obj->GetVelocity().x, 0.f});
                collision->SetIsPushingRightside(false);
                collision->SetGround(groundY - obj->GetScale().y / 2.f);

                if (collision->CheckIsPushingRightside(dt, rightX))
                {
                    obj->SetPosition({rightX, obj->GetPosition().y});
                    obj->SetVelocity({0.f, obj->GetVelocity().y});
                    collision->SetIsPushingRightside(true);
                }
            }
            else
            {
                // right first
                obj->SetPosition({rightX, obj->GetPosition().y});
                obj->SetVelocity({0.f, obj->GetVelocity().y});
                collision->SetIsOnGround(false);

                if (collision->CheckIsOnGround(dt, groundY))
                {
                    obj->SetPosition({objPosition.x, groundY});
                    obj->SetVelocity({obj->GetVelocity().x, 0.f});
                    collision->SetIsOnGround(true);
                    collision->SetGround(groundY - obj->GetScale().y / 2.f);
                }
            }
        }
    }
    else if (right != nullptr && top != nullptr)
    {
        if (objVelocity.x == 0.f)
        {
            obj->SetPosition({objPosition.x, cellingY});
            obj->SetVelocity({obj->GetVelocity().x, 0.f});
            collision->SetIsPushingRightside(false);

            if (collision->CheckIsPushingRightside(dt, rightX))
            {
                obj->SetPosition({rightX, obj->GetPosition().y});
                obj->SetVelocity({0.f, obj->GetVelocity().y});
                collision->SetIsPushingRightside(true);
            }
        }
        else if (objVelocity.y == 0.f)
        {
            obj->SetPosition({rightX, obj->GetPosition().y});
            obj->SetVelocity({0.f, obj->GetVelocity().y});
            collision->SetIsPushingRightside(true);

            if (collision->CheckIsTouchingCelling(dt, cellingY))
            {
                obj->SetPosition({objPosition.x, cellingY});
                obj->SetVelocity({obj->GetVelocity().x, 0.f});
                collision->SetIsTouchingCelling(true);
            }
        }
        else
        {
            float xArriveTime = std::abs(objPosition.x - rightX) / std::abs(objVelocity.x);
            float yArriveTime = std::abs(objPosition.y - cellingY) / std::abs(objVelocity.y);
            if (yArriveTime < xArriveTime)
            {
                obj->SetPosition({objPosition.x, cellingY});
                obj->SetVelocity({obj->GetVelocity().x, 0.f});
                collision->SetIsPushingRightside(false);
                if (collision->CheckIsPushingRightside(dt, rightX))
                {
                    obj->SetPosition({rightX, obj->GetPosition().y});
                    obj->SetVelocity({0.f, obj->GetVelocity().y});
                    collision->SetIsPushingRightside(true);
                }
            }
            else
            {
                obj->SetPosition({rightX, obj->GetPosition().y});
                obj->SetVelocity({0.f, obj->GetVelocity().y});
                collision->SetIsTouchingCelling(false);
                if (collision->CheckIsTouchingCelling(dt, cellingY))
                {
                    obj->SetPosition({objPosition.x, cellingY});
                    obj->SetVelocity({obj->GetVelocity().x, 0.f});
                    collision->SetIsTouchingCelling(true);
                }
            }
        }
    }
    else if (right)
    {
        obj->SetPosition({rightX, obj->GetPosition().y});
        obj->SetVelocity({0.f, obj->GetVelocity().y});
    }

    if (bottom && !(left || right))
    {
        obj->SetPosition({objPosition.x, groundY});
        obj->SetVelocity({obj->GetVelocity().x, 0.f});
        collision->SetGround(groundY - obj->GetScale().y / 2.f);
    }
    if (top && !(left || right))
    {
        obj->SetPosition({objPosition.x, cellingY});
        obj->SetVelocity({obj->GetVelocity().x, 0.f});
    }
}

void Physics::SolveStaticCollision(float dt, Object* staticObject, Object* dynamicObject)
{
    Collision* dynamicCollision = dynamicObject->GetComponentByType<Collision>(eComponentType::COLLISION);
    if (dynamicCollision->GetIsGhost() || staticObject->GetComponentByType<Collision>(eComponentType::COLLISION)->GetIsGhost())
    {
        return;
    }

    Vector2<float> dynamicVelo         = dynamicObject->GetVelocity();
    Vector2<float> dynamicPos          = dynamicObject->GetPosition();
    Vector2<float> dynamicScale        = dynamicObject->GetScale();
    Vector3<float> dynamicRotatedScale = MATRIX3x3::BuildRotation<float>(std::abs(dynamicObject->GetRotation())) * Vector3<float>{dynamicScale.x, dynamicScale.y, 1.f};
    dynamicScale                       = {dynamicRotatedScale.x, dynamicRotatedScale.y};
    dynamicScale.x                     = std::abs(dynamicScale.x);
    dynamicScale.y                     = std::abs(dynamicScale.y);
    Vector2<float> staticVelo          = staticObject->GetVelocity();
    Vector2<float> staticPos           = staticObject->GetPosition();
    Vector2<float> staticScale         = staticObject->GetScale();
    Vector3<float> staticRotatedScale  = MATRIX3x3::BuildRotation<float>(std::abs(staticObject->GetRotation())) * Vector3<float>{staticScale.x, staticScale.y, 1.f};
    staticScale                        = {staticRotatedScale.x, staticRotatedScale.y};
    staticScale.x                      = std::abs(staticScale.x);
    staticScale.y                      = std::abs(staticScale.y);

    float xDistance = (dynamicScale.x + staticScale.x) / 2.f;
    float yDistance = (dynamicScale.y + staticScale.y) / 2.f;

    bool left  = dynamicCollision->CheckAndPushObjectToLeftside(dt, staticObject);
    bool right = dynamicCollision->CheckAndPushObjectToRightside(dt, staticObject);
    bool down  = dynamicCollision->CheckIsOnObject(dt, staticObject);
    bool top   = dynamicCollision->CheckIsUnderObject(dt, staticObject);

    bool isAffectedByYAxis = dynamicCollision->GetIsOnObject() || dynamicCollision->GetIsUnderObject();

    if (left && isAffectedByYAxis)
    {
        Object* leftsideObject = dynamicCollision->GetLeftsideObject();
        Object* downsideObject = dynamicCollision->GetDownsideObject();

        // 1. left != down : already stand on something, stick to other object - need to side collsion solve
        if (leftsideObject != downsideObject)
        {
            dynamicObject->SetVelocity({0.f, dynamicObject->GetVelocity().y});
            dynamicObject->SetPosition({staticObject->GetPosition().x + xDistance, dynamicObject->GetPosition().y});
        }
        // 2. left == down : adjust
        else
        {
            if (dynamicVelo.y == 0.f)
            {
                dynamicObject->SetVelocity({0.f, dynamicObject->GetVelocity().y});
                dynamicObject->SetPosition({staticObject->GetPosition().x + xDistance, dynamicObject->GetPosition().y});
                dynamicCollision->SetIsOnObject(false);
            }
            else if (dynamicVelo.x == 0.f)
            {
                dynamicObject->SetPosition({dynamicPos.x, staticObject->GetPosition().y + yDistance});
                dynamicObject->SetVelocity({dynamicVelo.x, 0.f});
                dynamicCollision->SetIsPushingLeftsideObject(false);
            }
            else
            {
                float xArriveTime = (dynamicPos.x - staticPos.x - xDistance) / std::abs(dynamicVelo.x);
                float yArriveTime = (dynamicPos.y - staticPos.y - yDistance) / std::abs(dynamicVelo.y);

                if (xArriveTime < 0.f && yArriveTime < 0.f)
                {
                    // y axis collision first
                    if (yArriveTime > xArriveTime)
                    {
                        dynamicObject->SetPosition({dynamicPos.x, staticObject->GetPosition().y + yDistance});
                        dynamicObject->SetVelocity({dynamicVelo.x, 0.f});
                        dynamicCollision->SetIsPushingLeftsideObject(false);
                    }
                    // x axis collision first
                    else
                    {
                        dynamicObject->SetVelocity({0.f, dynamicObject->GetVelocity().y});
                        dynamicObject->SetPosition({staticObject->GetPosition().x + xDistance, dynamicObject->GetPosition().y});
                        dynamicCollision->SetIsOnObject(false);
                    }
                }
                else if (xArriveTime < 0.f)
                {
                    dynamicObject->SetPosition({dynamicPos.x, staticObject->GetPosition().y + yDistance});
                    dynamicObject->SetVelocity({dynamicVelo.x, 0.f});
                    dynamicCollision->SetIsPushingLeftsideObject(false);
                }
                else if (yArriveTime < 0.f)
                {
                    dynamicObject->SetVelocity({0.f, dynamicObject->GetVelocity().y});
                    dynamicObject->SetPosition({staticObject->GetPosition().x + xDistance, dynamicObject->GetPosition().y});
                    dynamicCollision->SetIsOnObject(false);
                }
                else
                {
                    // y axis collision first
                    if (yArriveTime < xArriveTime)
                    {
                        dynamicObject->SetPosition({dynamicPos.x, staticObject->GetPosition().y + yDistance});
                        dynamicObject->SetVelocity({dynamicVelo.x, 0.f});
                        dynamicCollision->SetIsPushingLeftsideObject(false);
                    }
                    // x axis collision first
                    else
                    {
                        dynamicObject->SetVelocity({0.f, dynamicObject->GetVelocity().y});
                        dynamicObject->SetPosition({staticObject->GetPosition().x + xDistance, dynamicObject->GetPosition().y});
                        dynamicCollision->SetIsOnObject(false);
                    }
                }
            }
        }
    }
    else if (left)
    {
        dynamicObject->SetVelocity({0.f, dynamicObject->GetVelocity().y});
        dynamicObject->SetPosition({staticObject->GetPosition().x + xDistance, dynamicObject->GetPosition().y});
    }

    if (right && isAffectedByYAxis)
    {
        Object* rightsideObject = dynamicCollision->GetRightsideObject();
        Object* downsideObject  = dynamicCollision->GetDownsideObject();

        // 1. right != down : already stand on something, stick to other object - need to side collision solve
        if (rightsideObject != downsideObject)
        {
            staticObject->SetPosition({dynamicObject->GetPosition().x + dynamicVelo.x * dt + xDistance, staticObject->GetPosition().y});
        }
        // 2. right == down : adjust
        else
        {
            if (dynamicVelo.y == 0.f)
            {
                dynamicObject->SetVelocity({0.f, dynamicObject->GetVelocity().y});
                dynamicObject->SetPosition({staticObject->GetPosition().x - xDistance, dynamicObject->GetPosition().y});
            }
            else if (dynamicVelo.x == 0.f)
            {
                dynamicObject->SetPosition({dynamicPos.x, staticObject->GetPosition().y + yDistance});
                dynamicObject->SetVelocity({dynamicVelo.x, 0.f});
                dynamicCollision->SetIsPushingRightsideObject(false);
            }
            else
            {
                float xArriveTime = (staticPos.x - dynamicPos.x - xDistance) / std::abs(dynamicVelo.x);
                float yArriveTime = (dynamicPos.y - staticPos.y - yDistance) / std::abs(dynamicVelo.y);

                if (xArriveTime < 0.f && yArriveTime < 0.f)
                {
                    if (yArriveTime > xArriveTime)
                    {
                        dynamicObject->SetPosition({dynamicPos.x, staticObject->GetPosition().y + yDistance});
                        dynamicObject->SetVelocity({dynamicVelo.x, 0.f});
                        dynamicCollision->SetIsPushingRightsideObject(false);
                    }
                    else
                    {
                        dynamicObject->SetVelocity({0.f, dynamicObject->GetVelocity().y});
                        dynamicObject->SetPosition({staticObject->GetPosition().x - xDistance, dynamicObject->GetPosition().y});
                        dynamicCollision->SetIsOnObject(false);
                    }
                }
                else if (xArriveTime < 0.f)
                {
                    dynamicObject->SetPosition({dynamicPos.x, staticObject->GetPosition().y + yDistance});
                    dynamicObject->SetVelocity({dynamicVelo.x, 0.f});
                    dynamicCollision->SetIsPushingLeftsideObject(false);
                }
                else if (yArriveTime < 0.f)
                {
                    dynamicObject->SetVelocity({0.f, dynamicObject->GetVelocity().y});
                    dynamicObject->SetPosition({staticObject->GetPosition().x - xDistance, dynamicObject->GetPosition().y});
                    dynamicCollision->SetIsOnObject(false);
                }
                else
                {
                    // y axis collision first
                    if (yArriveTime < xArriveTime)
                    {
                        dynamicObject->SetPosition({dynamicPos.x, staticObject->GetPosition().y + yDistance});
                        dynamicObject->SetVelocity({dynamicVelo.x, 0.f});
                        dynamicCollision->SetIsPushingRightsideObject(false);
                    }
                    // x axis collision first
                    else
                    {
                        dynamicObject->SetVelocity({0.f, dynamicObject->GetVelocity().y});
                        dynamicObject->SetPosition({staticObject->GetPosition().x - xDistance, dynamicObject->GetPosition().y});
                        dynamicCollision->SetIsOnObject(false);
                    }
                }
            }
        }
    }
    else if (right)
    {
        dynamicObject->SetVelocity({0.f, dynamicObject->GetVelocity().y});
        dynamicObject->SetPosition({staticObject->GetPosition().x - xDistance, dynamicObject->GetPosition().y});
    }

    if (right == false && left == false)
    {
        if (down)
        {
            if (dynamicObject->GetVelocity().y <= 0.f)
            {
                dynamicObject->SetPosition({dynamicPos.x, staticObject->GetPosition().y + yDistance});
                dynamicObject->SetVelocity({dynamicVelo.x, 0.f});
            }
        }
        else if (top)
        {
            staticObject->SetPosition({staticObject->GetPosition().x, dynamicPos.y + yDistance});
            staticObject->SetVelocity({staticObject->GetVelocity().x, 0.f});
        }
    }
}

void Physics::SolveDynamics(float dt, Object* obj1, Object* obj2)
{
    // if one of the object is ghost, stop
    if (obj1->GetComponentByType<Collision>(eComponentType::COLLISION)->GetIsGhost() || obj2->GetComponentByType<Collision>(eComponentType::COLLISION)->GetIsGhost())
    {
        return;
    }
    // if both are player, stop
    if (obj1->GetComponentByType<Player>(eComponentType::PLAYER) && obj2->GetComponentByType<Player>(eComponentType::PLAYER))
    {
        return;
    }

    Object* staticObject  = nullptr;
    Object* dynamicObject = nullptr;

    {
        Vector2<float> obj1Pos          = obj1->GetPosition();
        Vector2<float> obj2Pos          = obj2->GetPosition();
        Vector2<float> obj1Velo         = obj1->GetVelocity();
        Vector2<float> obj2Velo         = obj2->GetVelocity();
        Vector2<float> obj1Scale        = obj1->GetScale();
        Vector2<float> obj2Scale        = obj2->GetScale();
        float          xDistance        = (obj1Scale.x + obj2Scale.x) / 2.f;
        float          yDistance        = (obj1Scale.y + obj2Scale.y) / 2.f;
        float          overlappedWidth  = (xDistance - std::abs(obj1Pos.x - obj2Pos.x)) / 2.f;
        float          overlappedHeight = (yDistance - std::abs(obj1Pos.y - obj2Pos.y)) / 2.f;

        // 1. stop - moving objs for x axis
        if (obj1Velo.x == 0.f || obj2Velo.x == 0.f)
        {
            if (obj1Velo.x == 0.f)
            {
                // both are not moving for x axis - set upper one, so it allows top-down collision
                if (obj2Velo.x == 0.f)
                {
                    if (obj1Pos.y > obj2Pos.y)
                    {
                        staticObject  = obj2;
                        dynamicObject = obj1;
                    }
                    else
                    {
                        staticObject  = obj1;
                        dynamicObject = obj2;
                    }
                }
                else
                {
                    staticObject  = obj1;
                    dynamicObject = obj2;
                }
            }
            else
            {
                staticObject  = obj2;
                dynamicObject = obj1;
            }

            Collision* dynamicCollision = dynamicObject->GetComponentByType<Collision>(eComponentType::COLLISION);
            Collision* staticCollision  = staticObject->GetComponentByType<Collision>(eComponentType::COLLISION);
            bool       left             = dynamicCollision->CheckAndPushObjectToLeftside(dt, staticObject);
            bool       right            = dynamicCollision->CheckAndPushObjectToRightside(dt, staticObject);
            bool       up               = dynamicCollision->CheckIsUnderObject(dt, staticObject);
            bool       down             = dynamicCollision->CheckIsOnObject(dt, staticObject);

            Vector2<float> dynamicVelo         = dynamicObject->GetVelocity();
            Vector2<float> dynamicPos          = dynamicObject->GetPosition();
            Vector2<float> dynamicScale        = dynamicObject->GetScale();
            Vector3<float> dynamicRotatedScale = MATRIX3x3::BuildRotation<float>(dynamicObject->GetRotation()) * Vector3<float>{dynamicScale.x, dynamicScale.y, 1.f};
            dynamicScale                       = {dynamicRotatedScale.x, dynamicRotatedScale.y};
            Vector2<float> staticVelo          = staticObject->GetVelocity();
            Vector2<float> staticPos           = staticObject->GetPosition();
            Vector2<float> staticScale         = staticObject->GetScale();
            Vector3<float> staticRotatedScale  = MATRIX3x3::BuildRotation<float>(staticObject->GetRotation()) * Vector3<float>{staticScale.x, staticScale.y, 1.f};
            staticScale                        = {staticRotatedScale.x, staticRotatedScale.y};

            bool isAffectedByYAxis = dynamicCollision->GetIsOnObject() || dynamicCollision->GetIsUnderObject();


            if (left && isAffectedByYAxis)
            {
                Object* leftsideObject = dynamicCollision->GetLeftsideObject();
                Object* downsideObject = dynamicCollision->GetDownsideObject();

                // 1. left != down : already stand on something, stick to other object - need to side collsion solve
                if (leftsideObject != downsideObject)
                {
                    // if the object is pushing the wall, stop both and adjust
                    if (staticObject->GetComponentByType<Collision>(eComponentType::COLLISION)->IsPushableToLeftside() == false)
                    {
                        dynamicObject->SetVelocity({0.f, dynamicObject->GetVelocity().y});
                        dynamicObject->SetPosition({staticObject->GetPosition().x + xDistance, dynamicObject->GetPosition().y});
                    }
                    // if not pushing wall, push it
                    else
                    {
                        staticObject->SetPosition({dynamicObject->GetPosition().x + dynamicVelo.x * dt - xDistance, staticObject->GetPosition().y});
                    }
                    staticCollision->SetIsUnderObject(false);
                    staticCollision->SetDownsideObject(nullptr);
                }
                // 2. left == down : adjust
                else
                {
                    if (dynamicVelo.y == 0.f)
                    {
                        // if the object is pushing the wall, stop both and adjust
                        if (staticObject->GetComponentByType<Collision>(eComponentType::COLLISION)->IsPushableToLeftside() == false)
                        {
                            dynamicObject->SetVelocity({0.f, dynamicObject->GetVelocity().y});
                            dynamicObject->SetPosition({staticObject->GetPosition().x + xDistance, dynamicObject->GetPosition().y});
                        }
                        // if not pushing wall, push it
                        else
                        {
                            staticObject->SetPosition({dynamicObject->GetPosition().x + dynamicVelo.x * dt - xDistance, staticObject->GetPosition().y});
                        }
                        dynamicCollision->SetIsOnObject(false);
                    }
                    else if (dynamicVelo.x == 0.f)
                    {
                        dynamicObject->SetPosition({dynamicPos.x, staticObject->GetPosition().y + yDistance});
                        dynamicObject->SetVelocity({dynamicVelo.x, 0.f});
                        dynamicCollision->SetIsPushingLeftsideObject(false);
                    }
                    else
                    {
                        float xArriveTime = (dynamicPos.x - staticPos.x - xDistance) / std::abs(dynamicVelo.x);
                        float yArriveTime = (dynamicPos.y - staticPos.y - yDistance) / std::abs(dynamicVelo.y);

                        if (xArriveTime < 0.f && yArriveTime < 0.f)
                        {
                            // y axis collision first
                            if (yArriveTime > xArriveTime)
                            {
                                dynamicObject->SetPosition({dynamicPos.x, staticObject->GetPosition().y + yDistance});
                                dynamicObject->SetVelocity({dynamicVelo.x, 0.f});
                                dynamicCollision->SetIsPushingLeftsideObject(false);
                            }
                            // x axis collision first
                            else
                            {
                                // if the object is pushing the wall, stop both and adjust
                                if (staticObject->GetComponentByType<Collision>(eComponentType::COLLISION)->IsPushableToLeftside() == false)
                                {
                                    dynamicObject->SetVelocity({0.f, dynamicObject->GetVelocity().y});
                                    dynamicObject->SetPosition({staticObject->GetPosition().x + xDistance, dynamicObject->GetPosition().y});
                                    dynamicCollision->SetIsOnObject(false);
                                }
                                // if not pushing wall, push it
                                else
                                {
                                    staticObject->SetPosition({dynamicObject->GetPosition().x + dynamicVelo.x * dt - xDistance, staticObject->GetPosition().y});
                                    dynamicCollision->SetIsOnObject(false);
                                }
                            }
                        }
                        else if (xArriveTime < 0.f)
                        {
                            dynamicObject->SetPosition({dynamicPos.x, staticObject->GetPosition().y + yDistance});
                            dynamicObject->SetVelocity({dynamicVelo.x, 0.f});
                            dynamicCollision->SetIsPushingLeftsideObject(false);
                        }
                        else if (yArriveTime < 0.f)
                        {
                            // if the object is pushing the wall, stop both and adjust
                            if (staticObject->GetComponentByType<Collision>(eComponentType::COLLISION)->IsPushableToLeftside() == false)
                            {
                                dynamicObject->SetVelocity({0.f, dynamicObject->GetVelocity().y});
                                dynamicObject->SetPosition({staticObject->GetPosition().x + xDistance, dynamicObject->GetPosition().y});
                            }
                            // if not pushing wall, push it
                            else
                            {
                                staticObject->SetPosition({dynamicObject->GetPosition().x + dynamicVelo.x * dt - xDistance, staticObject->GetPosition().y});
                            }
                            dynamicCollision->SetIsOnObject(false);
                        }
                        else
                        {
                            // y axis collision first
                            if (yArriveTime < xArriveTime)
                            {
                                dynamicObject->SetPosition({dynamicPos.x, staticObject->GetPosition().y + yDistance});
                                dynamicObject->SetVelocity({dynamicVelo.x, 0.f});
                                dynamicCollision->SetIsPushingLeftsideObject(false);
                            }
                            // x axis collision first
                            else
                            {
                                // if the object is pushing the wall, stop both and adjust
                                if (staticObject->GetComponentByType<Collision>(eComponentType::COLLISION)->IsPushableToLeftside() == false)
                                {
                                    dynamicObject->SetVelocity({0.f, dynamicObject->GetVelocity().y});
                                    dynamicObject->SetPosition({staticObject->GetPosition().x + xDistance, dynamicObject->GetPosition().y});
                                    dynamicCollision->SetIsOnObject(false);
                                }
                                // if not pushing wall, push it
                                else
                                {
                                    staticObject->SetPosition({dynamicObject->GetPosition().x + dynamicVelo.x * dt - xDistance, staticObject->GetPosition().y});
                                    dynamicCollision->SetIsOnObject(false);
                                }
                            }
                        }
                    }
                }
            }
            else if (left)
            {
                // if the object is pushing the wall, stop both and adjust
                if (staticObject->GetComponentByType<Collision>(eComponentType::COLLISION)->IsPushableToLeftside() == false)
                {
                    dynamicObject->SetVelocity({0.f, dynamicObject->GetVelocity().y});
                    dynamicObject->SetPosition({staticObject->GetPosition().x + xDistance, dynamicObject->GetPosition().y});
                }
                // if not pushing wall, push it
                else
                {
                    staticObject->SetPosition({dynamicObject->GetPosition().x + dynamicVelo.x * dt - xDistance, staticObject->GetPosition().y});
                }
            }

            if (right && isAffectedByYAxis)
            {
                Object* rightsideObject = dynamicCollision->GetRightsideObject();
                Object* downsideObject  = dynamicCollision->GetDownsideObject();

                // 1. right != down : already stand on something, stick to other object - need to side collision solve
                if (rightsideObject != downsideObject)
                {
                    // if the object is pushing the wall, stop both and adjust
                    if (staticObject->GetComponentByType<Collision>(eComponentType::COLLISION)->IsPushableToRightside() == false)
                    {
                        dynamicObject->SetVelocity({0.f, dynamicObject->GetVelocity().y});
                        dynamicObject->SetPosition({staticObject->GetPosition().x - xDistance, dynamicObject->GetPosition().y});
                    }
                    // if not pushing wall, push it
                    else
                    {
                        staticObject->SetPosition({dynamicObject->GetPosition().x + dynamicVelo.x * dt + xDistance, staticObject->GetPosition().y});
                    }
                    staticCollision->SetIsUnderObject(false);
                    staticCollision->SetDownsideObject(nullptr);
                }
                // 2. right == down : adjust
                else
                {
                    if (dynamicVelo.y == 0.f)
                    {
                        // if the object is pushing the wall, stop both and adjust
                        if (staticObject->GetComponentByType<Collision>(eComponentType::COLLISION)->IsPushableToRightside() == false)
                        {
                            dynamicObject->SetVelocity({0.f, dynamicObject->GetVelocity().y});
                            dynamicObject->SetPosition({staticObject->GetPosition().x - xDistance, dynamicObject->GetPosition().y});
                        }
                        // if not pushing wall, push it
                        else
                        {
                            // staticObject->SetVelocity({dynamicObject->GetVelocity().x, staticObject->GetVelocity().y});
                            staticObject->SetPosition({dynamicObject->GetPosition().x + dynamicVelo.x * dt + xDistance, staticObject->GetPosition().y});
                        }
                        dynamicCollision->SetIsOnObject(false);
                    }
                    else if (dynamicVelo.x == 0.f)
                    {
                        dynamicObject->SetPosition({dynamicPos.x, staticObject->GetPosition().y + yDistance});
                        dynamicObject->SetVelocity({dynamicVelo.x, 0.f});
                        dynamicCollision->SetIsPushingRightsideObject(false);
                    }
                    else
                    {
                        float xArriveTime = (staticPos.x - dynamicPos.x - xDistance) / std::abs(dynamicVelo.x);
                        float yArriveTime = (dynamicPos.y - staticPos.y - yDistance) / std::abs(dynamicVelo.y);

                        if (xArriveTime < 0.f && yArriveTime < 0.f)
                        {
                            if (yArriveTime > xArriveTime)
                            {
                                dynamicObject->SetPosition({dynamicPos.x, staticObject->GetPosition().y + yDistance});
                                dynamicObject->SetVelocity({dynamicVelo.x, 0.f});
                                dynamicCollision->SetIsPushingRightsideObject(false);
                            }
                            else
                            {
                                // if the object is pushing the wall, stop both and adjust
                                if (staticObject->GetComponentByType<Collision>(eComponentType::COLLISION)->IsPushableToRightside() == false)
                                {
                                    dynamicObject->SetVelocity({0.f, dynamicObject->GetVelocity().y});
                                    dynamicObject->SetPosition({staticObject->GetPosition().x - xDistance, dynamicObject->GetPosition().y});
                                    dynamicCollision->SetIsOnObject(false);
                                }
                                // if not pushing wall, push it
                                else
                                {
                                    staticObject->SetPosition({dynamicObject->GetPosition().x + dynamicVelo.x * dt + xDistance, staticObject->GetPosition().y});
                                    dynamicCollision->SetIsOnObject(false);
                                }
                            }
                        }
                        else if (xArriveTime < 0.f)
                        {
                            dynamicObject->SetPosition({dynamicPos.x, staticObject->GetPosition().y + yDistance});
                            dynamicObject->SetVelocity({dynamicVelo.x, 0.f});
                            dynamicCollision->SetIsPushingLeftsideObject(false);
                        }
                        else if (yArriveTime < 0.f)
                        {
                            // if the object is pushing the wall, stop both and adjust
                            if (staticObject->GetComponentByType<Collision>(eComponentType::COLLISION)->IsPushableToRightside() == false)
                            {
                                dynamicObject->SetVelocity({0.f, dynamicObject->GetVelocity().y});
                                dynamicObject->SetPosition({staticObject->GetPosition().x - xDistance, dynamicObject->GetPosition().y});
                                dynamicCollision->SetIsOnObject(false);
                            }
                            // if not pushing wall, push it
                            else
                            {
                                staticObject->SetPosition({dynamicObject->GetPosition().x + dynamicVelo.x * dt + xDistance, staticObject->GetPosition().y});
                                dynamicCollision->SetIsOnObject(false);
                            }
                        }
                        else
                        {
                            // y axis collision first
                            if (yArriveTime < xArriveTime)
                            {
                                dynamicObject->SetPosition({dynamicPos.x, staticObject->GetPosition().y + yDistance});
                                dynamicObject->SetVelocity({dynamicVelo.x, 0.f});
                                dynamicCollision->SetIsPushingRightsideObject(false);
                            }
                            // x axis collision first
                            else
                            {
                                // if the object is pushing the wall, stop both and adjust
                                if (staticObject->GetComponentByType<Collision>(eComponentType::COLLISION)->IsPushableToRightside() == false)
                                {
                                    dynamicObject->SetVelocity({0.f, dynamicObject->GetVelocity().y});
                                    dynamicObject->SetPosition({staticObject->GetPosition().x - xDistance, dynamicObject->GetPosition().y});
                                    dynamicCollision->SetIsOnObject(false);
                                }
                                // if not pushing wall, push it
                                else
                                {
                                    staticObject->SetPosition({dynamicObject->GetPosition().x + dynamicVelo.x * dt + xDistance, staticObject->GetPosition().y});
                                    dynamicCollision->SetIsOnObject(false);
                                }
                            }
                        }
                    }
                }
            }
            else if (right)
            {
                // if the object is pushing the wall, stop both and adjust
                if (staticObject->GetComponentByType<Collision>(eComponentType::COLLISION)->IsPushableToRightside() == false)
                {
                    dynamicObject->SetVelocity({0.f, dynamicObject->GetVelocity().y});
                    dynamicObject->SetPosition({staticObject->GetPosition().x - xDistance, dynamicObject->GetPosition().y});
                }
                // if not pushing wall, push it
                else
                {
                    // staticObject->SetVelocity({dynamicObject->GetVelocity().x, staticObject->GetVelocity().y});
                    staticObject->SetPosition({dynamicObject->GetPosition().x + dynamicVelo.x * dt + xDistance, staticObject->GetPosition().y});
                }
            }

            if (right == false && left == false)
            {
                if (down)
                {
                    if (dynamicObject->GetVelocity().y <= 0.f)
                    {
                        dynamicObject->SetPosition({dynamicPos.x, staticObject->GetPosition().y + yDistance});
                        dynamicObject->SetVelocity({dynamicVelo.x, 0.f});
                    }
                }
                else if (up)
                {
                    staticObject->SetPosition({staticObject->GetPosition().x, dynamicPos.y + yDistance});
                    staticObject->SetVelocity({staticObject->GetVelocity().x, 0.f});
                }
            }


            if (left && staticCollision->IsPushableToLeftside() == true)
            {
                if (staticObject->GetObjectType() == eObjectType::STEEL_BOX)
                {
                    if (dynamicObject->GetObjectType() == eObjectType::PLAYER_COMBINED)
                    {
                        if (dynamicObject->GetVelocity().x < 0)
                        {
                            FMOD_MANAGER->SetPhysicsSFXPaused(false);
                        }
                        else
                        {
                            FMOD_MANAGER->SetPhysicsSFXPaused(true);
                        }
                    }
                }
            }
            else if (right && staticCollision->IsPushableToRightside() == true)
            {
                if (staticObject->GetObjectType() == eObjectType::STEEL_BOX)
                {
                    if (dynamicObject->GetObjectType() == eObjectType::PLAYER_COMBINED)
                    {
                        if (dynamicObject->GetVelocity().x > 0)
                        {
                            FMOD_MANAGER->SetPhysicsSFXPaused(false);
                        }
                        else
                        {
                            FMOD_MANAGER->SetPhysicsSFXPaused(true);
                        }
                    }
                }
            }
        }
        // 2. moving - moving
        else
        {
            if (!obj1->GetComponentByType<Collision>(eComponentType::COLLISION)->IsCollidingWith(obj2))
                return;
            obj1->SetVelocity({0.f, obj1Velo.y});
            obj2->SetVelocity({0.f, obj2Velo.y});

            // set connected objects' velocity as 0
            // obj1 is rightside
            if (obj1Pos.x > obj2Pos.x)
            {
                obj1->SetPosition(obj1Pos + Vector2<float>{overlappedWidth, 0.f});
                obj2->SetPosition(obj2Pos + Vector2<float>{-overlappedWidth, 0.f});
                Object* rightObj = obj1->GetComponentByType<Collision>(eComponentType::COLLISION)->GetRightsideObject();
                while (rightObj != nullptr)
                {
                    rightObj->SetVelocity({0.f, rightObj->GetVelocity().y});
                    rightObj = rightObj->GetComponentByType<Collision>(eComponentType::COLLISION)->GetRightsideObject();
                }
            }
            // obj2 is rightside
            else
            {
                obj1->SetPosition(obj1Pos + Vector2<float>{-overlappedWidth, 0.f});
                obj2->SetPosition(obj2Pos + Vector2<float>{overlappedWidth, 0.f});
                Object* leftObj = obj1->GetComponentByType<Collision>(eComponentType::COLLISION)->GetLeftsideObject();
                while (leftObj != nullptr)
                {
                    leftObj->SetVelocity({0.f, leftObj->GetVelocity().y});
                    leftObj = leftObj->GetComponentByType<Collision>(eComponentType::COLLISION)->GetLeftsideObject();
                }
            }
        }
    }
}