/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : Camera.hpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Haewon Shon
Secondary :
- End Header ----------------------------------------------------------------*/
#pragma once

#include "..//MathLibrary/Matrix3x3.hpp"
#include "../MathLibrary/Angles.hpp"

class Camera
{
    const float mMaximumZoomLevel = 1.f;
    const float mBaseWidth;
    const float mBaseHeight;

public:
    Camera(float width, float height);
    void             Initialize(void);
    void             Update(float dt);
    Matrix3x3<float> GetCameraMatrix(void) const;
    Matrix3x3<float> GetInverseCameraMatrix(void) const;

    void SetLevelWidth(float width);
    void SetLevelHeight(float height);

    void ZoomWidth(float zoomLevel);
    void ZoomHeight(float zoomLevel);

    Vector2<float> GetCenter(void) const;
    void           MoveCenter(const Vector2<float>& pos);

private:
    void AdjustCameraWithTwoPlayers(float dt);
    void AdjustCameraWithCombiendPlayer(float dt);
    void AdjustCamera(float dt, float top, float bottom, float left, float right);

    Vector2<float> mCenter;
    Vector2<float> mUp;
    Vector2<float> mRight;
    float          mXZoomLevel;
    float          mYZoomLevel;

    Vector2<float> mMapTopLeft;
    Vector2<float> mMapBottomRight;
    bool           mStateChangeFlag;

    float xVisibleDistance = 960.f;
    float yVisibleDistance = 540.f;

    const float mScreenRatio = 16.f / 9.f;
};