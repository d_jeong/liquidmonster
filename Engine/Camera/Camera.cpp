/* Start Header ---------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name : Camera.cpp
Language  : C++
Platform  : Visual Studio 2019
Project   : Liquid Monster
Primary   : Haewon Shon
Secondary :
- End Header ----------------------------------------------------------------*/

#include "Camera.hpp"
#include "../Application/Application.hpp"
#include "../Component/Player/Player.hpp"
#include "../Object/Object.hpp"                      // Object* Player
#include "../Object/ObjectManager/ObjectManager.hpp" // GetObject
#include "../TileMap/TileMap.hpp"
#include <iostream>

class Object;

Camera::Camera(float width, float height) : mBaseWidth(width), mBaseHeight(height)
{
    mCenter               = {0.f, 0.f};
    mUp                   = {0.f, 1.f};
    mRight                = {1.f, 0.f};
    mXZoomLevel           = 1.f;
    mYZoomLevel           = 1.f;
    mStateChangeFlag      = true;
}

void Camera::Initialize(void)
{
    auto p1Arrow = OBJECT_MANAGER->FindObjectByName("Arrow1");
    auto p2Arrow = OBJECT_MANAGER->FindObjectByName("Arrow2");

    // mLevelWidthHalf = (mMapBottomRight.x - mMapTopLeft.x) / 2.f;
    // mLevelHeightHalf = (mMapTopLeft.y - mMapBottomRight.y) / 2.f;

    if (p1Arrow || p2Arrow)
    {
        AdjustCameraWithTwoPlayers(20.f);
    }
    else
    {
        mCenter     = {0.f, 0.f};
        mXZoomLevel = 1.f;
        mYZoomLevel = 1.f;
    }
}

void Camera::Update(float dt)
{
    auto p1Arrow = OBJECT_MANAGER->FindObjectByName("Arrow1");
    auto p2Arrow = OBJECT_MANAGER->FindObjectByName("Arrow2");

    // if the game is playable state
    if (p1Arrow || p2Arrow)
    {
        Vector2<float> prevMapSize = mMapTopLeft - mMapBottomRight;
        TILE_MAP->GetTileMapInfo(mMapBottomRight, mMapTopLeft);
        Vector2<float> currentMapSize = mMapTopLeft - mMapBottomRight;
        if (prevMapSize.x != currentMapSize.x || prevMapSize.y != currentMapSize.y)
        {
            mStateChangeFlag = true;
        }
        Vector2<float> newCenter;

        // if players are seperated
        if (p1Arrow->GetIsActive() && p2Arrow->GetIsActive())
        {
            AdjustCameraWithTwoPlayers(dt);
        }
        // if players are combined
        else
        {
            AdjustCameraWithCombiendPlayer(dt);
        }
    }
}

Matrix3x3<float> Camera::GetCameraMatrix(void) const
{
    Matrix3x3<float> zoom        = MATRIX3x3::BuildScale<float>({mXZoomLevel, mYZoomLevel});
    Matrix3x3<float> translation = MATRIX3x3::BuildTranslation<float>({-mCenter.x, -mCenter.y});
    Matrix3x3<float> rotation(mRight.x, mRight.y, 0, mUp.x, mUp.y, 0, 0, 0, 1);

    return rotation * zoom * translation;
}

Matrix3x3<float> Camera::GetInverseCameraMatrix(void) const
{
    Matrix3x3<float> zoom        = MATRIX3x3::BuildScale<float>({1.f / mXZoomLevel, 1.f / mYZoomLevel});
    Matrix3x3<float> translation = MATRIX3x3::BuildTranslation<float>({mCenter.x, mCenter.y});
    Matrix3x3<float> rotation(mRight.x, mUp.x, 0, mRight.y, mUp.y, 0, 0, 0, 1);

    return translation * zoom * rotation;
}

void Camera::SetLevelWidth(float width)
{
    // mLevelWidthHalf = width / 2.f;
}

void Camera::SetLevelHeight(float height)
{
    // mLevelHeightHalf = height / 2.f;
}

void Camera::ZoomWidth(float zoomLevel)
{
    mXZoomLevel += zoomLevel;
}

void Camera::ZoomHeight(float zoomLevel)
{
    mYZoomLevel += zoomLevel;
}

Vector2<float> Camera::GetCenter(void) const
{
    return mCenter;
}

void Camera::MoveCenter(const Vector2<float>& pos)
{
    mCenter += pos;
}

void Camera::AdjustCameraWithTwoPlayers(float dt)
{
    auto p1 = OBJECT_MANAGER->FindObjectByName("Player1");
    auto p2 = OBJECT_MANAGER->FindObjectByName("Player2");

    // prevent crash when debugging
    if (p1 == nullptr || p2 == nullptr)
    {
        return;
    }

    Vector2<float> p1Pos = p1->GetPosition();
    Vector2<float> p2Pos = p2->GetPosition();

    // determine area to be shown through camera.
    float left, right, top, bottom;
    if (p1Pos.x < p2Pos.x)
    {
        left  = p1Pos.x - xVisibleDistance;
        right = p2Pos.x + xVisibleDistance;
    }
    else
    {
        left  = p2Pos.x - xVisibleDistance;
        right = p1Pos.x + xVisibleDistance;
    }

    if (p1Pos.y < p2Pos.y)
    {
        top    = p2Pos.y + yVisibleDistance;
        bottom = p1Pos.y - yVisibleDistance;
    }
    else
    {
        top    = p1Pos.y + yVisibleDistance;
        bottom = p2Pos.y - yVisibleDistance;
    }
    AdjustCamera(dt, top, bottom, left, right);
}

void Camera::AdjustCameraWithCombiendPlayer(float dt)
{
    Object* combinedPlayer = OBJECT_MANAGER->FindObjectByName("PlayerCombined");
    if (combinedPlayer->GetComponentByType<Player>(eComponentType::PLAYER)->IsPlayerAlive() == false)
    {
        AdjustCameraWithTwoPlayers(dt);
        return;
    }
    Vector2<float> combinedPlayerPos = OBJECT_MANAGER->FindObjectByName("PlayerCombined")->GetPosition();

    float top    = combinedPlayerPos.y + yVisibleDistance;
    float bottom = combinedPlayerPos.y - yVisibleDistance;
    float left   = combinedPlayerPos.x - xVisibleDistance;
    float right  = combinedPlayerPos.x + xVisibleDistance;

    AdjustCamera(dt, top, bottom, left, right);
}

void Camera::AdjustCamera(float dt, float top, float bottom, float left, float right)
{
    float originTop    = top;
    float originBottom = bottom;
    float originLeft   = left;
    float originRight  = right;
    // if out of map boundary, adjust
    if (top > mMapTopLeft.y)
    {
        top             = mMapTopLeft.y;
        float newBottom = mMapTopLeft.y - mBaseHeight;
        if (bottom > newBottom)
        {
            bottom = newBottom < mMapBottomRight.y ? mMapBottomRight.y : newBottom;
        }
    }
    if (bottom < mMapBottomRight.y)
    {
        bottom       = mMapBottomRight.y;
        float newTop = mMapBottomRight.y + mBaseHeight;
        if (top < newTop)
        {
            top = newTop > mMapTopLeft.y ? mMapTopLeft.y : newTop;
        }
    }

    if (left < mMapTopLeft.x)
    {
        left           = mMapTopLeft.x;
        float newRight = mMapTopLeft.x + mBaseWidth;
        if (right < newRight)
        {
            right = newRight > mMapBottomRight.x ? mMapBottomRight.x : newRight;
        }
    }
    if (right > mMapBottomRight.x)
    {
        right         = mMapBottomRight.x;
        float newLeft = mMapBottomRight.x - mBaseWidth;
        if (left > newLeft)
        {
            left = newLeft < mMapTopLeft.x ? mMapTopLeft.x : newLeft;
        }
    }

    float xSize = right - left;
    float ySize = top - bottom;
    float ratio = 1.f;

    // x ratio is larger than y
    if (xSize / ySize > mScreenRatio)
    {
        // in case it is impossible to make height larger
        if (xSize / mScreenRatio > mMapTopLeft.y - mMapBottomRight.y)
        {
            float height          = mMapTopLeft.y - mMapBottomRight.y;
            ratio                 = mBaseHeight / height;
            float widthDifference = ((originRight - originLeft) - (height * mScreenRatio)) / 2.f;

            top    = mMapTopLeft.y;
            bottom = mMapBottomRight.y;
            left   = originLeft + widthDifference;
            right  = originRight - widthDifference;

            xSize = right - left;
            ySize = top - bottom;

            if (left < mMapTopLeft.x)
            {
                left           = mMapTopLeft.x;
                float newRight = mMapTopLeft.x + xSize;
                if (right < newRight)
                {
                    right = newRight > mMapBottomRight.x ? mMapBottomRight.x : newRight;
                }
            }
            if (right > mMapBottomRight.x)
            {
                right         = mMapBottomRight.x;
                float newLeft = mMapBottomRight.x - xSize;
                if (left > newLeft)
                {
                    left = newLeft < mMapTopLeft.x ? mMapTopLeft.x : newLeft;
                }
            }
        }
        else
        {
            float heightDifference = (ySize - (xSize / mScreenRatio)) / 2.f;
            top -= heightDifference;
            bottom += heightDifference;
            float newHeight = top - bottom;
            ratio           = mBaseWidth / xSize;

            // if out of map boundary, adjust
            if (top > mMapTopLeft.y)
            {
                top             = mMapTopLeft.y;
                float newBottom = mMapTopLeft.y - newHeight;
                if (bottom > newBottom)
                {
                    bottom = newBottom < mMapBottomRight.y ? mMapBottomRight.y : newBottom;
                }
            }
            if (bottom < mMapBottomRight.y)
            {
                bottom       = mMapBottomRight.y;
                float newTop = mMapBottomRight.y + newHeight;
                if (top < newTop)
                {
                    top = newTop > mMapTopLeft.y ? mMapTopLeft.y : newTop;
                }
            }
        }
    }
    // y ratio is larger than x
    else if (xSize / ySize <= mScreenRatio)
    {
        // in case it is impossible to make width larger
        if (ySize * mScreenRatio > mMapBottomRight.x - mMapTopLeft.x)
        {
            float width            = mMapBottomRight.x - mMapTopLeft.x;
            ratio                  = mBaseWidth / width;
            float heightDifference = ((originTop - originBottom) - (width / mScreenRatio)) / 2.f;

            top    = originTop - heightDifference;
            bottom = originBottom + heightDifference;
            left   = mMapTopLeft.x;
            right  = mMapBottomRight.x;

            xSize = right - left;
            ySize = top - bottom;

            if (top > mMapTopLeft.y)
            {
                top             = mMapTopLeft.y;
                float newBottom = mMapTopLeft.y - ySize;
                if (bottom > newBottom)
                {
                    bottom = newBottom < mMapBottomRight.y ? mMapBottomRight.y : newBottom;
                }
            }
            if (bottom < mMapBottomRight.y)
            {
                bottom       = mMapBottomRight.y;
                float newTop = mMapBottomRight.y + ySize;
                if (top < newTop)
                {
                    top = newTop > mMapTopLeft.y ? mMapTopLeft.y : newTop;
                }
            }
        }
        else
        {
            float widthDifference = (xSize - (ySize * mScreenRatio)) / 2.f;
            right -= widthDifference;
            left += widthDifference;
            float newWidth = right - left;
            ratio          = mBaseHeight / ySize;

            if (left < mMapTopLeft.x)
            {
                left           = mMapTopLeft.x;
                float newRight = mMapTopLeft.x + newWidth;
                if (right < newRight)
                {
                    right = newRight > mMapBottomRight.x ? mMapBottomRight.x : newRight;
                }
            }
            if (right > mMapBottomRight.x)
            {
                right         = mMapBottomRight.x;
                float newLeft = mMapBottomRight.x - newWidth;
                if (left > newLeft)
                {
                    left = newLeft < mMapTopLeft.x ? mMapTopLeft.x : newLeft;
                }
            }
        }
    }

    if (mStateChangeFlag == false)
    {
        if (mXZoomLevel > ratio)
        {
            ratio = (mXZoomLevel - ratio) > 0.03f ? mXZoomLevel - 0.03f : ratio;
        }
        else
        {
            ratio = (ratio - mXZoomLevel) > 0.03f ? mXZoomLevel + 0.03f : ratio;
        }
    }

    mXZoomLevel = ratio;
    mYZoomLevel = ratio;

    Vector2<float> newCenter   = Vector2<float>{right + left, top + bottom} / 2.f;
    Vector2<float> toNewCenter = newCenter - mCenter;
    float          size        = std::sqrt((toNewCenter.x * toNewCenter.x) + (toNewCenter.y * toNewCenter.y));

    // cannot move in one frame
    if (size > 500.f * dt && mStateChangeFlag == false)
    {
        mCenter += toNewCenter * (500.f * dt / size);
    }
    else
    {
        mCenter = newCenter;
    }
    mStateChangeFlag = false;
    
}